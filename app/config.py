# Константы и настройки скрипта
import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

neuro_text_model = BASE_DIR + "/data/model_2"

# Смещение экрана
screen_shift = {'x': 8, 'y': 31}

# Координаты мини-карты
mon_map = {'x': 880, 'y': 49, 'w': 128}

# Размер Главного окна
main_window = {'x': 864, 'ym': 704, 'y': 768}
view_window = {'x': 832, 'ym': 672, 'y': 768}
full_window = {'x': 1024, 'y': 768}

center_window = {'x': 218, 'y': 181, 'x1': 638, 'y1': 571}

# Размер окошка с именем
name_window = {'top': 576, 'left': 910, 'width': 71, 'height': 20}

hero_name_window = {'top': 432, 'left': 720, 'width': 68, 'height': 9}

# Смещение экрана
hero_center = {'x': 368, 'y': 271, 'width': 150, 'height': 150}

# Сдавиг пикселей при переводе из данных экрана, в миникарту
mm_x_shift = -1
mm_y_shift = 2
