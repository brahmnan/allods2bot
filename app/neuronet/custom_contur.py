import cv2
import numpy as np


def custom_letters(matrix=[], word_size=12, min_color=90):
    gray = cv2.cvtColor(matrix, cv2.COLOR_BGR2GRAY)
    ret, gray_thresh = cv2.threshold(gray, min_color, 255, cv2.THRESH_BINARY_INV)

    # cv2.imshow("gray_thresh", gray_thresh)

    tr_img = gray_thresh.T
    words = dict()
    i = 0

    for col in tr_img:
        # Проходим по колонкам
        divider = True
        is_word = False
        for r in col:
            # Проходим по строкам
            if r == 0:
                is_word = True
                divider = False
                pass
        if divider:
            i += 1
        if is_word:
            if not words.get(i):
                words[i] = []
            words[i].append(col)
        pass

    letters = []

    x = 0
    y = 0

    for key, word in words.items():
        n_arr = np.array(word)
        # Поворачиваем буквы
        word_t = n_arr.T

        w = len(word_t[0])
        h = len(word_t)

        if w < 2:
            # Пропускаем запятые
            continue

        # print(word_t)
        # Resize letter canvas to square
        size_max = max(w, h)
        letter_square = 255 * np.ones(shape=[size_max, size_max], dtype=np.uint8)
        if w > h:
            # Enlarge image top-bottom
            # ------
            # ======
            # ------
            y_pos = size_max // 2 - h // 2
            letter_square[y_pos:y_pos + h, 0:w] = word_t
        elif w < h:
            # Enlarge image left-right
            # --||--
            x_pos = size_max // 2 - w // 2
            letter_square[0:h, x_pos:x_pos + w] = word_t
        else:
            letter_square = word_t

        # Resize letter to 28x28 and add letter and its X-coordinate
        word_resize = cv2.resize(letter_square, (word_size, word_size), interpolation=cv2.INTER_AREA)
        # print(word_resize)
        letters.append([x, y, w, word_resize])
        x += 1
    return letters


image_file = "../../img/money.png"

img = cv2.imread(image_file)
letters = custom_letters(img, 12, 90)
print(letters)

# cv2.imshow("gray_thresh", gray_thresh)

# cv2.waitKey(0)
