# Тест поиска и покупки товара в магазине
from app.neuronet.findtext import FindText
import app.shop as shop


findtext = FindText()
findtext.save = False
findtext.print_data = True
findtext.search_name = "МИФРИЛЬНЫЙ БОЛЬШОЙ ЩИТ"
findtext.init_model()
findtext.find_by_name_test()
found = findtext.found
if found:
    s = shop.Shop()
    # Покупаем
    s.buy_item(found[0], found[1])