from app.commands import Mouse, Color, Keys
import time
import numpy as np
import cv2
from app.mission.store import Mss
from hashlib import sha1
import os.path
import keras
import app.neuronet.words as w
import app.config as cfg
import ctypes

hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.2\\bin\\cudart64_102.dll")


class FindText:
    # Поиск текстов в магазине
    color = Color()
    keys = Keys()
    mouse = Mouse()
    save = False
    print_data = True
    model = ''
    words = w.words.items()
    # Поиск предмета по имени
    search_name = ''
    found = []
    min_color = 39
    model_name = cfg.neuro_text_model
    first_color = []
    # Экземпляр класса для валидации по имени
    name_validate = ''
    color_validate = []
    stop_price = 0
    break_cycle = False

    def init_model(self, name=''):
        if not name:
            name = self.model_name
        self.model = keras.models.load_model(name)

    def reset(self):
        self.name_validate = ''
        self.color_validate = []
        self.search_name = ''
        self.min_color = 39
        self.found = []
        self.break_cycle = False
        self.stop_price = 0
        pass

    def find_text_in_invent(self, exclude=[], pop=True):
        # Цикл прокрутки инвентаря
        x = 270
        y = 610

        x1 = x
        i = 0
        y1 = y

        self.found = False
        while i < 5:
            if self.found or self.break_cycle:
                break
            if exclude:
                for e in exclude:
                    if self.color.validate(x1+e[0], y+e[1], e[2], e[3], e[4]):
                        print('Пропуск предмета:', e[3])
                        x1 += 80
                        pass

            if pop:
                # Здесь цикл просмотра картинки
                if self.get_popover(x1, y1):
                    break
            else:
                # Поиск цены
                if self.find_by_price(x1, y1, self.search_name):
                    break

            x1 += 80
            i += 1
        pass

    def find_by_price(self, x1, y1, price):
        # Поиск цены
        ret = False
        money = self.get_money(x1, y1)
        if money == str(price):
            self.found = [x1, y1]
            if self.print_data:
                print('Сумма найдена')
            ret = True
        return ret

    def get_money(self, x1, y1):
        # Поиск цены
        px = x1 - 15
        py = y1 - 38
        mon_map = {'top': py, 'left': px, 'width': 52, 'height': 8}
        money = self.get_price_by_mon_map(mon_map, self.first_color)
        if self.print_data:
            print([x1, y1], [px, py], money)
            pass
        return money

    def find_text_in_pages(self, pop=True):
        # Поиск предмета в магазине с прокруткой страниц
        ret = []
        check = True
        # Координаты объекта
        x = 239
        y = 246

        while True:
            # Пока есть страницы листаем вниз
            self.show_text(x, y, pop)
            if self.found or self.break_cycle:
                break
            if not check:
                break
            self.keys.press('pgdn', 200)
            check = self.color.validate(282, 460, 192, 232, 112)
        pass

    def show_text(self, x, y, pop=True):
        # Цикл поиска элемента по цвету
        color = Color()
        find = 0
        row = 1
        x1 = x
        y1 = y
        ret = []
        while row <= 3:
            if self.found or self.break_cycle:
                break
            col = 1
            x1 = x
            while col <= 2:
                if pop:
                    # Здесь цикл просмотра картинки
                    if self.get_popover(x1, y1):
                        break
                else:
                    # Поиск цены
                    money = self.get_money(x1, y1)

                    if self.stop_price and int(money) >= self.stop_price:
                        print('Достигнут лемит цены', self.stop_price)
                        self.found = False
                        self.break_cycle = True
                        break

                    if money == str(self.search_name):
                        # Валидация по имени
                        valid = True
                        if self.color_validate:
                            c = self.color_validate
                            if not color.validate(x1+c[0], y1+c[1], c[2], c[3], c[4]):
                                # Цвет не соответствует
                                print('Цвет не соответствует')
                                self.found = False
                                valid = False

                        if valid and self.name_validate:
                            if not self.name_validate.get_popover(x1, y1):
                                # Если поповер не соответствует, продолжаем поиск
                                print('Имя не соответствует')
                                self.found = False
                                valid = False

                        if valid:
                            self.found = [x1, y1]
                            if self.print_data:
                                print('Сумма найдена')
                            break
                    pass

                col += 1
                x1 += 80
            row += 1
            y1 += 80
        return ret

    def get_popover(self, x, y):
        # Здесь цикл просмотра картинки
        self.mouse.move(x, y)
        time.sleep(500 / 1000)
        self.get_text_by_popover(x, y)
        self.mouse.move(886, 520)
        if self.found:
            return True
        return False

    def get_text_by_popover(self, x, y):
        # Получить текст с появляющегося окна
        w_max = 450
        h_max = 200
        y1 = y - 3 - h_max
        x1 = x + 3
        # Поиск объектов на карте. mon_map = {'x': 880, 'y': 49, 'w': 128}
        mon_map = {'top': y1, 'left': x1, 'width': w_max,
                   'height': h_max}
        # print(mon_map)

        img = Mss.sct.grab(mon_map)
        matrix = np.array(img)

        # Верхний угол сообщения
        c = (104, 144, 200, 255)
        indices = np.where(np.all(matrix == c, axis=-1))
        # Координаты: y, x
        map_crd = np.transpose(indices)

        # Координаты маленькой карты
        y2 = int(y1 + map_crd[0][0] + 2)
        h2 = int(h_max - map_crd[0][0] - 2)
        w2 = int(map_crd[0][1] - 2)
        mon_map_2 = {'top': y2, 'left': x1, 'width': w2, 'height': h2}
        self.get_text_by_mon_map(mon_map_2, x, y)
        pass

    def get_text_by_mon_map(self, mon_map, x=0, y=0):
        # Получить картинку по координатам mon_map
        img2 = Mss.sct.grab(mon_map)
        matrix2 = np.array(img2)
        letters = self.letters_extract(matrix2)
        if self.print_data:
            # Вывод в консоль распознанных данных
            sort_let = self.letters_sort(letters)

            for i in range(len(sort_let)):
                str = self.letters_to_str(sort_let[i], i)
                print(str)

                if self.search_name:
                    # Поиск товара по имени
                    if i == 0 and str == self.search_name:
                        self.found = [x, y]
                        break
        # cv2.imshow("img", matrix2)
        # cv2.waitKey(0)
        pass

    def get_price_by_mon_map(self, mon_map, first_color=[]):
        # Получить картинку по координатам mon_map
        img2 = Mss.sct.grab(mon_map)
        matrix = np.array(img2)

        if first_color:
            # Отрезаем по начальному цвету
            # Карта. Цвета карты, bgr
            map_c = (first_color[2], first_color[1], first_color[0], 255)
            indices = np.where(np.all(matrix == map_c, axis=-1))
            # Координаты: y, x
            map_crd = np.transpose(indices)
            if map_crd.__len__():
                i = 10000
                for crd in map_crd:
                    if crd[1] < i:
                        i = crd[1]

                matrix_crop = matrix[0:len(matrix), i - 2:len(matrix[0])]
                matrix = matrix_crop

            pass

        letters = self.custom_letters(matrix, min_color=self.min_color)
        # Вывод в консоль распознанных данных
        ret = ''
        for item in letters:
            word = self.emnist_predict_img(item[3])

            # if word == '6':
            #    word = '8'
            # elif word == 'Б':
            #    word = '6'

            ret += word

        if ret:
            return ret

        return 0

    def letters_extract(self, matrix, out_size=12):
        # Получить из картинки буквы
        img = matrix

        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        ret, thresh = cv2.threshold(gray, self.min_color, 255, cv2.THRESH_BINARY_INV)
        # scale_percent = 200  # percent of original size
        # width = int(thresh.shape[1] * scale_percent / 100)
        # height = int(thresh.shape[0] * scale_percent / 100)
        # dim = (width, height)
        # resize image
        # resized = cv2.resize(thresh, dim, interpolation=cv2.INTER_AREA)
        # Get contours
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
        output = thresh.copy()
        letters = []
        for idx, contour in enumerate(contours):
            (x, y, w, h) = cv2.boundingRect(contour)
            # print("R", idx, x, y, w, h, cv2.contourArea(contour), hierarchy[0][idx])
            # hierarchy[i][0]: the index of the next contour of the same level
            # hierarchy[i][1]: the index of the previous contour of the same level
            # hierarchy[i][2]: the index of the first child
            # hierarchy[i][3]: the index of the parent
            if hierarchy[0][idx][3] == 0:
                cv2.rectangle(output, (x, y), (x + w, y + h), (70, 0, 0), 1)
                letter_crop = thresh[y:y + h, x:x + w]
                # print(letter_crop.shape)
                # Resize letter canvas to square
                size_max = max(w, h)
                letter_square = 255 * np.ones(shape=[size_max, size_max], dtype=np.uint8)
                if w > h:
                    # Enlarge image top-bottom
                    # ------
                    # ======
                    # ------
                    y_pos = size_max // 2 - h // 2
                    letter_square[y_pos:y_pos + h, 0:w] = letter_crop
                elif w < h:
                    # Enlarge image left-right
                    # --||--
                    x_pos = size_max // 2 - w // 2
                    letter_square[0:h, x_pos:x_pos + w] = letter_crop
                else:
                    letter_square = letter_crop

                # Resize letter to 28x28 and add letter and its X-coordinate
                letters.append((x, y, w, cv2.resize(letter_square, (out_size, out_size), interpolation=cv2.INTER_AREA)))
                # letters.append((x, w, cv2.resize(letter_square, (out_size, out_size), interpolation=cv2.INTER_NEAREST)))

        # Sort array in place by X-coordinate
        # letters.sort(key=lambda x: x[0], reverse=False)
        # cv2.imshow("Output", output)
        # cv2.waitKey(0)
        if self.save:
            for letter in letters:
                self.save_image(letter[3])
            self.save_image(output, 'contours/')
            self.save_image(thresh, 'texts/')

        return letters

    def save_image(self, img, custom_folder=''):
        path = "../../img/words/"
        name = str(sha1(img).hexdigest())
        path_name = path + custom_folder + name + '.jpg'
        if not os.path.exists(path_name):
            print('img add', path_name)
            cv2.imwrite(path_name, img)

    def letters_sort(self, letters):
        # Сортировка букв по координатам. Создаём строки и распределяем в них буквы
        output = dict()
        for letter in letters:
            y = letter[1]
            if y == 0:
                y = 1
            i = 0
            z = 0
            while z < y:
                z = (i + 1) * 12
                if not output.get(i):
                    output[i] = []
                if y < z:
                    output[i].append(letter)
                i += 1

                pass

        sort_out = []
        for key, row in output.items():
            row.sort(key=lambda x: x[0], reverse=False)
            sort_out.append(row)
        return sort_out

    def letters_to_str(self, letters, row=0):
        s_out = ""
        last_x = 0
        space = 0
        for i in range(len(letters)):
            x = letters[i][0]
            w = letters[i][2]
            if last_x > 0:
                space = x - last_x
            if space > 5:
                s_out += " "
            word = self.emnist_predict_img(letters[i][3])
            # Валидация слов
            if word == '.':
                # Игнорируем точки
                word = ''
            if row == 0:
                # Для первой строки, заменяем все цифры на буквы
                if word == '4':
                    word = 'Ч'
                if word == '3':
                    word = 'З'
            if word == 'Ы':
                # Ы делаем из предыдущего мягкого знака
                word = ''
            if word == 'Ь':
                # Обработка мягкого знака и Ы
                if letters.__len__() > i + 1:
                    next_word = self.emnist_predict_img(letters[i + 1][3])
                    if next_word == 'Ы':
                        word = 'Ы'

            s_out += word
            last_x = x + w
        return s_out

    def emnist_predict_img(self, matrix):
        new_img = []
        # print(matrix)
        for i in matrix:
            for j in i:
                data = round(1 - j / 255, 1)
                new_img.append(data)
        n_arr = np.array(new_img)

        x_img_r = np.reshape(n_arr, (1, 12, 12, 1))
        # print(x_img_r)
        result_int = self.model.predict_classes(x_img_r)
        symbol_str = self.get_simbol_by_code(result_int)
        return symbol_str

    def custom_letters(self, matrix=[], word_size=12, min_color=90):
        gray = cv2.cvtColor(matrix, cv2.COLOR_BGR2GRAY)
        ret, gray_thresh = cv2.threshold(gray, min_color, 255, cv2.THRESH_BINARY_INV)

        # cv2.imshow("gray_thresh", gray_thresh)
        # cv2.waitKey(0)

        tr_img = gray_thresh.T
        # print(tr_img)
        words = dict()
        i = 0

        for col in tr_img:
            # Проходим по колонкам
            divider = True
            is_word = False
            for r in col:
                # Проходим по строкам
                if r == 0:
                    is_word = True
                    divider = False
                    pass
            if divider:
                i += 1
            if is_word:
                if not words.get(i):
                    words[i] = []
                words[i].append(col)
            pass

        letters = []

        x = 0
        y = 0

        for key, word in words.items():
            n_arr = np.array(word)
            # Поворачиваем буквы
            word_t = n_arr.T
            # print(word_t)
            w = len(word_t[0])
            h = len(word_t)

            if w < 2:
                # Пропускаем запятые
                continue

            # print(word_t)
            # Resize letter canvas to square
            size_max = max(w, h)
            letter_square = 255 * np.ones(shape=[size_max, size_max], dtype=np.uint8)
            if w > h:
                # Enlarge image top-bottom
                # ------
                # ======
                # ------
                y_pos = size_max // 2 - h // 2
                letter_square[y_pos:y_pos + h, 0:w] = word_t
            elif w < h:
                # Enlarge image left-right
                # --||--
                x_pos = size_max // 2 - w // 2
                letter_square[0:h, x_pos:x_pos + w] = word_t
            else:
                letter_square = word_t

            # Resize letter to 28x28 and add letter and its X-coordinate
            word_resize = cv2.resize(letter_square, (word_size, word_size), interpolation=cv2.INTER_AREA)
            # print(word_resize)
            letters.append([x, y, w, word_resize])
            x += 1

        if self.save:
            for letter in letters:
                self.save_image(letter[3], 'custom/')
        return letters

    def get_simbol_by_code(self, code):
        ret = ''
        for word in self.words:
            if word[1][1] == code:
                ret = word[1][0]
                break
        return ret
