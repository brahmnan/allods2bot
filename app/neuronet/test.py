import cv2
import numpy as np
import cv2
import imghdr
import numpy as np
import os.path
from hashlib import sha1
import pathlib

from scipy import io as spio

from matplotlib import pyplot as plt
from typing import *
import time

def save_image(img):
    path = "../../img/words/"
    name = str(sha1(img).hexdigest())
    path_name = path+name+'.jpg'
    print(path_name)
    if not os.path.exists(path_name):
        print('img add', path_name)
        cv2.imwrite(path_name, img)


def letters_extract(image_file: str, out_size=12):
    img = cv2.imread(image_file)

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    ret, thresh = cv2.threshold(gray, 39, 255, cv2.THRESH_BINARY_INV)

    # scale_percent = 200  # percent of original size
    # width = int(thresh.shape[1] * scale_percent / 100)
    # height = int(thresh.shape[0] * scale_percent / 100)
    # dim = (width, height)
    # resize image
    # resized = cv2.resize(thresh, dim, interpolation=cv2.INTER_AREA)

    # Get contours

    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    output = thresh.copy()

    letters = []
    for idx, contour in enumerate(contours):
        (x, y, w, h) = cv2.boundingRect(contour)
        # print("R", idx, x, y, w, h, cv2.contourArea(contour), hierarchy[0][idx])
        # hierarchy[i][0]: the index of the next contour of the same level
        # hierarchy[i][1]: the index of the previous contour of the same level
        # hierarchy[i][2]: the index of the first child
        # hierarchy[i][3]: the index of the parent
        if hierarchy[0][idx][3] == 0:
            cv2.rectangle(output, (x, y), (x + w, y + h), (70, 0, 0), 1)
            letter_crop = thresh[y:y + h, x:x + w]
            # print(letter_crop.shape)

            # Resize letter canvas to square
            size_max = max(w, h)
            letter_square = 255 * np.ones(shape=[size_max, size_max], dtype=np.uint8)
            if w > h:
                # Enlarge image top-bottom
                # ------
                # ======
                # ------
                y_pos = size_max // 2 - h // 2
                letter_square[y_pos:y_pos + h, 0:w] = letter_crop
            elif w < h:
                # Enlarge image left-right
                # --||--
                x_pos = size_max // 2 - w // 2
                letter_square[0:h, x_pos:x_pos + w] = letter_crop
            else:
                letter_square = letter_crop

            # Resize letter to 28x28 and add letter and its X-coordinate
            letters.append((x, w, cv2.resize(letter_square, (out_size, out_size), interpolation=cv2.INTER_AREA)))

    # Sort array in place by X-coordinate
    letters.sort(key=lambda x: x[0], reverse=False)

    # cv2.imshow("Input", img)
    # cv2.imshow("img_erode", img_erode)
    # cv2.imshow("resized", resized)
    #cv2.imshow("Output", output)
    #cv2.imshow("0", letters[0][2])
    #cv2.imshow("1", letters[1][2])
    #cv2.imshow("2", letters[2][2])
    #cv2.imshow("3", letters[3][2])
    #cv2.imshow("4", letters[4][2])
    for letter in letters:
        save_image(letter[2])
    save_image(output)
    save_image(thresh)
    # cv2.waitKey(0)
    # return letters


image_file = "../../img/load.png"

# letters_extract(image_file)

img = cv2.imread(image_file)

gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
ret, gray_thresh = cv2.threshold(gray, 90, 255, cv2.THRESH_BINARY_INV)

invert = cv2.bitwise_not(gray)

# ret, thresh = cv2.threshold(gray, 80, 255, cv2.THRESH_BINARY_INV)


scale_percent = 500  # percent of original size
width = int(gray.shape[1] * scale_percent / 100)
height = int(gray.shape[0] * scale_percent / 100)
dim = (width, height)
# resize image
resized = cv2.resize(gray, dim, interpolation=cv2.INTER_AREA)
resized_inv = cv2.resize(invert, dim, interpolation=cv2.INTER_AREA)

ret, thresh = cv2.threshold(resized, 100, 255, cv2.THRESH_BINARY)
# ret, thresh = cv2.threshold(resized, 39, 255, cv2.THRESH_BINARY_INV)

th2 = cv2.adaptiveThreshold(resized_inv, 255, cv2.ADAPTIVE_THRESH_MEAN_C, \
                            cv2.THRESH_BINARY, 11, 2)
th3 = cv2.adaptiveThreshold(resized_inv, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, \
                            cv2.THRESH_BINARY, 11, 2)
# Get contours
contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE, )

output = thresh.copy()

for idx, contour in enumerate(contours):
    (x, y, w, h) = cv2.boundingRect(contour)
    # print("R", idx, x, y, w, h, cv2.contourArea(contour), hierarchy[0][idx])
    # hierarchy[i][0]: the index of the next contour of the same level
    # hierarchy[i][1]: the index of the previous contour of the same level
    # hierarchy[i][2]: the index of the first child
    # hierarchy[i][3]: the index of the parent
    # if hierarchy[0][idx][3] == 0:
    # print(hierarchy[0][idx])
    cv2.rectangle(output, (x, y), (x + w, y + h), (70, 0, 0), 1)

#cv2.imshow("Input", img)
cv2.imshow("output", output)
#cv2.imshow("th2", th2)
#cv2.imshow("th3", th3)
cv2.imshow("gray_thresh", gray_thresh)
cv2.imshow("resized_inv", resized_inv)


cv2.waitKey(0)
