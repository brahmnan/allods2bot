# Тест нейронной сети
import numpy as np
import cv2
import app.neuronet.words as w
from keras.models import Sequential
from keras.layers import Dense, Convolution2D, Dropout, Flatten, MaxPooling2D
from keras.datasets import mnist
from keras.utils import to_categorical
import time


def text_model():
    model = Sequential()
    model.add(
        Convolution2D(filters=32, kernel_size=(3, 3), padding='valid', input_shape=(12, 12, 1), activation='relu'))
    model.add(Convolution2D(filters=64, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(43, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adadelta', metrics=['accuracy'])
    return model


# Количество выходов - 43

path = "../../img/words/"

words = w.words.items()

images = []
my_x_train = []
my_y_train = []
for word in words:
    # print(word)

    file_name = path + word[0] + '.jpg'
    img = cv2.imread(file_name)
    matrix = np.array(img)

    z = 0
    while z < 100:
        new_img = []
        for i in matrix:
            for j in i:
                data = round(1 - j[0] / 255, 1)
                new_img.append(data)

        n_arr = np.array(new_img)

        x_train_r = np.reshape(n_arr, (12, 12))

        my_x_train.append(x_train_r)
        my_y_train.append(word[1][1])
        images.append([word[0], word[1], new_img])
        z += 1


# Загружаем данные x_train и x_test содержат двухмерный массив с изображение цифр
# x_test, y_test массив с проверочными данными сети.
# (x_train, y_train), (x_test, y_test) = mnist.load_data()

my_y_train_cat = to_categorical(my_y_train)
my_x_train_np = np.array(my_x_train)
my_x_train_np = my_x_train_np.reshape(my_x_train_np.__len__(), 12, 12, 1)

model = text_model()

# print(my_x_train[100])
# print(my_y_train_cat[100])
time1 = time.time()
history = model.fit(my_x_train_np, my_y_train_cat, validation_data=(my_x_train_np, my_y_train_cat), epochs=7)
filepath = 'model_2'
model.save(filepath)
time2 = time.time()
print(time2 - time1)
