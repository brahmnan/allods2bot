# Покупка и продажа товаров в магазине. Екипировка персонажей

import app.ineterface as inter
from app.commands import Mouse, Color, Keys
from app.neuronet.findtext import FindText
from app.mission.monitor import Names
import time


class Shop:
    keys = Keys()
    color = Color()
    mouse = Mouse()
    findtext = FindText()
    purchased_items = dict()

    # shift x, shift y, r, g, b, name
    exclude = [[0, 0, 72, 48, 24, 'Корона'],
               [2, -2, 184, 76, 88, 'Лечебная мазь'],
               [0, 10, 224, 104, 120, 'Элексир здоровья'],
               [0, 10, 208, 104, 120, 'Большой элексир здоровья'],
               [0, 0, 104, 144, 232, 'Магическая мазь'],
               [0, 0, 160, 156, 104, 'Бумаги'],
               [0, 0, 144, 104, 40, 'Деньги'],
               # [0, 0, 120, 40, 128, 'Телепорт']
               ]

    exclude_price = []

    def reset(self):
        self.purchased_items = dict()

    def get_items(self, name):
        return self.purchased_items.get(name)

    def update_items(self, name, count):
        items = self.purchased_items.get(name)
        if items:
            self.purchased_items[name] = items + count
        else:
            self.purchased_items[name] = count

    def find_by_name(self, name, where=''):
        # Поиск по имени
        findtext = self.findtext
        findtext.reset()
        findtext.search_name = name

        if where == 'invent':
            findtext.find_text_in_invent(exclude=self.exclude)
        else:
            findtext.find_text_in_pages()
        found = findtext.found
        return found

    def find_by_price(self, price, where='', name_validate='', color_validate=[], stop_price=0):
        # Поиск по цене
        findtext = self.findtext
        findtext.reset()
        findtext.search_name = price
        findtext.min_color = 85
        findtext.stop_price = stop_price

        if name_validate:
            findtext.name_validate = FindText()
            findtext.name_validate.search_name = name_validate
            findtext.name_validate.model = self.findtext.model

        if color_validate:
            findtext.color_validate = color_validate

        if where == 'invent':
            findtext.first_color = [184, 156, 72]
            findtext.find_text_in_invent(exclude=self.exclude, pop=False)
        else:
            findtext.first_color = [184, 156, 72]
            findtext.find_text_in_pages(pop=False)
        found = findtext.found
        return found

    def find_or_load(self, item_price, next_hero=1, place='', name_validate='', color_validate=[], equip=True,
                     stop_price=0, item_count=1, current_stage=0, buy_all=False, hero=False):
        # Находим предмет или загружаем игру
        s = self
        item_found = False
        while not item_found:
            s.go_kaarg_shop()
            # Выбираем место
            if place == 'books_kaarg':
                s.books_kaarg()
            elif place == 'weapon_kaarg':
                s.weapon_kaarg()

            item = s.find_by_price(item_price, name_validate=name_validate, color_validate=color_validate,
                                   stop_price=stop_price)
            invent_price = int(item_price / 2)
            if item:
                name = ''
                # Выбираем героя
                i = 0
                if hero:
                    self.select_hero(hero)
                else:
                    while i < next_hero:
                        s.next_hero()
                        i += 1
                # Поиск предмета
                if item_count > 1:
                    # Покупаем несколько единиц
                    name = str(item_price)
                    if name_validate:
                        name += '_' + name_validate

                    item_count_total = item_count
                    items = s.get_items(name)
                    if items:
                        item_count_total -= items
                    if buy_all:
                        # Покупаем всё. В этом случае, единицы это количество стеков.
                        s.buy_all(item[0], item[1])
                        num = 1
                    else:
                        num = s.buy_items_count_validate(item[0], item[1], item_count_total)

                    self.update_items(name, num)

                else:
                    if buy_all:
                        s.buy_all(item[0], item[1])
                    else:
                        s.buy_item(item[0], item[1])

                if equip:
                    # Находим предмет в инвентаре
                    i_inv = s.find_by_price(invent_price, 'invent')

                    # Применяем предмет
                    if i_inv:
                        s.equip(i_inv[0], i_inv[1])

                item_found = True
                s.quit()
                # Если не все единицы куплены, сохраняемся и загружаемся
                if item_count > 1:
                    items = self.get_items(name)
                    if items and items < item_count:
                        # Нужно докупать
                        if current_stage:
                            # Сохранение игры и выбор следующего стажа
                            sg = inter.SaveGame()
                            sg.save_game(current_stage)
                            sg.load_game()
                            item_found = False
                        pass
                    pass
            else:
                s.quit()
                sg = inter.SaveGame()
                sg.load_game()

    # Лавка в первом городе
    def go_kaarg_shop(self):
        # Переход в лавку
        inter.validate_left_cicle(550, 467)
        pass

    def go_shop(self):
        # Переход в лавку
        inter.validate_left_cicle(490, 480)
        pass

    def buy(self):
        # Купить
        inter.validate_left_cicle(750, 268, 100, 270, 520)
        pass

    def sell(self):
        # Продать
        print('Продажа')
        inter.validate_left_cicle(750, 311, 100, 270, 520, more_pos=True, max_wait=200)
        pass

    def quit(self):
        # Выход из магазина
        inter.validate_left_cicle(750, 360)
        pass

    def books_kaarg(self):
        # Перейти к разделу книги
        inter.validate_left_cicle(477, 200)
        pass

    def armor_kaarg(self):
        # Перейти к разделу книги
        inter.validate_left_cicle(392, 203)
        pass

    def magic_kaarg(self):
        # Перейти к разделу книги
        inter.validate_left_cicle(455, 325)
        pass

    def weapon_kaarg(self):
        # Перейти к разделу книги
        inter.validate_left_cicle(606, 291)
        pass

    def books(self):
        # Перейти к разделу книги
        inter.validate_left_cicle(480, 237)
        pass

    def find_fire_book(self):
        mouse = Mouse()
        # Листаем в самый низ
        mouse.left_down(280, 460, 500, 100)

        # Ищем нужную книгу
        firebook = FireBook()
        coords = firebook.find_fire_book()

        if coords[2] == 1:
            # Книга найдена - покупаем
            return coords

        return [0, 0]

    def find_color_in_pages(self, item_color):
        # Поиск предмета в магазине с прокруткой страниц
        ret = []
        while self.color.validate(282, 460, 192, 232, 112):
            # Пока есть страницы листаем вниз
            find = self.find_color(239, 246, item_color[0], item_color[1], item_color[2])
            if find:
                ret = find
                break
            self.keys.press('pgdn', 200)

        return ret

    def move_shop_down(self):
        # Спуститься вниз магазина
        while self.color.validate(282, 460, 192, 232, 112):
            self.keys.press('pgdn', 10)

    def find_color(self, x, y, r, g, b):
        # Цикл поиска элемента по цвету
        color = Color()
        find = 0
        row = 1
        x1 = x
        y1 = y
        ret = []
        br = False
        while row <= 3:
            if br:
                break
            col = 1
            x1 = x
            while col <= 2:
                if color.validate(x1, y1, r, g, b):
                    ret = [x1, y1]
                    br = True
                    break
                col += 1
                x1 += 80
            row += 1
            y1 += 80
        return ret

    def next_hero(self):
        # Выбор следующего героя
        inter.validate_left_cicle(813, 631, 100, 751, 453)
        pass

    def select_hero(self, hero):
        # Выбор героя по классу
        if not self.color.validate(819, 440, 240, 236, 0):
            inter.validate_press_cicle('tab', 819, 440)
        names = Names()
        find_hero = True
        i = 0
        while find_hero:
            hero_name = names.get_hero_name()
            # print(hero_name)
            if hero_name == hero.code_name:
                break
            self.mouse.left(813, 631, sleep=100)
            i += 1
            if i > 10:
                # Ошибка выбора героя, прерываем цикл.
                exit()

    def buy_items(self, x, y, count):
        # Покупаем предметы в магазине
        mouse = Mouse()
        i = 0
        while i < count:
            mouse.left(x, y, double=True)
            i += 1
        self.buy()
        pass

    def buy_items_count_validate(self, x, y, count):
        # Покупаем предметы в магазине
        if count <= 0:
            count = 1
        mouse = Mouse()
        i = 1
        while i <= count:
            if not mouse.left_validate(x, y, double=True):
                i += 1
            else:
                break
        self.buy()
        print('Всего куплено товаров', i)
        return i

    def buy_items_validate(self, x, y, count=2):
        i = 0
        while i < count:
            self.buy_item(x, y)
            i += 1

    def buy_item(self, x, y):
        # Покупаем предмет в магазине
        inter.validate_left_cicle(x, y, 100, 270, 520, double=True)
        self.buy()
        pass

    def equip_color(self, color):
        # Найти предмет в инвентаре и снарядить его
        x = 270
        y = 610

        x1 = x
        while x1 <= 590:
            # Проходим по инвентарю
            if self.color.validate(x1, y, color[0], color[1], color[2]):
                self.equip(x1, y)
                break
            x1 += 80
        pass

    def equip(self, x, y):
        # Снаряжаем элемент из инвентаря
        inter.validate_left_cicle(x, y, double=True, more_pos=True)
        pass

    def de_equip_posoh(self):
        inter.validate_left_cicle(718, 463, double=True)
        pass

    def to_sell(self):
        # Перемещение объекта из инвентаря в магазин
        # Простая валидация - проверяем появление предмета в лотке магазина
        # TODO Сложная валидация - проверка изменения суммы продажи
        # Разница между лотком и инвентарём 90px
        inter.validate_drug_cicle(270, 610, 270, 520, 200, 260, 510)

    pass

    def buy_all(self, x, y):
        # Покупаем все предметы
        x1 = 270
        y1 = 520
        wait = 200
        self.keys.keyDown('shift')
        self.mouse.drug_to(x, y, x1, y1, wait)
        self.keys.keyUp('shift')
        self.buy()
        pass

    def sell_all_hero(self, hero=False, equip_exclude=False):
        if hero:
            self.select_hero(hero)
        self.sell_all(equip_exclude)

    def sell_all(self, equip_exclude=False):
        # Цикл продажи лута
        findtext = self.findtext
        findtext.reset()
        findtext.min_color = 85

        # Цвета предметов, которые нельзя продавать
        end_sell_color = [80, 72, 40]
        x = 270
        y = 610
        y_top = 520
        wait = 200

        x1 = x
        y1 = y
        sell_all = False
        count = 0
        error_drug = 0
        while not sell_all:
            i = 0
            self.keys.keyDown('shift')
            while i < 5:
                to_sell = True
                if self.exclude_price:
                    # Исключаем предметы определённой цены
                    money = findtext.get_money(x1, y1)
                    print('Цена предмета', [x1, y1], money)
                    for m in self.exclude_price:
                        if str(m) == money:
                            if not equip_exclude:
                                print('Пропуск предмета по цене:', money)
                                x1 += 80
                            else:
                                # Снаряжаем предмет
                                print('Снаряжение предмета по цене:', money)
                                self.equip(x1, y1)
                                to_sell = False

                for e in self.exclude:
                    if self.color.validate(x1 + e[0], y + e[1], e[2], e[3], e[4]):
                        print('Пропуск предмета:', e[3])
                        x1 += 80
                        to_sell = False

                if self.color.validate(x1, y, end_sell_color[0], end_sell_color[1], end_sell_color[2]):
                    sell_all = True
                    break

                if to_sell:
                    fist_color = self.color.color(x1, y)
                    self.mouse.drug_to(x1, y, x1, y_top, wait)
                    if self.color.validate(x1, y, fist_color[2], fist_color[1], fist_color[0]):
                        # Цвет не изменился, возможно предмет остался на месте
                        error_drug += 1
                    else:
                        error_drug = 0

                    if error_drug > 3:
                        error_drug = 0
                        x1 += 80

                    print('Продажа предмета', [x1, y1])
                    count += 1
                    i += 1
            self.keys.keyUp('shift')
            if count > 0:
                self.sell()
        pass

    def drug_to_shop(self, x1, y1):
        # Переместить предмет в магазин
        x = 270
        y = 610
        y_top = 520
        wait = 200
        self.mouse.drug_to(x1, y1, x, y_top, wait)
        pass

    def drug_to_invent(self):
        # Переместить предмет в магазин
        x = 270
        y = 610
        y_top = 520
        wait = 200
        self.mouse.drug_to(x, y_top, x, y, wait)
        pass


class FireBook:
    # Поиск книги огня

    def find_fire_book(self):
        # Три цикла поиск книги
        sleep = 50
        result = self.fire_book_cicle()
        if result[2] == 0:
            time.sleep(sleep / 1000)
            result = self.fire_book_cicle()
            if result[2] == 0:
                result = self.fire_book_cicle()
                time.sleep(sleep / 1000)

        return result

    def fire_book_cicle(self):
        # Цикл поиска книги
        x = 330
        y = 396
        book_color = [72, 24, 24]
        color = Color()
        find = 0
        while y >= 236:
            right_col = color.validate(x, y, book_color[0], book_color[1], book_color[2])
            left_col = color.validate(x - 80, y, book_color[0], book_color[1], book_color[2])
            if right_col or left_col:
                print(" книга найдена")
                if left_col:
                    x = x - 80
                find = 1
                break
            y = y - 80

        ret = [x, y, find]
        return ret

    pass
