class A:
    a = 0
    b = dict()
    c = []
    s = ''
    bl = True

    def __init__(self, i=0, j=0):
        print('a', i, j)


class B(A):
    def __init__(self, *args):
        # A.__init__(self, i)
        super(B, self).__init__(*args)
        print('b', *args)


class C(B):
    def __init__(self, i, j):
        # B.__init__(self, i)
        super(C, self).__init__(i, j)
        print('c', i, j)


c = C(10, 11)


class Base(object):
    def __init__(self):
        print("Base init'ed")


class ChildA(Base):
    def __init__(self):
        print("ChildA init'ed")
        Base.__init__(self)


class ChildB(Base):
    def __init__(self):
        print("ChildB init'ed")
        super(ChildB, self).__init__()


# And let's create a dependency that we want to be called after the Child:

class UserDependency(Base):
    def __init__(self):
        print("UserDependency init'ed")
        super(UserDependency, self).__init__()


# Now remember, ChildB uses super, ChildA does not:

class UserA(ChildA, UserDependency):
    def __init__(self):
        print("UserA init'ed")
        super(UserA, self).__init__()


class UserB(ChildB, UserDependency):
    def __init__(self):
        print("UserB init'ed")
        super(UserB, self).__init__()


usera = UserA()
userb = UserB()

a = A()
a.a = 10
a.b['a']='dd'
a.c.append('gg')
a.s = 'str'
a.bl = False
print(a.a)
print(a.b)
print(a.c)
print(a.s)
print(a.bl)

a = A()
print(a.a)
print(a.b)
print(a.c)
print(a.s)
print(a.bl)