# UNUSED
# Управление персонажами в зависимости от ситуации на карте
# Исользование элексиров лечения, отступление, выбор маги

import app.config as cfg
from app.mission.monitor import MiniMap, Mage, MainScreen, Positions, Names
from app.mission.heroes import *
from app.commands import Mouse, Color, Keys
from app.ineterface import MainMenu, Info
from app.mission.actions import *
import time


class CheckPoint:
    # Коэффициент размера мини карты к экрану
    k_size = 0
    # Команда текущего героя
    current_hero = 1

    # Флаги управления боем
    search_enemy = True
    # Дистанция до врагов
    # TODO Для разных врагов разные дистанции
    enemy_distance = 0
    retreat_distance = 3
    last_dst = 0
    # Дистанция мага до Иглеза, если меньше, маг сближается
    igles_dst = 0
    # Иглез защищает магов от тех, кто их атакует
    igles_defence_mages = 1
    # Цель не достингута, если есть враги
    point_wait_if_enemy = 1

    # Параметры перемещения
    # Определение расстояния до контрольной точки на карте
    # Точки на карте
    cp = [0, 0]
    cpm = [0, 0]
    # Точка на экране
    cpm_mouse = [0, 0]
    # Дистанция до КТ
    cp_distance = 100
    # Дистанция до мини-цели
    cpm_dist = 100
    # Разброс дистации для контрольной точки
    walk_min_ct_radius = 2
    walk_min_move_radius = 5
    # Разброс дистанции КТ для основгоно цикла
    walk_main_ct_radius = 5
    # Режим перемещения: 0 -s, 1 -m
    walk_mode = 0
    # Искать врагов во время перемещения
    find_enemy = 0
    find_enemy_name_to_win = ''
    found_enemy_units = []
    enemy_detected = False
    last_target = -1
    kill_enemy = 1
    # Необходимо убить всех врагов, для достижения цели перемещения
    need_kill_all_enemy = 0
    # После убийства врага, проверить его отсутствие
    kill_validate_count = 0
    # Количество циклов валидации
    kill_validate_cycles = 0
    # Минимальное количество персонажей, без имени, для удовлетворение условий победы. Нужно для миссии с Дайной.
    min_no_name_count = 0
    # Мониторинг здоровья
    health_monitor = 0
    # Таймер для циклов
    timer = 0
    timer_hook = 0
    # Уровень приёма элесиров 0-никогда, 1-красное хп, 2 - жёлтое хп
    heal_up_level = 0
    # Остановка при виде врага
    passive_defence = 0

    # Мини карта
    ms = MainScreen()
    mm = ms.mm
    act = Actions()
    magic = Magic()
    mouse = Mouse()
    color = Color()
    keys = Keys()
    p = Positions()
    info = Info()
    menu = MainMenu()

    # Классы героев
    igles = Igles()
    hildar = Hildar()
    diana = Diana()
    # Наёмники
    healer = Healer()
    elf = Elf()
    ork = Ork()
    archer = Archer()
    warrior = Warrior()
    # Группа героев
    group = Group()

    # Здоровье героев
    igles_hp = 0
    hildar_hp = 0

    # Счётчик диалога и статус победы
    dialogs = 0
    # Статус победы: 1 - победа, 2 - поражение
    win = 0
    # Если 1, то во время появления диалога победы мы выбираем победу. Иначе отменяем победу (и собираем пожитки)
    win_final = 0

    def reset(self):
        self.win = 0
        self.k_size = 0
        self.walk_min_ct_radius = 2
        self.walk_min_move_radius = 5
        self.walk_main_ct_radius = 5
        self.walk_mode = 0
        self.find_enemy = 0
        self.find_enemy_name_to_win = ''
        self.found_enemy_units = []
        self.enemy_detected = False
        self.last_target = -1
        self.kill_enemy = 1
        self.kill_validate_count = 0
        self.kill_validate_cycles = 1
        self.health_monitor = 0
        self.timer = 0
        self.timer_hook = 0
        self.need_kill_all_enemy = 0
        self.heal_up_level = 0
        self.passive_defence = 0
        pass

    def update_map(self):
        # Обновление данных экрана и мини-карты
        # self.mm.find()
        self.ms.find()
        self.mm = self.ms.mm
        # self.ms.find_heroes()
        self.update_heroes_info()
        pass

    def set_cp(self, x, y):
        # Назначить КТ
        # Перевод в относительные координаты
        self.cp = [x - cfg.mon_map['x'], y - cfg.mon_map['y']]
        print('Назначена контрольная точка: ', self.cp)
        pass

    def set_win_final(self, win_final=1):
        # Назначить победу, при появлении победного диалога
        self.win_final = win_final
        pass

    def defence_enemy_name_validate(self, name='', count=2, reset=True):
        # Защита от опредёлнного вида врагов с валидацией
        # name - имя врага
        # count - количество проверок
        if reset:
            self.reset()
        self.kill_validate_cycles = count
        self.timer_hook = 1
        self.defence_enemy_name(name, False)
        pass

    def defence_enemy_name(self, name='', reset=True):
        # Защита от опредёлнного вида врагов
        if reset:
            self.reset()
        self.find_enemy_name_to_win = name
        self.kill_enemy = 0
        self.defence_cycle()
        pass

    def find_and_kill_solo(self, x, y, n=1, name='', level=1, reset=True):
        # Убийство в соло, поразумевает приём элексиров здоровья, так как никто лечить не будет
        if reset:
            self.reset()
        self.heal_up_level = level
        self.health_monitor = 1
        self.find_and_kill(x, y, n, name, False)

    def find_and_kill(self, x, y, n=1, name='', reset=True):
        # Найти и убить врага
        if reset:
            self.reset()
        self.current_hero = n
        self.walk_mode = 1
        self.find_enemy = 1
        self.find_enemy_name_to_win = name
        self.walk(x, y)
        pass

    def walk_m(self, x, y, n=1, reset=True):
        if reset:
            self.reset()
        self.current_hero = n
        self.walk_mode = 1
        self.walk(x, y)

    def walk_s_mon(self, x, y, n=1, reset=True):
        # Движение в режиме боя, с проверкой здоровья
        if reset:
            self.reset()
        self.health_monitor = 1
        self.walk_s(x, y, n, False)

    def walk_s_kill_all(self, x, y, n=1, count=1, reset=True):
        # Движение в режиме боя, с проверкой убийства всех врагов
        if reset:
            self.reset()
        self.need_kill_all_enemy = 1
        self.timer_hook = 1
        self.find_enemy = 1
        self.kill_validate_cycles = count
        self.walk_s(x, y, n, False)

    def walk_m_defence_and_kill_all(self, x, y, n=1, count=1, reset=True):
        # Движение по маршруту, пока не заметим цель
        # Остановка и приманивание цели магом
        # Когда цели будут убиты, продолжаем движение по маршруту
        if reset:
            self.reset()
        self.need_kill_all_enemy = 1
        self.passive_defence = 1
        self.timer_hook = 1
        self.find_enemy = 1
        self.kill_validate_cycles = count
        self.walk_m(x, y, n, False)

    def walk_s(self, x, y, n=1, reset=True):
        if reset:
            self.reset()
        self.current_hero = n
        self.walk_mode = 0
        self.walk(x, y)

    def walk(self, x, y):
        # Назначаем контрольную точку и движемся к ней
        # Координаты абсолютные
        self.set_cp(x, y)
        self.act.center_group(self.current_hero)

        # Цикл движения к контольной точке
        self.cp_distance = 10
        while self.cp_distance > self.walk_main_ct_radius:
            # Проверка начавшегося диалога
            self.mon_dialog()
            # Проверка победы
            if self.win:
                break
            # Иначе движемся к цели мини циклами
            self.next_step()
            self.walk_action()
            # Цикл движения к мини цели
            self.walk_next_step_cycle()

        print('Мы прибыли к основной КТ')
        return True

    def walk_next_step_cycle(self):
        # print(self.win)
        # Цикл продвижения к мини-цели
        sleep = 100
        wat_for_break = 1

        target_done = False
        enemy_down = False
        self.timer = 0
        while not target_done:
            # Пока цель не достигнута, мониторим

            # Проверка начавшегося диалога
            self.mon_dialog()

            # Проверка победы
            if self.win:
                break

            # Обновление карты
            self.update_map()

            # Проверка начавшегося боя
            if self.find_enemy:
                self.update_units_info()
                self.p.units_info()
                # Поиск врагов
                enemy_spot = self.p.enemy_spot()

                # Поиск и убийство врагов
                if self.find_enemy_name_to_win:
                    self.find_and_kill_enemy()
                # Не завершать переход, если остались враги
                if self.need_kill_all_enemy:
                    enemy_down = self.enemy_down_monitor()
                # Остановка при виде врагов
                if self.passive_defence:
                    # enemy_down = self.passive_defence_tactic()
                    pass

            # Проверка показателей героев
            if self.health_monitor:
                self.health_monitor_actions()

            # Проверка продвижения к мини-цели
            self.act.center_group(self.current_hero)

            hero = self.get_hero_map_crd(self.current_hero)
            dst = self.get_distance(self.cpm, hero)

            # Обновляем переменную дистанции в памяти
            if self.cpm_dist != dst:
                wat_for_break = 1
                print('Дистанция до мини-цели :', dst)

            # Проверка остановки
            # Если мы стоим, назначаем новую цель not self.ms.other
            if self.cpm_dist == dst:
                wat_for_break += 1
                if self.find_enemy_name_to_win or self.need_kill_all_enemy:
                    wat_for_break = 1

            if wat_for_break > 5:
                # print('Остановка. Назначаем новую цель')
                break

            self.cpm_dist = dst
            min_radius = self.walk_min_ct_radius

            if self.cp_distance > 10:
                # При большом расстоянии до цели, перемещаемся свободно
                min_radius = self.walk_min_move_radius

            if dst < min_radius:
                target_done = True

            if self.find_enemy_name_to_win:
                # Если нужно убить врага для победы, достижение маршрута не учитывается
                target_done = False

            if self.need_kill_all_enemy and not enemy_down:
                # Если при достижении маршрута враги не убиты, достижение не учитывается
                target_done = False

            # Пауза между проверками
            time.sleep(sleep / 1000)
            self.timer += 1
            if self.timer > 10:
                self.timer = 0
            pass

        print('Мини цель достигнута')
        return True

    def passive_defence_tactic(self):
        # Тактика пассивной защиты
        # Маг приманивают добычу, воин её принимает
        # Две стадии работы:
        # 1 - сближение с целью
        # 2 - устранение цели

        pass

    def defence_cycle(self):
        # Оборона позиции. Иглез впереди, маги его защищают
        # Проверка начавшегося диалога
        # print(self.win)

        enemy_spot = 1
        sleep = 100
        self.timer = 0
        while enemy_spot > 0:
            self.mon_dialog()

            # Проверка победы
            if self.win:
                break

            # Обновление карты
            self.update_map()
            self.update_units_info()

            # Поиск врагов
            enemy_spot = self.p.enemy_spot()
            # Поиск врагов
            self.p.units_info()
            if self.find_enemy_name_to_win:
                enemy_spot = 1
                self.find_and_kill_enemy()

            # Параметры Иглеза
            if self.igles.health == 0:
                # Если полное ХП
                self.hildar.set_default_magic()
            else:
                self.hildar.select_auto_healing()
                # Гильдариус выбирает магию по умолчанию
                # TODO Контроль дистанции до Гильдариуса и сближение с ним
                pass

            # Параметры Гильдариуса
            if self.hildar.health == 0:
                # Если полное ХП
                pass
            else:
                # Гильдариус выбирает магию лечения и лечит себя
                self.hildar.auto_heal_self()
                pass

            # Пауза между проверками
            time.sleep(sleep / 1000)
            self.timer += 1
            if self.timer > 10:
                self.timer = 0
            pass

        print('Врагов не обнаружено')
        pass

    def find_and_kill_enemy(self):
        # Поиск и убийство врага. При убийстве врагов, ставит флаг победы win=1
        # print(self.p.units)
        enemy = self.p.search_enemy(self.find_enemy_name_to_win)
        # print(enemy)
        # ret = False
        if enemy:
            # Враг обнаружен
            self.enemy_detected = True
            print('Враг обнаружен:', self.find_enemy_name_to_win, enemy.__len__())
            if self.found_enemy_units:
                for e in enemy:
                    # Обновляем список
                    exist = False
                    for f in self.found_enemy_units:
                        if e[0] == f[0]:
                            exist = True
                            break
                    if not exist:
                        self.found_enemy_units.append([0, e])
                    pass
                # Проверяем убитых
                i = 0
                while i < self.found_enemy_units.__len__():
                    f = self.found_enemy_units[i]
                    killed = True
                    for e in enemy:
                        if e[0] == f[1][0]:
                            killed = False
                            break
                    if killed:
                        self.found_enemy_units[i][0] = 1
                        self.found_enemy_units[i][1]
                    i += 1
                    pass
            else:
                # Добавляем врагов в список
                for e in enemy:
                    self.found_enemy_units.append([0, e])

            if self.kill_enemy:
                # Если нападение разрешено
                # Нападаем на первого
                # TODO нападаем на ближайшего
                for f in self.found_enemy_units:
                    if f[0] != 1:
                        target_unit = f[1]
                        break
                if target_unit[0] != self.last_target:
                    # Атакуем на одну цель только один раз
                    # TODO проверить статус убийства цели с помщью дистанции и здоровья
                    target_crd = self.get_unit_abs_crd(target_unit)
                    self.act.a()
                    self.mouse.left(target_crd[0], target_crd[1])
                    self.last_target = target_unit[0]
                    print('Атака на цель', target_unit)

        else:
            # Врагов не найдено, потому-что они убиты
            if self.enemy_detected:
                # Неизвестных персонажей не найдено
                if self.p.no_name_cont <= self.min_no_name_count:
                    if self.kill_validate_count >= self.kill_validate_cycles:
                        # Подтверждаем победу только после трёх циклов поиска
                        self.win = 1
                        # ret = True
                    else:
                        self.kill_validate_count += 1
                        self.p.units = []
                        print('Цикл валидации убийства врагов')
            else:
                # Враг не был обнаружен
                if self.timer == 10:
                    # Если враг не был найден, отчищаем список юнитов. Он затесался где-то между ними
                    print('Враг не был найден, обновляем список юнитов')
                    self.p.units = []
                pass
            pass

        # Каждую секунду сбрасываем информацию о врагах
        if self.timer_hook:
            if self.timer == 10:
                self.p.remove_units_by_name(self.find_enemy_name_to_win)
                print('Обновляем список врагов')
            # print('Очистка списка врагов по таймеру')
        # return ret

    def enemy_down_monitor(self):
        # Поиск врагов. При наличии возращает False
        enemy = self.p.enemy
        ret = False
        if enemy:
            # Враг обнаружен
            self.enemy_detected = True
            print('Обнаружено врагов: ', enemy.__len__())
        else:
            # Врагов не найдено, потому-что они убиты
            if self.enemy_detected:
                # Неизвестных персонажей не найдено
                if self.p.no_name_cont == 0:
                    if self.kill_validate_count >= self.kill_validate_cycles:
                        # Подтверждаем победу только после трёх циклов поиска
                        # self.win = 1
                        ret = True
                    else:
                        self.kill_validate_count += 1
                        self.p.units = []
                        print('Цикл валидации убийства врагов')
            else:
                # Враг не был обнаружен
                if self.timer == 10:
                    # Если враг не был найден, отчищаем список юнитов. Он затесался где-то между ними
                    print('Враг не был найден, обновляем список юнитов')
                    self.p.units = []
                pass
            pass

        # Каждую секунду сбрасываем информацию о врагах
        if self.timer_hook:
            if self.timer == 10:
                self.p.remove_enemy()
                print('Обновляем список врагов')

        return ret

    def health_monitor_actions(self):
        # Мониторинг показателей героев
        # Гильдариус
        if self.hildar.health == 0:
            self.hildar.set_default_magic()
        elif self.hildar.health != 0:
            # Лечить себя
            print('Гильдариус лечит себя')
            self.hildar.auto_heal_self()
            # Проверить Щит
            self.hildar.shield_status(self.ms.matrix, self.ms.cmd)
        # Иглез
        if self.igles.health == 0:
            # Зелёное ХП
            pass
        elif self.igles.health != 0:
            # Жёлтое или красное ХП
            if self.igles.health == 1:
                # Жёлтое хп
                if self.heal_up_level == 2:
                    self.igles.heal_up()
                pass
            else:
                # Красное хп
                if self.heal_up_level > 0:
                    self.igles.heal_up()
                pass

            pass

        pass

    def go(self, x, y):
        # Назначаем контрольную точку и движемся к ней
        # Координаты абсолютные
        self.set_cp(x, y)

        self.act.center_group(self.current_hero)

        # Движемся к контрольной точке
        self.walk_to_main_ct()

        return True

    def walk_to_main_ct(self):
        # Цикл движения к контольной точке
        self.cp_distance = 10
        while self.cp_distance > self.walk_main_ct_radius:

            # Проверка начавшегося диалога
            self.mon_dialog()

            # Проверка победы
            if self.win:
                break

            # Иначе движемся к цели мини циклами
            self.next_step()

            # Командуем движением по координатам
            if self.current_hero == self.igles and self.igles_hp == 0:
                # Иглез идёт только с полным ХП
                self.walk_in_war_mode()
            else:
                # Остальные с любым
                self.walk_in_war_mode()

            # Цикл движения к мини цели
            self.walk_to_small_ct()
            # self.self_healing_test(command_num)

        print('Мы прибыли к основной КТ')
        return True

    def walk_to_small_ct(self):
        sleep = 100
        dst = 10
        wat_for_break = 1

        igles = self.igles.group
        hildar = self.hildar.group

        target_done = False

        while not target_done:

            # Проверка начавшегося диалога
            self.mon_dialog()

            # Проверка победы
            if self.win:
                break

            # Обновление карты
            self.update_map()

            # Координаты героев
            hildar_abs = self.ms.get_hero_abs(hildar)
            igles_abs = self.ms.get_hero_abs(igles)

            # Проверка начавшегося боя
            self.update_units_info()

            # Поиск врагов
            enemy_spot = 0
            if self.search_enemy:
                enemy_spot = self.p.enemy_spot()

            # Дистанция мага то врагов
            hildar_dst = [0, []]

            if enemy_spot > 0:
                self.info.p('Враг обнаружен: ' + str(enemy_spot))
                # print('Враг обнаружен: ' + str(enemy_spot))
                # TODO в зависимости от типа врага и его дистанции, используем разные тактики

                # Проверка здоровья
                self.igles_hp = self.health_status(igles)
                self.hildar_hp = self.health_status(hildar)

                # Анализ здоровья Иглеза
                if self.igles_hp == 1:
                    print('Иглез - половина ХП')
                elif self.igles_hp == 2:
                    print('Иглез - мало ХП!')
                else:
                    # У Иглеза полное ХП, устанавлинваем магию по умолчанию
                    # TODO Действия по умолчанию
                    self.hildar.set_default_magic()
                    pass

                if self.igles_hp != 0:
                    print('Гильдариус лечит Иглеза')
                    self.hildar.auto_heal(igles_abs[0], igles_abs[1])

                # Здововье Гильдариуса
                if self.hildar_hp == 1:
                    print('Гильдариус - половина ХП')

                elif self.hildar_hp == 2:
                    print('Гильдариус - мало ХП!')

                if self.hildar_hp != 0:
                    # Лечить себя
                    print('Гильдариус лечит себя')
                    if hildar_abs:
                        self.hildar.auto_heal(hildar_abs[0], hildar_abs[1])

                if self.enemy_distance > 0:
                    # Отслеживание дистанции мага до врагов
                    hildar_dst = self.stop_distance_monitor(hildar)
                    if hildar_dst[0] == 1:
                        print('Гильдариус останавливается - враг близко', round((hildar_dst[2]) / 32, 1))
                    elif hildar_dst[0] == 2:
                        # Отступаем
                        print('Гильдариус отступает - враг близко', round((hildar_dst[2]) / 32, 1))
                        if self.igles_defence_mages:
                            # Иглез атакует на врага, приближающегося к нам
                            print('Иглез защищает Гильдариуса')
                            close_enemy = hildar_dst[1]
                            enemy_crd = self.get_unit_abs_crd(close_enemy)
                            self.attack_to_target(igles, enemy_crd[0], enemy_crd[1])
                        pass
                    pass

                # Проверка магического щита. Проверяем только во время боя
                # Проверка щита Гильдариуса
                # Учитываестя количество здоровья Иглеза, перед активацией щита
                if self.igles_hp != 2 and hildar_abs:
                    self.hildar.shield_status(self.ms.matrix, self.ms.cmd, hildar_abs[0], hildar_abs[1])

                    # hs = self.shield_status(hildar)
                    # if hs == 2:
                    #    self.act.select_group(hildar)
                    #    self.act.use_shield(0)
                    #    self.mouse.left(hildar_abs[0], hildar_abs[1])
                    #    print('Активация щита')

            # Вывод информации о юнитах на карте
            # self.p.units_info()

            # Дистанция мага до Иглеза
            if self.igles_dst > 0:
                if self.igles_hp > 0:
                    # Если у Иглеза мало ХП, он идёт к магу
                    print('Иглез идёт к магу')
                    i_dst = self.distance_heroes_monitor(igles, hildar, self.igles_dst)
                else:
                    # Иначе маг идёт к Иглезу
                    if hildar_dst[0] == 0:
                        # Если дистанция уже не критическая
                        print('Маг идёт к Иглезу')
                        m_dst = self.distance_heroes_monitor(hildar, igles, self.igles_dst)

            # Проверка продвижения к мини-цели
            self.act.center_group(self.current_hero)

            hero = self.get_hero_map_crd(self.current_hero)
            dst = self.get_distance(self.cpm, hero)

            # Обновляем переменную дистанции в памяти
            if self.cpm_dist != dst:
                wat_for_break = 1
                print('Дистанция до мини-цели :', dst)

            # Проверка остановки
            # Если мы стоим, и нет врагов, назначаем новую цель not self.ms.other
            if self.cpm_dist == dst:
                # Если нет врагов
                if enemy_spot == 0:
                    wat_for_break += 1
                else:
                    # Если дистанция до врагов в норме, и у героев полное ХП
                    if hildar_dst[0] == 0 and self.igles_hp == 0 and self.hildar_hp == 0:
                        wat_for_break += 1
                    pass

            if wat_for_break > 5:
                # print('Остановка. Назначаем новую цель')
                break
                pass

            self.cpm_dist = dst

            if dst < self.walk_min_ct_radius:
                if self.point_wait_if_enemy == 0:
                    target_done = True
                elif enemy_spot == 0:
                    target_done = True

            # Пауза между проверками
            time.sleep(sleep / 1000)
        # Создаём новые координаты для мини маршрута
        print('Мини цель достигнута')
        return True

    def get_loot(self, groups=[1, 2], max_time=30):
        # Собрать лут накарте
        found = 1
        # Задержка между обновленями
        sleep = 1000
        # Время на сбор лута
        sbor_ms = max_time * 1000
        i = 0
        max_i = sbor_ms / sleep
        while found > 0:
            found = 0
            for g in groups:
                self.act.center_group(g)
                self.update_map()
                find = self.ms.find_loot()
                if found == 0:
                    found = find.__len__()
                print('Группа', g, 'обнаружила мешков:', find.__len__())
                self.act.p()

            time.sleep(sleep / 1000)

            if i > max_i:
                break
            i += 1

        # Финальный сбор
        for g in groups:
            self.act.center_group(g)
            self.act.p()

        pass

    def get_unit_abs_crd(self, unit):
        # Получение абсолютных координат на экране юнита
        # [11, [95, 64, 3, [10.0, 15.0, 305, 491]], 1, '74-101']
        # Координаты экрана
        x = unit[1][3][2]
        y = unit[1][3][3]
        abs_crd = self.screen_to_abs(x, y)
        return abs_crd

    def attack_to_target(self, n, x, y):
        # Атака на цель
        # n - группа
        # x,y - координаты цели
        self.act.select_group(n)
        self.act.a()
        print(x, y)
        self.mouse.left(x, y)
        pass

    def wait_dialog(self):
        # Ожидание диалога
        current_dialog = self.dialogs
        win = self.win
        while True:
            self.mon_dialog()
            if self.dialogs > current_dialog:
                break
            time.sleep(100 / 1000)

        self.win = win
        pass

    def mon_dialog(self):
        # Мониторинг начавшегося диалога
        if self.color.validate(395, 405, 40, 24, 40):
            # Проверяем ещё один цвет, на всякий случай
            if self.color.validate(292, 315, 248, 220, 152):
                # Стандартный диалог
                self.menu.propusk('enter', 335, 335)

                # Добавляем счётчик диалогов
                self.dialogs += 1

            elif self.color.validate(388, 346, 248, 220, 152):
                # Диалог победы
                self.win = 1
                print("Мы победили")
                if self.win_final == 1:
                    # self.keys.press('enter')
                    inter.validate_left_cicle(475, 427)
                else:
                    self.keys.press('esc')
                pass

        # Мониторинг проваленой миссии
        if self.color.validate(345, 555, 40, 24, 40):
            # Проверяем ещё один цвет, на всякий случай
            if self.color.validate(415, 348, 208, 208, 208):
                # Добавляем счётчик диалогов
                self.win = 2
                self.act.keys.press('esc', 100)
                self.act.keys.press('esc', 100)
                print('Миссия провалена')

        hero_abs = self.ms.get_hero(self.current_hero)
        if hero_abs[0] == 0 and hero_abs[1] == 0:
            # Герой не найден, миссия провалена
            # Проверяем цвет экрана
            if self.menu.is_start():
                self.win = 2
                print('Миссия провалена')

        pass

    def walk_action(self):
        # Идём в режиме боя к точке координат
        if self.walk_mode == 0:
            self.act.s()
            print('Движемся к мини-цели в режиме боеготовности')
        else:
            print('Движемся к мини-цели')
            self.act.m()
        self.mouse.left(self.cpm_mouse[0], self.cpm_mouse[1])
        pass

    def walk_in_war_mode(self):
        # Идём в режиме боя к точке координат
        print('Движемся к мини-цели в режиме боеготовности')
        self.act.s()
        self.mouse.left(self.cpm_mouse[0], self.cpm_mouse[1])
        pass

    def next_step(self):
        # Получаем данные с экрана
        self.update_map()
        # print('screen', self.mm.screen)
        # Вычисление следующиего шага

        # Расстояние до КТ в пикселях карты

        # Получаем координаты героя на большой карте
        # TODO проверить случаи ненахождения героя. Можно ставить координаты по центру карты
        hero = self.get_hero_map_crd(self.current_hero)
        # print('hero', hero)
        # Путь по x, y, который предстоит пройти
        x_path = self.cp[0] - hero[0]
        y_path = self.cp[1] - hero[1]
        # path = [x_path, y_path]
        # print("path", path)

        # Модуль пути
        x_path_abs = abs(x_path)
        y_path_abs = abs(y_path)
        # Дистанция до цели
        dist = round((x_path_abs ** 2 + y_path_abs ** 2) ** 0.5, 1)
        self.cp_distance = dist
        print('Дистанция до КТ :', dist)

        # Определяем сектор нашего движения

        # Размер экрана
        screen_hw = self.mm.screen_hw()
        # print('screen_hw', screen_hw)

        # Находится ли цель в пределах экрана
        if self.mm.screen[0][0] < self.cp[0] < self.mm.screen[1][0] and self.mm.screen[0][1] < self.cp[1] < \
                self.mm.screen[1][1]:
            # Назначаем координаты цели
            print('КТ в пределах видимости')
            mini_map_crd = [self.cp[0] - self.mm.screen[0][0], self.cp[1] - self.mm.screen[0][1]]

            # Локальная цель приравнивается к глобальной
            self.cpm = self.cp

        else:
            print('КТ за пределами видимости')
            radius = screen_hw[1] / 2
            if x_path >= 0 and y_path <= 0:
                # 0-90
                if x_path_abs <= y_path_abs:
                    # Составляем пропроцию
                    y_small = -radius
                    x_small = y_small * x_path / y_path
                else:
                    x_small = radius
                    y_small = x_small * y_path / x_path
            elif x_path >= 0 and y_path >= 0:
                # 90-180
                if x_path_abs <= y_path_abs:
                    # Составляем пропроцию
                    y_small = radius
                    x_small = y_small * x_path / y_path
                else:
                    x_small = radius
                    y_small = x_small * y_path / x_path
                pass
            elif x_path <= 0 and y_path >= 0:
                # 180 - 270
                if x_path_abs <= y_path_abs:
                    # Составляем пропроцию
                    y_small = radius
                    x_small = y_small * x_path / y_path
                else:
                    x_small = -radius
                    y_small = x_small * y_path / x_path

                pass
            else:
                # 270 - 360
                if x_path_abs <= y_path_abs:
                    # Составляем пропроцию
                    y_small = -radius
                    x_small = y_small * x_path / y_path
                else:
                    x_small = -radius
                    y_small = x_small * y_path / x_path

            dist_small = [x_small, y_small]
            # print('dist_small', dist_small)

            # Координаты мини-цели на мини-карте
            self.cpm = [round(dist_small[0] + hero[0], 0), round(dist_small[1] + hero[1], 0)]
            print('Назначена текущая мини цель: ', self.cpm)

            # Позиция героя на экране
            hero_screen = self.hero_screen(hero)
            # print('hero_screen', hero_screen)

            # Координаты на экране
            mini_map_crd = [hero_screen[0] + (dist_small[0]), hero_screen[1] + (dist_small[1])]

        mini_map_crd = [round(mini_map_crd[0], 0), round(mini_map_crd[1], 0)]

        # Определяем относительные координаты мыши
        abs_crd = self.abs_crd(mini_map_crd[0], mini_map_crd[1])

        # Валидация координат относительно реального экрана
        border = 30
        min_left = cfg.screen_shift['x'] + border
        if abs_crd[0] < min_left:
            abs_crd[0] = min_left

        max_right = cfg.screen_shift['x'] + cfg.main_window['x'] - border
        if abs_crd[0] > max_right:
            abs_crd[0] = max_right

        min_top = cfg.screen_shift['y'] + border
        if abs_crd[1] < min_top:
            abs_crd[1] = min_top

        max_bottom = cfg.screen_shift['y'] + cfg.main_window['ym'] - border
        if abs_crd[1] > max_bottom:
            abs_crd[1] = max_bottom

        # print('abs_crd', abs_crd)
        self.cpm_mouse = [abs_crd[0], abs_crd[1]]
        pass

    def get_hero_map_crd(self, num=1):
        hero_abs = self.ms.get_hero(num)
        # Положение героя на маленьком экране
        mini_crd = self.abs_to_mini(hero_abs[1], hero_abs[0])
        # Вычисляем координаты героя на мини-карте
        hero = self.hero_in_map(mini_crd)
        return hero

    def update_k_size(self):
        if self.k_size == 0:
            self.k_size = self.mm.k_size()

    def abs_crd(self, x, y):
        # Получаем абслютные координаты мыши из относительных коодринат мини-карты
        self.update_k_size()
        scr_crd = [x * self.k_size, y * self.k_size]
        abs_crd = [cfg.screen_shift['x'] + scr_crd[0], cfg.screen_shift['y'] + scr_crd[1]]
        return abs_crd

    def abs_to_mini(self, x, y):
        # Перевод из кординат экрана в координаты мини карты
        self.update_k_size()
        screen_crd = [x - cfg.screen_shift['x'], y - cfg.screen_shift['y']]
        mini_x = round(screen_crd[0] / self.k_size, 0)
        mini_y = round(screen_crd[1] / self.k_size, 0)
        mini_crd = [mini_x, mini_y]
        return mini_crd

    def screen_to_abs(self, x, y):
        # Перевод координат экрана в абсолютные
        screen_crd = [x + cfg.screen_shift['x'], y + cfg.screen_shift['y']]
        return screen_crd

    def hero_screen(self, hero):
        # Положение героя на экране
        hero_screen = [hero[0] - self.mm.screen[0][0], hero[1] - 1 - self.mm.screen[0][1]]
        return hero_screen

    def hero_in_map(self, hero):
        # Положение героя на карте
        hero_map = [hero[0] + self.mm.screen[0][0] + cfg.mm_x_shift, hero[1] + self.mm.screen[0][1] + cfg.mm_y_shift]
        return hero_map

    def get_distance(self, path, hero):
        # Дистанция до мини цели
        x_path_sm = path[0] - hero[0]
        y_path_sm = path[1] - hero[1]
        # Дистанция до мини цели
        cpm_dist = round((abs(x_path_sm) ** 2 + abs(y_path_sm) ** 2) ** 0.5, 1)
        return cpm_dist

    def update_units_info(self):
        # Получение информации о юнитах на карте
        # Отслеживание элементов на карте
        self.p.update_units(self.ms.other, self.mm.screen)
        names = Names()
        # Указание мышкой на новые
        new = self.p.get_new_units()
        if new:
            # print(new)
            for u in new:
                crd = self.p.get_mouse_position(u)
                self.mouse.move(crd[0], crd[1], 10)
                # Получаем имя юнита
                name = names.get_name()
                self.p.set_name(u[0], name)
                # Делаем снимок с имени
                # time.sleep(100 / 1000)

        # Указание мышкой на тех, кого с первого раза определить не удалось
        no_name = self.p.get_noname_units()
        if no_name:
            # print(no_name)
            for u in no_name:
                crd = self.p.get_mouse_position(u)
                self.mouse.move(crd[0], crd[1], 10)
                # Получаем имя юнита
                name = names.get_name()
                self.p.set_name(u[0], name)
                # Делаем снимок с имени
                # time.sleep(100 / 1000)

        self.p.no_name_cont = 0
        for u in self.p.units:
            if u[3] == self.p.no_name:
                self.p.no_name_cont += 1
        pass

    def shield_status(self, n):
        # TODO unused
        # Герой не найден
        ret = 0

        # Тест статуса щита героя
        a = self.ms.matrix
        f = self.ms.cmd.get(n)

        min_color = 127

        if f[0] != 0:
            # Поиск пикселя в матрице
            # Координаты команд x,y: 8,8,8
            # 15, -9
            # Проверка ОДЗ
            if f[0] - 9 < cfg.view_window['ym'] and f[1] + 15 < cfg.view_window['x']:
                # print(a[f[0] - 9, f[1] + 15])
                if a[f[0] - 9, f[1] + 15][0] > min_color and a[f[0] - 9, f[1] + 15][1] > min_color \
                        and a[f[0] - 9, f[1] + 15][2] > min_color:
                    ret = 1

                else:
                    # Щит отключён
                    ret = 2

        return ret

    def update_heroes_info(self):
        # Обновление информации о героях
        # Здоровье
        self.igles.health = self.health_status(self.igles.group)
        self.hildar.health = self.health_status(self.hildar.group)
        # Координаты
        self.igles.crd_abs = self.ms.get_hero_abs(self.igles.group)
        self.hildar.crd_abs = self.ms.get_hero_abs(self.hildar.group)
        pass

    def health_status(self, n):
        # Количество здоровья персонажа
        a = self.ms.matrix
        f = self.ms.cmd.get(n)

        # b,g,r
        full_hp = (0, 252, 0)
        half_hp = (0, 252, 248)
        small_hp = (0, 0, 200)

        ret = 0

        if f[0] == 0:
            return ret

        # Проверка ОДЗ
        if f[1] + 3 < cfg.view_window['x']:
            hp = a[f[0], f[1] + 3]

            # Проверка ХП
            if hp[1] > 120 and hp[2] > 120:
                # half hp
                ret = 1
            elif hp[1] < 70 and hp[2] > 120:
                # small hp
                ret = 2
            else:
                # full hp
                pass
            pass

        return ret

    # TODO Unused
    def set_map_pixel_size(self, n):
        # Размер пикселя героя на карте
        self.mm.map_pixel_size = n

    def self_shield_status(self, command_num):
        # TODO Unused
        ret = False
        # Мониторинг мага
        mage = Mage()
        # Тест статуса щита героя
        # self.act.g()
        # Ждём пол секунды
        if mage.magic_shield_off():
            print('Щит отключен')
            # Включаем щит
            # Останавливаемся
            self.act.t()
            ret = True
            # Ждём пол секунды
            time.sleep(500 / 1000)
            self.act.center_group(command_num)
            # Проверяем, скастован ли во время ожидания щит
            if mage.magic_shield_off():
                # Обновляем карту
                self.mm.find()
                # Используем магию принудительно.
                # TODO Учитывать движение и присутствие врагов
                hero = self.mm.heroes[0]
                hero_crd = self.hero_screen(hero)
                print('hero_crd', hero_crd)
                abs_crd = self.abs_crd(hero_crd[0], hero_crd[1])
                print('abs_crd shield', abs_crd)
                self.magic.use_magic('shield', abs_crd[0], abs_crd[1])
            else:
                print('Щит уже активен')

        else:
            # print('Щит активен')
            pass

        return ret

    def self_healing_test(self, command_num):
        # Тест самолечения героя
        i = 0
        while True:
            self.act.g()
            # Ждём пол секунды
            time.sleep(500 / 1000)
            self.act.center_group(command_num)
            # Обновляем карту
            self.mm.find()
            # Используем магию принудительно.
            hero = self.mm.heroes[0]
            hero_crd = self.hero_screen(hero)
            print('hero_crd', hero_crd)
            abs_crd = self.abs_crd(hero_crd[0], hero_crd[1])
            print('abs_crd shield', abs_crd)
            self.magic.use_magic('heal', abs_crd[0], abs_crd[1])
            # Перезапускаем цикл
            time.sleep(500 / 1000)
            i += 1
            if i > 10:
                break
        pass

    def find_hero_test(self):
        # Находим героя и наводим на него мышкой
        command_num = 2
        self.mouse.left(750, 750, sleep=200)
        self.act.center_group(command_num)

        self.mm.find()
        hero = self.mm.heroes
        print('hero map', hero)

        i = 0
        while i < 10:
            for hero_map in hero:
                print('hero_map', hero_map)
                hero_crd = self.hero_screen(hero_map)
                print('hero_crd', hero_crd)
                abs_crd = self.abs_crd(hero_crd[0], hero_crd[1])
                self.mouse.move(abs_crd[0] + 16, abs_crd[1], 100)
            i += 1

        pass

    def char_info_test(self):
        # Информация о герое
        # Находим героя и наводим на него мышкой
        command_num = 2
        self.mouse.left(750, 750, sleep=200)
        self.act.center_group(command_num)
        ms = MainScreen()
        find = ms.find()
        print(find)
        # ms.sort_result()
        # ms.find_heroes()
        print(ms.cmd)
        print(ms.other)
        pass

    def shield_status_cycle(self):
        # Находим героев на экране и отслеживаем их перемещение
        command_num = 2
        self.mouse.left(750, 750, sleep=200)
        self.act.center_group(command_num)

        i = 0
        while i < 1000:
            self.update_map()
            self.shield_status()

            time.sleep(100 / 1000)
            i += 1
        pass

    def distance_heroes_monitor(self, n, n1, dst):
        # Дистанция от одного героя до другого

        hero = self.ms.get_hero(n)
        hero2 = self.ms.get_hero(n1)
        max_dst = 32 * dst
        d = self.get_distance(hero, hero2)
        # print('d', d)
        # print('hero', hero)

        x_path_sm = hero2[1] - hero[1]
        y_path_sm = hero2[0] - hero[0]
        # print('x_path_sm, y_path_sm', x_path_sm, y_path_sm)

        if d > max_dst:
            # print('max_dst', max_dst)
            path = d - max_dst
            if path < 48:
                path = 48
            x = x_path_sm * (path / d)
            y = y_path_sm * (path / d)
            # print('path', path)
            # print('x, y', x, y)
            hero_x = int(round(hero[1] + x, 0))
            hero_y = int(round(hero[0] + y, 0))
            # print('hero_x, hero_y', hero_x, hero_y)
            hero_abs = self.screen_to_abs(hero_x, hero_y)
            self.act.select_group(n)
            self.act.m()
            self.mouse.left(hero_abs[0], hero_abs[1])
            print('Движение группы', n, 'к группе', n1, 'на', round(path / 32, 1))
        return d

    def stop_distance_monitor(self, n):
        # Дистанция мага до цели
        # 0 - Не каких действий
        # 1 - Оставновка
        # 2 - Отступление
        # TODO при разрыве дистнации вернуться к последнему действию
        if self.enemy_distance == 0:
            return

        ret = [0, [], 0]
        hero = self.ms.get_hero(n)
        safe_dst = 32 * self.enemy_distance
        ret_dst = 32 * self.retreat_distance

        # Проверка начавшегося боя
        self.update_units_info()
        if self.p.enemy_spot():
            enemy = self.p.enemy
            # print(enemy)
            min_dst = 10000

            close_enemy = []
            for e in enemy:
                if e[0] == 0:
                    continue
                path = [e[1][3][3], e[1][3][2]]
                dst = self.get_distance(path, hero)
                if dst < min_dst:
                    # print(e)
                    # print(dst)
                    min_dst = dst
                    close_enemy = e

            if safe_dst > min_dst > ret_dst:
                if self.last_dst != min_dst:
                    # Выбираем мага
                    self.act.select_group(n)
                    self.act.t()
                    # TODO сбрасывать last_dst
                    self.last_dst = min_dst
                ret = [1, close_enemy, min_dst]

            elif min_dst < ret_dst:
                if self.igles_hp == 0:
                    # Отступаем только если у Иглеза не красное хп
                    r_dst = safe_dst - min_dst
                    self.act.select_group(n)
                    self.act.r()
                    self.last_dst = 0
                    # print(ret)
                    # print(min_dst)
                    ret = [2, close_enemy, min_dst]
            else:
                # Враги далеко, сбрасываем остановку
                self.last_dst = 0
        else:
            # Врагов нет, сбрасываем остановку
            self.last_dst = 0

        return ret

    def walk_test(self):
        self.mouse.left(750, 750, sleep=200)

        # Движение к КТ
        self.current_hero = self.hildar.group
        self.walk_min_ct_radius = 2

        self.walk(974, 110)

    def distance_monitor_cycle(self):
        # Находим героев на экране и отслеживаем дистанцию между ними
        n = 2
        self.mouse.left(750, 750, sleep=200)
        self.act.center_group(n)

        i = 0
        last_dst = 0
        while i < 100:
            self.update_map()
            hero = self.ms.get_hero(n)
            print(hero)

            safe_dst = 32 * 6
            stop_dst = safe_dst + 32

            # Проверка начавшегося боя
            self.update_units_info()
            if self.p.enemy_spot():
                enemy = self.p.enemy
                # print(enemy)
                min_dst = 1000

                close_enemy = []
                for e in enemy:
                    path = [e[1][3][3], e[1][3][2]]
                    dst = self.get_distance(path, hero)
                    if dst < min_dst:
                        min_dst = dst
                        close_enemy = e

                if stop_dst > min_dst > safe_dst:
                    if last_dst == 0:
                        print('Остановка', min_dst, close_enemy)
                        self.act.center_group(n)
                        self.act.t()
                        last_dst = min_dst
                elif min_dst < safe_dst:
                    r_dst = safe_dst - min_dst
                    print('Отступаем', r_dst, close_enemy)
                    self.act.center_group(n)
                    self.act.r()

            time.sleep(100 / 1000)
            i += 1
        pass

    def find_in_screen_test(self):
        # Находим героев на экране и отслеживаем их перемещение
        command_num = 1
        self.mouse.left(750, 750, sleep=200)
        self.act.center_group(command_num)

        # Обновляем данные карты
        i = 0
        while i < 100:
            self.update_map()
            self.update_units_info()
            self.p.units_info()

            time.sleep(200 / 1000)
            i += 1
        pass

    def numpy_test(self):
        # Находим героя и наводим на него мышкой
        command_num = 2
        self.mouse.left(750, 750, sleep=200)
        self.act.center_group(command_num)

        # Поиск героев на экране
        ms = MainScreen()
        time1 = time.time()
        find = ms.find()
        time2 = time.time()
        print('numpy all', time2 - time1)

        i = 0
        while i < 10:
            for pixel in find:
                print('pixel', pixel)
                abs_crd = [cfg.screen_shift['x'] + pixel[1], cfg.screen_shift['y'] + pixel[0]]
                self.mouse.move(abs_crd[0] + 16, abs_crd[1] + 16, 100)
            i += 1

    def screen_move_test(self):
        # Движение мыши по экрану, соответствующему положению на мини карте
        command_num = 2
        self.mouse.left(750, 750, sleep=200)
        self.act.center_group(command_num)

        # Обновляем карту
        self.mm.find()

        screen = self.mm.screen
        print(screen)

        hw = self.mm.screen_hw()
        print(hw)

        x = 0
        x1 = hw[0]
        y = 0
        y1 = hw[1]
        while y < y1:
            x = 0
            while x < x1:
                abs_crd = self.abs_crd(x, y)
                print(abs_crd)
                self.mouse.move(abs_crd[0] + 16, abs_crd[1] + 16)
                x += 1
            y += 1
        pass

    def set_heroes_m7_stroy(self):
        # Назначения для формирования строя
        tank = self.warrior.group
        archers = [self.ork.group,
                   self.archer.group,
                   self.igles.group]
        mages = [self.elf.group,
                 self.healer.group,
                 self.hildar.group,
                 self.diana.group]
        self.group.set_group(tank, archers, mages)

    def set_heroes_m7_group(self):
        # Назначение групп для 7 миссии
        heroes = [self.healer,
                  self.elf,
                  self.ork,
                  self.archer,
                  self.warrior]
        self.set_heroes_group(heroes)

    def set_heroes_group(self, heroes):
        # Назначение групп, для героев из списка
        # Определяем всех персонажей, не состоящих в группах
        self.update_map()
        other = self.ms.other
        print(other)

        if other:
            for item in other:
                screen = self.ms.get_shift(self.screen_to_abs(item[1], item[0]))
                self.mouse.move(screen[0], screen[1], 100)
                for hero in heroes:
                    f = hero.face_pixel
                    if self.color.validate(f[0], f[1], f[2], f[3], f[4]):
                        # Назначаем группу героя
                        self.mouse.left(screen[0], screen[1], sleep=100)
                        self.act.make_group(hero.group)
                        pass

    def group_top(self, max_dst=0):
        # Сгруппироваться к северу
        # Допустимая дистанция отклонения от цели, px

        # Получаем координаты танка
        tank = self.group.tank
        tank_crd = self.ms.get_hero_abs(tank)
        if not tank_crd:
            return
        # Вычисляем координаты слоя лучников
        tank_x = tank_crd[0]
        tank_y = tank_crd[1]
        sleep = 1000
        last_crd = dict()
        while True:
            self.update_map()
            v1 = self.move_group(tank_x, tank_y, self.group.archers_layer, self.group.archers, last_crd, max_dst)
            v2 = self.move_group(tank_x, tank_y, self.group.mages_layer, self.group.mages, last_crd, max_dst)
            if v1 and v2:
                break
            time.sleep(sleep / 1000)
        pass
        print('Группировка войск успешно завершена')

    def move_group(self, tank_x=0, tank_y=0, dst=1, group=[], last_crd={}, max_dst=1):
        valid_dst = 16 + 32 * max_dst
        ret = True
        if group:
            ret = False
            valid_level = len(group)
            level = 0
            # Лучники
            unit_y = tank_y + dst * 32
            unit_left = int(tank_x - (round(len(group) / 2, 0)) * 32)
            unit_x = unit_left
            for unit in group:
                # Расставляем лучников по местам
                unit_dst = [unit_x, unit_y]
                unit_crd = self.ms.get_hero_abs(unit)
                if not last_crd.get(unit):
                    last_crd[unit] = [0, 0]

                if not self.validate_crd(unit_crd, unit_dst, valid_dst):
                    last_unit_crd = last_crd.get(unit)
                    # Сравниваем предыдущий ход с текущим. Если разница меньше 32 px, меняем координаты хода
                    if unit_crd and self.validate_crd(last_unit_crd, unit_crd, 0):
                        # Герой никуда не двигался, но конечной точки не достиг
                        x_offset = unit_dst[0] - unit_crd[0]
                        y_offset = unit_dst[1] - unit_crd[1]
                        unit_x += int(x_offset / 2)
                        unit_y += int(y_offset / 2)
                        # Если те

                    # Командуем идти на позицию
                    self.act.select_group(unit)
                    self.act.m()
                    self.mouse.left(unit_x, unit_y + 8)
                else:
                    level += 1

                last_crd[unit] = unit_crd

                unit_x += 32
            if level == valid_level:
                # Возвращаем успех, только тогда, когда все агенты на местах
                ret = True
        return ret

    def validate_crd(self, crd1, crd2, valid_dst=32):
        # Валидация координат
        print(crd1, crd2)
        if abs(crd1[0] - crd2[0]) <= valid_dst and abs(crd1[1] - crd2[1]) <= valid_dst:
            return True
        return False

    def update_speed_test(self):
        self.mouse.left(994, 165, sleep=200)
        self.act.center_group(1)
        time1 = time.time()

        i = 0
        while i < 10:
            #self.mm.find()
            self.ms.find()
            #self.ms.find_heroes()
            # self.update_heroes_info()
            i += 1

        time2 = time.time()
        print(time2 - time1)
