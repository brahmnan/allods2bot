# Стратегия управления героем.
# Создание персонажа, закупка в магазине, диалоги, запуск карт.

# Вся игра делится на этапы, для каждого этапа есть своя функция с набором комманд. Для упрощения тестирования, каждый
# этап можно запустить отдельно, он должен иметь сохранённую запись с номером этапа. По умолчанию игра запускается
# с 0 этапа, затем это значение меняется, запуская новые функции.

import app.ineterface as inter
import app.tavern as tavern
import time
import app.mission.mission as mission
from app.mission.mission import Mission
from app.shop import Shop


class Strategy:
    # Управление стратегией при прохождении игры

    # Этап прохождения
    stage = 0
    ms = Mission()
    mm = inter.MainMenu()
    tv = tavern.Tavern()
    shop = Shop()

    defeats = 0
    max_defeats = 10
    win_game = 0

    # Этапы прохождения
    stages = {
        0: 'new_char',
        1: 'buy_book',
        2: 'tavern',
        3: 'first_mission',
        4: 'mission_2_to_kraag',
        5: 'kaarg',
        6: 'mission_3_lairisha',
        7: 'mission_3_mage',
        8: 'mission_3_final_war',
        9: 'kaarg_2_sell_all',
        10: 'kaarg_2_byu_head',
        11: 'mission_4_dayna',
        12: 'mission_4_1_necromansers',
        13: 'kaarg_3_sell_loot',
        14: 'mission_5_drevniy_les',
        15: 'mission_5_2_necromansers',
        16: 'kaarg_4_sell_loot',
        17: 'mission_6_ork_star',
        18: 'kaarg_5_sell_loot',
        19: 'kaarg_5_buy_fireball',
        20: 'kaarg_5_taven',
        21: 'mission_7_elf',
        22: 'mission_7_1_go_to_necromansers',
        23: 'mission_7_2_necromansers_batle',
        24: 'mission_7_3_logovo_ludoedov',
        25: 'mission_7_4_logovo_ludoedov',
        26: 'mission_7_5_necromansers',
        27: 'mission_7_6_necromansers',
        28: 'mission_7_7_move_top',
        29: 'mission_7_8_most_battle',
        30: 'mission_7_9_to_elf',
        31: 'kaarg_6_sell_loot',
        32: 'kaarg_6_buy_fireball',
        33: 'kaarg_6_buy_shield',
        34: 'kaarg_6_buy_sword',
        35: 'kaarg_6_to_mission',
        36: 'mission_8_ork_star',
        37: 'mission_8_1_dragon',
        38: 'mission_8_2_trolls',
        39: 'mission_8_3_orks',
        40: 'kaarg_7_equip_the_gold_crown',
        41: 'kaarg_7_sell_loot',
        42: 'kaarg_7_buy_igles_1',
        43: 'kaarg_7_buy_igles_2',
        44: 'kaarg_7_buy_igles_3',
        45: 'kaarg_7_buy_igles_4',
        46: 'kaarg_7_buy_igles_5',
        47: 'kaarg_7_sell_loot_buy_hp',
        48: 'kaarg_7_tavern',
        49: 'mission_9_prikaz',
        50: 'mission_9_1_battle',
        51: 'kaarg_8_sell_loot',
        52: 'kaarg_8_equip_amulet',
        53: 'kaarg_8_buy_resist',
        54: 'kaarg_8_buy_grad',
        55: 'kaarg_8_buy_mana',
        56: 'kaarg_8_tavern',
        57: 'mission_10_king_hunting',
        58: 'mission_10_1',
        59: 'mission_10_2',
        60: 'mission_10_3',
        61: 'mission_10_4',
        62: 'mission_10_5',
        63: 'kaarg_9_sell_loot',
        64: 'kaarg_9_buy_sword',
        65: 'kaarg_9_buy_heal',
        66: 'kaarg_9_buy_heal_diana',
        67: 'kaarg_9_buy_heal_hildar',
        68: 'kaarg_9_buy_heal_hildar_2',
        69: 'kaarg_9_buy_grad',
        70: 'kaarg_9_buy_mana_regen',
        71: 'kaarg_9_sell_loot_2',
        711: 'kaarg_9_tavern',
        72: 'before_mission',
        73: 'mission_11',
        74: 'mission_11_1',
        75: 'mission_11_2',
        76: 'mission_11_3',
        77: 'mission_11_4',
        78: 'mission_11_5',
        79: 'mission_11_6',
        80: 'mission_11_7',
        81: 'mission_11_8'
    }

    def __init__(self):
        self.shop.findtext.init_model()
        pass

    # запуск стратегии
    def run(self):
        # Запускаем текущий этап
        if self.stages.get(self.stage):
            f = getattr(self, self.stages[self.stage])
            f()
        pass

    def set_stage(self, stage):
        self.stage = stage
        pass

    def ns(self):
        # Следующий стаж
        ck = self.stage
        nk = ck
        br = False
        for k in self.stages.keys():
            nk = k
            if br:
                break
            if k == ck:
                br = True
        return nk

    def ps(self):
        # Предыдущий стаж
        ck = self.stage
        pk = ck
        for k in self.stages.keys():
            if k == ck:
                break
            pk = k
        return pk

    def next_stage(self, stage=0, mission=True):
        # Сохранение игры и выбор следующего стажа
        if not stage:
            stage = self.ns()

        if self.win_game:
            exit()
        self.defeats = 0
        sg = inter.SaveGame()
        sg.save_game(stage)
        self.set_stage(stage)
        # Запускаем следующий стаж
        self.run()

    def retry_stage(self, stage=0):
        self.defeats += 1
        print('Миссия провалена', self.defeats)
        sg = inter.SaveGame()

        if self.defeats > self.max_defeats:
            # Загруаем предыдущий уровень.
            self.defeats = 0
            stage = self.ps()

        findtext = self.shop.findtext
        sg.load_from_menu(stage, findtext)

        self.set_stage(stage)
        self.run()

    def tol(self, result=True):
        # try or load
        if result:
            self.next_stage(self.ns())
        else:
            self.retry_stage(self.stage)
        pass

    def try_or_load(self, result=True, current_stage=0, next_stage=0):
        if result:
            self.next_stage(next_stage)
        else:
            self.retry_stage(current_stage)

    def new_char(self):
        print('Начало')

        # Проверка стартового меню
        mm = inter.MainMenu()
        if not mm.is_start():
            return
        # Новая игра
        mm.new_game()
        # Проверка подсказок
        mm.help_off()
        # Выбираем сложность
        mm.select_level()
        # Выбираем класс
        mm.select_char()
        # Подтверждаем изменения
        inter.validate_left_cicle(787, 612)
        # Принимаем настройки героя
        inter.validate_left_cicle(750, 240)

        # Меняем стаж
        next_stage = 1
        self.next_stage(next_stage, False)

    def buy_book(self):
        print('Покупаем книгу')
        s = self.shop
        # Заходим в лавку
        s.go_shop()
        # Переходим к меню книг
        s.books()
        # Ищем книгу
        coords = s.find_fire_book()
        # Книга найдена?
        book_find = False
        if coords[0] != 0:
            # Покупаем книгу
            print(' покупаем книгу')
            s.buy_item(coords[0], coords[1])
            # Применяем книгу
            s.equip(270, 610)
            # Книга найдена
            book_find = True
        else:
            # Книга не найдена - загружаем игру
            print(' книга не найдена, загружаемся')
            s.quit()
            sg = inter.SaveGame()
            sg.load_game()
            self.buy_book()

        if not book_find:
            # Продолжаем только в случае находки книги
            return False

        # Снимаем посох
        print('Продаём посох')
        s.de_equip_posoh()
        # Продаём посох
        s.to_sell()
        s.sell()
        print('Посох продан')
        # Выходим из лавки
        s.quit()

        # Меняем стаж
        next_stage = 2
        self.next_stage(next_stage, False)

    def tavern(self):
        print('Беседа в Таверне')
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern()
        tv.talk()
        # Пропуск диалога
        mm.propusk('enter', 330, 330)
        # Выход
        tv.quit()
        # Переход на миссию
        mm.go_to_first_mission()
        time.sleep(500 / 1000)
        # Пропускаем диалог
        mm.propusk('enter', 330, 330)
        # Первичные настройки
        mm.game_options()

        # Меняем стаж
        next_stage = 3
        self.next_stage(next_stage)

        pass

    def first_mission(self):
        print('Идём на миссию')
        mm = inter.MainMenu()
        ms = mission.Mission()
        ms.first_mission()

        # Ожидаем начала диалога второй миссии
        # time.sleep(10000 / 1000)
        # Ускоряем переход
        inter.validate_left_cicle(533, 557)
        mm.wait_for_color(335, 335, 40, 24, 40, 1000)
        # Пропускаем диалог
        mm.propusk('enter', 330, 330)

        # Меняем стаж
        next_stage = 4
        self.next_stage(next_stage)

    def mission_2_to_kraag(self):
        print('Вторая миссия')
        ms = mission.Mission()
        ms.second_mission()

        mm = inter.MainMenu()
        # Переход в город Каарг
        mm.caarg_click()

        # Меняем стаж
        next_stage = 5
        self.next_stage(next_stage, False)

    def kaarg(self):
        print('Диалоги в Каарге')
        print('Беседа в Таверне')
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern_kaarg()
        tv.talk_kaarg_2()
        # Пропуск диалога
        mm.propusk('enter', 330, 330)
        # Выход
        tv.quit()
        # Заходим в магазин и покупаем ускорители здоровья и маны
        s = self.shop
        print('Заходим в лавку')
        s.go_kaarg_shop()
        # Переходим к меню книг
        s.books_kaarg()
        print('Ищем элексир магии')
        # TODO Возможно цвет не будет найден. загрузить игру
        magic = s.find_color(240, 246, 64, 112, 208)
        if magic:
            s.buy_items(magic[0], magic[1], 5)
        # TODO Выбор героев через меню магазина
        # Выбираем Иглеза
        s.next_hero()
        print('Ищем элексир здоровья')
        health = s.find_color(240, 246, 200, 164, 184)
        if health:
            s.buy_items(health[0], health[1], 5)
        s.quit()
        print('Идём на миссию')
        inter.validate_left_cicle(380, 475)
        time.sleep(500 / 1000)
        # Ускоряем переход
        inter.validate_left_cicle(533, 557)
        # Ожидаем начала миссии
        mm.wait_for_color(335, 335, 40, 24, 40, 1000)

        # Пропускаем диалог
        mm.propusk('enter', 330, 330)

        next_stage = 6
        self.next_stage(next_stage)

    def mission_3_lairisha(self):
        print('Миссия - Деревня Лайриша')
        self.ms.mission_3()
        next_stage = 7
        self.next_stage(next_stage)
        pass

    def mission_3_mage(self):
        print('Сражение с магом')
        self.ms.mission_3_2()
        next_stage = 8
        self.next_stage(next_stage)
        pass

    def mission_3_final_war(self):
        print('Финальная битва')
        self.ms.mission_3_3()

        mm = inter.MainMenu()

        # Ускоряем переход
        inter.validate_left_cicle(533, 557)
        # Переход в город Каарг
        mm.caarg_click()

        # Меняем стаж
        next_stage = 9
        self.next_stage(next_stage, False)
        pass

    def kaarg_2_sell_all(self):
        print('Каарг. Покупка и продажа')
        print('Беседа в Таверне')
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern_kaarg()
        tv.talk_kaarg_3()
        # Пропуск диалога
        mm.propusk('enter', 330, 330)
        # Выход
        tv.quit()
        # Заходим в магазин и покупаем ускорители здоровья и маны
        s = self.shop
        print('Продажа лута')
        s.go_kaarg_shop()
        # Выбираем Иглеза
        s.next_hero()
        # Продаём лут: Всё кроме короны и мази
        s.sell_all()
        s.quit()
        # Сохраняем игру

        next_stage = 10
        self.next_stage(next_stage, False)
        pass

    def kaarg_2_byu_head(self):
        # Покупка шлема
        print('Каарг. Покупка шлема')
        # Задача - найти и купить железный закрытый шлем
        s = self.shop
        s.go_kaarg_shop()
        # Выбираем Иглеза
        s.next_hero()

        head_find = False
        head_color = [96, 96, 96]
        head = s.find_color_in_pages(head_color)
        if head:
            # Покупаем шлем
            print(' покупаем шлем')
            s.buy_item(head[0], head[1])
            # Одеваем шлем
            s.equip_color(head_color)
            head_find = True
        else:
            # Книга не найдена - загружаем игру
            print(' шлем не найден, загружаемся')
            s.quit()
            sg = inter.SaveGame()
            sg.load_game()
            self.kaarg_2_byu_head()

        if not head_find:
            # Продолжаем только в случае находки книги
            return False

        # Покупаем элексиры здоровья
        s.books_kaarg()
        heal_color = [72, 72, 64]
        heal = s.find_color_in_pages(heal_color)
        if heal:
            s.buy_items(heal[0], heal[1], 3)
        s.quit()

        print('Идём на миссию')
        inter.validate_left_cicle(380, 475)
        time.sleep(500 / 1000)
        # Ускоряем переход
        inter.validate_left_cicle(539, 390)
        # Ожидаем начала миссии
        self.mm.wait_for_color(335, 335, 40, 24, 40, 1000)

        # Пропускаем диалог
        self.mm.propusk('enter', 330, 330)

        next_stage = 11
        self.next_stage(next_stage)
        pass

    def mission_4_dayna(self):
        print('Миссия Звездочёт или спасение Дайны')
        self.ms.mission_4()
        next_stage = 12
        self.next_stage(next_stage)
        pass

    def mission_4_1_necromansers(self):
        print('Устранить некромансеров')
        self.ms.mission_4_1()
        mm = inter.MainMenu()

        # Ускоряем переход
        # inter.validate_left_cicle(533, 557)
        # Переход в город Каарг
        mm.caarg_click()

        # Меняем стаж
        next_stage = 13
        self.next_stage(next_stage, False)
        pass

    def kaarg_3_sell_loot(self):
        print('Задания в Каарге')
        print('Беседа в Таверне')
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern_kaarg()
        tv.talk_kaarg_3()
        # Пропуск диалога
        mm.propusk('enter', 330, 330)
        # Выход
        tv.quit()
        # Заходим в магазин и покупаем ускорители здоровья и маны
        s = self.shop
        print('Продажа лута')
        s.go_kaarg_shop()
        # Выбираем Иглеза
        s.next_hero()
        s.next_hero()
        s.sell_all()

        # Покупаем элексиры здоровья
        s.books_kaarg()
        heal_color = [72, 72, 64]
        heal = s.find_color_in_pages(heal_color)
        if heal:
            s.buy_items(heal[0], heal[1], 10)
        # s.quit()
        s.armor_kaarg()
        s.books_kaarg()
        heal_regen_color = [216, 220, 216]
        heal_regen = s.find_color_in_pages(heal_regen_color)
        if heal_regen:
            s.buy_items(heal_regen[0], heal_regen[1], 5)
        # Покупаем ману Гильдариусу
        s.next_hero()
        mana_regen_color = [104, 144, 232]
        regen = s.find_color_in_pages(mana_regen_color)
        if regen:
            s.buy_items(regen[0], regen[1], 5)

        # Покупаем ману Дайне
        s.next_hero()
        if regen:
            s.buy_items(regen[0], regen[1], 5)
        s.quit()
        # Идём на миссию
        self.mm.caarg_quit()
        time.sleep(500 / 1000)
        # Ускоряем переход
        inter.validate_left_cicle(413, 432)
        # Ожидаем начала миссии
        self.mm.wait_for_color(335, 335, 40, 24, 40, 1000)

        # Пропускаем диалог
        self.mm.propusk('enter', 330, 330)

        next_stage = 14
        self.next_stage(next_stage)
        # Сохраняем игру
        pass

    def mission_5_drevniy_les(self):
        print('Миссия 5. Древний лес')
        self.ms.mission_5()
        next_stage = 15
        self.next_stage(next_stage)
        # Сохраняем игру
        pass

    def mission_5_2_necromansers(self):
        print('Миссия 5. Устранение некромантов')
        self.ms.mission_5_2()
        self.mm.caarg_click()

        # Меняем стаж
        next_stage = 16
        self.next_stage(next_stage, False)

    def kaarg_4_sell_loot(self):
        print('Задания в Каарге')
        print('Беседа в Таверне')

        # Заходим в магазин и покупаем ускорители здоровья и маны
        s = self.shop
        print('Продажа лута')
        s.go_kaarg_shop()
        # Выбираем Иглеза
        s.sell_all()
        s.next_hero()
        s.sell_all()
        s.next_hero()
        s.sell_all()

        # Покупаем элексиры здоровья
        s.books_kaarg()
        heal_color = [72, 72, 64]
        heal = s.find_color_in_pages(heal_color)
        if heal:
            s.buy_items(heal[0], heal[1], 10)

        s.quit()
        # Идём на миссию
        self.mm.caarg_quit()
        time.sleep(500 / 1000)
        # Ускоряем переход
        inter.validate_left_cicle(712, 471)
        # Ожидаем начала миссии
        self.mm.wait_for_color(335, 335, 40, 24, 40, 1000)

        # Пропускаем диалог
        self.mm.propusk('enter', 330, 330)

        next_stage = 17
        self.next_stage(next_stage)
        # Сохраняем игру
        pass

    def mission_6_ork_star(self):
        print('Миссия 6. Орковская звезда')
        self.ms.mission_6()
        self.mm.caarg_click()

        # Меняем стаж
        next_stage = 18
        self.next_stage(next_stage)

    def kaarg_5_sell_loot(self):
        # Заходим в магазин и покупаем ускорители здоровья и маны
        s = self.shop
        print('Продажа лута')
        s.go_kaarg_shop()
        # Выбираем Иглеза
        s.sell_all()
        s.next_hero()
        s.sell_all()
        s.next_hero()
        s.sell_all()
        s.quit()
        # Сохраняем игру
        # Меняем стаж
        next_stage = 19
        self.next_stage(next_stage)
        pass

    def kaarg_5_buy_fireball(self):
        print('Покупка файрбола')
        s = self.shop
        s.go_kaarg_shop()
        # Переходим к меню книг
        s.next_hero()
        s.books_kaarg()

        print('Ищем элексир магии')
        # TODO Возможно цвет не будет найден. загрузить игру
        magic = s.find_color(240, 246, 64, 112, 208)
        if magic:
            s.buy_items(magic[0], magic[1], 10)

        s.move_shop_down()

        book_name = 'КНИГА С ЗАКЛИНАНИЕМ ОГНЕННЫЙ ШАР'
        book = s.find_by_name(book_name)

        book_find = False
        if book:
            # Покупаем книгу
            print(' покупаем книгу')
            s.buy_item(book[0], book[1])
            # Находим книгу в инвентаре
            book_inv = s.find_by_name(book_name, 'invent')
            # Применяем книгу
            if book_inv:
                s.equip(book_inv[0], book_inv[1])
            # Книга найдена
            book_find = True
        else:
            # Книга не найдена - загружаем игру
            print(' книга не найдена, загружаемся')
            s.quit()
            sg = inter.SaveGame()
            sg.load_game()
            self.kaarg_5_buy_firebol()

        if not book_find:
            # Продолжаем только в случае находки книги
            return False
        s.quit()

        next_stage = 20
        self.next_stage(next_stage)
        # Сохраняем игру

    def kaarg_5_taven(self):
        # Заходим в Таверну
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern_kaarg()
        tv.talk_1()
        mm.propusk('enter', 330, 330)
        tv.talk_2()
        mm.propusk('enter', 330, 330)
        tv.talk_3()
        mm.propusk('enter', 330, 330)
        # Выход
        tv.quit()
        mm.caarg_quit()

        # Идём к эльфам
        print('Нанимаем эльфов')
        mm.elf_click()
        tv.go_tavern_elf()
        tv.talk_1()
        mm.propusk('enter', 330, 330)
        tv.talk_2()
        mm.propusk('enter', 330, 330)
        tv.talk_4()
        mm.propusk('enter', 330, 330)
        tv.quit()
        tv.go_tavern_elf()
        tv.talk_2()
        mm.propusk('enter', 330, 330)
        tv.quit()
        mm.elf_quit()

        time.sleep(500 / 1000)
        # Ускоряем переход
        inter.validate_left_cicle(286, 367)
        # Ожидаем начала миссии
        self.mm.wait_for_color(335, 335, 40, 24, 40, 1000)

        # Пропускаем диалог
        self.mm.propusk('enter', 330, 330)

        next_stage = 21
        self.next_stage(next_stage)
        # Сохраняем игру

    def mission_7_elf(self):
        print('Миссия 7 - Эльф')
        self.ms.mission_7()
        next_stage = 22
        self.next_stage(next_stage)

    def mission_7_1_go_to_necromansers(self):
        print('Миссия 7.1 - Идём в гости к некромантам')
        self.ms.mission_7_1()
        next_stage = 23
        self.next_stage(next_stage)

    def mission_7_2_necromansers_batle(self):
        print('Миссия 7.2 - Битва с некоромантами')
        self.ms.mission_7_2()
        next_stage = 24
        self.next_stage(next_stage)
        pass

    def mission_7_3_logovo_ludoedov(self):
        print('Миссия 7.3 - Сражение с людоедами. Начало')
        self.ms.mission_7_3()
        next_stage = 25
        self.next_stage(next_stage)

    def mission_7_4_logovo_ludoedov(self):
        print('Миссия 7.4 - Сражение с людоедами. Продолжение')
        self.ms.mission_7_4()
        next_stage = 26
        self.next_stage(next_stage)

    def mission_7_5_necromansers(self):
        print('Миссия 7.5 - Подготовка к битве с некромантами')
        self.ms.mission_7_5()
        next_stage = 27
        self.next_stage(next_stage)

    def mission_7_6_necromansers(self):
        print('Миссия 7.6 - Битва с некромантами')
        self.ms.mission_7_6()
        next_stage = 28
        self.next_stage(next_stage)

    def mission_7_7_move_top(self):
        print('Миссия 7.7 - Идём наверх')
        self.ms.mission_7_7()
        next_stage = 29
        self.next_stage(next_stage)

    def mission_7_8_most_battle(self):
        print('Миссия 7.8 - Битва у моста')
        self.ms.mission_7_8()
        next_stage = 30
        self.next_stage(next_stage)

    def mission_7_9_to_elf(self):
        print('Миссия 7.9 - Идём к эльфу')
        self.ms.mission_7_9()
        # Переход в город Каарг
        self.mm.caarg_click()

        # Меняем стаж
        next_stage = 31
        self.next_stage(next_stage)

    def kaarg_6_sell_loot(self):
        # Заходим в магазин и покупаем ускорители здоровья и маны
        s = self.shop
        print('Продажа лута')
        s.go_kaarg_shop()
        # Выбираем Иглеза
        s.next_hero()
        s.sell_all()
        s.quit()
        # Сохраняем игру
        # Меняем стаж
        next_stage = 32
        self.next_stage(next_stage)
        pass

    def kaarg_6_buy_fireball(self):
        print('Покупка файрбола')
        s = self.shop
        s.go_kaarg_shop()
        # Переходим к меню книг

        s.books_kaarg()

        print('Ищем элексир магии')
        # TODO Возможно цвет не будет найден. загрузить игру
        magic = s.find_color(240, 246, 64, 112, 208)
        if magic:
            s.buy_items(magic[0], magic[1], 10)

        s.move_shop_down()

        book_name = 'КНИГА С ЗАКЛИНАНИЕМ ОГНЕННЫЙ ШАР'
        book = s.find_by_name(book_name)

        book_find = False
        if book:
            # Покупаем книгу
            print(' покупаем книгу')
            s.buy_item(book[0], book[1])
            # Находим книгу в инвентаре
            book_inv = s.find_by_name(book_name, 'invent')
            # Применяем книгу
            if book_inv:
                s.equip(book_inv[0], book_inv[1])
            # Книга найдена
            book_find = True
        else:
            # Книга не найдена - загружаем игру
            print(' книга не найдена, загружаемся')
            s.quit()
            sg = inter.SaveGame()
            sg.load_game()
            self.kaarg_5_buy_firebol()

        if not book_find:
            # Продолжаем только в случае находки книги
            return False
        s.quit()

        next_stage = 33
        self.next_stage(next_stage)
        # Сохраняем игру

    def kaarg_6_buy_shield(self):
        print('Покупка щита')

        # Мифрильный осадный щит
        item_price = 102400
        s = self.shop
        s.find_or_load(item_price)

        next_stage = 34
        self.next_stage(next_stage)
        # Сохраняем игру
        pass

    def kaarg_6_buy_sword(self):
        print('Покупка меча')

        # Адмантиновый ножичек
        sword_price = 30000
        sword_name = 'АДАМАНТИНОВЫЙ КИНЖАЛ'

        s = self.shop
        s.find_or_load(sword_price, place='weapon_kaarg', name_validate=sword_name)

        next_stage = 35
        self.next_stage(next_stage)
        # Сохраняем игру
        pass

    def kaarg_6_to_mission(self):
        mm = self.mm
        tv = self.tv
        mm.caarg_quit()

        # Идём к эльфам
        print('Нанимаем Тролля')
        mm.elf_click()
        tv.go_tavern_elf()
        tv.talk_2()
        mm.propusk('enter', 330, 330)
        tv.quit()
        mm.elf_quit()

        time.sleep(500 / 1000)
        # Ускоряем переход
        inter.validate_left_cicle(756, 468)
        # Ожидаем начала миссии
        self.mm.wait_for_color(335, 335, 40, 24, 40, 1000)

        # Пропускаем диалог
        self.mm.propusk('enter', 330, 330)

        next_stage = 36
        self.next_stage(next_stage)
        # Сохраняем игру

    def mission_8_ork_star(self):
        print('Миссия 8. Орковская звезда')
        self.ms.mission_8()
        next_stage = 37
        self.next_stage(next_stage)

    def mission_8_1_dragon(self):
        print('Миссия 8.1. Встреча дракона')
        self.ms.mission_8_1()
        next_stage = 38
        self.next_stage(next_stage)

    def mission_8_2_trolls(self):
        print('Миссия 8.2. Охота на троллей')
        self.ms.mission_8_2()
        next_stage = 39
        self.next_stage(next_stage)

    def mission_8_3_orks(self):
        print('Миссия 8.3. Орки')
        self.ms.mission_8_3()
        # Переход в город Каарг
        inter.validate_left_cicle(533, 557)
        time.sleep(500 / 1000)
        self.mm.caarg_click()
        next_stage = 40
        self.next_stage(next_stage)

    def kaarg_7_equip_the_gold_crown(self):
        print('Одеваем корону')
        s = self.shop
        s.go_kaarg_shop()

        s.next_hero()

        crown_name = 'ЗОЛОТАЯ КОРОНА'
        s.exclude = []
        crown = s.find_by_name(crown_name, 'invent')
        # Одеваем корону
        if crown:
            s.equip(crown[0], crown[1])
        s.quit()
        next_stage = 41
        self.next_stage(next_stage)

    def kaarg_7_sell_loot(self):
        # Заходим в магазин и покупаем ускорители здоровья и маны
        s = self.shop
        print('Продажа лута')
        s.go_kaarg_shop()
        # Выбираем Иглеза
        s.next_hero()
        s.sell_all()
        s.next_hero()
        s.sell_all()
        s.quit()
        # Сохраняем игру
        # Меняем стаж
        next_stage = 42
        self.next_stage(next_stage)

    def kaarg_7_buy_igles_1(self):
        # Покупка снаряжения для Иглеза
        # Кольчуга 42000
        # Кираса 210000
        # Наручи 90000
        # Перчатки 60000
        # Поножи 120000
        print('Покупка Кольчуги')
        price = 42000
        s = self.shop
        s.find_or_load(price)
        next_stage = 43
        self.next_stage(next_stage)
        pass

    def kaarg_7_buy_igles_2(self):
        # Покупка снаряжения для Иглеза
        # Кираса 210000
        print('Покупка Кирасы')
        price = 210000
        s = self.shop
        s.find_or_load(price)
        next_stage = 44
        self.next_stage(next_stage)
        pass

    def kaarg_7_buy_igles_3(self):
        # Покупка снаряжения для Иглеза
        # Наручи 90000
        print('Покупка Наручей')
        price = 90000
        s = self.shop
        s.find_or_load(price)

        next_stage = 45
        self.next_stage(next_stage)
        pass

    def kaarg_7_buy_igles_4(self):
        # Покупка снаряжения для Иглеза
        # Перчатки 60000
        print('Покупка Перчаток')
        price = 60000
        s = self.shop
        s.find_or_load(price)

        next_stage = 46
        self.next_stage(next_stage)
        pass

    def kaarg_7_buy_igles_5(self):
        # Покупка снаряжения для Иглеза
        # Поножи 120000
        print('Покупка Поножей')
        price = 120000
        s = self.shop
        s.find_or_load(price)

        next_stage = 47
        self.next_stage(next_stage)
        pass

    def kaarg_7_sell_loot_buy_hp(self):
        # Заходим в магазин и покупаем ускорители здоровья и маны
        s = self.shop
        print('Продажа лута')
        s.go_kaarg_shop()
        # Выбираем Иглеза
        s.next_hero()

        # Покупаем элексиры здоровья
        s.books_kaarg()
        heal_regen_color = [216, 220, 216]
        heal = s.find_color_in_pages(heal_regen_color)
        if heal:
            s.buy_all(heal[0], heal[1])

        heal_color = [72, 72, 64]
        heal = s.find_color_in_pages(heal_color)
        if heal:
            s.buy_all(heal[0], heal[1])

        s.sell_all()
        s.quit()
        # Сохраняем игру
        # Меняем стаж
        next_stage = 48
        self.next_stage(next_stage)

    def kaarg_7_tavern(self):
        # Заходим в Таверну
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern_kaarg()
        tv.talk_1()
        mm.propusk('enter', 330, 330)
        tv.talk_2()
        mm.propusk('enter', 330, 330)
        tv.talk_4()
        mm.propusk('enter', 330, 330)
        # Выход
        tv.quit()
        mm.caarg_quit()

        # Идём к эльфам
        print('Нанимаем эльфов')
        mm.elf_click()
        tv.go_tavern_elf()
        tv.talk_1()
        mm.propusk('enter', 330, 330)
        tv.talk_3()
        mm.propusk('enter', 330, 330)

        tv.quit()
        mm.elf_quit()

        time.sleep(500 / 1000)
        # Ускоряем переход
        inter.validate_left_cicle(482, 420)
        # Ожидаем начала миссии
        self.mm.wait_for_color(335, 335, 40, 24, 40, 1000)

        # Пропускаем диалог
        self.mm.propusk('enter', 330, 330)

        next_stage = 49
        self.next_stage(next_stage)
        # Сохраняем игру

    def mission_9_prikaz(self):
        print('Миссия - 9. Королевский приказ')
        self.ms.mission_9()
        next_stage = 50
        self.next_stage(next_stage)

    def mission_9_1_battle(self):
        print('Миссия - 9. Бой с некромантами')
        self.ms.mission_9_1()
        # Переход в город Каарг
        time.sleep(500 / 1000)
        self.mm.caarg_click()
        next_stage = 51
        self.next_stage(next_stage)

    def kaarg_8_sell_loot(self):
        # Заходим в магазин и продаём всё кроме амулета
        s = self.shop
        # Исключаем амулет
        s.exclude_price = [500000]
        print('Продажа лута')
        s.go_kaarg_shop()
        s.sell_all()
        s.next_hero()
        s.sell_all()
        s.next_hero()
        s.sell_all()
        s.quit()
        # Сохраняем игру
        # Меняем стаж
        next_stage = 52
        self.next_stage(next_stage)

    def kaarg_8_equip_amulet(self):
        # Одеваем амулет. Переключаемся между героями и ищем амулет.
        # Если он у Гильдариуса или Иглеза, перемещаем его в магазин.
        # Если он у Дайны одеваем сразу.
        # 1. Поиск амулета.
        # 2. Перемещение.
        # 3. Снаряжение
        s = self.shop
        s.go_kaarg_shop()
        price = 500000
        # Ищем у Гильдариуса
        amulet = s.find_by_price(price, 'invent')
        if amulet:
            s.drug_to_shop(amulet[0], amulet[1])
            s.next_hero()
            s.drug_to_invent()
        else:
            # Ищем амулет у Иглеза
            s.next_hero()
            s.next_hero()
            amulet = s.find_by_price(price, 'invent')
            if amulet:
                s.drug_to_shop(amulet[0], amulet[1])
                s.next_hero()
                s.next_hero()
                s.drug_to_invent()
            else:
                s.next_hero()
                s.next_hero()
            pass
        # Снаряжаем амулей Дайне

        amulet = s.find_by_price(price, 'invent')
        if amulet:
            s.equip(amulet[0], amulet[1])

        s.quit()
        next_stage = 53
        self.next_stage(next_stage)

    def kaarg_8_buy_resist(self):
        print('Покупаем защиту от магии Дайне')
        s = self.shop
        price = 20000
        name = 'КНИГА С ЗАКЛИНАНИЕМ ЗАЩИТА ОТ МАГИИ ВОДЫ'
        s.find_or_load(price, 1, 'books_kaarg', name_validate=name)
        next_stage = 54
        self.next_stage(next_stage)

    def kaarg_8_buy_grad(self):
        print('Покупаем град Дайне')
        s = self.shop
        price = 1000000
        name = 'КНИГА С ЗАКЛИНАНИЕМ ГРАД'
        s.find_or_load(price, 1, 'books_kaarg', name_validate=name)
        next_stage = 55
        self.next_stage(next_stage)

    def kaarg_8_buy_mana(self):
        print('Покупаем ману Дайне')
        s = self.shop
        s.go_kaarg_shop()
        # Выбираем Дайну
        s.next_hero()

        # Покупаем элексиры здоровья
        s.books_kaarg()
        mana_regen_color = [104, 144, 232]
        mana = s.find_color_in_pages(mana_regen_color)
        if mana:
            s.buy_all(mana[0], mana[1])

        heal_color = [72, 72, 64]
        heal = s.find_color_in_pages(heal_color)
        if heal:
            s.buy_all(heal[0], heal[1])

        s.quit()
        next_stage = 56
        self.next_stage(next_stage)

    def kaarg_8_tavern(self):
        # Заходим в Таверну
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern_kaarg()
        tv.talk_3()
        mm.propusk('enter', 330, 330)
        tv.talk_4()
        mm.propusk('enter', 330, 330)

        # Выход
        tv.quit()
        mm.caarg_quit()

        time.sleep(200 / 1000)

        # Идём на миссию
        inter.validate_left_cicle(589, 306)
        # Ожидаем начала миссии
        self.mm.wait_for_color(335, 335, 40, 24, 40, 1000)

        # Пропускаем диалог
        self.mm.propusk('enter', 330, 330)

        next_stage = 57
        self.next_stage(next_stage)
        # Сохраняем игру

    def mission_10_king_hunting(self):
        print('Королевская охота')
        result = self.ms.mission_10()
        self.try_or_load(result, 57, 58)

    def mission_10_1(self):
        print('Пчёлы')
        result = self.ms.mission_10_1()
        self.try_or_load(result, 58, 59)

    def mission_10_2(self):
        print('Людоеды')
        result = self.ms.mission_10_2()
        self.try_or_load(result, 59, 60)

    def mission_10_3(self):
        print('Ящеры')
        result = self.ms.mission_10_3()
        self.try_or_load(result, 60, 61)

    def mission_10_4(self):
        print('Дракон')
        result = self.ms.mission_10_4()
        self.try_or_load(result, 61, 62)

    def mission_10_5(self):
        print('Король')
        result = self.ms.mission_10_5()
        if result:
            # Переход в город Каарг
            time.sleep(500 / 1000)
            self.mm.caarg_click()
        self.try_or_load(result, 62, 63)

    def kaarg_9_sell_loot(self):
        # Заходим в магазин и продаём всё кроме амулета
        s = self.shop
        # Исключаем зелёную броню
        s.exclude_price = [1800000, 900000, 1350000, 3150000, 175000, 450000, 225000, 4500000]
        print('Продажа лута')
        s.go_kaarg_shop()
        s.sell_all()
        s.next_hero()
        s.sell_all(equip_exclude=True)
        s.next_hero()
        s.sell_all()
        s.quit()
        # Сохраняем игру
        # Меняем стаж
        self.next_stage()

    def kaarg_9_buy_sword(self):
        # Покупка меча
        price = 600000
        print('Покупка Меча')
        s = self.shop
        s.find_or_load(price, place='weapon_kaarg')
        self.next_stage()
        pass

    def kaarg_9_buy_heal(self):
        print('Покупаем целебные снадобья Иглезу')
        current_stage = 65
        s = self.shop
        price = 10000
        color = [0, 10, 208, 104, 120]
        stop_price = 100000
        item_count = 3
        s.find_or_load(price, 1, 'books_kaarg', color_validate=color, equip=False,
                       stop_price=stop_price, item_count=item_count, current_stage=current_stage, buy_all=True)
        print(s.purchased_items)

        self.next_stage()

    def kaarg_9_buy_heal_diana(self):
        print('Покупаем целебные снадобья Дайне')
        current_stage = 66
        s = self.shop
        price = 10000
        color = [0, 10, 208, 104, 120]
        stop_price = 100000
        s.find_or_load(price, 2, 'books_kaarg', color_validate=color, equip=False,
                       stop_price=stop_price, current_stage=current_stage, buy_all=True)
        print(s.purchased_items)

        self.next_stage()

    def kaarg_9_buy_heal_hildar(self):
        print('Покупаем целебные снадобья Гильдариусу')
        current_stage = 67
        s = self.shop
        price = 10000
        color = [0, 10, 208, 104, 120]
        stop_price = 100000
        s.find_or_load(price, 0, 'books_kaarg', color_validate=color, equip=False,
                       stop_price=stop_price, current_stage=current_stage, buy_all=True)
        print(s.purchased_items)

        self.next_stage()

    def kaarg_9_buy_heal_hildar_2(self):
        print('Покупаем целебные снадобья Гильдариусу')
        current_stage = 68
        s = self.shop
        price = 1000
        color = [0, 10, 224, 104, 120]
        stop_price = 100000
        s.find_or_load(price, 0, 'books_kaarg', color_validate=color, equip=False,
                       stop_price=stop_price, current_stage=current_stage, buy_all=True)
        print(s.purchased_items)

        self.next_stage()

    def kaarg_9_buy_grad(self):
        print('Покупаем град Гильдариусу')
        s = self.shop
        price = 1000000
        name = 'КНИГА С ЗАКЛИНАНИЕМ ГРАД'
        s.find_or_load(price, 0, 'books_kaarg', name_validate=name)
        self.next_stage()

    def kaarg_9_buy_mana_regen(self):
        print('Покупаем регенерацию магии Гильдариусу')
        s = self.shop
        price = 100
        color = [0, 0, 104, 144, 232]
        s.find_or_load(price, 0, 'books_kaarg', color_validate=color, buy_all=True)
        self.next_stage()

    def kaarg_9_buy_svet(self):
        # TODO UNUSED
        print('Покупаем свечение Дайне')
        current_stage = 701
        # Задача. Быстро найти свиток свечения.
        # Ищем по цене, пока она не станет больше 10к
        # Смотрим только сопадающие по цвету
        # После покупки смотрим количество, цикл продолжается до тех пор, пока нужное количество не будет достигнуто.
        s = self.shop
        price = 10000
        # Ставим букву З заместо Э
        name = 'ЗЛЬФИЙСКИЙ СВИТОК С ЗАКЛИНАНИЕМ СВЕЧЕНИЕ'
        color = [0, 0, 80, 120, 136]
        stop_price = 50000
        item_count = 2
        s.find_or_load(price, 1, 'books_kaarg', name_validate=name, color_validate=color, equip=False,
                       stop_price=stop_price, item_count=item_count, current_stage=current_stage, buy_all=True)
        print(s.purchased_items)

        self.next_stage()

    def kaarg_9_sell_loot_2(self):
        # Заходим в магазин и продаём всё кроме амулета
        s = self.shop
        # Исключаем зелёную броню
        print('Продажа лута')
        s.go_kaarg_shop()
        s.next_hero()
        s.sell_all()
        s.quit()
        # Сохраняем игру
        # Меняем стаж
        self.next_stage()

    def kaarg_9_tavern(self):
        # Заходим в Таверну
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern_kaarg()

        # tv.talk_4()
        # mm.propusk('enter', 330, 330)

        tv.talk_5()
        mm.propusk('enter', 330, 330)

        # Выход
        tv.quit()
        mm.caarg_quit()

        # Идём к эльфам
        print('Нанимаем эльфов')
        mm.elf_click()
        tv.go_tavern_elf()
        tv.talk_1()
        mm.propusk('enter', 330, 330)

        tv.quit()
        self.next_stage()

    def before_mission(self):
        mm = inter.MainMenu()
        mm.elf_quit()
        self.max_defeats = 10

        time.sleep(200 / 1000)

        # Идём на миссию
        inter.validate_left_cicle(650, 310)
        # Ожидаем начала миссии
        self.mm.wait_for_color(335, 335, 40, 24, 40, 1000)

        # Пропускаем диалог
        self.mm.propusk('enter', 330, 330)

        self.next_stage()
        # Сохраняем игру

    def mission_11(self):
        print('Миссия 11. Поход к Урду')
        self.max_defeats = 3
        self.tol(self.ms.mission_11())

    def mission_11_1(self):
        print('Миссия 11.1. Сражение с некромантами')
        self.max_defeats = 10
        self.tol(self.ms.mission_11_1())

    def mission_11_2(self):
        print('Миссия 11.2. Сражение с некромантами 2')
        self.tol(self.ms.mission_11_2())

    def mission_11_3(self):
        print('Миссия 11.3. Идём к мосту')
        self.tol(self.ms.mission_11_3())

    def mission_11_4(self):
        print('Миссия 11.4. Сражение у моста')
        self.tol(self.ms.mission_11_4())

    def mission_11_5(self):
        print('Миссия 11.5. Поход ко второму мосту')
        self.tol(self.ms.mission_11_5())

    def mission_11_6(self):
        print('Миссия 11.6. Сражение у второго моста')
        self.tol(self.ms.mission_11_6())

    def mission_11_7(self):
        print('Миссия 11.7. Сражение 2 у второго моста')
        self.tol(self.ms.mission_11_7())

    def mission_11_8(self):
        print('Миссия 11.8. Сражение с Урдом')
        result = self.ms.mission_11_8()
        if result:
            self.win_game = 1
        self.tol(result)
