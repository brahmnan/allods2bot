# Управление персонажем на миссии
# Для каждой миссии свой набор контрольных точек и сигналов
from app.mission.actions import *
from app.mission.task_manager import TaskManager
import app.mission.task as task
from app.commands import Mouse
from app.mission.tactic import CheckPoint
import app.ineterface as inter
from app.mission.heroes import *
import time


class Mission:
    # Управление миссиями
    magic = Magic()
    act = Actions()
    tm = TaskManager()
    mouse = Mouse()
    cp = CheckPoint()
    mm = inter.MainMenu()

    # Короткие назавания групп
    igl = cp.igles.group
    hil = cp.hildar.group
    dia = cp.diana.group

    # Маги
    hildar = Hildar()
    healer = Healer()
    diana = Diana()
    elf = Elf()
    # Воины
    igles = Igles()
    warrior = Warrior()
    troll = Troll()
    # Лучники
    ork = Ork()
    archer = Archer()
    group = Group()
    # Волшебницы из последней миссии
    en1 = Enchantress1()
    en2 = Enchantress2()
    en3 = Enchantress3()
    # Остальные
    other = Other()

    # Назначения для формирования строя
    tank = warrior.group
    archers = [igles.group,
               archer.group,
               ork.group]
    mages = [elf.group,
             healer.group,
             hildar.group,
             diana.group]

    group.set_group(tank, archers, mages)

    # Группа валидации, необходима, чтобы отличить своих от чужих
    validate_group = [hildar.group, healer.group, diana.group, elf.group, igles.group, warrior.group, ork.group,
                      archer.group]

    bless_group = [tank, igles.group, ork.group, archer.group]
    resist_group = [tank, igles.group, ork.group, archer.group, elf.group, healer.group, hildar.group,
                    diana.group]

    # Время старта миссии
    start_mission_time = 0

    def enter_game(self):
        self.start_mission_time = time.time()
        self.mm.enter_game()

    def defence(self, n1, n2):
        self.act.select_group(n1)
        self.act.d()
        n_abs = self.cp.ms.get_hero_abs(n2)
        self.mouse.left(n_abs[0], n_abs[1], sleep=0)
        pass

    def first_mission(self):
        # Первая миссия
        # Кликаем по карте, чтобы войти в игру
        self.mouse.left(904, 116, sleep=100)
        # Выбираем героя
        self.act.select_all()
        # Назначаем группу - 2, для мага Хильдариуса

        self.act.make_group(self.cp.hildar.group)
        self.act.center_group(self.cp.hildar.group)
        # Октрываем книгу магии
        self.mm.show_magic_menu()
        # Закрываем инвентарь
        self.mm.close_invent()

        # Назначаем горячие клавиши на лечение и онгенную стрелу
        self.magic.select_magic('shield')
        self.act.ctrl_key(self.act.magic_shield_key)
        self.magic.select_magic('heal')
        self.act.ctrl_key(self.act.magic_heal_key)
        self.magic.bind_auto_magic('fire')
        self.act.ctrl_key(self.act.magic_fire_key)

        # Назначаем победу при проявлнии диалога
        self.cp.set_win_final(1)
        # Назначаем главным Хильдара
        self.cp.current_hero = self.cp.hildar.group
        # Отправление к контрольной точке Мага
        self.cp.walk_s_mon(993, 109, self.hil)

        pass

    def second_mission(self):
        self.cp.reset()
        # Назначаем победу при проявлнии диалога
        self.cp.set_win_final(1)
        # Назначаем главным Хильдара
        self.cp.current_hero = self.cp.hildar.group
        self.mouse.left(750, 750, sleep=100)
        # Отправление к контрольной точке Мага
        self.cp.walk_s_mon(897, 123, self.hil)
        self.cp.walk_s_mon(894, 99, self.hil)
        self.cp.walk_s_mon(950, 83, self.hil)
        self.cp.walk_s_mon(950, 54, self.hil)

    def mission_3(self):
        self.cp.reset()
        # Назначаем победу при проявлнии диалога
        self.cp.set_win_final(1)

        self.cp.current_hero = self.cp.hildar.group

        # Кликаем по карте, чтобы войти в игру
        self.mouse.left(892, 58, sleep=100)
        self.act.center_group(self.cp.hildar.group)

        # Убираем выбор Хильдариуса
        self.act.select_group(self.cp.igles.group)

        # Находим Иглеза и определяем его в команду
        # Обновляем карту
        self.cp.update_map()
        # На карте видны два героя Гильдариус и Иглез. Выбираем Иглеза
        igles_crd = self.cp.ms.other
        igles_abs = self.cp.ms.get_shift(self.cp.screen_to_abs(igles_crd[0][1], igles_crd[0][0]))
        # Кликаем на Иглеза
        self.mouse.left(igles_abs[0], igles_abs[1], sleep=200)
        self.act.make_group(self.cp.igles.group)

        # Открываем инвентарь
        self.mm.open_invent()

        # Выбираем Иглеза
        self.act.center_group(self.cp.igles.group)
        # Назначаем горячие клавиши
        self.mouse.move(112, 755)
        self.act.shift_key(self.act.heal_regen_key)

        # Иглез охраняет мага
        self.act.d()

        hilda_abs = self.cp.ms.get_hero_abs(self.cp.hildar.group)

        self.mouse.left(hilda_abs[0], hilda_abs[1], sleep=200)
        # Выбираем мага
        # Октрываем книгу магии
        self.mm.show_magic_menu()
        self.act.select_group(self.cp.hildar.group)
        self.mouse.move(112, 755)
        self.act.shift_key(self.act.mana_regen_key)
        self.mm.close_invent()

        # Назначаем главным Иглеза и идем к КТ
        # self.act.select_group(self.cp.igles.group)
        # self.cp.current_hero = self.cp.igles.group

        # Идём к КТ
        self.cp.walk_s_mon(917, 142, self.hil)
        self.cp.walk_s_mon(952, 156, self.hil)
        self.cp.walk_s_mon(971, 113, self.hil)
        self.act.t()
        self.cp.current_hero = self.cp.igles.group
        self.act.center_group(self.cp.igles.group)
        self.cp.walk_s(979, 114, self.igl)
        self.act.t()
        time.sleep(1000 / 1000)

    def mission_3_2(self):
        self.mouse.left(750, 750, sleep=100)
        # Сражение с магом
        # Маг занимает позицию на горе и принимает элексир
        self.cp.walk_m(974, 111, self.hil)
        # Маг принимает элексир маны
        self.cp.hildar.mana_regen()
        # Ставим хилинг по умолчанию
        self.cp.hildar.set_auto_healing()

        # Иглез принимает элексир
        self.cp.igles.heal_regen()
        # Иглез идёт к вражескому магу
        self.cp.find_and_kill(980, 107, self.igl, 'Некромансер')
        # Иглез отступает к Хильдару
        self.cp.walk_m(978, 109, self.igl)
        # Хильдар выбирает огненную стрелу и мониторит здоровье Иглеза
        self.cp.hildar.set_auto_magic_arrow()
        # Оборона позиции
        self.cp.defence_enemy_name_validate('Рыцарь', 1)
        # Хильдар обораняет Иглеза
        self.defence(self.hil, self.igl)
        # Сбор мешков
        self.cp.get_loot([1])
        # Идём к крестьянам
        self.cp.walk_m(980, 105, self.igl)
        self.cp.walk_s(944, 154, self.igl)
        pass

    def mission_3_3(self):
        self.mouse.left(750, 750, sleep=100)
        # Финальное сражение
        self.act.center_group(self.hil)
        print('Гильдариус занимает позицию')
        self.cp.walk_s(906, 147, self.hil)
        # Иглез идёт на позицию
        # self.cp.walk_m(909, 148, self.igl)
        print('Иглез занимает позицию')
        self.cp.walk_m(907, 150, self.igl)
        # Иглез принимает реген
        self.cp.igles.heal_regen()
        self.cp.hildar.mana_regen()
        # Ставим лечение по умолчанию
        self.cp.hildar.set_auto_healing()
        # Ждём атаку рыцарей
        print('Ждём атаку рыцарей')
        self.cp.defence_enemy_name_validate('Рыцарь', 2)
        # Гиальдариус влючает огненную стрелу
        self.cp.hildar.set_auto_magic_arrow()
        # Иглез актакует на некромансера
        print('Атакуем Некромансера')
        self.cp.find_and_kill(903, 160, self.igl, 'Некромансер')
        print('Добиваем Арбалетчиц')
        self.cp.walk_s_kill_all(903, 155, self.igl, 2)
        # Сбор мешков
        print('Сбор мешков')
        self.cp.get_loot([1], 120)
        # Назначаем победу при проявлнии диалога
        self.cp.set_win_final(1)
        print('Идём на КТ')
        self.cp.walk_m(920, 141, self.igl)
        pass

    def mission_4(self):
        # Идём к мосту
        self.mouse.left(896, 167, sleep=100)
        self.act.center_group(self.igl)

        # Открываем инвентарь
        self.mm.open_invent()

        # Выбираем Иглеза
        self.act.center_group(self.cp.igles.group)
        # Назначаем горячие клавиши
        self.mouse.move(270, 755)
        self.act.shift_key(self.act.heal_up_key)
        self.mm.close_invent()

        self.mm.show_magic_menu()
        # Обновляем карту
        self.cp.update_map()

        # Иглез защищает Гилдариуса
        self.defence(self.igl, self.hil)
        self.cp.walk_s(929, 113, 2)
        self.cp.walk_s(931, 113, 1)
        # Убиваем разбойника
        self.defence(self.hil, self.igl)
        self.cp.hildar.set_auto_healing()
        self.cp.find_and_kill(948, 112, 1, 'Атаман')
        self.cp.get_loot([1])
        # Занимаем позицию у магов
        self.cp.walk_m(953, 108, 2)
        self.cp.walk_m(964, 116, 2)
        self.cp.walk_m(953, 108, 1)
        self.cp.walk_m(962, 116, 1)
        pass

    def mission_4_1(self):
        # Убиваем магов
        # Идём к мосту
        self.mouse.left(750, 750, sleep=100)
        self.act.center_group(self.igl)
        self.cp.igles.heal_regen()
        self.cp.min_no_name_count = 1
        self.cp.find_and_kill_solo(976, 122, 1, 'Некромансер', 1)
        # self.cp.walk_s_kill_all(976, 122, 1)
        self.cp.min_no_name_count = 0
        self.cp.hildar.set_auto_magic_arrow()
        self.cp.walk_s(976, 122, 2)
        self.cp.get_loot([1])
        self.cp.walk_m(953, 112, 1)
        self.cp.walk_m(925, 112, 1)
        self.cp.walk_m(909, 93, 1)
        self.cp.walk_m(909, 59, 1)
        # Обратный путь
        self.cp.walk_s(909, 93, 1)
        self.cp.walk_m(925, 112, 1)
        self.cp.walk_m(953, 112, 1)
        self.cp.set_win_final(1)
        self.cp.walk_m(974, 122, 1)
        pass

    def mission_5(self):
        # Кликаем по карте, чтобы войти в игру
        self.mouse.left(996, 59, sleep=100)
        self.act.center_group(self.igl)

        self.mm.close_invent()
        self.mm.show_magic_menu()

        # Убираем выбор
        self.act.select_group(self.dia)

        # Находим Дайну и определяем в команду
        # Обновляем карту
        self.cp.update_map()
        # На карте видны два героя Гильдариус и Иглез. Выбираем Иглеза
        dia_crd = self.cp.ms.other
        dia_abs = self.cp.ms.get_shift(self.cp.screen_to_abs(dia_crd[0][1], dia_crd[0][0]))
        # Кликаем на Дайну
        self.mouse.left(dia_abs[0], dia_abs[1], sleep=200)
        self.act.make_group(self.dia)
        # Выбираем огненную стрелу для Дайны
        self.act.select_group(self.dia)
        self.cp.diana.set_auto_magic_arrow(force=True)
        # Маги защищают война
        self.defence(self.dia, self.igl)
        self.defence(self.hil, self.igl)

        self.cp.walk_m(976, 60, self.igl)
        self.cp.defence_cycle()
        # Ждём две секунды, путь маги отлечат Иглеза
        time.sleep(2000 / 1000)
        self.cp.walk_s(956, 58, self.igl)
        # Движемся к Рапторам и встречаем их
        self.cp.walk_m(947, 58, self.igl)
        self.cp.igles.heal_regen()
        time.sleep(2000 / 1000)
        self.cp.defence_cycle()
        self.cp.walk_s(932, 60, self.igl)
        self.cp.walk_s(890, 106, self.igl)
        time.sleep(5000 / 1000)
        self.cp.walk_m(891, 119, self.igl)
        time.sleep(2000 / 1000)
        self.cp.defence_cycle()
        self.cp.walk_s(911, 139, self.igl)
        self.cp.walk_s(920, 136, self.igl)
        self.cp.get_loot([1, 2, 3])
        # Идём к друидам
        time.sleep(1000 / 1000)
        self.act.center_group(self.igl)
        self.act.t()
        self.cp.update_map()
        self.defence(self.dia, self.igl)
        self.defence(self.hil, self.igl)
        self.cp.walk_m(953, 99, self.igl)
        # Занимаем позицию у горы некромантов

    def mission_5_2(self):
        # Кликаем по карте, чтобы войти в игру
        self.mouse.left(750, 750, sleep=100)
        self.act.center_group(self.dia)
        self.cp.walk_m(956, 100, self.dia)
        self.cp.diana.cast_to_target(self.magic.fire_res, self.cp.igles.crd_abs[0], self.cp.igles.crd_abs[1])
        self.cp.diana.set_auto_healing()
        self.cp.walk_m(954, 100, self.hil)
        self.cp.hildar.set_auto_healing()
        # Уничтожаем зомби
        self.cp.walk_m(957, 95, self.igl)
        time.sleep(1000 / 1000)
        self.cp.defence_enemy_name_validate('Зомби', 2)
        self.act.speed_minus()
        # time.sleep(3000 / 1000)
        self.cp.diana.cast_to_target(self.magic.fire_res, self.cp.igles.crd_abs[0], self.cp.igles.crd_abs[1])
        self.cp.igles.heal_regen()
        # Воин идёт и убивает некромантов, принимая аптечки
        self.cp.find_and_kill_solo(958, 84, self.igl, 'Некромансер', 2)
        self.act.speed_plus()
        self.cp.get_loot([1, 3])
        self.cp.set_win_final(1)
        self.cp.walk_m(907, 140, self.hil)

    def mission_6(self):
        # Орковская звезда
        # Кликаем по карте, чтобы войти в игру
        self.mouse.left(889, 165, sleep=100)
        self.act.center_group(self.igl)

        mm = inter.MainMenu()
        mm.go_options()
        mm.set_healing_union()
        mm.set_healing_all()
        mm.quit_options()

        # Обновляем карту
        self.cp.update_map()

        self.mm.close_invent()
        self.mm.show_magic_menu()

        # Огненная стрела по умолчанию
        self.cp.diana.set_auto_magic_arrow(True)
        self.cp.hildar.set_auto_magic_arrow(True)

        # Защищаем Иглеза
        self.defence(self.dia, self.igl)
        self.defence(self.hil, self.igl)

        # Идём к КТ
        self.cp.walk_s(901, 161, self.igl)
        time.sleep(1000 / 1000)
        self.cp.walk_s(922, 158, self.igl)
        time.sleep(1000 / 1000)
        self.cp.walk_s(945, 136, self.igl)
        self.cp.defence_cycle()
        time.sleep(1000 / 1000)
        self.cp.walk_s(955, 124, self.igl)
        time.sleep(2000 / 1000)
        self.cp.walk_s(978, 119, self.igl)
        time.sleep(3000 / 1000)
        self.cp.walk_s(980, 94, self.igl)
        time.sleep(2000 / 1000)

        # Гильдариус за главного
        self.act.center_group(self.hil)
        self.cp.hildar.set_auto_healing()
        self.act.t()
        # Обновляем карту
        self.cp.update_map()

        self.defence(self.dia, self.hil)
        self.defence(self.igl, self.hil)
        time.sleep(1000 / 1000)
        self.cp.walk_s(994, 82, self.hil)
        self.cp.walk_s(984, 71, self.hil)
        self.cp.walk_s(996, 56, self.hil)
        self.cp.get_loot([1, 2, 3])

        mm.win_option()
        pass

    def test_cirlce_cp(self):
        self.mouse.left(904, 116, sleep=200)
        self.act.select_all()
        self.act.center_group(2)

        # Тестирование движение указателя по мини карте
        x = 888
        y = 55

        sleep = 0

        while x < 1000:
            self.cp.set_cp(x, y)

            self.cp.next_step()
            # self.mouse.move(x,y)
            # time.sleep(sleep / 1000)
            self.mouse.move(self.cp.cpm_mouse[0], self.cp.cpm_mouse[1])
            time.sleep(sleep / 1000)
            # print(x, y)
            x += 10

        while y < 170:
            self.cp.set_cp(x, y)

            self.cp.next_step()
            # self.mouse.move(x,y)
            # time.sleep(sleep / 1000)
            self.mouse.move(self.cp.cpm_mouse[0], self.cp.cpm_mouse[1])
            time.sleep(sleep / 1000)
            print(x, y)
            y += 10

        while x > 888:
            self.cp.set_cp(x, y)

            self.cp.next_step()
            # self.mouse.move(x,y)
            # time.sleep(sleep / 1000)
            self.mouse.move(self.cp.cpm_mouse[0], self.cp.cpm_mouse[1])
            time.sleep(sleep / 1000)
            # print(x, y)
            x -= 10

        while y > 55:
            self.cp.set_cp(x, y)

            self.cp.next_step()
            # self.mouse.move(x,y)
            # time.sleep(sleep / 1000)
            self.mouse.move(self.cp.cpm_mouse[0], self.cp.cpm_mouse[1])
            time.sleep(sleep / 1000)
            # print(x, y)
            y -= 10
        pass

    def mission_7(self):
        # Эльф
        # Кликаем по карте, чтобы войти в игру
        self.mouse.left(996, 163, sleep=100)
        # Что нужно уметь в этой миссии
        # 1. Группироваться согласно определённого пордядка. Воин впереди, далее лучники, сзади маги.
        #   Воин-танк занимает позицию и группа выстравивается от этой позиции, меняя команды и указывая куда им идти.
        #   Затем происходит валидация сбора.
        # 2. Многозадачность - каждый юнит выполняет свою задачу и отслеживает её статус:
        #   Маги Дамагеры:
        #   Маг - Эльф
        #       Основной скилл - каменная стрела.
        #       Дополнительный - лечение. Если у танка красное ХП - включает дополнительный скилл.
        #   Маг - Дайна
        #       Основной скилл - огненная стрела.
        #       Дополнительный - огненный шар. Будет использоваться для ликвидации именных врагов на расстоянии.
        #   Маги Хилеры:
        #   Маг - Гильдариус
        #       Основной скилл - лечение.
        #       Дополнительный - огненная стрела. Если у танка полное ХП - включает дополнительный скилл.
        #   Маг - наёмник:
        #       Основной скилл - лечение.
        #       Обязанности:
        #           - Благословение на войнов, отслеживание его статуса (по таймеру)
        #               Включается по требованию, во время циклов встречи врагов. Когда все герои на местах.
        #           - Защита от магии воздуха на всю команду, отслеживание статуса (по таймеру)
        #               Включается по требованию.
        # 3. Отслеживание зачистки сектора.
        #   Воин должен пройти по маршруту сектора, и если цели не найдены, группа сменяет позицию.
        #   Если цель обнаружена, воин должен приманить её к группе, ликвидировать, затем повторить обход сектора.
        # 4. Отслеживание количества врагов и изменении скорости игры, в зависимости от количества.
        #   Пример - один людоед не опасен, а если их трое, то лучше уменьшить скорость игры, чтобы лучше отслеживать
        #   статус героя-танка.
        # 5. Отслеживание здоровья наёмников и героев и в случае их смерти, загрузка с контрольной точки.

        # Задачи миссии
        # К нам идёт людоед, до его прихода нужно назначить наёмников в группы и сгруппироваться.
        # Первым делом, понижаем скорость игры.
        self.act.speed_minus()
        # Затем ставим в опциях - минимальный автокастинг.
        mm = inter.MainMenu()
        mm.go_options()
        mm.set_min_auto_cast()
        mm.quit_options()

        # Назначаем групы для формирования строя
        # Группа героев
        group = Group()
        # Классы героев
        igles = Igles()
        hildar = Hildar()
        diana = Diana()
        # Наёмники
        healer = Healer()
        elf = Elf()
        ork = Ork()
        archer = Archer()
        warrior = Warrior()
        # Назначения для формирования строя
        tank = warrior.group
        archers = [igles.group,
                   archer.group,
                   ork.group]
        mages = [elf.group,
                 healer.group,
                 hildar.group,
                 diana.group]
        group.set_group(tank, archers, mages)

        # Определяем группы
        self.act.center_group(tank)
        # TODO добавить проверку на назначение групп, с помощью выбора назначенной группы и получения её координат.
        self.cp.set_heroes_m7_group()

        self.mm.close_invent()
        self.mm.show_magic_menu()

        # Настраиваем магию по умолчанию, для наёмников
        self.act.select_group(healer.group)
        self.magic.bind_auto_magic('lighting')

        self.act.select_group(elf.group)
        self.magic.bind_auto_magic('earth')

        self.act.speed_plus()

        tm = TaskManager()
        # Воин идёт к месту сбора
        self.act.center_group(tank)

        walk_id = tm.add_task(task.Walk([1000, 159], tank))
        tm.add_task(task.Group(group), walk_id)
        # Убить людоеда
        enemy_down = tm.add_task(task.EnemyDown(1))
        tm.run()

        # Поднятся на север и убить волков
        tm.add_task(task.EnemyDown(0))
        walk_top = tm.add_task(task.Walk([999, 155], tank))
        tm.add_task(task.Group(group, 2), walk_top)
        tm.run()

        tm.add_task(task.GetLoot(1))
        tm.run()

    def mission_7_1(self):
        self.mouse.left(996, 163, sleep=100)

        # Паническое авто-отступление
        self.mm.opt_panic_retreat()

        tm = TaskManager()
        tm.set_validate_group(self.validate_group)
        # Воин идёт к месту сбора
        self.act.center_group(self.tank)

        # Медик лечит всех, у кого жёлтое ХП
        tm.add_task(task.HealthMon(self.healer.group))

        walk_group = self.ork.group

        retreat_group = [self.hildar.group, self.diana.group, self.elf.group, self.healer.group]

        # Группа магов, которые будут отступать, если враг вблизи.
        tm.add_task(task.Retreat(retreat_group, self.validate_group, 2))

        top = tm.add_task(task.WalkGroup([1005, 132], walk_group))
        left = tm.add_task(task.WalkGroup([1005, 132], walk_group), top)
        left_top = tm.add_task(task.WalkGroup([1000, 119], walk_group), left)

        wolf = tm.add_task(task.WalkGroup([970, 114], walk_group), left_top)
        wolf_clear = tm.add_task(task.EnemyClear([self.igles.group, self.warrior.group]), wolf)
        wait = tm.add_task(task.Wait(1), wolf_clear)

        raptor = tm.add_task(task.WalkGroup([962, 108], walk_group), wait)
        raptor_clear = tm.add_task(task.EnemyClear([self.igles.group, self.warrior.group]), raptor)
        wait2 = tm.add_task(task.Wait(1), raptor_clear)
        mouse = tm.add_task(task.WalkGroup([957, 122], walk_group), wait2)
        necro = tm.add_task(task.WalkGroup([961, 103], walk_group), mouse)
        wait3 = tm.add_task(task.Wait(5), necro)
        tm.run()
        pass

    def mission_7_2(self):
        self.mouse.left(960, 104, sleep=100)

        # Отключаем автоотступление
        mm = inter.MainMenu()
        mm.go_options()
        mm.set_no_retreat()
        mm.quit_options()

        # Маги
        hildar = Hildar()
        healer = Healer()
        diana = Diana()
        elf = Elf()
        # Воины
        igles = Igles()
        warrior = Warrior()
        # Лучники
        ork = Ork()
        archer = Archer()

        group = Group()
        # Назначения для формирования строя
        tank = warrior.group
        archers = [igles.group,
                   archer.group,
                   ork.group]
        mages = [elf.group,
                 healer.group,
                 hildar.group,
                 diana.group]
        group.set_group(tank, archers, mages)

        # Группа валидации, необходима, чтобы отличить своих от чужих
        validate_group = [hildar.group, healer.group, diana.group, elf.group, igles.group, warrior.group, ork.group,
                          archer.group]

        tm = TaskManager()
        tm.set_validate_group(validate_group)
        # Воин идёт к месту сбора
        self.act.center_group(tank)
        tank_pos = tm.add_task(task.Walk([954, 99], tank))
        tm.add_task(task.Group(group, 0, 1), tank_pos)
        tm.run()

        # Цикл защиты
        bless_group = [tank, igles.group, ork.group, archer.group]
        resist_group = [tank, igles.group, ork.group, archer.group, elf.group, healer.group, hildar.group, diana.group]
        bless_task = tm.add_task(task.Bless(bless_group, healer.group, 12))
        resist_task = tm.add_task(task.LightResist(resist_group, healer.group, 38))
        # tm.add_task(task.EnemyDown(3))
        # Ждём, пока маг накастует защиту
        wait = tm.add_task(task.Wait(10))

        # Приманиваем врагов
        clear_sector = tm.add_task(task.ClearSector([943, 97], tank), wait)
        tm.add_task(task.HealthMon(healer.group, wait_time=200, max_dst=6), clear_sector)

        # Убиваем врагов ближнего боя
        bb_clear = tm.add_task(task.EnemyClear([tank]), clear_sector)
        necro = tm.add_task(task.KillEnemyMagic(6, diana.group, 'fire_ball'), clear_sector)
        tm.run()

        # Контр-атака
        walk_group = ork.group
        kill_all = tm.add_task(task.WalkGroup([942, 99], walk_group))
        loot_1 = tm.add_task(task.GetLoot(igles.group), kill_all)
        tm.remove_task(bless_task)
        tm.remove_task(resist_task)
        tm.run()

        loot_2 = tm.add_task(task.GetLoot(igles.group))
        next = tm.add_task(task.WalkGroup([930, 107], walk_group), loot_2)
        tm.run()
        pass

    def mission_7_3(self):
        self.mouse.left(930, 107, sleep=100)
        # Маги
        hildar = Hildar()
        healer = Healer()
        diana = Diana()
        elf = Elf()
        # Воины
        igles = Igles()
        warrior = Warrior()
        # Лучники
        ork = Ork()
        archer = Archer()
        group = Group()
        # Назначения для формирования строя
        tank = warrior.group
        archers = [igles.group,
                   archer.group,
                   ork.group]
        mages = [elf.group,
                 healer.group,
                 hildar.group,
                 diana.group]
        group.set_group(tank, archers, mages)

        # Группа валидации, необходима, чтобы отличить своих от чужих
        validate_group = [hildar.group, healer.group, diana.group, elf.group, igles.group, warrior.group, ork.group,
                          archer.group]

        tm = TaskManager()
        tm.set_validate_group(validate_group)

        # Воин идёт к месту сбора
        self.act.center_group(tank)
        tank_pos = tm.add_task(task.Walk([931, 112], tank))
        top = tm.add_task(task.Group(group, 0, 0), tank_pos)
        tm.run()
        # Хилер включает лечение
        # healer.set_auto_healing()

        # Цикл защиты
        bless_group = [tank, ork.group, archer.group]
        bless_task = tm.add_task(task.Bless(bless_group, healer.group, 12))
        # Ждём, пока маг накастует защиту
        wait = tm.add_task(task.Wait(3))

        # Приманимаем врагов
        clear_sector = tm.add_task(task.ClearSector([930, 120], tank), wait)
        bb_clear = tm.add_task(task.EnemyClear([tank]), clear_sector)
        tm.add_task(task.HealthMon(healer.group, wait_time=200, max_dst=6), clear_sector)
        tm.run()

        tm.remove_task(bless_task)
        # Сбор лута
        loot_1 = tm.add_task(task.GetLoot(igles.group))
        tm.run()
        # Перегруппировка на юг
        walk_group = ork.group
        kill_all = tm.add_task(task.WalkGroup([927, 125], walk_group))
        tank_pos = tm.add_task(task.Walk([928, 129], tank), kill_all)
        top = tm.add_task(task.Group(group, 0, 0), tank_pos)
        tm.run()

        # Атака на людоедов
        bless_task = tm.add_task(task.Bless(bless_group, healer.group, 12))
        wait = tm.add_task(task.Wait(3))

        clear_sector = tm.add_task(task.ClearSector([929, 137], tank), wait)
        bb_clear = tm.add_task(task.EnemyClear([tank]), clear_sector)
        tm.run()
        tm.remove_task(bless_task)

        # Сбор лута
        loot_1 = tm.add_task(task.GetLoot(igles.group))
        tm.run()

        # Спускаемся южнее
        tank_pos = tm.add_task(task.Walk([938, 133], tank))
        top = tm.add_task(task.Group(group, 0, 0), tank_pos)
        tm.run()

        bless_task = tm.add_task(task.Bless(bless_group, healer.group, 12))
        wait = tm.add_task(task.Wait(3))
        clear_sector = tm.add_task(task.ClearSector([937, 140], tank), wait)
        # Эльф ставит стену
        tm.add_task(task.StoneWall(elf.group, [-1, 0], 8), clear_sector)
        tm.add_task(task.StoneWall(elf.group, [0, 2], 9), clear_sector)
        bb_clear = tm.add_task(task.EnemyClear([tank]), clear_sector)
        tm.run()

    def mission_7_4(self):
        self.mouse.left(750, 750, sleep=100)
        # Маги
        hildar = Hildar()
        healer = Healer()
        diana = Diana()
        elf = Elf()
        # Воины
        igles = Igles()
        warrior = Warrior()
        # Лучники
        ork = Ork()
        archer = Archer()
        group = Group()
        # Назначения для формирования строя
        tank = warrior.group
        archers = [igles.group,
                   archer.group,
                   ork.group]
        mages = [elf.group,
                 healer.group,
                 hildar.group,
                 diana.group]
        group.set_group(tank, archers, mages)

        # Группа валидации, необходима, чтобы отличить своих от чужих
        validate_group = [hildar.group, healer.group, diana.group, elf.group, igles.group, warrior.group, ork.group,
                          archer.group]

        tm = TaskManager()
        tm.set_validate_group(validate_group)

        tm.add_task(task.HealthMon(healer.group, wait_time=200, max_dst=6))

        tank_pos = tm.add_task(task.Walk([935, 132], tank))
        clear_sector1 = tm.add_task(task.ClearSector([925, 150], tank), tank_pos)
        l1_clear = tm.add_task(task.EnemyClear([tank]), clear_sector1)
        clear_sector2 = tm.add_task(task.ClearSector([919, 143], tank), l1_clear)
        l2_clear = tm.add_task(task.EnemyClear([tank]), clear_sector2)
        clear_sector3 = tm.add_task(task.ClearSector([920, 152], tank), l2_clear)
        l2_clear = tm.add_task(task.EnemyClear([tank]), clear_sector3)
        tm.run()

        # Сбор лута
        loot_1 = tm.add_task(task.GetLoot(igles.group))
        tm.run()

    def mission_7_5(self):
        self.mouse.left(750, 750, sleep=100)
        # Маги
        hildar = Hildar()
        healer = Healer()
        diana = Diana()
        elf = Elf()
        # Воины
        igles = Igles()
        warrior = Warrior()
        # Лучники
        ork = Ork()
        archer = Archer()
        group = Group()
        # Назначения для формирования строя
        tank = warrior.group
        archers = [igles.group,
                   archer.group,
                   ork.group]
        mages = [elf.group,
                 healer.group,
                 hildar.group,
                 diana.group]
        group.set_group(tank, archers, mages)

        # Группа валидации, необходима, чтобы отличить своих от чужих
        validate_group = [hildar.group, healer.group, diana.group, elf.group, igles.group, warrior.group, ork.group,
                          archer.group]

        tm = TaskManager()
        tm.set_validate_group(validate_group)
        walk_group = ork.group
        mode_down = tm.add_task(task.WalkGroup([920, 172], walk_group))
        tank_pos = tm.add_task(task.Walk([912, 172], tank), mode_down)
        gr = tm.add_task(task.Group(group, 0, 1), tank_pos)

        tm.run()

    def mission_7_6(self):
        self.mouse.left(750, 750, sleep=100)

        self.act.center_group(self.tank)
        tm = TaskManager()
        tm.set_validate_group(self.validate_group)

        resist_task = tm.add_task(task.LightResist(self.resist_group, self.healer.group, 38))
        bless_task = tm.add_task(task.Bless(self.bless_group, self.healer.group, 12))

        # Ждём, пока маг накастует защиту
        wait = tm.add_task(task.Wait(10))
        tm.run()
        # self.act.speed_minus()
        clear_sector = tm.add_task(task.ClearSector([904, 167], self.tank))
        tm.remove_task(bless_task)
        tm.remove_task(resist_task)
        tm.run()

        self.act.speed_minus()
        self.diana.mana_regen()
        self.hildar.mana_regen()

        enemy_spot = task.EnemyClear([self.tank])
        enemy_spot.min_dst = 10
        сlear = tm.add_task(enemy_spot)
        wait = tm.add_task(task.Wait(10))

        resist_task = tm.add_task(task.LightResist(self.resist_group, self.healer.group, 80), wait)
        bless_task = tm.add_task(task.Bless(self.bless_group, self.healer.group, 25), wait)
        necro = tm.add_task(task.KillEnemyMagic(6, self.diana.group, 'fire_ball', wait_time=200), wait)
        # Медик лечит всех, у кого жёлтое ХП
        tm.add_task(task.HealthMon(self.healer.group, wait_time=200), wait)

        tm.run()
        tm.clear_tasks()
        # Паническое авто-отступление
        self.mm.opt_panic_retreat()

        # walk_group = self.ork.group
        # mode_left = tm.add_task(task.WalkGroup([906, 167], walk_group))
        # mode_up = tm.add_task(task.WalkGroup([913, 164], walk_group), mode_left)
        tank_pos = tm.add_task(task.Walk([909, 172], self.tank))
        ork_pos = tm.add_task(task.Walk([909, 170], self.ork.group))

        # Дайна расстреливает файрболов всех врагов
        enemy_spot = task.EnemyClear([self.tank])
        enemy_spot.min_dst = 10
        сlear = tm.add_task(enemy_spot)

        # Файрбол повышенного радиуса
        fire_ball = task.KillEnemyMagic(6, self.diana.group, 'fire_ball', wait_time=200)
        fire_ball.max_dst = 16
        necro = tm.add_task(fire_ball)

        # Медик лечит всех, у кого жёлтое ХП
        # tm.add_task(task.HealthMon(self.hildar.group, wait_time=200))
        tm.add_task(task.HealthMon(self.healer.group, wait_time=200))

        tm.run()

        self.act.speed_plus()
        mode_up = tm.add_task(task.WalkGroup([913, 164], self.ork.group))
        walk = tm.add_task(task.Walk([909, 171], self.igles.group), mode_up)
        loot_1 = tm.add_task(task.GetLoot(self.igles.group), walk)
        wait = tm.add_task(task.Wait(5), loot_1)
        loot_2 = tm.add_task(task.GetLoot(self.igles.group), wait)
        tm.run()

    def mission_7_7(self):
        # Идём наверх
        self.mouse.left(750, 750, sleep=100)
        # Маги
        hildar = Hildar()
        healer = Healer()
        diana = Diana()
        elf = Elf()
        # Воины
        igles = Igles()
        warrior = Warrior()
        # Лучники
        ork = Ork()
        archer = Archer()
        group = Group()
        # Назначения для формирования строя
        tank = warrior.group
        archers = [igles.group,
                   archer.group,
                   ork.group]
        mages = [elf.group,
                 healer.group,
                 hildar.group,
                 diana.group]

        group.set_group(tank, archers, mages)

        # Группа валидации, необходима, чтобы отличить своих от чужих
        validate_group = [hildar.group, healer.group, diana.group, elf.group, igles.group, warrior.group, ork.group,
                          archer.group]

        walk_group = ork.group

        self.act.center_group(tank)
        tm = TaskManager()
        tm.set_validate_group(validate_group)

        move_up = tm.add_task(task.WalkGroup([895, 116], walk_group))
        tank_pos = tm.add_task(task.Walk([895, 111], tank), move_up)
        gr = tm.add_task(task.Group(group, 0, 2), tank_pos)

        tm.run()

    def mission_7_8(self):
        # Сражение у моста
        self.mouse.left(750, 750, sleep=100)

        self.act.center_group(self.tank)
        tm = TaskManager()
        tm.set_validate_group(self.validate_group)

        # Не отступаем
        self.mm.opt_no_retreat()

        # Воин поднимается наврех и приманиевает зомби
        clear_sector = tm.add_task(task.ClearSector([895, 100], self.tank))
        wait = tm.add_task(task.Wait(3), clear_sector)
        tm.run()

        # Дайна обестреливает некроманта и лучников файрболом
        self.diana.mana_regen()
        сlear = tm.add_task(task.EnemyClear([self.tank]))
        necro = tm.add_task(task.KillEnemyMagic(6, self.diana.group, 'fire_ball', wait_time=200))

        # Медик лечит всех, у кого жёлтое ХП
        tm.add_task(task.HealthMon(self.healer.group, wait_time=200, max_dst=6))
        tm.run()

        # Иглез собирает лут, пока другие дерутся
        i_pos = tm.add_task(task.Walk([895, 106], self.igles.group))
        loot = tm.add_task(task.GetLoot(self.igles.group), i_pos)
        tm.run()

        # Вся команда атакует выживших врагов
        move_top = tm.add_task(task.WalkGroup([894, 89], self.ork.group))
        tm.run()
        tm.clear_tasks()

        #  Иглез собирает лут
        loot = tm.add_task(task.GetLoot(self.igles.group))
        tm.run()

        # Танк занимает новую позицию
        tank_pos = tm.add_task(task.Walk([890, 82], self.tank))
        gr = tm.add_task(task.Group(self.group, 0, 2), tank_pos)
        tm.run()

        # Воин поднимается наврех и приманиевает зомби
        clear_sector = tm.add_task(task.ClearSector([885, 69], self.tank))
        сlear = tm.add_task(task.EnemyClear([self.tank]), clear_sector)
        tm.run()

        #  Иглез собирает лут
        loot = tm.add_task(task.GetLoot(self.igles.group))
        tm.run()

        # Танк занимает новую позицию
        tank_pos = tm.add_task(task.Walk([886, 73], self.tank))
        gr = tm.add_task(task.Group(self.group, 0, 2), tank_pos)
        tm.run()

        # Воин поднимается наврех и приманивает магов
        clear_sector = tm.add_task(task.ClearSector([894, 63], self.tank))
        # Дайна защищает от магии огня
        resist_task = tm.add_task(task.FireResist([self.tank], self.diana.group, 38), clear_sector)

        tm.run()
        necro = tm.add_task(task.KillEnemyMagic(6, self.diana.group, 'fire_ball', wait_time=200))
        # Медик лечит всех, у кого жёлтое ХП
        tm.add_task(task.HealthMon(self.healer.group, wait_time=200))
        сlear = tm.add_task(task.EnemyClear([self.tank]))
        tm.run()
        tm.clear_tasks()

        # Танк поднимается к выжившим магам, чтобы подсветить их для Дайны
        tank_pos = tm.add_task(task.Walk([887, 72], self.tank))
        # Дайна защищает от магии огня
        resist_task = tm.add_task(task.FireResist([self.tank], self.diana.group, 38), tank_pos)
        tm.run()

        # Дайна расстреливает файрболов всех врагов
        enemy_spot = task.EnemyClear([self.tank, self.ork])
        enemy_spot.min_dst = 10
        сlear = tm.add_task(enemy_spot)

        # Файрбол
        self.diana.mana_regen()
        fire_ball = task.KillEnemyMagic(6, self.diana.group, 'fire_ball', wait_time=200)
        fire_ball.max_dst = 16
        necro = tm.add_task(fire_ball)

        # Хилер лечит
        tm.add_task(task.HealthMon(self.healer.group, wait_time=200))
        tm.run()

        #  Иглез собирает лут
        loot = tm.add_task(task.GetLoot(self.igles.group))
        tm.run()

        tm.clear_tasks()
        # Вся команда атакует выживших врагов
        move_top = tm.add_task(task.WalkGroup([890, 58], self.ork.group))
        tm.add_task(task.HealthMon(self.healer.group, wait_time=200))
        tm.add_task(task.HealthMon(self.hildar.group, wait_time=200))
        tm.run()

        #  Иглез собирает лут
        loot = tm.add_task(task.GetLoot(self.igles.group))
        tm.run()

    def mission_7_9(self):
        # Идём к Эльфу
        self.mouse.left(750, 750, sleep=100)
        self.act.center_group(self.hildar.group)

        self.mm.go_options()
        self.mm.set_med_auto_cast()
        self.mm.quit_options()

        tm = TaskManager()

        tm.add_task(task.Walk([895, 91], self.hildar.group))
        tm.run()
        tm.add_task(task.Walk([895, 109], self.hildar.group))
        tm.run()
        tm.add_task(task.Walk([963, 105], self.hildar.group))
        tm.run()
        tm.add_task(task.Walk([953, 135], self.hildar.group))
        tm.run()
        tm.add_task(task.Walk([953, 150], self.hildar.group))
        tm.run()
        tm.add_task(task.Walk([956, 171], self.hildar.group))
        tm.run()

    def mission_8(self):
        # 1. Назначаем тролля в группу.
        # 2. Идём на север, убиваем черепах и ждём дракона.
        # 3. Принимаем дракона и идём на восток к троллям.
        # 4. Принимаем всех троллей и занимаем мост.
        # 5. Приманиваем орков и расстреливаем их файрболом.

        self.mm.enter_game()
        self.act.center_group(self.igles.group)

        self.mm.close_invent()
        self.mm.show_magic_menu()

        # Назначение групп для 7 миссии
        heroes = [self.troll]
        self.cp.set_heroes_group(heroes)

        self.hildar.set_auto_magic_arrow(True)

        tm = TaskManager()
        self.validate_group = [self.igles.group, self.hildar.group, self.diana.group, self.troll.group]
        tm.set_validate_group(self.validate_group)

        tm.add_task(task.Wait(10))
        tm.run()
        # Идём наверх
        left = tm.add_task(task.WalkGroup([904, 90], self.hildar.group))
        top = tm.add_task(task.WalkGroup([904, 75], self.hildar.group), left)
        tm.run()

        tm.add_task(task.Walk([906, 80], self.igles.group))
        tm.run()

    def mission_8_1(self):

        self.mm.enter_game()
        self.act.center_group(self.igles.group)
        # self.hildar.set_auto_healing()

        validate_group = [self.igles.group, self.hildar.group, self.diana.group, self.troll.group]
        tm = TaskManager()

        tm.set_validate_group(validate_group)

        tm.add_task(task.Wait(3))
        tm.run()

        # Дайна защищает от магии огня
        tm.add_task(task.FireResist([self.igles.group], self.diana.group, 38))

        # Ожидаем дракона
        attack = tm.add_task(task.WaitingForAnAttack([self.igles.group], 3))

        # Иглез спускается чуть ниже
        tm.add_task(task.Walk([906, 81], self.igles.group), attack)
        tm.run()

        # Гильдариус лечит, Дайна атакует
        tm.add_task(task.HealthMon(self.hildar.group, wait_time=200))

        # Дайна расстреливает всех врагов
        enemy_spot = task.EnemyClear([self.hildar.group])
        enemy_spot.min_dst = 15
        сlear = tm.add_task(enemy_spot)

        # Файрбол
        self.diana.mana_regen()
        fire = task.KillEnemyMagic(6, self.diana.group, cast_magic='fire', wait_time=3000)
        fire.max_dst = 16
        tm.add_task(fire)
        tm.run()

        tm.add_task(task.Wait(3))
        tm.run()
        tm.clear_tasks()

        tm.add_task(task.GetLoot(self.diana.group))
        tm.run()
        pass

    def mission_8_2(self):
        self.mm.enter_game()
        self.act.center_group(self.igles.group)
        validate_group = [self.igles.group, self.hildar.group, self.diana.group, self.troll.group]
        tm = TaskManager()
        tm.set_validate_group(validate_group)

        # Идём вниз
        move1 = tm.add_task(task.WalkGroup([904, 86], self.hildar.group))
        troll_move = tm.add_task(task.Walk([912, 84], self.troll.group), move1)
        tm.run()

        move1 = tm.add_task(task.WalkGroup([916, 82], self.troll.group))
        tm.add_task(task.EnemyClear([self.troll.group]))
        tm.run()

        # Сбор лута
        tm.add_task(task.GetLoot(self.igles.group))
        tm.run()

        move1 = tm.add_task(task.Walk([917, 87], self.igles.group))
        move2 = tm.add_task(task.Walk([919, 85], self.hildar.group), move1)
        move3 = tm.add_task(task.Walk([918, 85], self.diana.group), move2)
        move4 = tm.add_task(task.Walk([922, 87], self.troll.group), move3)
        tm.run()

        # Гильдариус лечит
        tm.add_task(task.HealthMon(self.hildar.group, wait_time=200))
        tm.add_task(task.EnemyClear([self.hildar.group], validation_interval=5, min_dst=7))

        # Дайна расстреливает мышей
        tm.add_task(task.KillEnemyMagic(4, self.diana.group, 'fire_ball', wait_time=200,
                                        target_type=0, far_target=False, max_dst=8))

        tm.run()
        tm.clear_tasks()

        # Дайна расстреливает всех выживших врагов
        enemy_spot = task.EnemyClear([self.hildar.group])
        enemy_spot.min_dst = 15
        сlear = tm.add_task(enemy_spot)

        tm.add_task(task.KillEnemyMagic(6, self.diana.group, cast_magic='fire_ball', max_dst=16))
        tm.run()
        # Ждём прибытия всех троллей

        tm.add_task(task.GetLoot(self.igles.group))
        tm.run()
        pass

    def mission_8_3(self):
        self.mm.enter_game()
        self.act.center_group(self.igles.group)
        validate_group = [self.igles.group, self.hildar.group, self.diana.group, self.troll.group]
        tm = TaskManager()
        tm.set_validate_group(validate_group)

        move1 = tm.add_task(task.Walk([929, 87], self.troll.group))
        move2 = tm.add_task(task.Walk([926, 82], self.hildar.group), move1)
        move3 = tm.add_task(task.Walk([926, 83], self.diana.group), move2)
        move4 = tm.add_task(task.Walk([926, 85], self.igles.group), move3)
        tm.run()

        # Иглез приманивает орков
        clear = task.ClearSector([942, 91], self.igles.group)
        # Минимальная дистанция до героя
        clear.min_dst = 5
        # Максимальная дистанция до героя
        clear.max_dst = 7
        # Дистанция, после которой мы возвращаемя к объекту
        clear.return_dst = 9
        tm.add_task(clear)
        tm.run()
        # Дайна расстреливает всех фарболом
        self.diana.mana_regen()
        self.igles.heal_regen()
        # Бьём всех ближних
        close = tm.add_task(task.EnemyClear([self.diana.group], min_dst=11, validation_interval=3))
        close_dst = tm.add_task(task.KillEnemyMagic(6, self.diana.group, cast_magic='fire_ball', max_dst=11))

        # Затем Тролль сближается с врагами
        next_pos = tm.add_task(task.Walk([932, 91], self.troll.group), close)
        tm.run()

        tm.clear_tasks()
        self.diana.mana_regen()

        # Гильдариус лечит
        tm.add_task(task.EnemyClear([self.diana.group], min_dst=16))
        # tm.add_task(task.HealthMon(self.hildar.group, wait_time=200))
        far_dst = tm.add_task(task.KillEnemyMagic(6, self.diana.group, cast_magic='fire_ball', max_dst=16))
        tm.run()

        tm.win_final = 0

        loot1 = tm.add_task(task.GetLoot(self.igles.group))
        tm.add_task(task.GetLoot(self.igles.group), loot1)
        tm.run()

        if tm.win:
            self.mm.win_option()

    def mission_9(self):
        # Миссия 9
        # Назначаем группы.
        # Занимаем позицию слева.
        # Воин идёт к позиции некроманта.
        # Иглез атакует главного некроманта, ожидает диалога и возвращается к команде.
        # Воин возвращается к команде.
        # Дайна отстреливает лучников, кастует на Иглеза защиту от огня.
        # Когда ближних врагов не остаётся, Иглез добивает оставшихся некромантов.
        self.mm.enter_game()
        self.mm.open_invent()
        # Мы появляемся в самом низу карты, поэтому поднимаемся немного
        self.mm.hide_magic_menu()
        self.act.center_group(self.igles.group)
        tm = TaskManager()
        tm.add_task(task.WalkGroup([955, 167], self.igles.group))
        tm.run()
        self.act.select_all()
        self.act.t()
        wait = tm.add_task(task.Wait(1))
        tm.run()

        self.act.center_group(self.igles.group)

        # Назначение групп для 7 миссии
        heroes = [self.warrior, self.ork, self.archer, self.elf]
        self.cp.set_heroes_group(heroes)

        self.mm.close_invent()
        self.mm.show_magic_menu()

        self.act.center_group(self.igles.group)
        self.hildar.set_auto_healing(True)

        tm = TaskManager()
        self.validate_group = [self.igles.group, self.hildar.group, self.diana.group,
                               self.warrior.group, self.ork.group, self.archer.group, self.elf.group]
        tm.set_validate_group(self.validate_group)

        # Идём к некроманту
        tm.add_task(task.WalkGroup([954, 161], self.igles.group))
        tm.run()

        # Иглез занимает позицию
        tm.add_task(task.WalkGroup([938, 157], self.igles.group))
        tm.run()

        # Иглез занимает позицию
        tm.add_task(task.Walk([936, 155], self.igles.group))
        tm.run()

        # Назначаем групы для формирования строя
        # Группа героев
        group = Group()
        # Классы героев

        # Назначения для формирования строя
        tank = self.igles.group
        archers = [self.archer.group,
                   self.ork.group,
                   self.warrior.group]
        mages = [self.elf.group,
                 self.hildar.group,
                 self.diana.group]
        group.set_group(tank, archers, mages)

        # Выстраиваем группу
        tm.add_task(task.Group(group, 0, 3))
        tm.run()

        # Орк занимает позицию у леса
        tm.add_task(task.Walk([935, 160], self.ork.group))
        tm.run()

        # Воин занимает позицию впереди
        tm.add_task(task.Walk([938, 156], self.igles.group))
        tm.run()
        tm.add_task(task.Wait(3))
        tm.run()

    def mission_9_1(self):
        # Бой с некромантами. Дайна кастует защиту от магии огня.
        # Иглез идёт к главному некроманту и нападает на него.
        # Затем Иглез отступает на свою позицию. Если у него мало хп, принмает эликсиры.
        # Дайна обстреливает всех файрболом.
        # Когда врагов нет, орк идёт скать выживших, а Дайна их отстреливает.
        self.mm.enter_game()
        self.act.center_group(self.igles.group)

        tm = TaskManager()
        self.validate_group = [self.igles.group, self.hildar.group, self.diana.group,
                               self.warrior.group, self.ork.group, self.archer.group, self.elf.group]

        # Дайна защищает от магии огня
        tm.add_task(task.FireResist(self.validate_group, self.diana.group, wait_time=50, max_dst=7))
        tm.add_task(task.Wait(7))
        tm.run()

        tm.add_task(task.Walk([952, 160], self.igles.group))
        tm.run()
        self.igles.heal_regen()
        # Иглез нападает на некромансера
        tm.add_task(task.KillEnemy(min_dst=0, unit_group=self.igles.group, max_dst=3))
        tm.add_task(task.Wait(1))
        tm.run()

        # Иглез возвращается домой
        tm.clear_tasks()
        walk = task.Walk([938, 156], self.igles.group)
        walk.walk_main_ct_radius = 1
        tm.add_task(walk)
        tm.add_task(task.SelfHealthMon(self.igles.group, 0))
        tm.run()

        tm.win_final = 0
        self.diana.mana_regen()
        # Дайна повторно защищает от магии огня
        wait = tm.add_task(task.Wait(7))
        tm.add_task(task.FireResist([self.ork.group, self.igles.group], self.diana.group, wait_time=50, max_dst=7),
                    wait)

        clear_visible = tm.add_task(task.EnemyClear([self.diana.group], min_dst=16, validation_interval=2))
        tm.add_task(task.KillEnemyMagic(6, self.diana.group, cast_magic='fire_ball', max_dst=16))

        # Орк подходит
        ork_walk = tm.add_task(task.Walk([937, 160], self.ork.group), clear_visible)
        tm.run()
        self.act.center_group(self.igles.group)

        clear_visible2 = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16, validation_interval=2))

        wait_2 = tm.add_task(task.Wait(10), clear_visible2)
        tm.add_task(task.HealthMon(self.hildar.group, wait_time=200), clear_visible2)
        clear_visible3 = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16, validation_interval=2), wait_2)

        # Иглез отправляется на охоту
        top = tm.add_task(task.WalkS([944, 151], self.igles.group), clear_visible2)
        w1 = tm.add_task(task.Wait(3), top)
        btn = tm.add_task(task.WalkS([945, 162], self.igles.group), w1)
        btnl = tm.add_task(task.WalkS([952, 160], self.igles.group), btn)
        wait = tm.add_task(task.Wait(5), btnl)
        tm.run()
        tm.clear_tasks()

        tm.add_task(task.GetLoot(units=[self.igles.group, self.diana.group, self.hildar.group]))
        tm.run()

        if tm.win:
            self.mm.win_option()
        pass

    def mission_10(self):
        # 1. Назначаем команды
        # 2. Дайна защищает от магии воды и кастует град на дальние цели. На ближних разбирается огненной стрелой
        # 3. Наёмник бъёт молнией и лечит, когда мало хп.
        # 4. Гильдариус бъёт стрелой.
        # 5. Пчёл и ящеров убиваем файрболом с защитой от огня. Остальных градом.

        self.mm.enter_game()

        # Назначаем горячие клавиши на Осветление
        # self.act.center_group(self.diana.group)
        # self.mm.open_invent()
        # self.mouse.move(192, 755)
        # self.act.shift_key(self.act.light_key)
        # self.mm.close_invent()

        validate_group = [self.igles.group, self.hildar.group, self.diana.group, self.healer.group]
        tm = TaskManager()
        tm.set_validate_group(validate_group)

        # Центрирование группы
        tm.add_task(task.Center(self.igles.group))
        tm.run()

        # Назначение групп для 10 миссии
        heroes = [self.healer]
        tm.add_task(task.SetHeroesGroup(heroes))
        tm.run()
        # Валидация жизни наёмника
        tm.add_task(task.LifeHiredUnits(heroes))
        tm.run()

        # self.mm.show_magic_menu()

        # Сначала движемся наверх
        run = tm.add_task(task.WalkGroup([888, 114], self.hildar.group, walk_s=False))
        tm.run()

        tm.add_task(task.AllStop())
        tm.run()

        # Настраиваем магию по умолчанию, для наёмников
        tm.add_task(task.BindAutoMagic(self.healer.group, 'lighting'))
        # Гильдариус выбирает огненную стрелу
        tm.add_task(task.BindAutoMagic(self.hildar.group, 'fire'))

        # Магия по умолчанию
        tm.add_task(task.HealthMon(self.healer.group, wait_time=200))

        retreat_group = [self.hildar.group, self.diana.group, self.healer.group]
        tm.add_task(task.Retreat(retreat_group))

        run1 = tm.add_task(task.WalkGroup([894, 112], self.igles.group))
        clear_1 = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16), run1)
        tm.run()
        run1 = tm.add_task(task.WalkGroup([891, 110], self.igles.group))
        clear_1 = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16))
        tm.run()
        run2 = tm.add_task(task.WalkGroup([893, 85], self.igles.group))
        clear_2 = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16))
        run3 = tm.add_task(task.WalkGroup([896, 62], self.igles.group), clear_2)
        clear_3 = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16, validation_interval=2), run3)
        tm.run()

        if tm.defeat:
            return False
        return True

    def mission_10_1(self):
        # Схватка с пчёлами
        self.mm.enter_game()

        validate_group = [self.igles.group, self.hildar.group, self.diana.group, self.healer.group]
        tm = TaskManager()
        tm.set_validate_group(validate_group)

        # Центрирование группы
        tm.add_task(task.Center(self.igles.group))
        tm.run()

        # Валидация жизни наёмника
        tm.add_task(task.LifeHiredUnits([self.healer]))

        run3 = tm.add_task(task.WalkGroup([896, 62], self.igles.group))
        tm.add_task(task.Wait(2), run3)
        tm.run()

        # Остановка группы
        tm.add_task(task.AllStop())
        tm.run()

        tm.add_task(task.HealRegen(self.igles.group))
        tm.add_task(task.ManaRegen(self.diana.group))

        # Гильдариус выбирает лечение
        tm.add_task(task.BindAutoMagic(self.hildar.group, 'heal'))

        # Дайна - град
        tm.add_task(task.BindAutoMagic(self.diana.group, 'grad'))
        tm.run()

        run1 = tm.add_task(task.Walk([901, 64], self.igles.group))
        clear_3 = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16, validation_interval=2), run1)
        tm.run()

        run1 = tm.add_task(task.WalkGroup([918, 63], self.igles.group))
        run2 = tm.add_task(task.Walk([922, 62], self.igles.group), run1)
        tm.run()

        # Встречаем белок у моста
        clear = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16))
        tm.add_task(task.HealthMon(self.healer.group, wait_time=200, target_group=[self.igles.group]))
        tm.add_task(task.HealthMon(self.hildar.group, wait_time=200, target_group=[self.igles.group]))
        tm.run()

        run1 = tm.add_task(task.WalkGroup([940, 62], self.hildar.group))
        tm.run()

        # Встречаем волков
        tm.add_task(task.HealRegen(self.igles.group))
        tm.add_task(task.ManaRegen(self.diana.group))
        tm.run()

        tm.add_task(task.SelfHealthMon(self.igles.group, 1))
        run2 = tm.add_task(task.Walk([945, 63], self.igles.group))
        clear = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16, validation_interval=2))
        # Дальним Дайна раздаёт Градом по дальним (но не по своим)
        grad = tm.add_task(task.KillEnemyMagic(5, self.diana.group, cast_magic='grad', max_dst=16, self_validate=True))
        tm.run()

        # Убираем файрбол
        tm.remove_task(grad)

        # Подходим к людоедам
        run1 = tm.add_task(task.WalkGroup([965, 60], self.igles.group))
        tm.run()
        clear = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16))
        tm.run()

        run2 = tm.add_task(task.Walk([970, 60], self.igles.group))
        tm.run()
        # Группа героев
        group = Group()
        # Классы героев

        # Назначения для формирования строя
        tank = self.igles.group
        mages = [self.healer.group,
                 self.hildar.group,
                 self.diana.group]
        group.set_group(tank, [], mages)

        # Выстраиваем группу
        tm.add_task(task.Group(group, 0, 3))
        tm.run()

        if tm.defeat:
            return False
        return True

    def mission_10_2(self):
        # Людоеды 4 уровня.
        # 1. Дайна кастует защиту от магии воды и град.
        # 2. Гильдариус включает автолечение.
        # 3. Иглез приманивает людоедов и возвращается к отряду.
        self.mm.enter_game()

        validate_group = [self.igles.group, self.hildar.group, self.diana.group, self.healer.group]
        tm = TaskManager()
        tm.set_validate_group(validate_group)

        # Центрирование группы
        tm.add_task(task.Center(self.igles.group))
        tm.run()

        # Валидация жизни наёмника
        tm.add_task(task.LifeHiredUnits([self.healer]))

        # Наёмник выбирает лечение
        tm.add_task(task.BindAutoMagic(self.healer.group, 'heal'))

        resist = tm.add_task(task.WaterResist([self.igles.group], self.diana.group, wait_time=20, max_dst=5))
        tm.add_task(task.SelfHealthMon(self.igles.group, 0, num_if_red=3))
        tm.add_task(task.Wait(1))
        tm.run()

        # Иглез приманивает людоедов
        tm.add_task(task.ClearSector([977, 60], self.igles.group))
        # Проходим до отметки сектора, если врагов нет, включаем следующую позицию.
        tm.run()

        # Регенерация здоровья и маны
        tm.add_task(task.HealRegen(self.igles.group))
        tm.add_task(task.ManaRegen(self.diana.group))
        tm.run()

        # tm.add_task(task.HealthMon(self.healer.group, wait_time=200, target_group=[self.igles.group], health_level=2, max_dst=5))
        # tm.add_task(task.HealthMon(self.hildar.group, wait_time=200, target_group=[self.igles.group], health_level=2, max_dst=5))
        clear = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16))
        tm.run()

        # Отменяем резист
        tm.remove_task(resist)

        tm.add_task(task.GetLoot(self.igles.group))
        tm.run()

        # Иглез продвигается дальше
        run2 = tm.add_task(task.Walk([980, 59], self.igles.group))
        tm.run()

        # Выстраиваем группу
        group = Group()
        # Назначения для формирования строя
        tank = self.igles.group
        mages = [self.healer.group,
                 self.hildar.group,
                 self.diana.group]
        group.set_group(tank, [], mages)

        # Выстраиваем группу
        tm.add_task(task.Group(group, 0, 3))
        tm.run()

        resist = tm.add_task(task.WaterResist([self.igles.group], self.diana.group, wait_time=20, max_dst=5))
        tm.add_task(task.Wait(1))
        tm.run()

        # Иглез приманивает врагов слева
        tm.add_task(task.ClearSector([995, 71], self.igles.group))
        tm.run()

        tm.add_task(task.SpeedMinus())
        tm.run()

        # Регенерация здоровья и маны
        tm.add_task(task.HealRegen(self.igles.group))
        tm.add_task(task.ManaRegen(self.diana.group))
        tm.run()

        # tm.add_task(task.Wait(3))
        # tm.run()

        # tm.add_task(task.HealthMon(self.healer.group, wait_time=200, target_group=[self.igles.group], health_level=2))
        # tm.add_task(task.HealthMon(self.hildar.group, wait_time=200, target_group=[self.igles.group], health_level=2))
        clear = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16))
        tm.run()
        # tm.clear_tasks()

        tm.add_task(task.SpeedPlus())
        tm.run()

        clear = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16, validation_interval=2))
        tm.run()

        # Иглез идёт вниз
        # run = tm.add_task(task.ClearSector([987, 94], self.igles.group))
        # tm.run()

        # tm.add_task(task.Wait(3))
        # tm.run()

        # tm.add_task(task.HealthMon(self.healer.group, wait_time=200, target_group=[self.igles.group], health_level=2))
        # tm.add_task(task.HealthMon(self.hildar.group, wait_time=200, target_group=[self.igles.group], health_level=2))
        # clear = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16))
        # tm.run()

        tm.add_task(task.GetLoot(self.igles.group))
        tm.run()

        run1 = tm.add_task(task.WalkGroup([985, 90], self.igles.group))
        tm.run()

        # Иглез занимает позицию у рапторов
        run = tm.add_task(task.Walk([988, 94], self.igles.group))
        tm.run()

        # Выстраиваем группу
        tm.add_task(task.Group(group, 0, 0))
        tm.run()

        if tm.defeat:
            return False
        return True

    def mission_10_3(self):
        # Ящеры
        # 1. Дайна кастует защиту от огня и файрбол
        # 2. Гильдариус включает файрбол
        # 3. Иглез приманивает ящеров и возвращается к отряду.
        self.mm.enter_game()

        validate_group = [self.igles.group, self.hildar.group, self.diana.group, self.healer.group]
        tm = TaskManager()
        tm.set_validate_group(validate_group)

        # Центрирование группы
        tm.add_task(task.Center(self.igles.group))
        tm.run()

        # Валидация жизни наёмника
        tm.add_task(task.LifeHiredUnits([self.healer]))

        # Защита от огня
        tm.add_task(task.FireResist([self.igles.group], self.diana.group, wait_time=55, max_dst=5))
        tm.add_task(task.SelfHealthMon(self.igles.group, 0))
        tm.add_task(task.Wait(1))
        tm.run()

        # Гильдариус выбирает файрбол
        tm.add_task(task.BindAutoMagic(self.hildar.group, 'fire'))
        # Дайна - файрбол
        tm.add_task(task.BindAutoMagic(self.diana.group, 'fire_ball'))
        tm.run()

        # Иглез идёт к ящерам
        run = tm.add_task(task.Walk([992, 99], self.igles.group))
        tm.run()

        tm.add_task(task.Wait(1))
        tm.run()
        # И возвращается назад
        run = tm.add_task(task.Walk([988, 94], self.igles.group))
        tm.run()

        # Регенерация здоровья и маны
        tm.add_task(task.HealRegen(self.igles.group))
        tm.add_task(task.ManaRegen(self.diana.group))
        tm.add_task(task.ManaRegen(self.hildar.group))
        tm.run()

        tm.add_task(task.SpeedMinus())
        fire_ball = tm.add_task(
            task.KillEnemyMagic(5, self.hildar.group, cast_magic='fire_ball', max_dst=12, far_target=False))
        tm.run()

        clear = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16))
        tm.run()

        tm.add_task(task.SpeedPlus())
        tm.run()

        clear = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16, validation_interval=2))
        tm.run()

        if tm.defeat:
            return False
        return True

    def mission_10_4(self):
        # Дракон
        # 1. Иглез приманивает дракона.
        # Попробовать тактику со свечением.

        self.mm.enter_game()

        validate_group = [self.igles.group, self.hildar.group, self.diana.group, self.healer.group]
        tm = TaskManager()
        tm.set_validate_group(validate_group)

        # Центрирование группы
        tm.add_task(task.Center(self.igles.group))
        tm.run()

        # Валидация жизни наёмника
        tm.add_task(task.LifeHiredUnits([self.healer]))

        # Иглез идёт на позицию
        run = tm.add_task(task.Walk([989, 97], self.igles.group))
        tm.run()

        # Выстраиваем группу
        group = Group()
        # Назначения для формирования строя
        tank = self.igles.group
        mages = [self.healer.group,
                 self.hildar.group,
                 self.diana.group]
        group.set_group(tank, [], mages)

        # Выстраиваем группу
        tm.add_task(task.Group(group, 0, 0))
        tm.run()

        # Гильдариус лечит
        tm.add_task(task.BindAutoMagic(self.hildar.group, 'heal'))
        tm.run()

        tm.add_task(task.BindAutoMagic(self.diana.group, 'fire'))
        tm.run()

        # Резист на Иглеза и Дайну
        fire = tm.add_task(
            task.FireResist([self.igles.group, self.diana.group], self.diana.group, wait_time=55, max_dst=5))
        water = tm.add_task(
            task.WaterResist([self.igles.group, self.diana.group], self.diana.group, wait_time=30, max_dst=5))
        light = tm.add_task(
            task.LightResist([self.igles.group, self.diana.group], self.healer.group, wait_time=70, max_dst=5))
        earth = tm.add_task(
            task.EarthResist([self.igles.group, self.diana.group], self.healer.group, wait_time=50, max_dst=5))

        # Мониторинг здоровья
        tm.add_task(task.SelfHealthMon(self.igles.group, 0))
        tm.add_task(task.SelfHealthMon(self.diana.group, 0))
        tm.add_task(task.Wait(7))
        tm.run()

        # Дайна готовит град
        grad = tm.add_task(task.KillEnemyMagic(5, self.diana.group, cast_magic='grad', max_dst=20, target_type=2))
        run = tm.add_task(task.Walk([988, 97], self.diana.group))
        tm.run()

        # Регенерация здоровья и маны
        tm.add_task(task.HealRegen(self.igles.group))
        tm.add_task(task.ManaRegen(self.diana.group))
        tm.run()

        tm.add_task(task.Center(self.igles.group))
        tm.run()

        run = tm.add_task(task.Walk([988, 101], self.igles.group))
        tm.run()
        clear = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16, validation_interval=2))
        tm.run()

        # Дайна проверяет или погибает
        run = tm.add_task(task.Walk([994, 105], self.diana.group))
        tm.run()
        clear = tm.add_task(task.EnemyClear([self.diana.group], min_dst=16))
        tm.run()

        # Отчистка задач
        tm.remove_task(fire)
        tm.remove_task(light)
        tm.remove_task(water)
        tm.remove_task(earth)
        tm.remove_task(grad)

        tm.add_task(task.GetLoot(self.igles.group))
        tm.run()

        # Собираемся внизу
        run = tm.add_task(task.WalkGroup([988, 112], self.hildar.group))
        tm.run()

        tm.add_task(task.Wait(3))
        tm.run()

        # Обновляем резист
        fire = tm.add_task(
            task.FireResist([self.igles.group, self.diana.group], self.diana.group, wait_time=55, max_dst=5))
        water = tm.add_task(
            task.WaterResist([self.igles.group, self.diana.group], self.diana.group, wait_time=30, max_dst=5))
        light = tm.add_task(
            task.LightResist([self.igles.group, self.diana.group], self.healer.group, wait_time=70, max_dst=5))
        earth = tm.add_task(
            task.EarthResist([self.igles.group, self.diana.group], self.healer.group, wait_time=50, max_dst=5))
        tm.add_task(task.Wait(7))
        tm.run()

        # Регенерация здоровья и маны
        tm.add_task(task.HealRegen(self.igles.group))
        tm.add_task(task.ManaRegen(self.diana.group))
        tm.run()

        # Дайна готовит град
        grad = tm.add_task(task.KillEnemyMagic(5, self.diana.group, cast_magic='grad', max_dst=20, target_type=2))
        run = tm.add_task(task.Walk([989, 116], self.diana.group))
        tm.run()

        run = tm.add_task(task.Walk([980, 117], self.igles.group))
        tm.run()
        clear = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16, validation_interval=2))
        tm.run()

        # Дайна проверяет или погибает
        run = tm.add_task(task.Walk([976, 121], self.diana.group))
        tm.run()
        clear = tm.add_task(task.EnemyClear([self.diana.group], min_dst=16))
        tm.run()

        tm.add_task(task.GetLoot(self.igles.group))
        tm.run()
        run = tm.add_task(task.WalkGroup([960, 122], self.hildar.group))
        tm.run()

        if tm.defeat:
            return False
        return True

    def mission_10_5(self):
        # Король
        # 1. Иглез идёт к королю
        # 2. Ждёт победного диалога
        # 3. Вся группа собирает лут
        # 4. Выходит из миссии

        self.mm.enter_game()

        validate_group = [self.igles.group, self.hildar.group, self.diana.group, self.healer.group]
        tm = TaskManager()
        tm.set_validate_group(validate_group)
        tm.win_final = False

        # Центрирование группы
        tm.add_task(task.Center(self.igles.group))
        tm.run()

        run = tm.add_task(task.WalkGroup([950, 115], self.igles.group, walk_s=False))
        tm.run()

        # Мониторинг здоровья
        tm.add_task(task.SelfHealthMon(self.igles.group, 0))
        tm.add_task(task.HealRegen(self.igles.group))

        run = tm.add_task(task.Walk([944, 111], self.igles.group))
        tm.run()

        tm.add_task(task.Wait(10))
        tm.run()

        run = tm.add_task(task.Walk([938, 110], self.diana.group))
        run = tm.add_task(task.Walk([942, 104], self.hildar.group))
        tm.run()

        tm.add_task(task.GetLoot(units=[self.igles.group, self.diana.group, self.hildar.group]))
        tm.run()

        if tm.win:
            self.mm.win_option()

        if tm.defeat:
            return False
        return True

    def mission_11(self):
        # Если проваливаем миссию 5 раз - перезапуск с предыдущей записи
        # Понижаем скорость игры.
        # Ставим магам монитор лечения.
        # Иглез атакует на эльфов.
        # Гильдариус выбирает огненную стрелу.
        self.enter_game()

        validate_group = [self.igles.group, self.hildar.group, self.diana.group]
        tm = TaskManager()
        tm.set_validate_group(validate_group)

        # Мониторинг здоровья
        tm.add_task(task.SelfHealthMon(self.igles.group, 0))
        tm.add_task(task.SelfHealthMon(self.diana.group, 0))
        tm.add_task(task.SelfHealthMon(self.hildar.group, 0))
        tm.run()

        # tm.run_task(task.MagicMenu(show=False))
        tm.run_task(task.Center(self.diana.group))

        # Лечим всех у кого красное хп
        tm.add_task(task.HealthMon(self.hildar.group, wait_time=200, health_level=0))
        tm.add_task(task.HealthMon(self.diana.group, wait_time=200, health_level=0))
        tm.run()

        # enemy_name = 'ДРУИД'
        # kill = tm.add_task(task.KillEnemy(0, self.igles.group, max_dst=6, far_target=False, target_type=3,
        #                                  target_name=enemy_name, wait_time=1000, validate_target=True,
        #                                  enemy_detect_block=1))

        tm.add_task(task.Wait(10))
        kill = tm.add_task(task.KillEnemy(0, self.diana.group, max_dst=10, far_target=True, target_type=3))
        tm.add_task(task.BindAutoMagic(self.hildar.group, 'fire'))
        tm.run()
        tm.remove_task(kill)

        # Гильдариус выбирает огненную стрелу.
        tm.add_task(task.WalkGroup([997, 167], self.igles.group))
        tm.run()

        tm.add_task(task.WalkGroup([994, 163], self.igles.group))
        tm.run()

        tm.add_task(task.WalkGroup([994, 163], self.hildar.group))
        tm.add_task(task.Wait(5))
        tm.run()

        tm.add_task(task.AllStop())
        tm.run()

        tm.add_task(task.Center(self.igles.group))
        tm.run()

        # Назначение групп для 10 миссии
        tm.add_task(task.SetHeroesGroup([self.en1]))
        tm.run()
        tm.add_task(task.SetHeroesGroup([self.en2]))
        tm.run()
        tm.add_task(task.SetHeroesGroup([self.en3]))
        tm.run()
        # Назначение наёмника
        tm.run_task(task.SetHeroesGroup([self.elf]))

        # Валидация жизни наёмника
        tm.run_task(task.LifeHiredUnits([self.elf]))

        # Назначаем большой элексир здоровья на горячуюю клавишу
        color = [0, 10, 208, 104, 120]
        tm.add_task(task.BindHotKeyInvent(self.igles.group, self.act.heal_up_key, color))
        tm.run()

        # Эльф вибирает каменное проклятье
        tm.add_task(task.BindAutoMagic(self.elf.group, 'stone'))
        tm.run()

        if tm.defeat:
            return False
        return True

    def mission_11_1(self):
        self.enter_game()
        validate_group = [self.igles.group, self.hildar.group, self.diana.group, self.elf.group,
                          self.en1.group, self.en2.group, self.en3.group]
        tm = TaskManager()
        tm.find_in_black_sector(True)

        tm.set_validate_group(validate_group)

        tm.add_task(task.WalkGroup([995, 164], self.igles.group))
        tm.run()

        tm.run_task(task.Center(self.igles.group))

        # Выстраиваем группу
        group = Group()
        # Назначения для формирования строя
        tank = self.igles.group
        archers = [self.en1.group, self.en2.group, self.en3.group]
        mages = [self.hildar.group, self.diana.group, self.elf.group]

        group.archers_layer = 6
        group.mages_layer = 7

        group.set_group(tank, archers, mages)

        # Выстраиваем группу
        tm.run_task(task.Group(group, 1, 2))
        tm.run_task(task.ShiftWalk(self.igles.group, self.en2.group, [0, -2], 1))
        tm.run_task(task.Wait(1))

        tm.run_task(task.SetOtherHeroesGroup(self.other.group))
        tm.run_task(task.ShiftWalk(self.other.group, self.igles.group, [3, 1], 1))
        tm.run_task(task.Wait(1))
        tm.run_task(task.SetOtherHeroesGroup(self.other.group))

        mm = inter.MainMenu()
        mm.go_options()
        mm.set_no_damage_info()
        mm.quit_options()

        # Лечим всех у кого красное хп
        tm.run_task(task.HealthMon(self.hildar.group, wait_time=200, health_level=0))
        tm.run_task(task.HealthMon(self.diana.group, wait_time=200, health_level=0))
        # Валидация жизни наёмника
        tm.run_task(task.LifeHiredUnits([self.elf, self.en1, self.en2, self.en3]))
        # Мониторинг здоровья
        tm.add_task(task.SelfHealthMon(self.igles.group, 0))
        tm.add_task(task.SelfHealthMon(self.diana.group, 0))
        tm.add_task(task.SelfHealthMon(self.hildar.group, 0))
        tm.run()
        # Идём к некромантам
        tm.run_task(task.Walk([988, 162], self.igles.group))
        # Остальные герои становятся над строем
        tm.run_task(task.ShiftWalk(self.other.group, self.igles.group, [4, -4], 1, False))
        tm.run_task(task.Center(self.igles.group))

        # Выстраиваем группу
        group = Group()
        # Назначения для формирования строя
        tank = self.igles.group
        archers = [self.en1.group, self.en2.group, self.en3.group]
        mages = [self.hildar.group, self.diana.group, self.elf.group]
        group.archers_layer = 7
        group.mages_layer = 8
        group.set_group(tank, archers, mages)
        # Выстраиваем группу
        tm.run_task(task.Group(group, 0, 1))

        # Дайна и Гильдариус вибирают град
        tm.run_task(task.BindAutoMagic(self.diana.group, 'grad'))
        tm.run_task(task.BindAutoMagic(self.hildar.group, 'grad'))

        # Наёмники лечат
        tm.run_task(task.BindAutoMagic(self.en1.group, 'chain'))
        tm.run_task(task.BindAutoMagic(self.en2.group, 'heal'))
        tm.run_task(task.BindAutoMagic(self.en3.group, 'chain'))

        # Регенерация здоровья и маны
        tm.run_task(task.HealRegen(self.igles.group))
        tm.run_task(task.ManaRegen(self.diana.group))
        tm.run_task(task.ManaRegen(self.hildar.group))

        # Защита от магии воды и воздуха
        # resist = tm.add_task(task.WaterResist([self.igles.group], self.diana.group, wait_time=40, max_dst=6))
        tm.run_task(task.LightResist(validate_group, self.elf.group, wait_time=20, max_dst=5))
        tm.run_task(task.Wait(5))

        # Иглез приманивает некромантов
        tm.run_task(task.ClearSector([980, 157], self.igles.group, type_to_kill=[3, 4]))
        tm.run_task(task.Center(self.en2.group))
        tm.run_task(task.ShiftWalk(self.igles.group, self.en2.group, [-6, 0], 1))
        tm.run_task(task.Wait(3))

        grad = tm.add_task(task.KillEnemyMagic(5, self.diana.group, cast_magic='grad', max_dst=20))
        clear = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16, validation_interval=2))
        tm.run()
        tm.remove_task(grad)

        tm.add_task(task.WalkGroup([980, 157], self.hildar.group))
        tm.run()

        if tm.defeat:
            return False
        return True

    def mission_11_2(self):
        self.enter_game()

        validate_group = [self.igles.group, self.hildar.group, self.diana.group, self.elf.group,
                          self.en1.group, self.en2.group, self.en3.group]
        tm = TaskManager()
        tm.find_in_black_sector(True)

        tm.set_validate_group(validate_group)

        # Лечим всех у кого красное хп
        tm.add_task(task.HealthMon(self.hildar.group, wait_time=200, health_level=0))
        tm.add_task(task.HealthMon(self.diana.group, wait_time=200, health_level=0))

        # Валидация жизни наёмника
        tm.add_task(task.LifeHiredUnits([self.elf, self.en1, self.en2, self.en3]))
        tm.run()

        # Мониторинг здоровья
        tm.add_task(task.SelfHealthMon(self.igles.group, 0))
        tm.add_task(task.SelfHealthMon(self.diana.group, 0))
        tm.add_task(task.SelfHealthMon(self.hildar.group, 0))
        tm.run()

        # Идём к некромантам
        tm.run_task(task.WalkGroup([975, 158], self.hildar.group, walk_s=False))
        tm.run_task(task.Walk([970, 158], self.igles.group))

        # Остальные герои становятся над строем
        tm.run_task(task.ShiftWalk(self.other.group, self.igles.group, [3, -4], 0, False))

        # Выстраиваем группу
        group = Group()
        # Назначения для формирования строя
        tank = self.igles.group
        archers = [self.en1.group, self.en2.group, self.en3.group]
        mages = [self.hildar.group, self.diana.group, self.elf.group]

        group.archers_layer = 6
        group.mages_layer = 7

        group.set_group(tank, archers, mages)

        # Выстраиваем группу
        tm.add_task(task.Group(group, 0, 1))
        tm.run()

        # Регенерация здоровья и маны
        tm.add_task(task.HealRegen(self.igles.group))
        tm.add_task(task.ManaRegen(self.diana.group))
        tm.add_task(task.ManaRegen(self.hildar.group))
        tm.run()

        # Защита от магии воды и воздуха
        # resist = tm.add_task(task.WaterResist([self.igles.group], self.diana.group, wait_time=40, max_dst=6))
        resist2 = tm.add_task(task.LightResist(validate_group, self.elf.group, wait_time=20, max_dst=5))
        tm.add_task(task.Wait(5))
        tm.run()

        # Иглез приманивает некромантов
        tm.add_task(task.ClearSector([954, 166], self.igles.group, type_to_kill=[3, 4]))
        tm.run()

        tm.add_task(task.ShiftWalk(self.igles.group, self.en2.group, [-6, 0], 1))
        tm.run()

        tm.add_task(task.Wait(3))
        tm.run()

        grad = tm.add_task(task.KillEnemyMagic(5, self.diana.group, cast_magic='grad', max_dst=20))
        clear = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16, validation_interval=2))
        tm.run()

        # Иглез идёт по координатам и вскрывает всё, что находит по пути
        clear = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16, validation_interval=2))
        kill = tm.add_task(task.KillEnemy(0, self.igles.group, max_dst=6, far_target=False))
        tm.add_task(task.WalkS([963, 158], self.igles.group), block_task_id=clear)
        tm.run()
        tm.remove_task(kill)
        tm.remove_task(grad)
        tm.run()

        # Дайна и Гильдариус вибирают огонь
        tm.add_task(task.BindAutoMagic(self.diana.group, 'fire'))
        tm.add_task(task.BindAutoMagic(self.hildar.group, 'fire'))
        tm.run()

        clear = tm.add_task(task.EnemyClear([self.hildar.group], min_dst=16, validation_interval=2))
        tm.add_task(task.WalkGroup([963, 158], self.hildar.group), block_task_id=clear)
        tm.run()
        # Остальные герои становятся над строем
        tm.run_task(task.ShiftWalk(self.other.group, self.igles.group, [-3, -4], 0, False))

        if tm.defeat:
            return False
        return True

    def mission_11_3(self):
        # Идём к мосту
        self.enter_game()

        validate_group = [self.igles.group, self.hildar.group, self.diana.group, self.elf.group,
                          self.en1.group, self.en2.group, self.en3.group]
        tm = TaskManager()
        tm.set_validate_group(validate_group)

        # Лечим всех у кого красное хп
        tm.add_task(task.HealthMon(self.hildar.group, wait_time=200, health_level=0))
        tm.add_task(task.HealthMon(self.diana.group, wait_time=200, health_level=0))

        # Валидация жизни наёмников
        tm.add_task(task.LifeHiredUnits([self.elf, self.en1, self.en2, self.en3]))
        tm.run()

        # Мониторинг здоровья
        tm.add_task(task.SelfHealthMon(self.igles.group, 0))
        tm.add_task(task.SelfHealthMon(self.diana.group, 0))
        tm.add_task(task.SelfHealthMon(self.hildar.group, 0))
        tm.run()

        # Идём на север, к мосту. По пути уничтожаем летучих мышей.
        tm.add_task(task.WalkGroup([946, 137], self.hildar.group))
        tm.run()
        tm.add_task(task.WalkGroup([929, 118], self.hildar.group))
        tm.run()
        # Остальные герои становятся над строем
        tm.run_task(task.ShiftWalk(self.other.group, self.igles.group, [-3, -4], 0, False))

        tm.add_task(task.Wait(5))
        tm.run()

        if tm.defeat:
            return False
        return True

    def mission_11_4(self):
        self.enter_game()

        validate_group = [self.igles.group, self.hildar.group, self.diana.group, self.elf.group,
                          self.en1.group, self.en2.group, self.en3.group]
        tm = TaskManager()
        tm.find_in_black_sector(True)

        tm.set_validate_group(validate_group)

        # Лечим всех у кого красное хп
        tm.add_task(task.HealthMon(self.hildar.group, wait_time=200, health_level=0))
        tm.add_task(task.HealthMon(self.diana.group, wait_time=200, health_level=0))

        # Валидация жизни наёмников
        tm.add_task(task.LifeHiredUnits([self.elf, self.en1, self.en2, self.en3]))
        tm.run()

        # Мониторинг здоровья
        tm.add_task(task.SelfHealthMon(self.igles.group, 0))
        tm.add_task(task.SelfHealthMon(self.diana.group, 0))
        tm.add_task(task.SelfHealthMon(self.hildar.group, 0))
        tm.run()

        # Иглез занимает позицию
        tm.run_task(task.Walk([925, 116], self.igles.group))

        # Остальные герои становятся перед строем
        tm.run_task(task.ShiftWalk(self.other.group, self.igles.group, [-5, 2], 0, False))

        tm.add_task(task.Center(self.igles.group))

        # Выстраиваем группу
        group = Group()
        # Назначения для формирования строя
        tank = self.igles.group
        archers = [self.en1.group, self.en2.group, self.en3.group]
        mages = [self.hildar.group, self.diana.group, self.elf.group]
        group.archers_layer = 7
        group.mages_layer = 8
        group.set_group(tank, archers, mages)

        tm.add_task(task.Group(group, 0, 2))
        tm.run()

        # Регенерация здоровья и маны
        tm.add_task(task.HealRegen(self.igles.group))
        tm.add_task(task.ManaRegen(self.diana.group))
        tm.add_task(task.ManaRegen(self.hildar.group))
        tm.run()

        # Защита от магии воздуха
        resist2 = tm.add_task(task.LightResist(validate_group, self.elf.group, wait_time=20, max_dst=5))
        tm.add_task(task.Wait(5))
        tm.run()

        # Дайна и Гильдариус вибирают град
        tm.add_task(task.BindAutoMagic(self.diana.group, 'grad'))
        tm.add_task(task.BindAutoMagic(self.hildar.group, 'grad'))

        # Наёмник 1 лечит
        tm.add_task(task.BindAutoMagic(self.en1.group, 'heal'))

        # Иглез приманивает некромантов
        tm.add_task(task.ClearSector([921, 111], self.igles.group, type_to_kill=[3, 4]))
        tm.run()

        tm.add_task(task.ShiftWalk(self.igles.group, self.en2.group, [0, -7], 1))
        tm.run()

        tm.add_task(task.Wait(3))
        tm.run()

        grad = tm.add_task(task.KillEnemyMagic(5, self.diana.group, cast_magic='grad', max_dst=20))
        clear = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16, validation_interval=2))
        tm.run()
        tm.remove_task(grad)

        # Регенерация здоровья и маны
        tm.add_task(task.HealRegen(self.igles.group))
        tm.add_task(task.ManaRegen(self.diana.group))
        tm.add_task(task.ManaRegen(self.hildar.group))
        tm.run()

        # Дайна и Гильдариус вибирают огонь
        tm.add_task(task.BindAutoMagic(self.diana.group, 'fire'))
        tm.add_task(task.BindAutoMagic(self.hildar.group, 'fire'))
        tm.run()

        tm.run_task(task.WalkGroup([921, 101], self.igles.group))
        tm.run_task(task.WalkGroup([923, 101], self.hildar.group))
        # Остальные герои становятся над строем
        tm.run_task(task.ShiftWalk(self.other.group, self.igles.group, [-3, -4], 0, False))

        tm.add_task(task.Wait(5))
        tm.run()

        if tm.defeat:
            return False
        return True

    def mission_11_5(self):
        self.enter_game()
        # Поход к мышкам

        validate_group = [self.igles.group, self.hildar.group, self.diana.group, self.elf.group,
                          self.en1.group, self.en2.group, self.en3.group]
        tm = TaskManager()
        tm.set_validate_group(validate_group)

        # Валидация жизни наёмников
        tm.add_task(task.LifeHiredUnits([self.elf, self.en1, self.en2, self.en3]))
        tm.run()

        # Мониторинг здоровья
        tm.add_task(task.SelfHealthMon(self.igles.group, 0, num=2, num_if_red=4))
        tm.add_task(task.SelfHealthMon(self.diana.group, 0))
        tm.add_task(task.SelfHealthMon(self.hildar.group, 0))
        tm.run()

        tm.add_task(task.BindAutoMagic(self.en1.group, 'chain'))
        tm.add_task(task.BindAutoMagic(self.en2.group, 'chain'))
        tm.add_task(task.BindAutoMagic(self.en3.group, 'chain'))
        tm.add_task(task.BindAutoMagic(self.elf.group, 'stone'))
        tm.run()

        # Идём на север, к мышкам
        tm.run_task(task.WalkGroup([928, 84], self.hildar.group))
        tm.run_task(task.Center(self.igles.group))

        tm.add_task(task.BindAutoMagic(self.diana.group, 'heal'))
        tm.add_task(task.BindAutoMagic(self.hildar.group, 'heal'))
        tm.add_task(task.BindAutoMagic(self.en1.group, 'heal'))
        tm.add_task(task.BindAutoMagic(self.en2.group, 'heal'))
        tm.add_task(task.BindAutoMagic(self.en3.group, 'heal'))
        tm.add_task(task.BindAutoMagic(self.elf.group, 'heal'))
        tm.run()

        # Идём на восток, к троллям
        tm.run_task(task.WalkGroup([946, 79], self.hildar.group, walk_s=False))
        tm.run_task(task.Center(self.igles.group))
        # Остальные герои становятся над строем
        tm.run_task(task.ShiftWalk(self.other.group, self.igles.group, [0, -4], 0, False))

        # Защита от магии земли
        resist2 = tm.add_task(task.EarthResist([self.igles.group], self.elf.group, wait_time=20, max_dst=9))
        tm.add_task(task.Wait(1))
        tm.run()

        # Регенерация здоровья и маны
        tm.add_task(task.HealRegen(self.igles.group))
        tm.add_task(task.ManaRegen(self.diana.group))
        tm.add_task(task.ManaRegen(self.hildar.group))
        tm.run()

        # Дайна и Гильдариус вибирают град
        tm.add_task(task.BindAutoMagic(self.diana.group, 'grad'))
        tm.add_task(task.BindAutoMagic(self.hildar.group, 'grad'))

        # Лечим всех у кого красное хп
        tm.add_task(task.HealthMon(self.en1.group, wait_time=200))
        tm.add_task(task.HealthMon(self.en2.group, wait_time=200))
        tm.add_task(task.HealthMon(self.en3.group, wait_time=200))

        # Иглез приманивает троллей
        tm.run_task(task.ClearSector([950, 73], self.igles.group, first_abs=[949, 75]))
        tm.run_task(task.ShiftWalk(self.igles.group, self.diana.group, [5, -5], 1))

        grad = tm.add_task(task.KillEnemyMagic(5, self.diana.group, cast_magic='grad', max_dst=20))
        grad2 = tm.add_task(task.KillEnemyMagic(5, self.hildar.group, cast_magic='grad', max_dst=20))
        clear = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16))
        tm.run()

        tm.run_task(task.Center(self.igles.group))
        tm.run_task(task.Wait(5))

        tm.remove_task(grad)
        tm.remove_task(grad2)
        tm.run()

        tm.run_task(task.WalkGroup([950, 75], self.hildar.group))

        # Остальные герои становятся под строем
        tm.run_task(task.ShiftWalk(self.other.group, self.igles.group, [4, 4], 0, False))

        if tm.defeat:
            return False
        return True

    def mission_11_6(self):
        # Сражение у моста
        self.enter_game()

        validate_group = [self.igles.group, self.hildar.group, self.diana.group, self.elf.group,
                          self.en1.group, self.en2.group, self.en3.group]
        tm = TaskManager()
        tm.find_in_black_sector(True)

        tm.set_validate_group(validate_group)

        # Лечим всех у кого красное хп
        tm.add_task(task.HealthMon(self.hildar.group, wait_time=200, health_level=0))
        tm.add_task(task.HealthMon(self.diana.group, wait_time=200, health_level=0))

        # Валидация жизни наёмников
        tm.add_task(task.LifeHiredUnits([self.elf, self.en1, self.en2, self.en3]))
        tm.run()

        # Мониторинг здоровья
        tm.add_task(task.SelfHealthMon(self.igles.group, 0, num=2, num_if_red=4))
        tm.add_task(task.SelfHealthMon(self.diana.group, 0))
        tm.add_task(task.SelfHealthMon(self.hildar.group, 0))
        tm.run()

        # Команда занимает позицию
        tm.run_task(task.WalkGroup([952, 76], self.igles.group, walk_s=False))
        # Остальные герои становятся под строем
        tm.run_task(task.ShiftWalk(self.other.group, self.igles.group, [0, 4], 0, False))

        # Регенерация здоровья и маны
        tm.add_task(task.HealRegen(self.igles.group))
        tm.add_task(task.ManaRegen(self.diana.group))
        tm.add_task(task.ManaRegen(self.hildar.group))
        tm.run()

        tm.add_task(task.BindAutoMagic(self.en1.group, 'heal'))
        tm.add_task(task.BindAutoMagic(self.en2.group, 'chain'))
        tm.add_task(task.BindAutoMagic(self.en3.group, 'heal'))
        tm.add_task(task.BindAutoMagic(self.elf.group, 'stone'))
        tm.run()

        # Остальные герои становятся под строем
        tm.run_task(task.ShiftWalk(self.other.group, self.igles.group, [1, 4], 0, False))

        # Иглез приманивает некромантов
        tm.run_task(task.Walk([957, 76], self.igles.group))

        # Выстраиваем группу
        group = Group()
        # Назначения для формирования строя
        tank = self.igles.group
        archers = [self.en1.group, self.en2.group, self.en3.group]
        mages = [self.hildar.group, self.diana.group, self.elf.group]
        group.archers_layer = 7
        group.mages_layer = 8
        group.set_group(tank, archers, mages)

        tm.add_task(task.Center(self.igles.group))
        tm.run_task(task.Group(group, 0, 3))

        # Остальные герои становятся под строем
        tm.run_task(task.ShiftWalk(self.other.group, self.en2.group, [1, 4], 0, False))

        water_res = tm.add_task(task.WaterResist([self.igles.group], self.diana.group, wait_time=50, max_dst=8))
        tm.add_task(task.Wait(1))
        tm.run()

        # Защита от магии воздуха
        light_res = tm.add_task(task.LightResist(validate_group, self.elf.group, wait_time=20, max_dst=5))

        # Убиваем всех ближних и дальних врагов
        grad = tm.add_task(task.KillEnemyMagic(5, self.diana.group, cast_magic='grad', max_dst=14))
        grad2 = tm.add_task(task.KillEnemyMagic(5, self.hildar.group, cast_magic='grad', max_dst=10))
        clear = tm.add_task(task.EnemyClear([self.igles.group], min_dst=3, validation_interval=2))
        tm.run()
        tm.remove_task(grad)
        tm.remove_task(grad2)

        grad = tm.add_task(task.KillEnemyMagic(5, self.diana.group, cast_magic='grad', max_dst=20))
        clear = tm.add_task(task.EnemyClear([self.igles.group], min_dst=20, validation_interval=2))
        tm.run()

        if tm.defeat:
            return False
        return True

    def mission_11_7(self):
        self.enter_game()
        # Уничтожение катапульт

        validate_group = [self.igles.group, self.hildar.group, self.diana.group, self.elf.group,
                          self.en1.group, self.en2.group, self.en3.group]
        tm = TaskManager()
        tm.find_in_black_sector(True)

        tm.run_task(task.Center(self.igles.group))

        tm.set_validate_group(validate_group)

        # Лечим всех у кого красное хп
        tm.add_task(task.HealthMon(self.hildar.group, wait_time=200, health_level=0))
        tm.add_task(task.HealthMon(self.diana.group, wait_time=200, health_level=0))

        # Валидация жизни наёмников
        tm.add_task(task.LifeHiredUnits([self.elf, self.en1, self.en2, self.en3]))
        tm.run()

        # Мониторинг здоровья
        tm.add_task(task.SelfHealthMon(self.igles.group, 0, num=3, num_if_red=5))
        tm.add_task(task.SelfHealthMon(self.diana.group, 0))
        tm.add_task(task.SelfHealthMon(self.hildar.group, 0))
        tm.run()

        # Дайна возвращается на место
        tm.run_task(task.ShiftWalk(self.diana.group, self.en2.group, [0, -2]))

        # Регенерация здоровья и маны
        tm.run_task(task.HealRegen(self.igles.group))

        tm.add_task(task.ClearSector([965, 76], self.igles.group, first_abs=[956, 76], type_to_kill=[3, 4]))
        tm.run()
        tm.add_task(task.ShiftWalk(self.igles.group, self.en2.group, [7, 0], 1))
        tm.run()

        grad = tm.add_task(task.KillEnemyMagic(5, self.diana.group, cast_magic='grad', max_dst=20))
        clear = tm.add_task(task.EnemyClear([self.igles.group], min_dst=10, validation_interval=2))
        tm.run()

        tm.remove_task(grad)

        # Дайна возвращается на место
        tm.run_task(task.Center(self.en3.group))
        tm.run_task(task.ShiftWalk(self.diana.group, self.en2.group, [0, -2]))

        # Иглез идёт по координатам и вскрывает всё, что находит по пути
        clear = tm.add_task(task.EnemyClear([self.igles.group], min_dst=16))
        kill = tm.add_task(task.KillEnemy(0, self.igles.group, max_dst=6, far_target=False))
        walk2 = tm.add_task(task.WalkS([973, 75], self.igles.group), block_task_id=clear)
        walk3 = tm.add_task(task.WalkS([974, 79], self.igles.group), walk2, block_task_id=clear)
        walk4 = tm.add_task(task.Walk([973, 77], self.igles.group), walk3)
        tm.run()

        if tm.defeat:
            return False
        return True

    def mission_11_8(self):
        self.enter_game()

        validate_group = [self.igles.group, self.hildar.group, self.diana.group, self.elf.group,
                          self.en1.group, self.en2.group, self.en3.group]
        tm = TaskManager(bs=True, validate=validate_group)

        tm.run_task(task.Center(self.igles.group))

        # Мониторинг здоровья
        tm.run_task(task.SelfHealthMon(self.igles.group, 0, num=3, num_if_red=5))

        # Регенерация здоровья и маны
        tm.run_task(task.HealRegen(self.igles.group))

        tm.run_task(task.SpeedMinus())

        # Иглез приманивает некромантов
        tm.run_task(task.ClearSector([984, 76], self.igles.group, first_abs=[956, 76], type_to_kill=[4]))

        tm.run_task(task.ShiftWalk(self.igles.group, self.en2.group, [7, 0], 1))

        tm.add_task(task.SelfHealthMon(self.diana.group, 0))
        tm.add_task(task.SelfHealthMon(self.hildar.group, 0))
        tm.add_task(task.BindAutoMagic(self.en1.group, 'heal'))
        tm.add_task(task.BindAutoMagic(self.en2.group, 'chain'))
        tm.add_task(task.BindAutoMagic(self.en3.group, 'heal'))
        tm.run()

        tm.run_task(task.SpeedPlus())

        # Лечим всех у кого красное хп
        tm.add_task(task.HealthMon(self.hildar.group, wait_time=200, health_level=0))
        tm.add_task(task.HealthMon(self.diana.group, wait_time=200, health_level=0))
        tm.run()

        # Валидация жизни наёмников
        tm.run_task(task.LifeHiredUnits([self.elf, self.en1, self.en2, self.en3]))

        # Регенерация здоровья и маны
        tm.add_task(task.HealRegen(self.igles.group))
        tm.add_task(task.ManaRegen(self.diana.group))
        tm.add_task(task.ManaRegen(self.hildar.group))
        tm.run()

        # Защита от магии воздуха
        resist = tm.add_task(task.LightResist(validate_group, self.elf.group, wait_time=20, max_dst=5))
        grad = tm.add_task(task.KillEnemyMagic(5, self.diana.group, cast_magic='grad', max_dst=12, wait_time=500))
        grad2 = tm.add_task(task.KillEnemyMagic(5, self.hildar.group, cast_magic='grad', max_dst=10, wait_time=500))
        clear = tm.add_task(task.EnemyClear([self.igles.group], min_dst=3))
        tm.run()

        tm.run_task(task.Center(self.igles.group))
        tm.remove_task(resist)
        tm.remove_task(grad)
        tm.remove_task(grad2)

        tm.run_task(task.ShiftWalk(self.igles.group, self.en2.group, [5, 0], 1))

        # Выстраиваем группу
        group = Group()
        # Назначения для формирования строя
        tank = self.igles.group
        archers = [self.en1.group, self.en2.group, self.en3.group]
        mages = [self.hildar.group, self.diana.group, self.elf.group]
        group.archers_layer = 8
        group.mages_layer = 9
        group.set_group(tank, archers, mages)
        tm.run_task(task.Group(group, 0, 3))

        tm.run_task(task.ShiftWalk(self.other.group, self.en2.group, [1, 4]))

        tm.run_task(task.EnemyClear([self.igles.group], min_dst=3, validation_interval=2))

        # Регенерация здоровья и маны
        tm.add_task(task.HealRegen(self.igles.group))
        tm.add_task(task.ManaRegen(self.diana.group))
        tm.add_task(task.ManaRegen(self.hildar.group))
        tm.run()

        tm.run_task(task.SpeedMinus())

        # Иглез убивает некромантов и возвращается к команде
        # Эксперементальная функция поиска в темноте. Иногда находит врагов там, где их нет,
        # собирая их из костей и мешков.

        enemy_name = 'СКЕЛЕТ-МАГ'
        kill = tm.add_task(task.KillEnemy(0, self.igles.group, max_dst=6, far_target=False,
                                          target_name=enemy_name, wait_time=1000, validate_target=True,
                                          enemy_detect_block=3))
        walk1 = tm.add_task(task.Walk([959, 74], self.igles.group), block_task_id=kill)
        walk2 = tm.add_task(task.Walk([963, 75], self.igles.group), walk1, block_task_id=kill)
        walk3 = tm.add_task(task.Walk([959, 76], self.igles.group), walk2, block_task_id=kill)
        tm.run()

        tm.run_task(task.Walk([956, 76], self.igles.group))
        tm.run_task(task.Center(self.igles.group))

        # Иглез возвращается к команде
        tm.run_task(task.Center(self.en2.group))
        tm.run_task(task.ShiftWalk(self.igles.group, self.en2.group, [7, 0], 1))
        tm.run_task(task.Center(self.igles.group))
        tm.run_task(task.SpeedPlus())
        tm.run_task(task.Wait(3))

        # Вся команда атакует Урда
        fire1 = tm.add_task(task.KillEnemyMagic(5, self.diana.group, cast_magic='fire', max_dst=20, wait_time=10000,
                                                magic_attack=True))
        fire2 = tm.add_task(task.KillEnemyMagic(5, self.hildar.group, cast_magic='fire', max_dst=20, wait_time=10000,
                                                magic_attack=True))
        grad = tm.add_task(task.KillEnemyMagic(5, self.elf.group, cast_magic='grad', max_dst=20, wait_time=7000))
        chain1 = tm.add_task(task.KillEnemyMagic(5, self.en1.group, cast_magic='chain', max_dst=20, wait_time=1000))
        chain2 = tm.add_task(task.KillEnemyMagic(5, self.en2.group, cast_magic='chain', max_dst=20, wait_time=1000))
        chain3 = tm.add_task(task.KillEnemyMagic(5, self.en3.group, cast_magic='chain', max_dst=20, wait_time=1000))
        tm.add_task(task.EnemyClear([self.igles.group], min_dst=20, validation_interval=2, max_wait=300))
        tm.run()
        tm.run_task(task.Wait(10))
        tm.run_task(task.WalkGroup([963, 76], self.igles.group))

        if tm.defeat:
            return False
        return True
