# Чтение данных с экрана монитора
import app.config as cfg
import numpy as np
from app.commands import Color
from app.mission.store import Mss
import time


class MiniMapScreen:
    # Экран карты
    map_top_left = [0, 0]
    map_btn_right = [0, 0]

    def get_screen(self):
        return [self.map_top_left, self.map_btn_right]

    def get_screen_hw(self):
        # Размер экрана, x, y
        return [self.map_btn_right[0] - self.map_top_left[0], self.map_btn_right[1] - self.map_top_left[1]]

    def reset(self):
        self.map_top_left = [0, 0]
        self.map_btn_right = [0, 0]
        pass

    def add(self, x, y):
        # Назначение крайних точек экрана мини-карты
        if self.map_top_left[0] == 0:
            self.map_top_left = [x, y]
            pass
        if self.map_btn_right[0] < x:
            self.map_btn_right[0] = x
            pass
        if self.map_btn_right[1] < y:
            self.map_btn_right[1] = y
            pass

        pass

    pass


class MiniMap:
    # Анализ карты
    # Чтение мини карты и нахождение на ней значимых объектов, героев, врагов (нейтралов), положения экрана
    mms = MiniMapScreen()
    # Размер героя на карте
    map_pixel_size = 1

    # Положение экрана. Верхняя левая и нижняя правая точки
    screen = [[0, 0], [0, 0]]
    heroes = []
    enemy = []

    # Объекты поиск, r,g,b
    objects = [
        [248, 252, 248, 'map'],  # map
        [128, 228, 80, 'hero'],  # hero
        [80, 120, 224, 'hero'],
        [224, 84, 120, 'enemy'],  # enemies
        [224, 112, 80, 'enemy'],  # civilian and enemy
    ]

    found = {}

    def find(self):
        # Поиск экрана на мини-карте
        # Поиск объектов на карте. mon_map = {'x': 880, 'y': 49, 'w': 128}
        mon_map = {'top': cfg.mon_map['y'], 'left': cfg.mon_map['x'], 'width': cfg.mon_map['w'],
                   'height': cfg.mon_map['w']}
        img = Mss.sct.grab(mon_map)
        matrix = np.array(img)

        # Сброс настроек
        self.mms.reset()

        # Карта. Цвета карты, bgr
        map_c = (248, 252, 248, 255)
        indices = np.where(np.all(matrix == map_c, axis=-1))
        # Координаты: y, x
        map_crd = np.transpose(indices)
        for crd in map_crd:
            self.mms.add(crd[1], crd[0])

        # Сохраняем данные о положении экрана
        self.screen = self.mms.get_screen()

    def k_size(self):
        # Коэффициент масштаба карты
        k_size_one = round((cfg.main_window['x'] / self.screen_hw()[0]) / 16, 0)
        k_size = int(k_size_one * 16)
        return k_size

    def screen_hw(self):
        # Размер экрана
        return self.mms.get_screen_hw()

    # TODO unused
    def clear_double(self, old_heroes):
        # Удаляем дубли героев, размером больше одной клетки
        new_heroes = []
        for old_hero in old_heroes:
            if not new_heroes:
                # Если объект пуст - создаём
                new_heroes.append(old_hero)
            else:
                for new_hero in new_heroes:
                    # Иначе проверяем наличие героев
                    if new_hero[0] == old_hero[0] or new_hero[0] == (old_hero[0] - 1) \
                            or new_hero[1] == old_hero[1] or new_hero[1] == (old_hero[1] - 1):
                        break
                    else:
                        new_heroes.append(old_hero)
        return new_heroes

    # TODO unused
    def clear_triple(self, heroes):
        # Удаляем дубли героев, размером больше одной клетки

        new_heroes = []
        exlude = []
        i = 0
        len = heroes.__len__()
        while i < len:
            hero = heroes[i]
            j = i + 1
            k = 1
            x_hero = hero[0]
            if j >= len:
                break
            x_next = heroes[j][0] - k
            to_exclude = [hero]
            while x_hero == x_next:
                to_exclude.append(heroes[j])
                j += 1
                if j >= len:
                    break
                k += 1
                x_next = heroes[j][0] - k

            if k > self.map_pixel_size:
                exlude.append(to_exclude)
            else:
                new_heroes.append(hero)
            i += k

        # print('inc', new_heroes)
        # print('ex', exlude)
        return new_heroes

    # TODO unused
    def find_old(self):
        # Поиск объектов на карте. mon_map = {'x': 880, 'y': 49, 'w': 128}
        mon_map = {'top': cfg.mon_map['y'], 'left': cfg.mon_map['x'], 'width': cfg.mon_map['w'],
                   'height': cfg.mon_map['w']}
        img = Mss.sct.grab(mon_map)
        matrix = np.array(img)

        x = 0
        y = 0
        self.mms.reset()
        self.heroes = []
        self.enemy = []
        self.found = {'hero': [], 'enemy': []}

        for row in matrix:
            for item in row:
                r = item[0]
                g = item[1]
                b = item[2]

                # Отсеиваем все тёмные пиксели
                if r < 70 or g < 70 or b < 70:
                    x += 1
                    continue

                # Поиск координат карты по светлым пикселям
                for pixel in self.objects:
                    if pixel[2] == r:
                        if pixel[1] == g:
                            if pixel[0] == b:
                                type = pixel[3]
                                if type == 'map':
                                    self.mms.add(x, y)
                                else:
                                    self.found[type].append([x, y])
                                break
                x += 1
            y += 1
            x = 0

        self.find_validate()

    # TODO unused
    def find_validate(self):
        # Сохраняем данные о положении экрана
        self.screen = self.mms.get_screen()

        if self.found.get('hero'):
            self.heroes = self.found['hero']
        else:
            # Если герои не найдены, добавляем метку героя по центру карты
            print('Герой не найден, добавляем метку героя по центру карты')
            hw = self.mms.get_screen_hw()
            hero = [self.screen[0][0] + round(hw[0] / 2, 0), self.screen[0][1] + round(hw[1] / 2, 0)]

            self.heroes = [hero]

        # Add enemy
        if self.found.get('enemy'):
            self.enemy = self.clear_triple(self.found['enemy'])

        # Удаляем лишние данные, для маленьких карт
        if self.map_pixel_size > 1:
            # print('Карта маленькая, удаляем лишние пиксели, mk:', mk)
            self.heroes = self.clear_double(self.heroes)
            if self.enemy:
                self.enemy = self.clear_double(self.enemy)

        pass

    # TODO unused
    def find_full(self):
        # Тестирование поиска индексов numpy
        # Поиск объектов на карте. mon_map = {'x': 880, 'y': 49, 'w': 128}
        mon_map = {'top': cfg.mon_map['y'], 'left': cfg.mon_map['x'], 'width': cfg.mon_map['w'],
                   'height': cfg.mon_map['w']}
        img = Mss.sct.grab(mon_map)
        matrix = np.array(img)

        # Сброс настроек
        self.mms.reset()
        self.heroes = []
        self.enemy = []
        self.found = {'hero': [], 'enemy': []}

        # Карта. Цвета карты, bgr
        map_c = (248, 252, 248, 255)
        indices = np.where(np.all(matrix == map_c, axis=-1))
        # Координаты: y, x
        map_crd = np.transpose(indices)
        for crd in map_crd:
            self.mms.add(crd[1], crd[0])

        # Герой
        hero_c = [
            (80, 228, 128, 255),
            (224, 120, 80, 255),
        ]
        for c in hero_c:
            indices = np.where(np.all(matrix == c, axis=-1))
            hero_crd = np.transpose(indices)
            if hero_crd.any():
                for crd in hero_crd:
                    self.found['hero'].append([crd[1], crd[0]])
        # Враги
        enemy_c = [
            (120, 84, 224, 255),  # enemies
            (80, 112, 224, 255),  # civilian and enemy
        ]
        for c in enemy_c:
            indices = np.where(np.all(matrix == c, axis=-1))
            enemy_crd = np.transpose(indices)
            if enemy_crd.any():
                for crd in enemy_crd:
                    self.found['enemy'].append([crd[1], crd[0]])

        self.find_validate()


class Positions:
    # Отслеживание позиций героев на карте
    # Получаем данные с экрана и мини-карты
    # Находим для всех героев положение на мини-карте
    # Обходим всех найденных героев и привязываем им классы
    # При обновлении карты, сравниваем новые позиции, со старыми и фиксируем перемещение героев
    # Для новых герев совершаем обход и привязку классов
    # Выводим список найденых героев и их статус
    map_crd = []
    units = []
    enemy = []
    screen = []
    num = 0

    new_status = 0
    stay_status = 1
    move_status = 2
    old_status = 3

    exclude_status = 3
    no_name = '0-0'
    no_name_cont = 0

    def map_position(self, items):
        # Находим позиции героев на карте
        map_crd = []

        # Сдвиг в пикселях, относительно точки поиска
        shift = 16

        if items:
            for item in items:
                # Координаты со сдвигом на центр модельки
                xs = item[1] + shift
                ys = item[0] + shift
                # Координаты на мини экране
                x0 = round(xs / 32, 0)
                y0 = round(ys / 32, 0)
                # Координаты на мини-карте
                x = int(x0 + self.screen[0][0] + cfg.mm_x_shift)
                y = int(y0 + self.screen[0][1] + cfg.mm_y_shift)

                health = item[2]
                custom_type = item[3]
                map_crd.append([x, y, self.new_status, [x0, y0, xs, ys], health, custom_type])
                pass

        self.map_crd = map_crd
        # print('map_crd',self.map_crd)
        pass

    def get_mouse_position(self, unit):
        # Найти позицию мыши для юнита
        # Юнит [0, [32, 84, 0, [11.0, 1.0, 337, 29]], 1]
        crd = unit[1]
        # Находим координаты на мини экране
        x = crd[3][2]
        y = crd[3][3]
        # Координаты с учётом сдвига
        x1 = x + cfg.screen_shift['x']
        y1 = y + cfg.screen_shift['y']
        return [x1, y1]

    def get_mouse_position_old(self, unit):
        # TODO UNUSED
        # Найти позицию мыши для юнита
        # Юнит [0, [32, 84, 0, [11.0, 1.0, 337, 29]], 1]
        crd = unit[1]
        # Находим координаты на мини экране
        screen_crd = [crd[0] - self.screen[0][0] - cfg.mm_x_shift, crd[1] - self.screen[0][1] - cfg.mm_y_shift]
        old_screen = [crd[3][0], crd[3][1]]

        if screen_crd[0] == old_screen[0] and screen_crd[1] == old_screen[1]:
            # Экран в том-же положении, возвращаем сохранённые данные
            return [crd[3][2], crd[3][3]]
        else:
            # Положение экрана изменилось, ищем разницу
            x1 = (old_screen[0] - screen_crd[0]) * 32
            y1 = (old_screen[1] - screen_crd[1]) * 32
            return [crd[3][2] + x1, crd[3][3] + y1]
            pass

        pass

    def search_enemy(self, enemy_name):
        # Поиск координат врага
        names = Names()
        ret = []
        if self.units:
            for unit in self.units:
                # Имя юнита
                n = names.name(unit[3])
                if n[1] == enemy_name:
                    ret.append(unit)
        return ret

    def remove_units_by_name(self, enemy_name):
        # Убрать из списка юнитов всех врагов, с определённым именем
        names = Names()
        i = 0
        new_units = []
        while i < self.units.__len__():
            # Имя юнита
            n = names.name(self.units[i][3])
            if n[1] != enemy_name:
                new_units.append(self.units[i])

            i += 1
        self.units = new_units

    def enemy_spot(self):
        # Поиск врагов на карте
        name = Names()
        self.enemy = []
        enemy = 0
        if self.units:
            for unit in self.units:
                # Имя юнита
                n = name.name(unit[3])
                if n[0] == 1:
                    enemy += 1
                    self.enemy.append(unit)
        return enemy

    def remove_enemy(self):
        # Убрать из списка всех врагов
        name = Names()
        i = 0
        new_units = []
        while i < self.units.__len__():
            # Имя юнита
            n = name.name(self.units[i][3])
            # Если это не враг, добавляем в новый массив
            if n[0] != 1:
                new_units.append(self.units[i])

            i += 1
        self.units = new_units

    def units_info(self):
        name = Names()
        status = {self.stay_status: 0, self.move_status: 0, self.new_status: 0}
        type = dict()
        if self.units:
            for unit in self.units:
                # Список юнитов и их статус
                status[unit[2]] += 1
                if not type.get(unit[3]):
                    type[unit[3]] = 1
                else:
                    type[unit[3]] += 1
                pass

        items = type.items()
        ret = ''
        # if self.units:
        # print(self.units)
        # print(items)
        for item in items:
            ret += name.name(item[0])[1] + ': ' + str(item[1]) + ', '
        if ret:
            print(ret, self.units)
        # print('Всего:', self.units.__len__(), ' Стоят:', status[self.stay_status], ' Идут:', status[self.move_status],
        #      ' Новые:', status[self.new_status])

    def get_new_units(self):
        return self.get_units(self.new_status)

    def get_stay_units(self):
        return self.get_units(self.stay_status)

    def get_units(self, status):
        # Возвращаем элементы определённого статуса
        ret = []
        if self.units:
            for unit in self.units:
                # Список юнитов и их статус
                if unit[2] == status:
                    ret.append(unit)

        return ret

    def get_valid_units(self):
        # Возвращаем элементы статуса: идут и стоят
        ret = []
        if self.units:
            for unit in self.units:
                # Список юнитов и их статус
                if unit[2] == self.stay_status or unit[2] == self.move_status:
                    ret.append(unit)

        return ret

    def get_valid_by_type(self, t=0):
        # Получить элемент по типу
        # [225, 643, 0, 1]
        # [20, [119, 109, 3, [19.0, 6.0, 595, 203], 0, 1], 1, '0-0']
        ret = []
        if self.units:
            for unit in self.units:
                if unit[2] == self.stay_status or unit[2] == self.move_status:
                    if unit[1][5] == t:
                        ret.append(unit)
        return ret

    def get_noname_units(self):
        # Получаем список юнитов, для которых не получилось получить имена
        ret = []
        if self.units:
            for unit in self.units:
                # Список юнитов и их статус
                if unit[3] == self.no_name:
                    ret.append(unit)

        return ret
        pass

    def update_units(self, items, screen):
        self.screen = screen
        # Обновление юнитов на карте
        self.map_position(items)
        # print('units', self.units)
        units = []
        if self.units:
            # Карта уже есть, сравниваем результаты
            new_units = []
            units = self.units
            map_crd = self.map_crd
            # print('map_crd', map_crd)
            # Поиск неподвижных координат
            i = 0
            map_crd_used = 0
            while i < units.__len__():
                # Назначаем устаревший статус для юнита
                units[i][2] = self.old_status
                j = 0
                while j < map_crd.__len__():
                    if map_crd[j][2] != self.old_status:
                        if units[i][1][0] == map_crd[j][0] and units[i][1][1] == map_crd[j][1] \
                                and units[i][1][5] == map_crd[j][5]:
                            # Юнит находится на месте
                            units[i][2] = self.stay_status
                            # Обновляем координаты мыши для юнита
                            units[i][1] = map_crd[j]
                            # Удаляем найденные объекты
                            map_crd_used += 1
                            map_crd[j][2] = self.old_status
                            break
                    j += 1
                i += 1
            # print('map_crd', map_crd)
            # Поиск движения
            if map_crd.__len__() > map_crd_used:
                # Если координаты ещё остались
                i = 0
                while i < units.__len__():
                    if units[i][2] == self.old_status:
                        # Проверяем толко устаревшие элементы
                        j = 0
                        while j < map_crd.__len__():
                            if map_crd[j][2] != self.old_status:
                                # Проверяем только не использованные координаты
                                if units[i][1][5] == map_crd[j][5]:
                                    # Проверяем только однотипные элементы
                                    x = units[i][1][0]
                                    y = units[i][1][1]
                                    xc = map_crd[j][0]
                                    yc = map_crd[j][1]
                                    if x == xc - 1 or x == xc or x == xc + 1:
                                        if y == yc - 1 or y == yc or y == yc + 1:
                                            # Передвижение на одну клетку
                                            units[i][2] = self.move_status
                                            # Обновляем координаты мыши для юнита
                                            units[i][1] = map_crd[j]
                                            # Удаляем найденные объекты
                                            map_crd_used += 1
                                            map_crd[j][2] = self.old_status
                                            pass
                            pass
                            j += 1
                    i += 1
                    pass

            # Удаление старых, добавление новых
            i = 0
            new_units = []
            use_crd = dict()
            while i < units.__len__():
                if units[i][2] != self.old_status:
                    crd_key = int(str(units[i][1][0]) + '0000' + str(units[i][1][1]))
                    if not use_crd.get(crd_key):
                        use_crd[crd_key] = 1
                        new_units.append(units[i])
                i += 1
                pass

            if map_crd.__len__() > map_crd_used:
                j = 0
                while j < map_crd.__len__():
                    if map_crd[j][2] != self.old_status:
                        crd_key = int(str(map_crd[j][0]) + '0000' + str(map_crd[j][1]))
                        if not use_crd.get(crd_key):
                            use_crd[crd_key] = 1
                            unit = [self.num, map_crd[j], self.new_status, self.no_name]
                            new_units.append(unit)
                        self.num += 1
                    j += 1
                    pass
            units = new_units

        else:
            # Карты ещё нет, первый запуск
            # print('first run')
            for item in self.map_crd:
                units.append([self.num, item, self.new_status, self.no_name])
                self.num += 1

        # print('units', units)
        self.units = units
        pass

    def set_name(self, num, name):
        # Добавляем имя к юниту
        # Пропускаем имена героев
        names = Names()
        n = names.name(name)
        if n[0] == 2:
            # Пропускаем имена героев
            return

        i = 0
        while i < self.units.__len__():
            if self.units[i][0] == num:
                self.units[i][3] = name
                break
            i += 1


class Names:
    # Получение и хранение имён юнитов

    # Список имён юнитов и их тип
    units = []

    names = {
        # Типы персонажей
        # '59-79' - цветовая метка
        # 2 - Тип персонажа:
        #   0 - мирный
        #   1 -враг
        #   2 -герой
        # 'Иглез' - Имя персонажа.
        # 1 - Вид юнита:
        #   0 - обычный
        #   1 -людоед / тролль
        #   2 - дракон
        #   3 - маг
        # Пропуск
        '59-79': [2, 'Иглез', 0],
        '73-91': [2, 'Дайна', 3],
        '115-162': [2, 'Гильдариус', 3],

        # Мирные
        '0-0': [0, 'Не известен', 0],
        '58-88': [0, 'Рекрут', 0],
        '74-115': [0, 'Скракан', 3],
        '77-119': [0, 'Староста', 0],
        '102-153': [0, 'Крестьянка', 0],
        '103-139': [0, 'Командир', 0],
        '111-153': [0, 'Крестьянин', 0],

        # Враги
        '30-49': [1, 'Дух', 0],
        '32-51': [1, 'Урд', 3],
        '41-70': [1, 'Волк', 0],
        '56-71': [1, 'Ящер', 0],
        '59-85': [1, 'Белка', 0],
        '64-85': [1, 'Пчёлы', 0],
        '62-88': [1, 'ДРУИД', 3],
        '60-86': [1, 'Зомби', 0],
        '70-104': [1, 'Дракон', 2],
        '74-101': [1, 'Рыцарь', 0],
        '82-117': [1, 'Людоед', 1],
        '83-99': [1, 'Атаман', 0],
        '93-125': [1, 'Черепаха', 0],
        '105-152': [1, 'Разбойник', 0],
        '107-144': [1, 'СКЕЛЕТ-МАГ', 3],
        '119-187': [1, 'Сороконожка', 0],
        '129-167': [1, 'Летучая мышь', 0],
        '140-176': [1, 'Арбалетчица', 0],
        '131-181': [1, 'Некромансер', 3],
    }

    def name(self, code):
        # Получить имя по коду
        if self.names.get(code):
            return self.names.get(code)
        return self.names.get('0-0')

    def get_code_by_name(self, name):
        ret = '0-0'
        for code, item in self.names.items():
            if item[1] == name:
                ret = code
                break
        return ret

    def get_name(self):
        # Получаем имя с экрана
        img = Mss.sct.grab(cfg.name_window)
        matrix = np.array(img)

        # Жёлтые буквы, b,g,r
        c = (72, 156, 184, 255)
        # Чёрная тень
        c1 = (8, 8, 8, 255)

        indices = np.where(np.all(matrix == c, axis=-1))
        # Координаты: y, x
        map_crd = np.transpose(indices)
        c1_len = map_crd.__len__()

        indices = np.where(np.all(matrix == c1, axis=-1))
        # Координаты: y, x
        map2_crd = np.transpose(indices)
        c2_len = map2_crd.__len__()

        ret = str(c1_len) + '-' + str(c2_len)

        return ret

    def get_hero_name(self):
        # Получаем имя с экрана
        img = Mss.sct.grab(cfg.hero_name_window)
        matrix = np.array(img)

        # Жёлтые буквы, b,g,r
        c = (72, 156, 184, 255)

        indices = np.where(np.all(matrix == c, axis=-1))
        # Координаты: y, x
        map_crd = np.transpose(indices)
        c1_len = map_crd.__len__()
        return c1_len

    pass


class Mage:
    # TODO Unused.
    # Мониторинг характеристик мага
    color = Color()

    def magic_shield_off(self):
        # Проверка магического щита
        # Проверяем наличие нуля в меню брони
        if self.color.validate(1008, 649, 64, 92, 72):
            return True
        return False

    pass


class MainScreen:
    # Поиск данных на основном экране
    mm = MiniMap()
    p = Positions()

    find_result = []
    # Тип юнита: 0 - обычный, 1 - людоед, 2 - Дракон, 3 - маг
    custom_type = []
    matrix = []

    window = {'top': cfg.screen_shift['y'], 'left': cfg.screen_shift['x'], 'width': cfg.view_window['x'],
              'height': cfg.view_window['ym']}

    # Координаты команд героев
    cmd = {}
    # Прошлые координаты героев
    last_cmd = {}
    # Позапрошлые координаты героев
    last_last_cmd = {}
    other = []
    # Валидация положения героев
    validate_groups = []
    # Счётчик поиска героев
    hero_not_found = dict()
    # Максимальное расстояние, для признания координат валидными.
    max_dst = 16

    # Поиск чёрных секторов
    find_black_sector = False

    found_in_black_sector = dict()

    def find(self):
        # Обновление мини-карты
        self.mm.find()

        img = Mss.sct.grab(self.window)
        a = np.array(img)

        self.matrix = a

        # Герои: g,b,r
        c = (104, 144, 200, 255)

        self.found_in_black_sector = dict()
        if self.find_black_sector:
            # center_window = {x: 218, y: 181, x1: 638, y1: 571}
            cw = cfg.center_window
            # Поиск чёрных секторов в центре экрана
            center_crop = a[cw['y']:cw['y1'], cw['x']:cw['x1']]

            c1 = (0, 4, 0, 255)
            indices_1 = np.where(np.all(center_crop == c1, axis=-1))
            find_с1 = np.transpose(indices_1)

            c2 = (0, 0, 0, 255)
            indices_2 = np.where(np.all(center_crop == c2, axis=-1))
            find_с2 = np.transpose(indices_2)

            # Добавляем найденные пиксели в словарь
            self.validate_found_in_black_sector(find_с1, a)
            self.validate_found_in_black_sector(find_с2, a)

            # Находим действительные пиксели
            if len(self.found_in_black_sector):
                # Меняем цвета матрицы
                # print('bs', self.found_in_black_sector)
                for key, matrix_crd in self.found_in_black_sector.items():

                    crop_key = str(matrix_crd[0]) + '-' + str(matrix_crd[1] + 28)

                    if self.found_in_black_sector.get(crop_key):
                        # Найден второй элемент
                        # Добавляем его в матрицу
                        # print('before', a[matrix_crd[0], matrix_crd[1]])
                        a[matrix_crd[0], matrix_crd[1]] = c
                        # print('after', a[matrix_crd[0], matrix_crd[1]])
                        a[matrix_crd[0], matrix_crd[1] + 28] = c
                        pass

        indices = np.where(np.all(a == c, axis=-1))
        find = np.transpose(indices)

        first = []
        res = []
        custom_type = []

        if find.any():
            for f in find:
                # Поиск левого верхнего угла

                # Герой, но не мана мага
                if f[1] + 28 < cfg.view_window['x'] and f[0] + 4 < cfg.view_window['ym'] and f[0] - 4 > 0 and \
                        self.compare_items(a[f[0], f[1]], a[f[0], f[1] + 28]):
                    # Герой или мана
                    if not self.compare_items(a[f[0], f[1]], a[f[0] - 4, f[1]]):
                        # print('hero')
                        if self.compare_items(a[f[0], f[1]], a[f[0] + 4, f[1]]):
                            custom_type.append([f[0], f[1], 3])

                        res.append([f[0], f[1]])

                elif f[1] + 56 < cfg.view_window['x'] and \
                        self.compare_items(a[f[0], f[1]], a[f[0], f[1] + 56]):
                    # Людоед
                    # print('ludoed')
                    res.append([f[0], f[1]])
                    custom_type.append([f[0], f[1], 1])

                elif f[1] + 92 < cfg.view_window['x'] and \
                        self.compare_items(a[f[0], f[1]], a[f[0], f[1] + 92]):
                    # Дракон
                    # print('dragon')
                    res.append([f[0], f[1]])
                    custom_type.append([f[0], f[1], 2])

                elif f[1] + 60 < cfg.view_window['x'] and \
                        self.compare_items(a[f[0], f[1]], a[f[0], f[1] + 60]):
                    # Катапульта
                    # print('Катаульта')
                    res.append([f[0], f[1]])
                    custom_type.append([f[0], f[1], 4])
                elif f[1] + 44 < cfg.view_window['x'] and \
                        self.compare_items(a[f[0], f[1]], a[f[0], f[1] + 44]):
                    # Всадник
                    # print('Всадник')
                    res.append([f[0], f[1]])
                    custom_type.append([f[0], f[1], 5])
                pass

        self.custom_type = custom_type
        self.find_result = res
        # Поиск команд героев
        self.find_heroes()

        # Удаление дубликатов
        unique_other = dict()
        new_other = []
        if self.other:
            for item in self.other:
                item_key = str(item[0]) + '-' + str(item[1])
                if unique_other.get(item_key):
                    continue
                unique_other[item_key] = 1
                new_other.append(item)
        self.other = new_other

        return res

    def validate_found_in_black_sector(self, find, a):
        if find.any():
            cw = cfg.center_window
            for found in find:
                x = found[1] + 1 + cw['x']
                y = found[0] - 2 + cw['y']

                x1 = found[1] + cw['x']
                y1 = found[0] + cw['y']
                # Если у чёрного цвета есть верхний пиксель R больше 80 (96)
                # и следующий писель не такой-же
                if a[y, x][2] > 80 \
                        and not self.compare_items(a[y1, x1 + 1], a[y1, x1]) \
                        and not self.compare_items(a[y1, x1 - 1], a[y1, x1]) \
                        and not self.compare_items(a[y1 + 1, x1], a[y1, x1]) \
                        and not self.compare_items(a[y1 - 1, x1], a[y1, x1]):
                    self.found_in_black_sector[str(y) + '-' + str(x)] = [y, x]

    def set_validate_groups(self, groups):
        # Валидация групп героев
        self.validate_groups = groups

    def find_loot(self):
        # Поиск мешков на карте
        c = [(96, 172, 208, 255), [(88, 164, 200, 255)]]

        indices = np.where(np.all(self.matrix == c[0], axis=-1))
        find = np.transpose(indices)
        if find.__len__() == 0:
            indices = np.where(np.all(self.matrix == c[1], axis=-1))
            find = np.transpose(indices)

        return find

    def get_hero_abs(self, n=1):
        # Получить абсолютные координаты героя x,y
        hero = self.get_hero(n)
        if hero:
            # Координаты с учётом сдвига экрана
            x1 = hero[1] + cfg.screen_shift['x']
            y1 = hero[0] + cfg.screen_shift['y']
            return [x1, y1]

        return []

    def get_hero(self, n=1):
        # Получить координаты героя: y,x
        hero = self.cmd.get(n)
        if hero and hero[0] > 0:
            # Добавляем 16 px, чтобы метка указывала на центр героя
            hero = self.get_shift(hero)
            return hero

        return []

    def get_shift(self, hero):
        # Получить данные со сдвигом 16 px
        return [hero[0] + 16, hero[1] + 16]

    def screen_to_abs(self, x, y):
        x1 = x + cfg.screen_shift['x']
        y1 = y + cfg.screen_shift['y']
        return [x1, y1]

    def abs_to_screen(self, x, y):
        # Перевод абсолютных координат, в координаты экрана
        screen_crd = [x - cfg.screen_shift['x'], y - cfg.screen_shift['y']]
        return screen_crd

    def find_heroes(self):
        # Поиск героев трёх команд
        self.last_last_cmd = self.last_cmd
        self.last_cmd = self.cmd
        self.cmd = {1: [0, 0], 2: [0, 0], 3: [0, 0], 4: [0, 0], 5: [0, 0], 6: [0, 0], 7: [0, 0], 8: [0, 0], 9: [0, 0],
                    0: [0, 0]}

        self.other = []
        if self.find_result.__len__() > 0:
            # Координаты команд x,y: 8,8,8
            # 1 - 1,8
            # 2 - 2,13
            # 3 - 3,14
            a = self.matrix
            for f in self.find_result:
                # Проверка на вход в ОДЗ
                if f[0] + 14 < cfg.view_window['ym'] and f[1] + 3 < cfg.view_window['x']:

                    if self.compare_items(a[f[0] + 7, f[1] + 1], [8, 8, 8], '1,6,7,8,9'):
                        # Команда 1,6,7,8
                        if not self.compare_items(a[f[0] + 7, f[1]], [8, 8, 8], 'n 1') \
                                and self.compare_items(a[f[0] + 11, f[1] + 2], [8, 8, 8], 'n 1'):
                            self.cmd[1] = f
                        else:
                            # 6,7,8
                            if not self.compare_items(a[f[0] + 9, f[1] + 1], [8, 8, 8], 'n 7'):
                                # 7
                                self.cmd[7] = f
                            else:
                                # 6,8,9
                                if self.compare_items(a[f[0] + 7, f[1] + 2], [8, 8, 8], 6):
                                    self.cmd[6] = f
                                else:
                                    # 8,9
                                    if self.compare_items(a[f[0] + 8, f[1] + 2], [208, 208, 208], 9):
                                        self.cmd[9] = f
                                    elif self.compare_items(a[f[0] + 8, f[1] + 2], [248, 252, 248], 9):
                                        # 9 война
                                        self.cmd[9] = f
                                    else:
                                        self.cmd[8] = f
                    elif self.compare_items(a[f[0] + 13, f[1] + 2], [8, 8, 8], 2):
                        # Команда 2
                        self.cmd[2] = f
                    elif self.compare_items(a[f[0] + 14, f[1] + 3], [8, 8, 8], '3,4,5,9,0'):
                        # Команда 3,4,5
                        if self.compare_items(a[f[0] + 14, f[1] + 1], [8, 8, 8], 4):
                            self.cmd[4] = f
                        elif self.compare_items(a[f[0] + 13, f[1] + 1], [8, 8, 8], '5,9'):
                            if self.compare_items(a[f[0] + 11, f[1] + 2], [8, 8, 8], 5):
                                self.cmd[5] = f
                            else:
                                # 9 мага
                                self.cmd[9] = f
                        elif self.compare_items(a[f[0] + 13, f[1]], [8, 8, 8], 0):
                            self.cmd[0] = f
                        else:
                            self.cmd[3] = f
                    else:
                        # Здоровье
                        health = self.health_status(f)
                        f.append(health)
                        # Тип
                        custom_type = self.get_unit_type(f)
                        f.append(custom_type)
                        self.other.append(f)

            # Валидация данных. Отслеживаем прошлые координаты героев и сравниваем с текущими.
            if self.validate_groups and self.last_cmd:
                invalid = []
                for hero in self.validate_groups:
                    # Проверяем все ли герои найдены.
                    crd = self.cmd.get(hero)
                    if not crd[0]:
                        invalid.append(hero)

                # Если не все, проверяем новые элементы.
                if invalid:
                    new_other = []
                    for hero in invalid:
                        last_hero_crd = self.last_cmd.get(hero)
                        if self.other:
                            for item in self.other:
                                #   1. Если есть, проверяем координаты, и добавляем цель в пределах 16 рх.
                                if last_hero_crd[0] - self.max_dst < item[0] < last_hero_crd[0] + self.max_dst and \
                                        last_hero_crd[1] - self.max_dst < item[1] < last_hero_crd[1] + self.max_dst:
                                    # Добавляем координаты героя из неизвестных
                                    self.cmd[hero] = item
                                    self.append_not_found_hero(hero)
                                    # print('Команда', hero, 'не найдена, берём из непознанных координат')
                                else:
                                    # another_search()
                                    # Добавляем неизвестный элемент
                                    new_other.append(item)
                                pass
                        else:
                            # 2. Если нет, берём координаты из старых. Добавляем счётчик не найденной группы.
                            # Он нужен, чтобы определить смерть наёмника. Если счётчик больше 10.
                            # По окончании цикла проверим, жив ли наёмник. Нахождение наёмника сбрасывает счётчик.
                            # TODO счётчик не найденного героя
                            self.cmd[hero] = self.last_cmd.get(hero)
                            self.append_not_found_hero(hero)
                            # print('Команда', hero, 'не найдена, берём из прошлых координат')
                            # print(self.hero_not_found)
                            pass
                    self.other = new_other
                else:
                    # Валидация пройдена успешно
                    self.clear_not_found_hero()
        pass

    def another_search(self):
        # Unused
        # Если герой не найден и находится дальше 16рх от своей позиции,
        # проверяем его расстояние до остальных героев-магов.
        # Обычно герой не находится, когда все герои стоят вместе и перекрывают друг друга.
        i = 0
        j = 0
        for k, cmd in self.cmd.items():
            if not self.hero_not_found.get(k):
                # Проверяем расстояние от неизвестного героя до валидных героев.
                dst = self.get_distance(cmd, item)
                if dst < 100:
                    # Если расстояние в пределах 100 пикселей, добавляем счётчик.
                    i += 1
                j += 1
        if i / j > 0.6:
            # Если в целом, точка рядом с другими, добавляем координаты героя из неизвестных
            self.cmd[hero] = item
            self.append_not_found_hero(hero)
        else:
            pass

    def compare_items(self, item1, item2, show=0):
        # Сравнение цветов двух элементов
        if show:
            # print('compare', show, item1, item2)
            pass
        if item1[0] == item2[0] and item1[1] == item2[1] and item1[2] == item2[2]:
            return True
        return False

    def append_not_found_hero(self, hero):
        if not self.hero_not_found.get(hero):
            self.hero_not_found[hero] = 1
        else:
            self.hero_not_found[hero] += 1
        pass

    def clear_not_found_hero(self):
        self.hero_not_found = dict()
        pass

    def get_by_type(self, t=0):
        # Получить элемент по типу
        # [[225, 643, 0, 1]]
        ret = []
        if self.other:
            for item in self.other:
                if item[3] == t:
                    ret.append(item)
        return ret

    def get_unit_type(self, f):
        # Тип юнита
        custom_type = 0

        for u in self.custom_type:
            if u[0] == f[0] and u[1] == f[1]:
                custom_type = u[2]
                break
        return custom_type

    def health_status(self, f):
        # Количество здоровья персонажа
        a = self.matrix

        # b,g,r
        full_hp = (0, 252, 0)
        half_hp = (0, 252, 248)
        small_hp = (0, 0, 200)

        ret = 0

        if f[0] == 0:
            return ret

        # Проверка ОДЗ
        if f[1] + 3 < cfg.view_window['x']:
            hp = a[f[0], f[1] + 3]

            # Проверка ХП
            if hp[1] > 120 and hp[2] > 120:
                # half hp
                ret = 1
            elif hp[1] < 70 and hp[2] > 120:
                # small hp
                ret = 2
            else:
                # full hp
                pass
            pass

        return ret

    def hero_health_not_full(self, n):
        f = self.cmd.get(n)
        ret = False
        if f[0]:
            a = self.matrix
            hp = a[f[0], f[1] + 26]
            if hp[0] < 100 and hp[1] > 120 and hp[2] < 100:
                # Полное ХП
                pass
            else:
                # Потраченое ХП
                ret = True
                # print(hp)
                pass

        return ret

    def dark_detect(self, n):
        # Детектор затемнения
        f = self.cmd.get(n)
        ret = False
        if f[0]:
            a = self.matrix
            # Цвета пикселей
            pix1 = a[f[0] - 1, f[1] - 1]
            pix2 = a[f[0] - 1, f[1] + 2]
            pix3 = a[f[0] + 2, f[1] + 2]
            # print(pix1, pix2, pix3)
            # Средний цвет
            c1 = (int(pix1[0]) + int(pix1[1]) + int(pix1[2])) / 3
            c2 = (int(pix2[0]) + int(pix2[1]) + int(pix2[2])) / 3
            c3 = (int(pix3[0]) + int(pix3[1]) + int(pix3[2])) / 3
            # print(c1, c2, c3)
            if c1 < 30 and c2 < 30 and c3 < 30:
                ret = True
        return ret

    def get_unit_abs_crd(self, unit):
        # Получение абсолютных координат на экране юнита
        # [11, [95, 64, 3, [10.0, 15.0, 305, 491]], 1, '74-101']
        # Координаты экрана
        x = unit[1][3][2]
        y = unit[1][3][3]
        abs_crd = self.screen_to_abs(x, y)
        return abs_crd

    def hero_is_stay(self, n):
        # Герой стоит
        f1 = self.cmd.get(n)
        f2 = self.last_cmd.get(n)
        f3 = self.last_last_cmd.get(n)
        if f1 and f2 and f3:
            # Все координаты получены
            if f1[0] and f2[0] and f3[0]:
                # Не равны нулю
                if f1[0] == f2[0] == f3[0] and f1[1] == f2[1] == f3[1]:
                    # Все координаты равны
                    if not self.hero_not_found.get(n):
                        # Ошибок нет
                        # print('Герой', n, 'стоит')
                        return True

        # print('Герой', n, 'движется', f1, f2, f3)
        return False

    def get_distance(self, path, hero):
        # Дистанция до мини цели
        x_path_sm = path[0] - hero[0]
        y_path_sm = path[1] - hero[1]
        # Дистанция до мини цели
        cpm_dist = round((abs(x_path_sm) ** 2 + abs(y_path_sm) ** 2) ** 0.5, 1)
        return cpm_dist

    def find_color_crd(self, color=[]):
        a = self.matrix
        # Герои: g,b,r
        c = (color[2], color[1], color[0], 255)
        indices = np.where(np.all(a == c, axis=-1))
        find = np.transpose(indices)
        return find

    pass
