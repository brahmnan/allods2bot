# Управление персонажем на миссии
# Для каждой миссии свой набор контрольных точек и сигналов
from app.mission.actions import *
from app.mission.task_manager import TaskManager
import app.mission.task as task
from app.commands import Mouse
from app.mission.tactic import CheckPoint
import app.ineterface as inter
from app.mission.heroes import *
import time


class Mission:
    # Управление миссиями
    mm = inter.MainMenu()

    # Маги
    hildar_cl = Hildar()
    healer_cl = Healer()
    diana_cl = Diana()
    elf_cl = Elf()
    # Воины
    igles_cl = Igles()
    warrior_cl = Warrior()
    troll_cl = Troll()
    # Лучники
    ork_cl = Ork()
    archer_cl = Archer()
    catapult_cl = Catapult()
    # Волшебницы из последней миссии
    en1_cl = Enchantress1()
    en2_cl = Enchantress2()
    en3_cl = Enchantress3()
    # Остальные
    other_cl = Other()

    hildar = hildar_cl.group
    healer = healer_cl.group
    diana = diana_cl.group
    elf = elf_cl.group
    igles = igles_cl.group
    warrior = warrior_cl.group
    troll = troll_cl.group
    ork = ork_cl.group
    archer = archer_cl.group
    catapult = catapult_cl.group
    en1 = en1_cl.group
    en2 = en2_cl.group
    en3 = en3_cl.group
    other = other_cl.group

    # Время старта миссии
    start_mission_time = 0

    def enter_game(self):
        self.start_mission_time = time.time()
        self.mm.enter_game()

    def first_mission(self):
        # Первая миссия
        # Кликаем по карте, чтобы войти в игру
        self.enter_game()
        # Настраиваем необходимые опции
        mm = inter.MainMenu()
        mm.go_options()
        mm.set_no_damage_info()
        mm.set_life_all()
        mm.set_day_and_night(False)
        mm.quit_options()
        act = Actions()
        mouse = Mouse()
        # Назначаем группу - 2, для мага Хильдариуса
        mouse.left(904, 116, sleep=100)
        act.select_all()
        act.make_group(self.hildar)
        act.center_group(self.hildar)

        magic = Magic()
        magic.select_magic('heal')
        act.ctrl_key(act.magic_heal_key)

        tm = TaskManager([self.hildar])
        tm.run_task(task.BindAutoMagic(self.hildar, 'fire'))
        tm.run_task(task.SpeedPlus())
        # Отправление к контрольной точке Мага
        tm.run_task(task.WalkS([993, 109], self.hildar))

        if tm.defeat:
            return False
        return True

    def second_mission(self):
        # Первая миссия
        # Кликаем по карте, чтобы войти в игру
        self.enter_game()
        tm = TaskManager([self.hildar])

        tm.run_task(task.BindAutoMagic(self.hildar, 'heal'))
        # Отправление к контрольной точке Мага
        tm.run_task(task.Walk([965, 137], self.hildar))
        tm.run_task(task.Walk([950, 52], self.hildar))

        if tm.defeat:
            return False
        return True

    def mission_3(self):
        self.enter_game()
        tm = TaskManager([self.hildar])
        tm.run_task(task.Center(self.hildar))
        # Назначаем Иглеза в команду
        tm.run_task(task.SetHeroesGroup([self.igles_cl]))
        tm.set_validate_group([self.igles, self.hildar])
        act = Actions()
        # Назначаем элексир регенерации маны
        color = [0, 0, 104, 144, 232]
        tm.run_task(task.BindHotKeyInvent(self.hildar, act.mana_regen_key, color))
        # Назначаем элексир регенерации жизни
        color = [2, -2, 184, 76, 88]
        tm.run_task(task.BindHotKeyInvent(self.igles, act.heal_regen_key, color))
        tm.run_task(task.BindAutoMagic(self.hildar, 'fire'))
        # Идём к КТ
        tm.run_task(task.WalkGroup([917, 142], self.hildar))
        tm.run_task(task.WalkGroup([952, 156], self.hildar))
        tm.run_task(task.WalkGroup([971, 113], self.hildar))
        tm.run_task(task.AllStop())
        tm.run_task(task.WalkS([979, 114], self.igles))
        tm.run_task(task.Wait(1))
        if tm.defeat:
            return False
        return True

    def mission_3_2(self):
        self.enter_game()
        tm = TaskManager([self.igles, self.hildar])

        # Сражение с магом
        # Маг занимает позицию на горе и принимает элексир
        tm.run_task(task.Walk([974, 112], self.hildar))

        # Иглез идёт к вражескому магу
        tm.run_task(task.Walk([977, 110], self.igles))

        # Регенерация здоровья и маны
        tm.run_task(task.HealRegen(self.igles))
        tm.run_task(task.ManaRegen(self.hildar))

        # Оборона позиции
        # Маг лечит, если хп не полное
        tm.add_task(task.HealthMon(self.hildar, wait_time=100, health_level=1))
        tm.add_task(task.KillEnemyMagic(3, self.hildar, 'fire', wait_time=10000, target_type=3,
                                        magic_attack=True, magic_cursor=False))
        tm.add_task(task.EnemyClear([self.igles], min_dst=2, validation_interval=2))
        tm.run()

        # Сбор мешков
        tm.run_task(task.GetLoot(self.igles))

        # Идём к крестьянам
        tm.run_task(task.WalkGroup([980, 105], self.hildar))
        tm.run_task(task.WalkGroup([944, 154], self.hildar))

        # Финальное сражение
        tm.run_task(task.WalkGroup([908, 147], self.hildar))
        tm.run_task(task.Walk([906, 147], self.hildar))
        tm.run_task(task.Walk([907, 150], self.igles))

        if tm.defeat:
            return False
        return True

    def mission_3_3(self):
        self.enter_game()
        tm = TaskManager([self.igles, self.hildar])
        tm.run_task(task.Center(self.igles))
        health = tm.run_task(task.HealthMon(self.hildar, wait_time=100, health_level=1))
        # Регенерация здоровья и маны
        tm.run_task(task.HealRegen(self.igles))
        tm.run_task(task.ManaRegen(self.hildar))
        tm.run_task(task.BindAutoMagic(self.hildar, 'heal'))
        tm.run_task(task.Wait(10))
        enemy1_name = 'Рыцарь'
        kill = tm.add_task(task.KillEnemy(0, self.igles, max_dst=7, far_target=False, target_name=enemy1_name,
                                          wait_time=1000, validate_target=True,
                                          enemy_detect_block=3))
        walk = tm.add_task(task.Walk([907, 151], self.igles), block_task_id=kill)
        tm.run()
        tm.remove_task(kill)
        tm.run_task(task.Wait(3))

        tm.run_task(task.BindAutoMagic(self.hildar, 'fire'))
        tm.remove_task(health)
        tm.add_task(task.HealthMon(self.hildar, wait_time=100, health_level=2, max_dst=3))

        kill = tm.add_task(task.KillEnemy(0, self.igles, max_dst=7, far_target=False, wait_time=1000, target_type=3,
                                          stop_if_target_found=False))
        tm.add_task(task.WalkS([903, 160], self.igles), block_task_id=kill)
        tm.add_task(task.EnemyClear([self.igles], 3))
        tm.run()
        tm.remove_task(kill)

        # Сбор мешков
        tm.run_task(task.GetLoot(units=[self.igles, self.hildar]))
        tm.run_task(task.WalkGroup([920, 141], self.igles))
        if tm.defeat:
            return False
        return True

    def mission_4(self):
        # Идём к мосту
        self.enter_game()
        tm = TaskManager([self.igles, self.hildar])

        # Назначаем элексир здоровья
        color = [0, 10, 224, 104, 120]
        act = Actions()
        tm.run_task(task.BindHotKeyInvent(self.igles, act.heal_up_key, color))

        # Иглез защищает Гилдариуса
        tm.run_task(task.WalkGroup([935, 113], self.hildar))
        tm.run_task(task.EnemyClear([self.igles, self.hildar], 6))
        tm.run_task(task.Walk([939, 111], self.hildar))
        # Регенерация здоровья и маны
        tm.run_task(task.HealRegen(self.igles))
        tm.run_task(task.ManaRegen(self.hildar))
        # Убиваем разбойника
        tm.run_task(task.Walk([943, 111], self.igles))
        tm.add_task(task.EnemyClear([self.igles], 3))
        # Маг лечит, если хп не полное
        tm.add_task(task.HealthMon(self.hildar, wait_time=100, health_level=1))
        tm.add_task(task.KillEnemyMagic(3, self.hildar, 'fire', wait_time=10000, target_type=1,
                                        magic_attack=True, magic_cursor=False))
        tm.run()
        tm.run_task(task.GetLoot(units=[self.igles]))
        tm.run_task(task.WalkGroup([953, 108], self.hildar, walk_s=False))
        tm.run_task(task.WalkGroup([964, 116], self.hildar, walk_s=False))
        if tm.defeat:
            return False
        return True

    def mission_4_1(self):
        # Убиваем магов
        # Идём к мосту
        self.enter_game()
        tm = TaskManager([self.igles, self.hildar], True)
        # Регенерация здоровья и маны
        tm.run_task(task.HealRegen(self.igles))
        tm.run_task(task.ManaRegen(self.hildar))
        # Мониторинг здоровья
        tm.run_task(task.SelfHealthMon(self.igles, 0))

        name = 'Некромансер'
        # Маг лечит, если хп не полное
        tm.add_task(task.HealthMon(self.hildar, wait_time=200, health_level=1))
        tm.add_task(task.WalkS([976, 122], self.igles))
        kill = tm.add_task(task.KillEnemyMagic(3, self.hildar, 'fire', wait_time=3000, target_type=3, max_dst=16,
                                               magic_attack=True, target_name=name))
        tm.run()
        tm.remove_task(kill)
        tm.run_task(task.WalkGroup([976, 122], self.hildar))
        tm.run_task(task.GetLoot(self.igles))

        tm.run_task(task.Walk([953, 112], self.igles))
        tm.run_task(task.Walk([925, 112], self.igles))
        tm.run_task(task.Walk([909, 93], self.igles))
        tm.run_task(task.Walk([909, 59], self.igles))

        # Обратный путь
        tm.run_task(task.WalkS([909, 93], self.igles))
        tm.run_task(task.Walk([925, 112], self.igles))
        tm.run_task(task.Walk([953, 112], self.igles))
        tm.run_task(task.Walk([974, 122], self.igles))
        if tm.defeat:
            return False
        return True

    def mission_5(self):
        # Миссия 5. Начало
        self.enter_game()
        tm = TaskManager([self.igles, self.hildar], True)
        tm.run_task(task.Center(self.igles))
        tm.run_task(task.SetHeroesGroup([self.diana_cl]))
        tm = TaskManager([self.igles, self.hildar, self.diana], True)
        # Выбираем огненную стрелу для Дайны
        tm.run_task(task.BindAutoMagic(self.diana, 'fire'))
        tm.run_task(task.HealthMon(self.hildar, wait_time=200, health_level=1))
        tm.run_task(task.HealthMon(self.diana, wait_time=200, health_level=1))

        tm.add_task(task.WalkGroup([976, 60], self.igles))
        tm.add_task(task.Retreat([self.hildar, self.diana], min_dst=2))
        tm.add_task(task.EnemyClear([self.igles], 3, validation_interval=2))
        tm.run()

        tm.add_task(task.WalkGroup([956, 58], self.igles))
        tm.add_task(task.EnemyClear([self.igles], 3))
        tm.run()
        if tm.defeat:
            return False
        return True

    def group_m5(self):
        # Выстраиваем группу
        group = Group()
        # Назначения для формирования строя
        tank = self.igles
        archers = [self.hildar, self.diana]
        group.archers_layer = 4
        group.set_group(tank, archers)
        return group

    def mission_5_1(self):
        self.enter_game()
        tm = TaskManager([self.igles, self.hildar, self.diana], True)
        tm.run_task(task.BindAutoMagic(self.hildar, 'heal'))
        # Мониторинг здоровья
        tm.run_task(task.HealRegen(self.igles))
        tm.run_task(task.HealthMon(self.hildar, wait_time=200, health_level=1))
        tm.run_task(task.HealthMon(self.diana, wait_time=200, health_level=1))
        tm.run_task(task.SelfHealthMon(self.igles, 1))

        # Движемся к Рапторам и встречаем их
        tm.run_task(task.Walk([947, 58], self.igles))
        tm.run_task(task.Group(self.group_m5(), 0, 1))
        tm.run_task(task.EnemyClear([self.igles], 3))
        # Движемся дальше
        tm.run_task(task.BindAutoMagic(self.hildar, 'fire'))
        tm.run_task(task.WalkGroup([932, 60], self.igles))
        tm.run_task(task.WalkGroup([890, 106], self.igles))
        tm.run_task(task.WalkGroup([890, 113], self.igles))
        # Гильдариус догоняет
        tm.run_task(task.Walk([890, 111], self.hildar))
        if tm.defeat:
            return False
        return True

    def mission_5_2(self):
        self.enter_game()
        tm = TaskManager([self.igles, self.hildar, self.diana], True)
        # Мониторинг здоровья
        tm.run_task(task.HealRegen(self.igles))
        tm.run_task(task.HealthMon(self.hildar, wait_time=200, health_level=1))
        tm.run_task(task.HealthMon(self.diana, wait_time=200, health_level=1))
        tm.run_task(task.SelfHealthMon(self.igles, 1))

        tm.run_task(task.Walk([891, 119], self.igles))
        tm.run_task(task.Group(self.group_m5(), 0, 0))
        tm.run_task(task.EnemyClear([self.igles], 3))
        # Идём к друидам
        tm.run_task(task.WalkGroup([911, 139], self.igles))
        tm.run_task(task.WalkGroup([920, 136], self.igles))
        tm.run_task(task.GetLoot(units=[self.igles, self.hildar, self.diana]))

        # Занимаем позицию у горы некромантов
        tm.run_task(task.WalkGroup([953, 99], self.igles))
        if tm.defeat:
            return False
        return True

    def mission_5_3(self):
        # Кликаем по карте, чтобы войти в игру
        self.enter_game()
        tm = TaskManager([self.igles, self.hildar, self.diana], True)
        # Мониторинг здоровья
        tm.run_task(task.HealRegen(self.igles))
        tm.run_task(task.HealthMon(self.hildar, wait_time=200, health_level=1))
        tm.run_task(task.HealthMon(self.diana, wait_time=200, health_level=1))
        tm.run_task(task.SelfHealthMon(self.igles, 0))

        tm.run_task(task.Walk([956, 99], self.diana))
        tm.run_task(task.Walk([954, 99], self.hildar))

        resist = tm.run_task(task.FireResist([self.igles], self.diana, 20, 6))
        # Регенерация здоровья и маны
        tm.run_task(task.HealRegen(self.igles))
        tm.run_task(task.ManaRegen(self.hildar))
        tm.run_task(task.ManaRegen(self.diana))
        tm.run_task(task.BindAutoMagic(self.hildar, 'heal'))
        tm.run_task(task.BindAutoMagic(self.diana, 'heal'))

        tm.run_task(task.Walk([957, 95], self.igles))

        # Уничтожаем зомби
        tm.run_task(task.EnemyClear([self.igles], 3, validation_interval=2))
        tm.remove_task(resist)

        resist = tm.run_task(task.FireResist([self.igles, self.diana], self.diana, 20, 6))
        tm.run_task(task.Wait(1))

        tm.add_task(task.WalkS([958, 84], self.igles))
        kill = tm.add_task(task.KillEnemyMagic(3, self.diana, 'fire', wait_time=3000, target_type=3, max_dst=20,
                                               magic_attack=True))

        kill2 = tm.add_task(task.KillEnemyMagic(3, self.hildar, 'fire', wait_time=3000, target_type=3, max_dst=20,
                                                magic_attack=True))
        tm.run()
        tm.run_task(task.EnemyClear([self.igles], 3))
        tm.run_task(task.WalkGroup([958, 84], self.hildar))
        tm.clear_tasks()

        tm.run_task(task.GetLoot(units=[self.igles, self.hildar, self.diana]))
        tm.run_task(task.Walk([907, 140], self.igles))
        if tm.defeat:
            return False
        return True

    def mission_6(self):
        # Орковская звезда
        # Кликаем по карте, чтобы войти в игру
        self.enter_game()
        mm = self.mm
        mm.go_options()
        mm.set_healing_union()
        mm.set_healing_all()
        mm.quit_options()

        tm = TaskManager([self.igles, self.hildar, self.diana], True)

        tm.run_task(task.HealthMon(self.hildar, wait_time=200, health_level=1))
        tm.run_task(task.HealthMon(self.diana, wait_time=200, health_level=1))

        tm.run_task(task.WalkGroup([901, 161], self.hildar))
        tm.run_task(task.WalkGroup([922, 158], self.hildar))
        tm.run_task(task.WalkGroup([945, 136], self.hildar))
        tm.run_task(task.WalkGroup([955, 124], self.hildar))
        tm.run_task(task.WalkGroup([978, 119], self.hildar))
        tm.run_task(task.WalkGroup([980, 94], self.hildar))
        if tm.defeat:
            return False
        return True

    def mission_6_1(self):
        self.enter_game()
        tm = TaskManager([self.igles, self.hildar, self.diana], True)
        tm.win_final = 0
        tm.run_task(task.HealthMon(self.hildar, wait_time=200, health_level=1))
        tm.run_task(task.HealthMon(self.diana, wait_time=200, health_level=1))
        tm.run_task(task.BindAutoMagic(self.hildar, 'heal'))
        tm.run_task(task.WalkGroup([994, 82], self.hildar))
        tm.run_task(task.WalkGroup([984, 70], self.hildar))
        tm.run_task(task.WalkGroup([996, 56], self.hildar))

        tm.run_task(task.GetLoot(units=[self.igles, self.hildar, self.diana]))
        if tm.win:
            self.mm.win_option()

        if tm.defeat:
            return False
        return True

    def group_m7(self):
        # Выстраиваем группу
        group = Group()
        # Назначения для формирования строя
        tank = self.warrior
        archers = [self.igles,
                   self.archer,
                   self.ork]
        mages = [self.elf,
                 self.healer,
                 self.hildar,
                 self.diana]
        group.set_group(tank, archers, mages)
        return group

    def validate_m7(self):
        return [self.igles, self.hildar, self.diana, self.warrior, self.ork, self.healer, self.archer, self.elf]

    def mission_7(self):
        self.enter_game()
        tm = TaskManager([self.igles, self.hildar, self.diana], True)
        tm.run_task(task.Walk([999, 163], self.igles))
        tm.run_task(task.Center(self.igles))

        tm.run_task(task.SetHeroesGroup([self.warrior_cl, self.elf_cl, self.archer_cl, self.healer_cl, self.ork_cl]))
        tm.set_validate_group(self.validate_m7())
        tm.run_task(task.Walk([1000, 159], self.warrior))
        tm.run_task(task.Group(self.group_m7(), 0, 2))

        tm.run_task(task.LifeHiredUnits([self.warrior_cl, self.archer_cl, self.ork_cl, self.elf_cl, self.healer_cl]))
        # Настраиваем магию по умолчанию, для наёмников

        tm.run_task(task.BindAutoMagic(self.healer, 'lighting'))
        tm.run_task(task.BindAutoMagic(self.elf, 'earth'))

        tm.run_task(task.HealthMon(self.hildar, wait_time=200, health_level=1))
        tm.run_task(task.HealthMon(self.healer, wait_time=200, health_level=1))
        tm.run_task(task.HealthMon(self.diana, wait_time=200, health_level=1))

        tm.run_task(task.EnemyClear([self.warrior], 3))

        tm.run_task(task.Walk([999, 155], self.warrior))
        tm.run_task(task.Group(self.group_m7(), 0, 2))
        tm.run_task(task.EnemyClear([self.warrior], 3))

        tm.run_task(task.GetLoot(self.igles))
        if tm.defeat:
            return False
        return True

    def mission_7_1(self):
        self.enter_game()
        # Паническое авто-отступление
        self.mm.opt_panic_retreat()

        tm = TaskManager(self.validate_m7(), True)
        tm.run_task(task.LifeHiredUnits([self.warrior_cl, self.archer_cl, self.ork_cl, self.elf_cl, self.healer_cl]))

        # Медик лечит всех, у кого жёлтое ХП
        tm.run_task(task.HealthMon(self.hildar))
        tm.run_task(task.HealthMon(self.healer))

        # Группа магов, которые будут отступать, если враг вблизи.
        tm.run_task(task.Retreat([self.hildar, self.diana, self.elf, self.healer], min_dst=2))
        # Идём на север
        tm.run_task(task.WalkGroup([1004, 132], self.ork))
        tm.run_task(task.WalkGroup([1000, 119], self.ork))
        # Волки
        tm.run_task(task.WalkGroup([970, 114], self.ork))
        tm.run_task(task.EnemyClear([self.igles, self.warrior]))
        tm.run_task(task.Wait(1))
        # Ящеры
        tm.run_task(task.Walk([967, 110], self.warrior))
        tm.run_task(task.WalkGroup([962, 108], self.warrior))
        tm.run_task(task.EnemyClear([self.igles, self.warrior]))
        tm.run_task(task.Wait(1))
        tm.run_task(task.WalkGroup([957, 122], self.ork))
        tm.run_task(task.WalkGroup([961, 103], self.ork))
        tm.run_task(task.Wait(5))
        if tm.defeat:
            return False
        return True

    def mission_7_2(self):
        self.enter_game()
        # Отключаем автоотступление
        self.mm.opt_no_retreat()
        tm = TaskManager(self.validate_m7(), True)
        tm.run_task(task.LifeHiredUnits([self.warrior_cl, self.archer_cl, self.ork_cl, self.elf_cl, self.healer_cl]))

        # Воин идёт к месту сбора
        tm.run_task(task.Walk([954, 99], self.warrior))
        tm.run_task(task.Group(self.group_m7(), 0, 1))

        # Регенерация здоровья и маны
        tm.run_task(task.ManaRegen(self.diana))
        tm.run_task(task.BindAutoMagic(self.healer, 'heal'))

        # Приманиваем врагов
        tm.run_task(task.ClearSector([943, 97], self.warrior))
        tm.run_task(task.HealthMon(self.hildar, wait_time=200, health_level=0))
        tm.run_task(task.HealthMon(self.healer, wait_time=200, health_level=0))

        # Убиваем врагов ближнего боя
        tm.run_task(task.KillEnemyMagic(3, self.diana, 'fire_ball'))
        tm.run_task(task.EnemyClear([self.warrior]))
        # Контр-атака
        tm.run_task(task.WalkGroup([942, 99], self.ork))
        tm.run_task(task.GetLoot(self.igles))
        tm.run_task(task.WalkGroup([930, 107], self.igles))
        if tm.defeat:
            return False
        return True

    def mission_7_3(self):
        self.enter_game()
        tm = TaskManager(self.validate_m7(), True)
        tm.run_task(task.LifeHiredUnits([self.warrior_cl, self.archer_cl, self.ork_cl, self.elf_cl, self.healer_cl]))
        tm.run_task(task.WalkGroup([895, 114], self.ork))
        tm.run_task(task.Walk([895, 111], self.warrior))
        tm.run_task(task.Group(self.group_m7(), 0, 2))
        if tm.defeat:
            return False
        return True

    def mission_7_4(self):
        # Сражение у моста
        self.enter_game()
        tm = TaskManager(self.validate_m7(), True)
        tm.run_task(task.LifeHiredUnits([self.warrior_cl, self.archer_cl, self.ork_cl, self.elf_cl, self.healer_cl]))

        tm.run_task(task.HealthMon(self.hildar, wait_time=200, health_level=0))
        tm.run_task(task.HealthMon(self.healer, wait_time=200))

        # Воин поднимается наврех и приманиевает зомби
        tm.run_task(task.ClearSector([895, 100], self.warrior))

        # Дайна обестреливает некроманта и лучников файрболом
        tm.run_task(task.ManaRegen(self.diana))
        tm.run_task(task.KillEnemyMagic(6, self.diana, 'fire_ball', wait_time=100, max_dst=16))
        tm.run_task(task.EnemyClear([self.warrior]))

        # Иглез собирает лут, пока другие дерутся
        tm.run_task(task.GetLoot(self.igles))

        # Вся команда атакует выживших врагов
        tm.run_task(task.WalkGroup([894, 89], self.ork))
        tm.clear_tasks()

        #  Иглез собирает лут
        tm.run_task(task.GetLoot(self.igles))

        # Танк занимает новую позицию
        tm.run_task(task.Walk([890, 82], self.warrior))
        tm.run_task(task.Group(self.group_m7(), 0, 2))

        # Воин поднимается наврех и приманиевает зомби
        tm.run_task(task.ClearSector([885, 69], self.warrior))
        tm.run_task(task.EnemyClear([self.warrior]))

        #  Иглез собирает лут
        tm.run_task(task.GetLoot(self.igles))

        if tm.defeat:
            return False
        return True

    def mission_7_5(self):
        # Сражение у моста
        self.enter_game()
        tm = TaskManager(self.validate_m7(), True)
        tm.run_task(task.LifeHiredUnits([self.warrior_cl, self.archer_cl, self.ork_cl, self.elf_cl, self.healer_cl]))

        tm.run_task(task.HealthMon(self.hildar, wait_time=200, health_level=0))
        tm.run_task(task.HealthMon(self.healer, wait_time=200))
        group = self.group_m7()
        group.mages_layer = 3
        group.archers_layer = 4
        # Танк занимает новую позицию
        tm.run_task(task.WalkGroup([886, 73], self.warrior))
        tm.run_task(task.Group(group, 0, 2))
        # Воин поднимается наврех и приманивает магов
        resist = tm.run_task(task.FireResist([self.warrior], self.diana, 38))
        tm.run_task(task.Wait(1))
        tm.run_task(task.ClearSector([894, 63], self.warrior))
        tm.run_task(task.ShiftWalk(self.warrior, self.hildar, [0, -3]))
        tm.run_task(task.ManaRegen(self.diana))
        fireball = tm.run_task(
            task.KillEnemyMagic(6, self.diana, 'fire_ball', wait_time=200, max_dst=16))
        fire_arrow = tm.run_task(
            task.KillEnemyMagic(6, self.hildar, 'fire', wait_time=1000, max_dst=10, magic_attack=True))
        tm.run_task(task.EnemyClear([self.warrior]))

        # Танк поднимается к выжившим магам, чтобы подсветить их для Дайны
        tm.run_task(task.ShiftWalk(self.warrior, self.hildar, [0, -5]))
        tm.run_task(task.EnemyClear([self.warrior, self.ork], min_dst=10))

        #  Иглез собирает лут
        tm.run_task(task.GetLoot(units=[self.igles, self.hildar, self.diana]))
        tm.clear_tasks()

        # Вся команда атакует выживших врагов
        tm.run_task(task.WalkGroup([890, 58], self.ork))
        tm.run_task(task.WalkGroup([895, 54], self.hildar))

        #  Иглез собирает лут
        tm.run_task(task.GetLoot(units=[self.igles, self.hildar, self.diana]))
        tm.run_task(task.GetLoot(units=[self.igles, self.hildar, self.diana]))
        if tm.defeat:
            return False
        return True

    def mission_7_6(self):
        # Идём к Эльфу
        self.enter_game()
        tm = TaskManager([self.hildar], False)
        tm.run_task(task.Walk([895, 91], self.hildar))
        tm.run_task(task.Walk([895, 109], self.hildar))
        tm.run_task(task.Walk([963, 105], self.hildar))
        tm.run_task(task.Walk([953, 135], self.hildar))
        tm.run_task(task.Walk([953, 150], self.hildar))
        tm.run_task(task.Walk([956, 171], self.hildar))
        if tm.defeat:
            return False
        return True

    def mission_8(self):
        self.enter_game()
        tm = TaskManager([self.igles, self.hildar, self.diana], True)
        tm.run_task(task.Center(self.igles))
        tm.run_task(task.SetHeroesGroup([self.troll_cl]))
        tm.run_task(task.BindAutoMagic(self.diana, 'fire'))
        tm.run_task(task.Wait(10))
        tm.run_task(task.WalkGroup([904, 90], self.hildar))
        tm.run_task(task.WalkGroup([904, 75], self.hildar))
        tm.run_task(task.Walk([906, 80], self.igles))
        if tm.defeat:
            return False
        return True

    def mission_8_1(self):
        self.enter_game()
        tm = TaskManager([self.igles, self.hildar, self.diana, self.troll], True)
        tm.run_task(task.Center(self.igles))
        tm.run_task(task.LifeHiredUnits([self.troll_cl]))
        tm.run_task(task.Wait(3))
        # Дайна защищает от магии огня
        tm.run_task(task.FireResist([self.igles], self.diana, 38))
        # Ожидаем дракона
        tm.run_task(task.WaitingForAnAttack([self.igles], 3))
        # Иглез спускается чуть ниже
        tm.run_task(task.Walk([906, 81], self.igles))
        # Гильдариус лечит, Дайна атакует
        tm.run_task(task.HealthMon(self.hildar, wait_time=200))
        # Дайна расстреливает всех врагов
        tm.run_task(task.ManaRegen(self.diana))
        tm.run_task(
            task.KillEnemyMagic(6, self.diana, cast_magic='fire', wait_time=3000, max_dst=20, magic_attack=True))
        tm.run_task(task.EnemyClear([self.hildar], min_dst=16))
        tm.clear_tasks()
        tm.run_task(task.GetLoot(self.igles))
        if tm.defeat:
            return False
        return True

    def mission_8_2(self):
        self.enter_game()
        tm = TaskManager([self.igles, self.hildar, self.diana, self.troll], True)
        tm.run_task(task.LifeHiredUnits([self.troll_cl]))
        # Гильдариус лечит
        tm.run_task(task.HealthMon(self.hildar, wait_time=200, health_level=0))
        tm.run_task(task.HealthMon(self.healer, wait_time=200))
        # Идём вниз
        tm.run_task(task.WalkGroup([904, 86], self.hildar))
        tm.run_task(task.Walk([912, 84], self.troll))
        tm.run_task(task.WalkGroup([916, 82], self.troll))
        tm.run_task(task.EnemyClear([self.troll]))
        # Сбор лута
        tm.run_task(task.GetLoot(self.igles))
        tm.run_task(task.Walk([917, 87], self.igles))
        tm.run_task(task.Walk([919, 85], self.hildar))
        tm.run_task(task.Walk([918, 85], self.diana))
        tm.run_task(task.Walk([922, 87], self.troll))
        # Дайна расстреливает мышей
        fireball = tm.run_task(task.KillEnemyMagic(4, self.diana, 'fire_ball', wait_time=200,
                                                   target_type=0, max_dst=8, far_target=False))
        tm.run_task(task.EnemyClear([self.hildar], validation_interval=5, min_dst=7))
        tm.remove_task(fireball)
        fireball = tm.run_task(task.KillEnemyMagic(4, self.diana, 'fire_ball', wait_time=200,
                                                   target_type=0, max_dst=16))
        tm.run_task(task.EnemyClear([self.hildar], min_dst=16))
        tm.run_task(task.GetLoot(self.igles))
        if tm.defeat:
            return False
        return True

    def mission_8_3(self):
        self.mm.enter_game()
        tm = TaskManager([self.igles, self.hildar, self.diana, self.troll], True)
        tm.run_task(task.LifeHiredUnits([self.troll_cl]))

        tm.run_task(task.WalkGroup([926, 85], self.igles, walk_s=False))
        tm.run_task(task.Walk([929, 87], self.troll))
        tm.run_task(task.Walk([926, 83], self.hildar))
        tm.run_task(task.Walk([926, 84], self.diana))

        # Иглез приманивает орков
        tm.run_task(task.ClearSector([942, 91], self.troll))
        tm.run_task(task.ShiftWalk(self.troll, self.diana, [3, 3]))
        # Гильдариус лечит
        tm.run_task(task.HealthMon(self.hildar, wait_time=200, health_level=0))
        tm.run_task(task.HealthMon(self.healer, wait_time=200))

        # Дайна расстреливает всех фарболом
        tm.run_task(task.HealRegen(self.igles))
        tm.run_task(task.ManaRegen(self.hildar))
        tm.run_task(task.ManaRegen(self.diana))
        # Бьём всех ближних
        fireball = tm.run_task(task.KillEnemyMagic(6, self.diana, cast_magic='fire_ball', max_dst=11))
        tm.run_task(task.EnemyClear([self.diana], min_dst=11, validation_interval=2))
        # Затем Тролль сближается с врагами
        tm.remove_task(fireball)
        tm.run_task(task.ShiftWalk(self.troll, self.diana, [7, 7]))
        tm.run_task(task.ManaRegen(self.diana))
        fireball = tm.run_task(task.KillEnemyMagic(6, self.diana, cast_magic='fire_ball', max_dst=20))
        tm.run_task(
            task.KillEnemyMagic(6, self.hildar, cast_magic='fire', wait_time=1000, max_dst=20, magic_attack=True,
                                far_target=False))
        tm.run_task(task.EnemyClear([self.diana], min_dst=20))
        tm.run_task(task.Walk([926, 83], self.hildar))

        tm.win_final = 0

        tm.run_task(task.GetLoot(self.igles))
        tm.run_task(task.GetLoot(self.igles))

        if tm.win:
            self.mm.win_option()

        if tm.defeat:
            return False
        return True

    def group_m9(self):
        # Выстраиваем группу
        group = Group()
        # Назначения для формирования строя
        tank = self.igles
        archers = [self.archer,
                   self.ork]
        mages = [self.elf,
                 self.healer,
                 self.hildar,
                 self.diana]
        group.set_group(tank, archers, mages)
        return group

    def validate_m9(self):
        return [self.igles, self.hildar, self.diana, self.ork, self.healer, self.archer, self.elf]

    def mission_9(self):
        # Миссия 9
        # Назначаем группы.
        # Занимаем позицию слева.
        # Воин идёт к позиции некроманта.
        # Иглез атакует главного некроманта, ожидает диалога и возвращается к команде.
        # Воин возвращается к команде.
        # Дайна отстреливает лучников, кастует на Иглеза защиту от огня.
        # Когда ближних врагов не остаётся, Иглез добивает оставшихся некромантов.

        self.mm.enter_game()
        tm = TaskManager([self.igles, self.hildar, self.diana], True)
        tm.run_task(task.WalkGroup([954, 161], self.hildar))
        tm.run_task(task.Wait(3))
        tm.run_task(task.AllStop())
        tm.run_task(task.Center(self.hildar))

        # Назначение групп для 7 миссии
        heroes = [self.healer_cl, self.ork_cl, self.archer_cl, self.elf_cl]
        tm.run_task(task.SetHeroesGroup(heroes))

        # Иглез занимает позицию
        tm.run_task(task.WalkGroup([938, 157], self.igles))

        # Иглез занимает позицию
        tm.run_task(task.Walk([936, 155], self.igles))

        # Выстраиваем группу
        tm.run_task(task.Group(self.group_m9(), 0, 3))

        # Орк занимает позицию у леса
        tm.run_task(task.Walk([935, 160], self.ork))

        # Воин занимает позицию впереди
        tm.run_task(task.ShiftWalk(self.igles, self.archer, [5, 0]))
        tm.run_task(task.Wait(3))

        if tm.defeat:
            return False
        return True

    def mission_9_1(self):
        # Бой с некромантами. Дайна кастует защиту от магии огня.
        # Иглез идёт к главному некроманту и нападает на него.
        # Затем Иглез отступает на свою позицию. Если у него мало хп, принмает эликсиры.
        # Дайна обстреливает всех файрболом.
        # Когда врагов нет, орк идёт скать выживших, а Дайна их отстреливает.
        self.mm.enter_game()
        tm = TaskManager(self.validate_m9(), True)
        tm.run_task(task.Center(self.igles))
        tm.run_task(task.BindAutoMagic(self.hildar, 'heal'))
        tm.run_task(task.BindAutoMagic(self.healer, 'heal'))
        tm.run_task(task.BindAutoMagic(self.elf, 'stone'))

        # Дайна защищает от магии огня
        tm.run_task(task.FireResist([self.igles], self.diana, wait_time=50, max_dst=7))
        tm.run_task(task.Wait(1))

        tm.run_task(task.Walk([947, 160], self.igles))
        tm.run_task(task.HealRegen(self.igles))
        tm.run_task(task.SelfHealthMon(self.igles, 0, num=4, num_if_red=8))

        # Иглез нападает на некромансера
        kill = tm.run_task(task.KillEnemy(min_dst=0, unit_group=self.igles, max_dst=7, target_type=3, just_attack=True))
        tm.run_task(task.Wait(1))
        # Иглез возвращается домой

        tm.run_task(task.Walk([938, 156], self.igles))
        tm.run_task(task.ShiftWalk(self.igles, self.archer, [5, 0]))

        tm.run_task(task.LifeHiredUnits([self.ork_cl, self.healer_cl, self.archer_cl, self.elf_cl]))
        tm.run_task(task.HealthMon(self.hildar, wait_time=200, health_level=0))
        tm.run_task(task.HealthMon(self.healer, wait_time=200))

        tm.win_final = 0

        tm.run_task(task.ManaRegen(self.diana))
        tm.run_task(task.KillEnemyMagic(6, self.diana, cast_magic='fire_ball', max_dst=16))
        tm.run_task(task.EnemyClear([self.diana], min_dst=16))

        # Орк подходит
        tm.run_task(task.Walk([937, 160], self.ork))
        tm.run_task(task.Center(self.igles))

        tm.run_task(task.EnemyClear([self.igles], min_dst=16))

        # Иглез отправляется на охоту
        tm.run_task(task.HealRegen(self.igles))
        top = tm.run_task(task.WalkS([944, 151], self.igles))
        tm.run_task(task.BindAutoMagic(self.healer, 'chain'))
        btn = tm.run_task(task.WalkGroup([942, 150], self.hildar))
        btn = tm.run_task(task.WalkGroup([952, 160], self.igles))
        btn = tm.run_task(task.WalkGroup([947, 161], self.hildar))
        top = tm.run_task(task.WalkS([954, 160], self.igles))
        tm.clear_tasks()

        tm.run_task(task.GetLoot(units=[self.igles, self.diana, self.hildar]))

        if tm.win:
            self.mm.win_option()

        if tm.defeat:
            return False
        return True

    def mission_10(self):
        self.mm.enter_game()

        tm = TaskManager([self.igles], True)

        # Центрирование группы
        tm.run_task(task.Center(self.igles))
        tm.run_task(task.BindAutoMagic(self.hildar, 'fire'))
        tm.run_task(task.SelfHealthMon(self.igles, 0, num=4, num_if_red=8))
        tm.run_task(task.HealRegen(self.igles))
        tm.run_task(task.Walk([898, 153], self.igles))
        tm.run_task(task.Walk([907, 153], self.igles))
        tm.run_task(task.Walk([923, 136], self.igles))
        tm.run_task(task.Walk([935, 134], self.igles))
        tm.run_task(task.Walk([950, 135], self.igles))
        tm.run_task(task.Wait(3))

        if tm.defeat:
            return False
        return True

    def mission_10_1(self):
        self.mm.enter_game()
        tm = TaskManager([self.igles], True)
        # Центрирование группы
        tm.run_task(task.Center(self.igles))
        tm.run_task(task.Walk([955, 137], self.igles))
        tm.run_task(task.Center(self.igles))
        tm.run_task(task.Wait(1))
        tm.run_task(task.Teleport(self.igles, [6, -6]))
        tm.run_task(task.Wait(2))

        # Центрирование группы
        tm.run_task(task.Center(self.igles))

        tm.win_final = False

        # Мониторинг здоровья
        tm.run_task(task.SelfHealthMon(self.igles, 0, num=4, num_if_red=8))
        tm.run_task(task.HealRegen(self.igles))

        # Центрирование группы
        tm.run_task(task.Walk([950, 115], self.igles))
        tm.run_task(task.Walk([944, 111], self.igles))
        tm.run_task(task.Walk([950, 115], self.igles))
        tm.run_task(task.Walk([944, 111], self.igles))

        tm.run_task(task.GetLoot(self.igles))

        if tm.win:
            self.mm.win_option()

        if tm.defeat:
            return False
        return True

    def validate_m11(self):
        return [self.igles, self.hildar, self.diana, self.catapult, self.en1, self.en2, self.en3]

    def units_m11(self):
        return [self.catapult_cl, self.en1_cl, self.en2_cl, self.en3_cl]

    def group_m11(self, a=3, m=4):
        # Выстраиваем группу
        group = Group()
        # Назначения для формирования строя
        tank = self.igles
        archers = [self.en1, self.en2, self.en3]
        mages = [self.hildar, self.diana]
        group.archers_layer = a
        group.mages_layer = m
        group.set_group(tank, archers, mages)
        return group

    def mission_11(self):
        self.enter_game()
        tm = TaskManager([self.igles, self.hildar, self.diana], True)

        tm.run_task(task.Center(self.diana))
        tm.run_task(task.KillEnemy(3, self.diana, max_dst=6, far_target=False, target_type=3, just_attack=True))

        # Мониторинг здоровья
        tm.run_task(task.SelfHealthMon(self.igles, 0))
        # Лечим всех у кого красное хп
        tm.run_task(task.HealthMon(self.hildar, wait_time=200, health_level=0))
        tm.run_task(task.HealthMon(self.diana, wait_time=200, health_level=0))
        tm.run_task(task.BindAutoMagic(self.hildar, 'fire'))

        tm.run_task(task.Wait(3))
        tm.run_task(task.KillEnemy(3, self.hildar, max_dst=6, far_target=False, target_type=3, just_attack=True))
        tm.run_task(task.Wait(3))

        tm.run_task(task.WalkGroup([997, 167], self.igles))
        tm.run_task(task.WalkGroup([994, 163], self.igles))
        tm.run_task(task.WalkGroup([994, 163], self.hildar))
        tm.run_task(task.Wait(5))
        tm.run_task(task.AllStop())
        tm.run_task(task.Center(self.igles))

        # Назначение наёмника
        tm.run_task(task.SetHeroesGroup([self.catapult_cl]))
        tm.run_task(task.ShiftWalk(self.hildar, self.igles, [-2, 4]))
        tm.run_task(task.ShiftWalk(self.diana, self.igles, [0, 4]))
        tm.run_task(task.Wait(2))
        tm.run_task(task.ShiftWalk(self.igles, self.diana, [2, 0]))
        tm.run_task(task.Wait(1))
        # Назначение групп для 10 миссии
        tm.run_task(task.SetHeroesGroup([self.en1_cl]))
        tm.run_task(task.SetHeroesGroup([self.en2_cl]))
        tm.run_task(task.SetHeroesGroup([self.en3_cl]))

        # tm.run_task(task.ShiftWalk(self.en1, self.igles, [-2, 6]))
        # tm.run_task(task.ShiftWalk(self.en2, self.igles, [0, 6]))
        # tm.run_task(task.ShiftWalk(self.en3, self.igles, [2, 6]))

        # Назначаем большой элексир здоровья на горячуюю клавишу
        color = [0, 10, 208, 104, 120]
        act = Actions()
        tm.run_task(task.BindHotKeyInvent(self.igles, act.heal_up_key, color))
        # Назначаем элексир регенерации маны
        color = [0, 0, 64, 68, 88]
        tm.run_task(task.BindHotKeyInvent(self.hildar, act.mana_up_key, color))
        if tm.defeat:
            return False
        return True

    def mission_11_1(self):
        self.enter_game()
        tm = TaskManager(self.validate_m11(), True)
        tm.run_task(task.Center(self.igles))
        tm.run_task(task.LifeHiredUnits(self.units_m11()))
        # tm.run_task(task.Group(self.group_m11(), 1, 2))
        # tm.run_task(task.SetOtherHeroesGroup(self.other))

        # Лечим всех у кого красное хп
        tm.run_task(task.HealthMon(self.hildar, wait_time=200, health_level=0))
        tm.run_task(task.HealthMon(self.diana, wait_time=200, health_level=0))

        tm.run_task(task.BindAutoMagic(self.en1, 'chain'))
        tm.run_task(task.BindAutoMagic(self.en2, 'heal'))
        tm.run_task(task.BindAutoMagic(self.en3, 'chain'))

        tm.run_task(task.WalkGroup([996, 146], self.en1))
        tm.run_task(task.Wait(5))
        tm.run_task(task.WalkGroup([996, 135], self.en1, walk_s=False))
        tm.run_task(task.Wait(5))
        tm.run_task(task.WalkGroup([996, 112], self.en1, walk_s=False))
        tm.run_task(task.Wait(5))
        tm.run_task(task.WalkGroup([994, 111], self.en1, walk_s=False))
        tm.run_task(task.Walk([986, 106], self.igles))
        tm.run_task(task.Wait(2))

        if tm.defeat:
            return False
        return True

    def mission_11_2(self):
        self.enter_game()
        tm = TaskManager(self.validate_m11(), True)
        tm.run_task(task.Center(self.igles))
        tm.run_task(task.LifeHiredUnits(self.units_m11()))
        color = [208, 208, 176]
        # tm.run_task(task.ShiftWalkColor(self.other, [-162, 200], color))
        tm.run_task(task.Wait(1))
        tm.run_task(task.ShiftWalkColor(self.igles, [30, 70], color))
        tm.run_task(task.Wait(2))
        tm.run_task(task.Teleport(self.igles, [-3, -2]))
        tm.run_task(task.Wait(2))
        tm.run_task(task.Center(self.igles))
        tm.run_task(task.ShiftWalkColor(self.diana, [-140, 72], color))
        tm.run_task(task.ShiftWalkColor(self.hildar, [-205, 72], color))
        tm.run_task(task.ShiftWalkColor(self.en3, [-235, 72], color))
        tm.run_task(task.ShiftWalkColor(self.en2, [-265, 72], color))
        tm.run_task(task.ShiftWalkColor(self.en1, [-300, 72], color))
        tm.run_task(task.ShiftWalkColor(self.catapult, [-88, 110], color))
        tm.run_task(task.BindAutoMagic(self.en1, 'heal'))
        tm.run_task(task.BindAutoMagic(self.en3, 'heal'))
        tm.run_task(task.BindAutoMagic(self.diana, 'grad'))

        if tm.defeat:
            return False
        return True

    def mission_11_3(self):
        self.enter_game()
        tm = TaskManager(self.validate_m11(), True)
        tm.run_task(task.Center(self.igles))
        tm.run_task(task.LifeHiredUnits(self.units_m11()))

        # Мониторинг здоровья
        tm.run_task(task.SelfHealthMon(self.igles, 0, num=3, num_if_red=5))
        # Регенерация здоровья и маны
        tm.run_task(task.HealRegen(self.igles))

        tm.run_task(task.ClearSector([983, 96], self.igles, first_abs=[982, 102]))
        tm.run_task(task.ShiftWalk(self.igles, self.hildar, [0, -6], 1))
        tm.run_task(task.HealthMon(self.hildar, wait_time=200, health_level=0))
        tm.run_task(task.HealthMon(self.diana, wait_time=200, health_level=0))

        tm.run_task(task.Wait(3))

        # Регенерация здоровья и маны
        tm.run_task(task.HealRegen(self.igles))
        tm.run_task(task.ManaRegen(self.diana))
        tm.run_task(task.ManaRegen(self.hildar))
        grad = tm.run_task(task.KillEnemyMagic(7, self.diana, cast_magic='grad', max_dst=9, wait_time=500, up_mana=3,
                                               far_target=False))
        fire1 = tm.run_task(task.KillEnemyMagic(7, self.hildar, cast_magic='fire_ball', max_dst=10, wait_time=200,
                                                up_mana=1, far_target=False))
        cat = tm.run_task(task.KillEnemy(5, self.catapult, max_dst=20, wait_time=3000))

        tm.run_task(task.Center(self.igles))
        clear = tm.run_task(task.EnemyClear([self.igles], min_dst=3, validation_interval=2))
        tm.run_task(task.ShiftWalk(self.igles, self.hildar, [-2, -6], 1))
        tm.remove_task(grad)
        tm.remove_task(fire1)

        tm.run_task(task.ManaRegen(self.diana))
        tm.run_task(task.ManaRegen(self.hildar))
        fire1 = tm.run_task(task.KillEnemyMagic(7, self.hildar, cast_magic='fire_ball', max_dst=12, wait_time=200,
                                                up_mana=1))
        fire2 = tm.run_task(task.KillEnemyMagic(7, self.diana, cast_magic='fire_ball', max_dst=12, wait_time=200,
                                                up_mana=1))
        tm.run_task(task.Wait(2))
        tm.run_task(task.Center(self.igles))
        clear = tm.run_task(task.EnemyClear([self.igles], min_dst=16, validation_interval=2))
        tm.run_task(task.ShiftWalk(self.igles, self.en1, [-3, -6], 1))
        tm.run_task(task.Wait(2))
        tm.run_task(task.Center(self.igles))
        clear = tm.run_task(task.EnemyClear([self.igles], min_dst=16))

        # Сбрасываем настройки и идём дальше
        tm = TaskManager([self.igles], True)
        tm.run_task(task.Center(self.igles))
        tm.run_task(task.SelfHealthMon(self.igles, 0, num=3, num_if_red=5))
        tm.run_task(task.HealRegen(self.igles))

        # Иглез идёт по координатам и вскрывает всё, что находит по пути
        clear = tm.add_task(task.EnemyClear([self.igles], min_dst=16))
        kill = tm.add_task(task.KillEnemy(0, self.igles, max_dst=6, far_target=False))
        walk2 = tm.add_task(task.WalkS([984, 90], self.igles), block_task_id=clear)
        walk3 = tm.add_task(task.WalkS([982, 82], self.igles), walk2, block_task_id=clear)
        tm.run()

        if tm.defeat:
            return False
        return True

    def mission_11_5(self):
        self.enter_game()
        tm = TaskManager([self.igles], True)
        tm.run_task(task.Center(self.igles))
        tm.add_task(task.SelfHealthMon(self.igles, 0, num=3, num_if_red=5))
        tm.run_task(task.HealRegen(self.igles))

        # Иглез приманивает некромантов
        tm.add_task(task.ClearSector([979, 74], self.igles, first_abs=[982, 102], type_to_kill=[4]))
        wait = tm.add_task(task.Wait(5))
        tm.add_task(task.SpeedMinus(), wait)
        tm.run()
        tm.run_task(task.ShiftWalk(self.igles, self.hildar, [0, -6], 1))

        if tm.defeat:
            return False
        return True

    def mission_11_6(self):
        self.enter_game()
        tm = TaskManager([self.igles], True)
        tm.run_task(task.Center(self.igles))
        tm.add_task(task.SelfHealthMon(self.igles, 0, num=3, num_if_red=5))

        tm.run_task(task.HealthMon(self.hildar, wait_time=200, health_level=0))
        tm.run_task(task.HealthMon(self.diana, wait_time=200, health_level=0))
        # Регенерация здоровья и маны
        tm.run_task(task.HealRegen(self.igles))
        tm.run_task(task.ManaRegen(self.diana))
        tm.run_task(task.ManaRegen(self.hildar))

        tm.run_task(task.LifeHiredUnits(self.units_m11()))
        tm.run_task(task.Wait(3))
        tm.run_task(task.Center(self.igles))
        grad = tm.run_task(task.KillEnemyMagic(5, self.diana, cast_magic='grad', max_dst=9, wait_time=500, up_mana=3))
        fire1 = tm.run_task(task.KillEnemyMagic(5, self.hildar, cast_magic='fire_ball', max_dst=10, wait_time=200,
                                                up_mana=1))
        cat = tm.run_task(task.KillEnemy(5, self.catapult, max_dst=10, wait_time=3000))

        tm.run_task(task.Center(self.igles))
        tm.run_task(task.Wait(20))
        tm.run_task(task.SpeedPlus())
        clear = tm.run_task(task.EnemyClear([self.igles], min_dst=3, validation_interval=2))

        if tm.defeat:
            return False
        return True

    def mission_11_7(self):
        self.enter_game()
        tm = TaskManager([self.igles], True)
        tm.run_task(task.Center(self.igles))
        tm.add_task(task.SelfHealthMon(self.igles, 0, num=3, num_if_red=5))
        tm.run_task(task.HealRegen(self.igles))

        # Иглез убивает некромантов и возвращается к команде
        # Эксперементальная функция поиска в темноте. Иногда находит врагов там, где их нет,
        # собирая их из костей и мешков.
        tm.add_task(task.SpeedMinus())
        enemy_name = 'СКЕЛЕТ-МАГ'
        cat = tm.run_task(task.KillEnemy(5, self.catapult, max_dst=20, wait_time=3000,
                                         target_name=enemy_name, validate_target=True))

        kill = tm.run_task(task.KillEnemy(0, self.igles, max_dst=6, far_target=False,
                                          target_name=enemy_name, wait_time=1000, validate_target=True,
                                          enemy_detect_block=3))

        walk1 = tm.run_task(task.Walk([982, 98], self.igles), block_task_id=kill)

        tm.run_task(task.Center(self.igles))
        walk1 = tm.run_task(task.Walk([982, 102], self.igles), block_task_id=kill)
        tm.run_task(task.SpeedPlus())

        tm.remove_task(kill)
        tm.remove_task(cat)
        tm.run_task(task.Wait(3))
        tm.run_task(task.ShiftWalk(self.igles, self.hildar, [-1, -2], 1))
        tm.run_task(task.Wait(5))
        tm.run_task(task.Center(self.igles))
        # Вся команда атакует Урда
        cat = tm.run_task(task.KillEnemy(5, self.catapult, max_dst=20, wait_time=10000))
        fire1 = tm.run_task(task.KillEnemyMagic(5, self.diana, cast_magic='fire', max_dst=20, wait_time=10000,
                                                magic_attack=True))
        fire2 = tm.run_task(task.KillEnemyMagic(5, self.hildar, cast_magic='fire', max_dst=20, wait_time=10000,
                                                magic_attack=True))

        chain1 = tm.run_task(task.KillEnemyMagic(5, self.en1, cast_magic='chain', max_dst=20, wait_time=1000))
        chain2 = tm.run_task(task.KillEnemyMagic(5, self.en2, cast_magic='chain', max_dst=20, wait_time=1000))
        chain3 = tm.run_task(task.KillEnemyMagic(5, self.en3, cast_magic='chain', max_dst=20, wait_time=1000))
        tm.run_task(task.EnemyClear([self.igles], min_dst=20, validation_interval=2, max_wait=300))
        tm.run()
        # Если Урд убегает - перезапускаем
        if not tm.win:
            tm.defeat = 1

        if tm.defeat:
            return False
        return True
