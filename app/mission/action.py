# Действия, которые порождают задачи
from app.commands import Mouse, Color, Keys
from app.ineterface import MainMenu
from app.mission.actions import Actions, Magic
from app.mission.monitor import Names
from app.ineterface import pr
import app.config as cfg


class Action:
    act_id = 0

    # Тип действия
    # 0 - не указано,
    # 1 - движение,
    # 2 - центровка
    # 5 - Каст магии
    # 10 - Поиск врага
    act_type = 0

    # Приоритет выполнения
    priority = 10
    # id родительской задачи
    task_id = 0
    # id задачи, котороая может заблокировать действие
    block_task_id = 0
    # Может ли задача быть заблокирована
    blocked = True

    # Группа, котороя должна совершить действие
    unit_group = 1

    # Цель
    unit_target = 0

    act_name = ''

    # Используемые классы
    act = Actions()
    magic = Magic()
    mouse = Mouse()
    keys = Keys()

    # Статус поражения
    defeat = 0

    # Результат выполнения действия
    result = 0

    def __init__(self):
        self.act_id = 0
        self.act_type = 0
        self.priority = 10
        self.task_id = 0
        self.block_task_id = 0
        self.blocked = True
        self.unit_group = 1
        self.unit_target = 0
        self.act_name = ''
        self.defeat = 0
        self.result = 0

    def do(self):
        # Выполнить действие
        pass

    def module_name(self):
        return 'Action-' + str(self.task_id) + '-' + str(self.act_id)


class Center(Action):
    # Центрирование группы
    priority = 1
    act_type = 2
    blocked = False

    def __init__(self, unit_group=1):
        self.priority = 1
        self.act_type = 2
        self.blocked = False

        self.unit_group = unit_group
        self.act_name = 'Центрирование группы: ' + str(unit_group)

    def do(self):
        # После центрирования группы нужно подождать 20 мс, чтобы обновился экран, с учётом 60 кадров/сек.
        self.act.center_group(self.unit_group, 20)


class Walk(Action):
    # Действие по передвижению к цели
    abs_crd = []
    priority = 5
    act_type = 1

    def __init__(self, unit_group=1, abs_crd=[]):
        self.priority = 5
        self.act_type = 1

        self.unit_group = unit_group
        self.abs_crd = abs_crd
        self.act_name = 'Движение группы: ' + str(unit_group) + ' к координатам: ' + str(abs_crd[0]) + ' ' + str(
            abs_crd[1])

    def do(self):
        self.act.select_group(self.unit_group)
        self.act.m()
        self.mouse.left(self.abs_crd[0], self.abs_crd[1])

    pass


class WalkS(Walk):

    def do(self):
        self.act.select_group(self.unit_group)
        self.act.s()
        self.mouse.left(self.abs_crd[0], self.abs_crd[1])


class Shift(Action):
    # Действие по передвижению к цели
    abs_crd = []
    priority = 1
    act_type = 1
    target_group = 1

    def __init__(self, unit_group=1, target_group=1, abs_crd=[]):
        self.priority = 1
        self.act_type = 1

        self.unit_group = unit_group
        self.target_group = target_group
        self.abs_crd = abs_crd
        self.act_name = 'Смещение группы: ' + str(unit_group) + ' группе ' + str(
            self.target_group) + ' по координатам: ' + str(abs_crd)

    def do(self):
        self.act.select_group(self.unit_group)
        self.act.m()
        self.mouse.left(self.abs_crd[0], self.abs_crd[1])

    pass


class GroupWalkS(Walk):
    # Движение группы к цели
    def do(self):
        self.act.select_all()
        self.act.s()
        self.mouse.left(self.abs_crd[0], self.abs_crd[1])


class GroupWalk(Walk):
    # Движение группы к цели
    def do(self):
        self.act.select_all()
        self.act.m()
        self.mouse.left(self.abs_crd[0], self.abs_crd[1])


class GetLoot(Action):
    # Сбор мешков
    priority = 4
    act_type = 3

    def __init__(self, unit_group=1):
        self.priority = 4
        self.act_type = 3

        self.unit_group = unit_group
        self.act_name = 'Сбор лута группой ' + str(unit_group)

    def do(self):
        self.act.select_group(self.unit_group)
        self.act.p()

    pass


class Heal(Action):
    # Лечение цели
    abs_crd = []
    priority = 1
    act_type = 4

    def __init__(self, unit_group=1, abs_crd=[]):
        self.priority = 1
        self.act_type = 4
        self.unit_group = unit_group
        self.abs_crd = abs_crd
        self.act_name = 'Лечение цели ', abs_crd, ' группой ', unit_group

    def do(self):
        self.act.select_group(self.unit_group)
        self.act.magic_heal()
        self.mouse.left(self.abs_crd[0], self.abs_crd[1])


class SelfHeal(Action):
    # Принятие элексиров
    priority = 0
    act_type = 4
    # Количество нажатий
    custom_num = 1

    def __init__(self, unit_group=1, custom_num=1):
        self.unit_group = unit_group
        self.act_name = 'Принятие элексира здоровья группой ', unit_group
        self.custom_num = custom_num

    def do(self):
        self.act.select_group(self.unit_group)

        for i in range(self.custom_num):
            self.act.heal_up()


class CastMagic(Action):
    abs_crd = []
    act_type = 5
    target_id = 0
    magic_name = 'fire'
    cursor = True
    attack = False
    up_mana=0

    def __init__(self, unit_group=1, abs_crd=[], magic_name='fire', unit_target=0, attack=False, cursor=True,
                 priority=2, up_mana=0):
        self.act_type = 5
        self.target_id = 0

        self.unit_group = unit_group
        self.abs_crd = abs_crd
        self.magic_name = magic_name
        self.unit_target = unit_target
        self.attack = attack
        self.cursor = cursor
        self.priority = priority
        self.up_mana = up_mana
        self.act_name = 'Группа ' + str(unit_group) + ' использует магию ' + magic_name + ', на цель ' + str(
            self.unit_target) + ', по координатам ' + str(abs_crd)

    def do(self):
        self.act.select_group(self.unit_group)
        self.magic.select_magic(self.magic_name)
        if self.cursor:
            # Валидация курсора
            self.mouse.move(self.abs_crd[0], self.abs_crd[1], 20)
            if not self.act.color.validate(self.abs_crd[0] - 3, self.abs_crd[1], 248, 180, 80):
                # Валидация не пройдена. Отменяем атаку.
                pr(self.module_name(), 'Ошибка валидации курсора. Отмена действия')
                self.mouse.mouse_hide()
                return

        if not self.attack:
            self.mouse.left(self.abs_crd[0], self.abs_crd[1])
        else:
            if not self.magic.is_auto(self.magic_name):
                self.act.auto_magic()
            self.act.a()
            self.mouse.left(self.abs_crd[0], self.abs_crd[1])

        if self.up_mana:
            for i in range(self.up_mana):
                self.act.mana_up()
        self.result = 1


class Attack(CastMagic):
    # Атака на цель
    priority = 2
    validate_target = False
    target_id = 0
    stop = False

    def __init__(self, unit_group=1, abs_crd=[], target_code='', target_id=0, stop=False):
        self.priority = 2
        self.validate_target = False

        self.unit_group = unit_group
        self.abs_crd = abs_crd
        self.target_code = target_code
        self.target_id = target_id
        self.stop = stop
        self.act_name = 'Атака группы ', unit_group, ' по координатам ', abs_crd

    def do(self):
        self.act.select_group(self.unit_group)
        if self.target_code:
            names = Names()
            self.mouse.move(self.abs_crd[0], self.abs_crd[1], 10)
            # Получаем имя юнита
            name = names.get_name()
            self.mouse.mouse_hide()
            self.result = name
            if name == self.target_code:
                pr(self.module_name(), 'Цель подтверждена', name)
            else:
                pr(self.module_name(), 'Ошибка атаки, имя цели не совпадает с заданым')
                if self.stop:
                    self.act.select_group(self.unit_group)
                    pr(self.module_name(), 'Остановка', self.unit_group)
                    self.act.t()
                return

        self.act.a()
        self.mouse.left(self.abs_crd[0], self.abs_crd[1])
        if not self.target_code:
            self.result = 1


class StoneWall(CastMagic):
    priority = 2
    magic_name = 'stone_wall'

    def __init__(self, unit_group=1, abs_crd=[]):
        self.priority = 2
        self.magic_name = 'stone_wall'
        self.unit_group = unit_group
        self.abs_crd = abs_crd
        self.act_name = 'Каменная стена группы ', unit_group, ' на координаты ', abs_crd


class Retreat(Action):
    # Отступление игрока
    priority = 3
    act_type = 5

    def __init__(self, unit_group=1):
        self.priority = 3
        self.act_type = 5
        self.unit_group = unit_group
        self.act_name = 'Герой', unit_group, ' отступает'

    def do(self):
        self.act.select_group(self.unit_group)
        self.act.r()


class Stop(Action):
    # Остановка игрока
    priority = 3
    act_type = 5

    def __init__(self, unit_group=1):
        self.priority = 3
        self.act_type = 5
        self.unit_group = unit_group
        self.act_name = 'Герой', unit_group, ' останавливается'

    def do(self):
        self.act.select_group(self.unit_group)
        self.act.t()


class BindAutoMagic(Action):
    # Назначение авто-магии
    priority = 1
    act_type = 5
    magic_name = ''

    def __init__(self, unit_group=1, magic_name=''):
        self.priority = 1
        self.act_type = 5

        self.unit_group = unit_group
        self.magic_name = magic_name
        self.act_name = 'Герой', unit_group, ' назначает авто-магию', magic_name

    def do(self):
        self.act.select_group(self.unit_group)
        # Проверяем цвет. Если он не соответствует магии, назначаем бинд.
        if not self.magic.is_auto(self.magic_name):
            self.magic.select_magic(self.magic_name)
            self.act.auto_magic()

    pass


class BindHotKeyInvent(Action):
    # Назначение авто-магии
    priority = 1
    act_type = 5
    key = ''
    color = []

    def __init__(self, unit_group=1, key='', color=[]):
        self.priority = 1
        self.act_type = 5

        self.unit_group = unit_group
        self.key = key
        self.color = color
        self.act_name = 'Герой', unit_group, ' назначает горячую клавишу', key

    def do(self):
        self.act.select_group(self.unit_group)
        mm = MainMenu()
        mm.open_invent()
        crd = mm.find_in_invent_mission(self.color)
        if crd:
            # Назначаем горячие клавиши
            self.mouse.move(crd[0], crd[1])
            self.act.shift_key(self.key)
        mm.close_invent()


class ValidateLife(Action):
    # Валидация жизни юнита
    priority = 7
    act_type = 5
    hero = []

    color = Color()

    def __init__(self, hero=[]):
        self.priority = 7
        self.act_type = 5

        self.hero = hero
        self.unit_group = hero.group
        self.act_name = 'Проверка жизни группы ', self.unit_group

    def do(self):
        if not self.validate_life(20):
            # Вторая проверка
            if not self.validate_life(20):
                # Герой мёртв
                pr(self.module_name(), 'Герой мёртв', self.unit_group)
                self.result = 1

    def validate_life(self, wait=20):
        self.act.select_group(self.unit_group, wait)
        f = self.hero.face_pixel
        return self.color.validate(f[0], f[1], f[2], f[3], f[4])


class AllStop(Action):
    # Остановка группы
    priority = 5
    act_type = 2

    def __init__(self):
        self.priority = 5
        self.act_type = 2

        self.act_name = 'Остановка группы'

    def do(self):
        self.act.select_all()
        self.act.t()


class HealRegen(Action):
    # Регенерация здоровья группы
    priority = 2
    act_type = 2

    def __init__(self, unit_group=1):
        self.priority = 2
        self.act_type = 2
        self.unit_group = unit_group
        self.act_name = 'Регенерация здоровья группы: ' + str(unit_group)

    def do(self):
        # После центрирования группы нужно подождать 20 мс, чтобы обновился экран, с учётом 60 кадров/сек.
        self.act.select_group(self.unit_group)
        self.act.heal_regen()


class ManaRegen(Action):
    # Регенерация здоровья группы
    priority = 2
    act_type = 2

    def __init__(self, unit_group=1):
        self.priority = 2
        self.act_type = 2
        self.unit_group = unit_group
        self.act_name = 'Регенерация маны группы: ' + str(unit_group)

    def do(self):
        # После центрирования группы нужно подождать 20 мс, чтобы обновился экран, с учётом 60 кадров/сек.
        self.act.select_group(self.unit_group)
        self.act.mana_regen()


class Speed(Action):
    # Изменение скорости
    priority = 0
    act_type = 2
    speed = 4
    plus = True

    def __init__(self, speed=1, plus=True):
        self.priority = 0
        self.act_type = 2

        self.speed = speed
        self.plus = plus
        if plus:
            self.act_name = 'Увеличение скорости ' + str(speed)
        else:
            self.act_name = 'Уменьшение скорости ' + str(speed)

    def do(self):
        if self.plus:
            self.act.speed_plus(self.speed)
        else:
            self.act.speed_minus(self.speed)


class MagicMenu(Action):
    # Изменение скорости
    priority = 0
    act_type = 2
    show = True

    def __init__(self, show=True):
        self.priority = 0
        self.act_type = 2

        self.show = show
        if show:
            self.act_name = 'Открытие меню магии'
        else:
            self.act_name = 'Скрытие меню магии'

    def do(self):
        mm = MainMenu()
        if self.show:
            mm.show_magic_menu()
        else:
            mm.hide_magic_menu()


class SetGroups(Action):
    # Назначение группы
    priority = 5
    act_type = 2
    units = []
    ms = []

    def __init__(self, units=[], ms=[]):
        self.priority = 5
        self.act_type = 2

        self.units = units
        self.ms = ms
        self.act_name = 'Назначение групп'

    def screen_to_abs(self, x, y):
        # Перевод координат экрана в абсолютные
        screen_crd = [x + cfg.screen_shift['x'], y + cfg.screen_shift['y']]
        return screen_crd

    def do(self):
        other = self.ms.other
        color = Color()
        total = 0
        if other:
            mm = MainMenu()
            mm.hide_magic_menu()
            for item in other:
                if total >= len(self.units):
                    break

                screen = self.ms.get_shift(self.screen_to_abs(item[1], item[0]))
                self.mouse.move(screen[0], screen[1], 30)
                for hero in self.units:
                    f = hero.face_pixel
                    if color.validate(f[0], f[1], f[2], f[3], f[4]):
                        # Назначаем группу героя
                        self.mouse.left(screen[0], screen[1], sleep=100)
                        self.act.make_group(hero.group)
                        pr(self.module_name(), 'Создана группа', hero.group, 'по координатам', screen)
                        total += 1
                        pass
            mm.show_magic_menu()
        if total < len(self.units):
            pr(self.module_name(), 'Провал. Количество юнитов меньше необходимых групп')
            self.defeat = 1


class SetOtherGroups(Action):
    # Назначение группы
    priority = 5
    act_type = 2
    unit_group = 6
    units = []
    ms = []

    def __init__(self, unit_group=6, ms=[]):
        self.priority = 5
        self.act_type = 2
        self.units = []

        self.ms = ms
        self.unit_group = unit_group
        self.act_name = 'Назначение групп'

    def screen_to_abs(self, x, y):
        # Перевод координат экрана в абсолютные
        screen_crd = [x + cfg.screen_shift['x'], y + cfg.screen_shift['y']]
        return screen_crd

    def do(self):
        other = self.ms.other
        if other:
            mm = MainMenu()
            mm.hide_magic_menu()
            self.act.select_group(self.unit_group)
            self.keys.keyDown('shift')
            for item in other:
                screen = self.ms.get_shift(self.screen_to_abs(item[1], item[0]))
                self.mouse.move(screen[0], screen[1], 100)
                self.mouse.left(screen[0], screen[1], sleep=100)

            self.keys.keyUp('shift')
            self.act.make_group(self.unit_group)
            mm.show_magic_menu()


class TargetInfo(Action):
    # Информация о цели
    abs_crd = []
    priority = 2
    act_type = 10
    target_id = 0
    stop = False

    def __init__(self, abs_crd=[], target_id=0, unit_group=1, stop=False):
        self.priority = 2
        self.act_type = 10

        self.abs_crd = abs_crd
        self.target_id = target_id
        self.unit_group = unit_group
        self.stop = stop
        self.act_name = 'Информация о цели ', target_id, abs_crd

    def do(self):
        if self.stop:
            self.act.select_group(self.unit_group)
            pr(self.module_name(), 'Остановка', self.unit_group)
            self.act.t()
        self.mouse.move(self.abs_crd[0], self.abs_crd[1], 10)
        # Получаем имя юнита
        names = Names()
        name = names.get_name()
        self.mouse.mouse_hide()
        self.result = name
        pr(self.module_name(), 'Получено имя', name)


class Defeat(Action):
    act_name = 'Мы проиграли'

    def do(self):
        self.defeat = 1
