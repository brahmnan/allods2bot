# Чтение данных с экрана монитора

import cv2
import numpy as np
from mss import mss
import time
import pyautogui as pg

mon = {'top': 31, 'left': 8, 'width': 1024, 'height': 768}
mon_main = {'top': 31, 'left': 8, 'width': 850, 'height': 675}
mon_map = {'top': 49, 'left': 880, 'width': 128, 'height': 128}

# Размер юнита в пикселях, зависит от размера карты
# Может вычисляться в зависимости от размера экрана на миникарте
unit_size = 2


class Heroes:
    # Управление героями
    # TODO Unused
    # Клавиши групп героев
    group = {
        'Igles': 1,
        'Hildar': 2,
        'Diana': 3
    }

    def group_Igles(self):
        return self.group.get('Igles')

    def group_Hildar(self):
        return self.group.get('Hildar')

    pass


def screen_record():
    init_hero = 0
    sct = mss()
    last_time = time.time()
    heroes = Heroes()
    enemies = Enemies()
    map = Map()

    # Цикл
    cicle = 1
    while cicle == 1:

        # Поиск данных на миникарте
        find_in_map(sct, heroes, enemies, map, last_time)

        # Инициализация героя
        # Находим координаты героя, кликаем по карте, а потом на него мышкой, добавляем его в группу
        if init_hero == 0:
            init_heroes(heroes, sct, enemies, map, last_time)
            init_hero = 1

        # Временная остановка цикла
        break

        if cv2.waitKey(25) & 0xFF == ord('q'):
            cv2.destroyAllWindows()
            break
    pass


# Инициализация героев
def init_heroes(heroes, sct, enemies, map, last_time):
    num_hero = 0
    for hero in heroes.coords:
        # Кликаем мышкой по миникарте на пиксель героя
        hero_map_x = hero[0] + mon_map['left']
        hero_map_y = hero[1] + mon_map['top']

        pg.moveTo(hero_map_x, hero_map_y)
        pg.click(hero_map_x, hero_map_y)

        # Находим положение героя на карте
        # Убираем мышку с карты
        pg.moveTo(mon_map['left'] - 50, mon_map['top'] + 50)
        # Обновляем карту
        find_in_map(sct, heroes, enemies, map, last_time)
        # Верхний левый угол карты
        map_top_left = map.map_top_left
        koef = 768 / (map.map_btn_right[1] - map.map_top_left[1] + 2)
        print(koef)
        # Герой
        hero_coords = heroes.coords[num_hero]
        # small map
        sdvig_x = 1.5
        sdvig_y = 1
        # big map
        # sdvig_x = 1
        # sdvig_y = 0.7
        click_x = [(hero_coords[0] + sdvig_x - map.map_top_left[0]) * koef,
                   (hero_coords[1] - sdvig_y - map.map_top_left[1]) * koef]
        moveTo(click_x[0], click_x[1])
        print(hero_coords);
        print(click_x);
        # Находим абсолютные координаты для клика по герою

        num_hero += 1
    pass


def moveTo(x, y):
    pg.moveTo(x + mon['left'], y + mon['top'])
    pass


def click_left(x, y):
    pg.click(x + mon['left'], y + mon['top'])
    pass


def find_in_map(sct, heroes, enemies, map, last_time):
    img = sct.grab(mon_map)

    curr_time = time.time()
    print('loop took {} sec.'.format(curr_time - last_time))
    last_time = curr_time
    img = np.array(img)

    matrix = img

    found = Found()

    # Добавляем информацию о классах
    heroes.newcicle()
    enemies.newcicle()

    found.init(heroes, enemies, map)

    objects = found.objects

    x = 0
    y = 0
    for row in matrix:

        for item in row:
            r = item[0]
            g = item[1]
            b = item[2]
            # print(item, end=' ')

            # Поиск координат карты
            for pixel in objects:
                if pixel[0] == r:
                    if pixel[1] == g:
                        if pixel[2] == b:
                            type = pixel[3]
                            found.add(x, y, type)
            x += 1
        y += 1
        x = 0
        # print()

    # print(found.get_map())
    # print(heroes.coords)
    # print(enemies.coords)

    pass


class Found:
    # bgr
    objects = [
        [248, 252, 248, 0],  # map
        [80, 228, 128, 1],  # hero
        [120, 84, 224, 2],  # enemies
        [80, 112, 224, 3],  # civilian and enemy
    ]

    # map
    map_top_left = [0, 0]
    map_btn_right = [0, 0]

    # heroes
    heroes = 0
    # enemies
    enemies = 0
    map = 0

    def init(self, heroes, enemies, map):
        self.heroes = heroes
        self.enemies = enemies
        self.map = map
        self.map_top_left = [0, 0]
        self.map_btn_right = [0, 0]
        pass

    def get_map(self):
        return [self.map_top_left, self.map_btn_right]

    def add(self, x, y, type):
        # map
        if type == 0:
            if self.map_top_left[0] == 0:
                self.map_top_left = [x, y]
                pass
            if self.map_btn_right[0] < x:
                self.map_btn_right[0] = x
                pass
            if self.map_btn_right[1] < y:
                self.map_btn_right[1] = y
                pass

            self.map.update_map(self.map_top_left, self.map_btn_right)

        # print("found", type, "x:", x, "; y:", y)
        elif type == 1:
            # hero
            self.heroes.add_map_info(x, y)
        elif type == 2 or type == 3:
            # enemy
            self.enemies.add_map_info(x, y)
            pass

        pass

    pass


class Map:
    # map
    map_top_left = [0, 0]
    map_btn_right = [0, 0]

    def update_map(self, top_left, btn_right):
        self.map_top_left = top_left
        self.map_btn_right = btn_right

    pass


# Герои. Класс хранит данные о героях на карте
class Heroes:
    # Типы героев: наш маг, Иглез, наёмники.
    # Версия 1. Отслеживаем мага и Иглеза.
    # Добавление информации о героях на карте

    last_coords = []
    coords = []

    def newcicle(self):
        self.last_coords = self.coords
        self.coords = []
        pass

    def add_map_info(self, x, y):
        # Если размер на карте равен двум
        if unit_size == 2:
            # Добавляем только левую верхнюю точку
            if self.coords:
                for hero in self.coords:
                    if hero[0] == x or hero[0] + 1 == x:
                        continue
                    elif hero[1] == y or hero[1] + 1 == y:
                        continue
                    else:
                        self.coords.append([x, y])
            else:
                self.coords.append([x, y])

        else:
            # Если размер равен пикселю
            self.coords.append([x, y])
            pass
        pass

    pass


# Враги
class Enemies:
    last_coords = []
    coords = []

    def newcicle(self):
        self.last_coords = self.coords
        self.coords = []
        pass

    # Получаем сырае данные о врагах и объектах
    # Необходимо определить где враги и где объекты и идентифицировать врагов
    def add_map_info(self, x, y):
        self.coords.append([x, y])
        pass

    pass


# Герой. Данные о герое
class Hero:
    pass


screen_record()
