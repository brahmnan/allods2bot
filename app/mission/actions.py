# Набор команд управления героем

from app.commands import Keys, Mouse, Color


class Actions:
    # Нажатие клавиш
    keys = Keys()
    mouse = Mouse()
    color = Color()

    # Ожидание 0 ms
    sleep = 10
    # Горячие клавиши
    heal_regen_key = 'f4'
    mana_regen_key = 'f5'
    magic_heal_key = 'f6'
    magic_fire_key = 'f7'
    mana_up_key = 'f7'
    magic_shield_key = 'f8'
    heal_up_key = 'f9'
    light_key = 'f8'

    # Управление героем
    def speed_plus(self, n=4):
        i = 0
        while i < n:
            self.keys.press('add')
            i += 1

    def speed_minus(self, n=4):
        i = 0
        while i < n:
            self.keys.press('subtract')
            i += 1

    def select_all(self):
        # Выбрать всех героев
        self.keys.press('e', sleep=self.sleep)
        pass

    def make_group(self, n):
        # Создать группу
        self.keys.press_two('ctrl', str(n), sleep=self.sleep)
        pass

    def ctrl_key(self, key):
        self.keys.press_two('ctrl', key, sleep=self.sleep)
        pass

    def shift_key(self, key):
        self.keys.press_two('shift', key, sleep=self.sleep)
        pass

    def select_group(self, n, sleep=0):
        # Выбрать группу
        self.keys.press(str(n), sleep)
        pass

    def center_group(self, n, sleep=20):
        # Отцентрировать экран на группе
        self.keys.press_two('alt', str(n), sleep)
        pass

    def a(self, sleep=0):
        # Атаковать
        self.keys.press('a', sleep)
        pass

    def d(self):
        # Защищать
        self.keys.press('d', sleep=self.sleep)
        pass

    def g(self):
        # Держать позицию
        self.keys.press('g', sleep=self.sleep)
        pass

    def m(self):
        # Идит
        self.keys.press('m', sleep=self.sleep)
        pass

    def p(self):
        # Сбор мешков
        self.keys.press('p', sleep=self.sleep)
        pass

    def s(self):
        # Идти в боевой готовности
        self.keys.press('s', sleep=self.sleep)
        pass

    def t(self):
        # Держать позицию
        self.keys.press('t', sleep=self.sleep)
        pass

    def r(self):
        # Отступать
        self.keys.press('r', sleep=self.sleep)
        pass

    def auto_magic(self):
        # Включить автомагию
        self.keys.press_two('ctrl', 'a', sleep=self.sleep)
        pass

    def heal_regen(self):
        # Регенерация здоровья
        self.keys.press(self.heal_regen_key, sleep=self.sleep)
        pass

    def heal_up(self):
        # Восстановление здоровья
        self.keys.press(self.heal_up_key, sleep=self.sleep)
        pass

    def mana_up(self):
        # Восстановление здоровья
        self.keys.press(self.mana_up_key, sleep=self.sleep)
        pass

    def mana_regen(self):
        # Регенерация маны
        self.keys.press(self.mana_regen_key, sleep=self.sleep)
        pass

    def magic_heal(self):
        # Лечение
        self.keys.press(self.magic_heal_key, sleep=self.sleep)
        pass

    def fire_arrow(self):
        # Лечение
        self.keys.press(self.magic_fire_key, sleep=self.sleep)
        pass

    def light(self):
        # Освещение
        self.keys.press(self.light_key, sleep=self.sleep)
        pass

    def use_shield(self, sleep=0):
        # Магический щит
        self.keys.press(self.magic_shield_key, sleep)
        pass

    pass


class Magic:
    # Выбор магии

    mouse = Mouse()
    keys = Keys()
    color = Color()

    # Ожидание 200 ms
    sleep = 0

    # Типы магии
    spell = {
        'fire': [222, 740],
        'fire_ball': [260, 740],
        'fire_res': [336, 740],
        'heal': [374, 740],
        'bless': [412, 740],
        'light_res': [526, 740],
        'chain': [602, 740],
        'lighting': [640, 740],
        'grad': [298, 778],
        'water_res': [336, 778],
        'teleport': [450, 778],
        'shield': [488, 778],
        'earth_res': [526, 778],
        'stone': [564, 778],
        'stone_wall': [602, 778],
        'earth': [640, 778]

    }

    # Magic
    fire_arrow = 'fire'
    heal = 'heal'
    shield = 'shield'
    fire_res = 'fire_res'

    def is_auto(self, magic_name=''):
        # Эта магия уже включена на автомат
        name_crd = self.spell.get(magic_name)
        auto_crd = [name_crd[0] - 11, name_crd[1] + 12]
        if self.color.validate(auto_crd[0], auto_crd[1], 208, 208, 208):
            return True
        return False

    def select_magic(self, magic_name, sleep=10):
        # Выбрать магию
        if self.spell.get(magic_name):
            self.mouse.left(self.spell[magic_name][0], self.spell[magic_name][1], sleep=sleep)
            return True
        return False

    def use_magic(self, magic_name, x, y):
        # Использовать магию на координаты
        if self.select_magic(magic_name):
            self.mouse.left(x, y)
            return True
        return False

    def bind_auto_magic(self, type):
        # Выбрать автомагию
        if self.spell.get(type):
            self.mouse.left(self.spell[type][0], self.spell[type][1], sleep=self.sleep)
            self.keys.press_two('ctrl', 'a', sleep=self.sleep)
        pass

    pass
