# Идея. Добавлять в менеджер задач, задания с разным приоритетом, чтобы менеджер решал их
import time
import app.mission.action as action
import app.config as cfg
from app.mission.monitor import Names
from app.mission.monitor import MainScreen, Positions
from app.mission.heroes import *
from app.mission.store import FoundEnemy
from app.ineterface import pr


class Task:
    # Задача, добавляемая в цикл задач
    # Уникальный номер задачи. Назначается, при добавлении в менеджер задач.
    task_id = 0
    # Родительская задача.
    # 0 - Нет родителя
    # id - родителя. Задача будет выполнятся только после выполнения родительской
    task_parent = 0
    # id высшей задачи, которая может блокировать данную задачу.
    block_task_id = 0
    # Блокировка связанной задачи
    block = 0
    # id - действия, порождённого задачей
    act_id = 0
    last_run_act_id = 0
    last_run_act_result = 0

    # Имя задачи
    task_name = ''

    # Юнит, который должен выполнить задачу
    unit_group = 1

    # Время создания задачи
    start_time = 0

    # Время, по истечении которого, задача снимается
    close_time = 0

    # Что делать в случаи истечения времени
    # 0 - задача считается успешно выполненной
    # 1 - остановка скрипта
    # 2 - загрузка игры
    time_out_action = 0

    # Результат:
    #   0 - действие не требуется,
    #   1 - победа,
    #   2 - остановка скрипта,
    #   3 - загрузка игры
    result = 0

    # Вес задачи:
    #   0 - Основные задчаи. Если их нет, цикл прерывается. Например достижение КТ
    #   1 - Дополнительные задачи. Не влияют на приривание цикла. Например каст защиты от магии.
    weight = 1

    # Статус задачи.
    #   0 - На удаление.
    #   1 - Активная.
    #   2 - Ожидает выполнения родительской.
    status = 1

    # Действие, которое нужно совершить
    action = False
    last_action = False

    # Приоритет задач
    priority_custom = 0

    # Основная информация с экрана
    ms = MainScreen()

    def __init__(self):
        pass

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        self.ms = ms
        self.last_run_act_id = last_run_act_id
        self.last_run_act_result = last_run_act_result
        self.last_action = self.action
        self.action = False

    def get_action(self):
        # Получить задачу
        return self.action

    def module_name(self):
        return 'Задача-' + str(self.task_id)

    pass


class TaskOne(Task):
    # Одинарная задача. Снимается после выполнения.
    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        # Проверяем, когда была последняя команда
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Задача была выполнена, снимаем её
            self.status = 0
            return
        self.default_action()

    def default_action(self):
        pass


class WalkTask(Task):
    # Набор функций, используемых для ходьбы

    def walk_shift_from_mini_map_abs_crd(self, crd):
        # Смещение координаты y для ходьбы
        return [crd[0], crd[1] + 10 + 32]

    def walk_shift_abs_crd(self, crd):
        # Смещение координаты y для ходьбы
        return [crd[0], crd[1] + 10]

    def get_distance(self, path, hero):
        # Дистанция до мини цели
        x_path_sm = path[0] - hero[0]
        y_path_sm = path[1] - hero[1]
        # Дистанция до мини цели
        cpm_dist = round((abs(x_path_sm) ** 2 + abs(y_path_sm) ** 2) ** 0.5, 1)
        return cpm_dist

    def validate_screen_crd(self, crd):
        # Валидация клика в пределах экрана
        if cfg.screen_shift.get('x') < crd[0] < cfg.main_window.get('x') and \
                cfg.screen_shift.get('y') < crd[1] < cfg.main_window.get('ym'):
            return True
        return False


class Walk(WalkTask):
    # Идти к контрольной точке. Задача создаётся в модуле стратегии
    task_name = 'Передвижение к цели'
    priority = 5
    # Основная задача
    weight = 1
    # Координаты группы на экране
    unit_abs = []
    unit_abs_last = []
    # Координаты следующиего шага на экране
    next_click_abs_crd = []

    # Координаты главной КТ, на мини-карте
    main_target_crd = []
    # Координаты, локальной КТ, на мини-карте
    local_target_crd = []
    # Текущие координаты юнита, на мини-карте
    unit_crd = []
    unit_crd_last = []
    # Отношение размера экрана к размеру карты
    k_size = 0
    # Разброс дистации для контрольной точки
    walk_min_ct_radius = 2
    # Разброс дистацнии для локальной цели
    walk_min_move_radius = 5
    walk_main_ct_radius = 5

    # Последняя команда движения
    last_move_time = 0
    # Последнее центрирование
    last_center_time = 0
    # Максимальное количество центрирований подряд
    need_center_max_count = 5
    # Всего центрирований
    need_center_count = 0

    # Влияние передвижения персонажа, на создание нового движения к КТ.
    # Счётчик остановки юнита. Обнуляется после создания действия по передвижению.
    unit_stop = 0
    # Если юнит стоит, больше допустимого, создаётся следующая задача по передвжиению.
    max_unit_stop = 10

    # Центрирование положения героя на карте
    last_center_map_validate = 0

    # Центрируем с небольшим интервалом, чтобы не забивать эфир в случае,
    # когда герой стоит на горе и не проходит валидацию.
    # Если герой стоит, понижаем интервал обновления центра.
    center_map_validate_interval = 1

    def __init__(self, target_abs=[], unit_group=1):
        self.unit_abs = []
        self.unit_abs_last = []
        self.next_click_abs_crd = []
        self.main_target_crd = []
        self.local_target_crd = []
        self.unit_crd = []
        self.unit_crd_last = []

        self.set_main_target(target_abs)
        self.unit_group = unit_group

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        # Получаем координаты героя на мини-карте
        self.unit_crd_last = self.unit_crd
        self.unit_crd = self.get_hero_map_crd()
        if not self.unit_crd:
            pr(self.module_name(), 'Координаты героя не найдены', self.ms.cmd)
            if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id \
                    and self.last_action.act_type == 2:
                pr(self.module_name(), 'Берём координаты по центру карты')
                # Последняя выполненная задача - центрирование
                # Берём координаты по центру карты
                # y,x
                unit_abs = [332, 425]
                hero_shift = self.ms.get_shift(unit_abs)
                self.unit_crd = self.abs_to_map_crd(hero_shift)
            else:
                # Центрируем
                self.action = action.Center(self.unit_group)
                return

        # Получаем координаты героя на экране
        self.unit_abs_last = self.unit_abs
        self.unit_abs = self.ms.get_hero_abs(self.unit_group)

        # Требуется ли центрирование?
        if self.need_center():
            self.action = action.Center(self.unit_group)
            return

        # Была ли достигнута локальная цель?
        task_target_done = False
        action_target_done = False
        if self.local_target_crd:
            # Да. Проверяем её достижение
            # Расстояние до локальной цели
            local_target_dst = self.get_distance(self.local_target_crd, self.unit_crd)

            # Минимальный радиус до цели
            min_radius = self.walk_min_ct_radius
            if local_target_dst > 10:
                # При большом расстоянии до цели, перемещаемся свободно
                min_radius = self.walk_min_move_radius

            if local_target_dst < min_radius:
                main_target_dst = self.get_distance(self.main_target_crd, self.unit_crd)
                # Была ли достигнута глобальная цель:
                if main_target_dst < self.walk_main_ct_radius:
                    # Глобальная цель достигнута
                    task_target_done = True
                # Локальная цель достигнута, создаём новую.
                action_target_done = True

        if task_target_done:
            # Если цель достигнута. Ставим статус задачи - на удаление. Выходим из цикла.
            pr(self.module_name(), 'Мы прибыли к КТ')
            self.status = 0
            return

        if action_target_done:
            # Мы достигли мини-цели. Создаём новую цель
            self.last_move_time = 0
            self.add_walk_action()
            return

        # Ожидаем прибытия к цели. Создаём новую цель, если герой стоит на месте.

        # Это первый вход в цикл?
        if not self.local_target_crd:
            # Создаём новую цель
            self.add_walk_action()
            return

        # Проверяем, когда была последняя команда движения
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Наша предыдущая команад была запущена
            if self.last_action.act_type == 1:
                # Если это была команда передвижения
                # Ждём 1 сек.
                self.last_move_time = time.time()
                # Сбрасываем счётчик остановки
                self.unit_stop = 0

        # Проверка времени последней команды движения
        if self.last_move_time:
            curr_time = time.time()
            if curr_time - self.last_move_time < 1:
                # Последняя команда была менее секунды назад, выходим.
                return

        # Проверяем движение к цели. Прошло более секунды, после последней команды
        if self.max_unit_stop_count():
            # Цель стоит более 10 циклов. Создаём новую задачу перемещения
            self.add_walk_action()

        pass

    def max_unit_stop_count(self):
        # Счётчик движения юинита
        ret = False
        if not self.unit_move_validate():
            self.unit_stop += 1

        if self.unit_stop > self.max_unit_stop:
            ret = True

        return ret

    def unit_move_validate(self):
        # Валидация движения юнита
        move = True
        # Если абсолютные коодринаты равны
        if self.unit_abs_last and self.unit_abs and self.unit_abs_last[0] == self.unit_abs[0] \
                and self.unit_abs_last[1] == self.unit_abs[1]:
            # И если кординаты на мини-карте равны
            if self.unit_crd_last and self.unit_crd_last[0] == self.unit_crd[0] \
                    and self.unit_crd_last[1] == self.unit_crd[1]:
                move = False

        return move

    def need_center(self):
        # Проверяем, когда была последняя команда центрирования
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Наша предыдущая команад была запущена
            if self.last_action.act_type == 2:
                # Если это была команда центрирования
                # Ждём 1 сек.
                self.last_center_time = time.time()

        # Центрирование каждую секунду
        if self.last_center_time == 0:
            # Обновления небыло ни разу
            pr(self.module_name(), '# Первое центрирование')
            return True

        curr_time = time.time()

        if curr_time - self.last_center_time > 1:
            # Если прошло больше 1 секунды, центруем.
            pr(self.module_name(), '# Центрирование каждую секунду')
            return True

        # Логика центрирования карты вокруг героя
        need_center = True
        # Наши действия:
        # 1. Проверяем положение целевой группы на карте. Группа на карте?
        if self.unit_abs:
            # 1. Да.
            # Проверяем группа в центре карты или с краю. Группа в центре?
            if self.center_map_validate():
                # 1. Да - центровка не требуется
                need_center = False
                self.need_center_count = 0
            else:
                # 2. Нет. Мы с краю мини-карты или нет?
                if self.map_far_away_border_validate():
                    # 1. Далеко от края. Центруем
                    self.need_center_count += 1
                    pr(self.module_name(), '# Мы не в центре карты и далеко от края')
                else:
                    # 2. С краю - создаём задание для перемещения.
                    need_center = False
                    self.need_center_count = 0
        else:
            # 2. Нет - центруем карту вогкруг группы.
            need_center = True
            pr(self.module_name(), '# Координаты героя не найдены, центрируем', self.need_center_count)
            self.need_center_count += 1
            # Возможно группа просто не найдена
            pass

        if self.need_center_count > self.need_center_max_count:
            self.need_center_count = 0
            need_center = False

        return need_center

    def add_walk_action(self):
        # Создание действия перемещения героя к контрольной точке.
        self.next_step()
        self.action = action.Walk(self.unit_group, self.next_click_abs_crd)

    def set_main_target(self, target_abs):
        # Назначить КТ
        # Перевод в относительные координаты
        self.main_target_crd = [target_abs[0] - cfg.mon_map['x'], target_abs[1] - cfg.mon_map['y']]
        pr(self.module_name(), 'Назначена контрольная точка ', self.main_target_crd, ' для группы ', self.unit_group)
        pass

    def map_far_away_border_validate(self):
        # Проверка положения героя. С края карты или нет
        min_b = 15
        if min_b < self.unit_crd[0] < 128 - min_b and min_b < self.unit_crd[1] < 128 - min_b:
            # Герой далеко от края
            return True
        # Герой с краю карты
        return False

    def center_map_validate(self):
        curr_time = time.time()
        if curr_time - self.last_center_map_validate < self.center_map_validate_interval:
            # Проверяем движение героя
            if self.ms.hero_is_stay(self.unit_group):
                # Если герой стоит, пропускаем проверки и центрируем не чаще чем указано
                # в настройках center_map_validate_interval
                return True

        self.last_center_map_validate = curr_time

        # Проверка нахождения героя в центре экрана
        # hero_center = {'x': 368, 'y': 271, 'width': 150, 'height': 150}
        hero_center = cfg.hero_center
        if hero_center['x'] < self.unit_abs[0] < (hero_center['x'] + hero_center['width']) \
                and hero_center['y'] < self.unit_abs[1] < (hero_center['y'] + hero_center['height']):
            return True
        else:
            pr(self.module_name(), 'Герой не в центре :', hero_center['x'], '<', self.unit_abs[0], '<',
               (hero_center['x'] + hero_center['width']), 'and', hero_center['y'], '<', self.unit_abs[1], '<',
               (hero_center['y'] + hero_center['height']))
        # Валидация положения героя по центру карты
        return False

    def abs_to_map_crd(self, hero_abs):
        # Положение героя на маленьком экране
        mini_crd = self.abs_to_mini(hero_abs[1], hero_abs[0])
        # Вычисляем координаты героя на мини-карте
        hero = self.hero_in_map(mini_crd)
        return hero

    def get_hero_map_crd(self):
        hero = []
        hero_abs = self.ms.get_hero(self.unit_group)
        if hero_abs:
            hero = self.abs_to_map_crd(hero_abs)
        return hero

    def abs_to_mini(self, x, y):
        # Перевод из кординат экрана в координаты мини карты
        self.update_k_size()
        screen_crd = [x - cfg.screen_shift['x'], y - cfg.screen_shift['y']]
        mini_x = round(screen_crd[0] / self.k_size, 0)
        mini_y = round(screen_crd[1] / self.k_size, 0)
        mini_crd = [mini_x, mini_y]
        return mini_crd

    def update_k_size(self):
        if self.k_size == 0:
            self.k_size = self.ms.mm.k_size()

    def hero_in_map(self, hero):
        # Положение героя на карте
        hero_map = [hero[0] + self.ms.mm.screen[0][0] + cfg.mm_x_shift, hero[1] +
                    self.ms.mm.screen[0][1] + cfg.mm_y_shift]
        return hero_map

    def hero_screen(self, hero):
        # Положение героя на экране
        hero_screen = [hero[0] - self.ms.mm.screen[0][0] - cfg.mm_x_shift,
                       hero[1] - self.ms.mm.screen[0][1] - cfg.mm_y_shift]
        return hero_screen

    def abs_crd(self, x, y):
        # Получаем абслютные координаты мыши из относительных коодринат мини-карты
        self.update_k_size()
        scr_crd = [x * self.k_size, y * self.k_size]
        abs_crd = [cfg.screen_shift['x'] + scr_crd[0], cfg.screen_shift['y'] + scr_crd[1]]
        return abs_crd

    def next_step(self):
        # Вычилсяем данные для следующего шага
        # Получаем координаты героя на большой карте
        hero = self.unit_crd
        # pr(self.module_name(),'hero', hero)
        # Путь по x, y, который предстоит пройти
        x_path = self.main_target_crd[0] - hero[0]
        y_path = self.main_target_crd[1] - hero[1]
        # path = [x_path, y_path]
        # pr(self.module_name(),"path", path)

        # Модуль пути
        x_path_abs = abs(x_path)
        y_path_abs = abs(y_path)
        # Дистанция до цели
        dist = round((x_path_abs ** 2 + y_path_abs ** 2) ** 0.5, 1)
        pr(self.module_name(), 'Дистанция до КТ :', dist)

        # Определяем сектор нашего движения
        # Размер экрана
        screen_hw = self.ms.mm.screen_hw()
        # pr(self.module_name(),'screen_hw', screen_hw)

        # Находится ли цель в пределах экрана
        if self.ms.mm.screen[0][0] < self.main_target_crd[0] < self.ms.mm.screen[1][0] and self.ms.mm.screen[0][1] < \
                self.main_target_crd[1] < self.ms.mm.screen[1][1]:
            # Назначаем координаты цели
            pr(self.module_name(), 'КТ в пределах видимости')
            mini_map_crd = self.hero_screen(self.main_target_crd)

            # Локальная цель приравнивается к глобальной
            self.local_target_crd = self.main_target_crd

        else:
            pr(self.module_name(), 'КТ за пределами видимости')
            radius = screen_hw[1] / 2
            if x_path >= 0 and y_path <= 0:
                # 0-90
                if x_path_abs <= y_path_abs:
                    # Составляем пропроцию
                    y_small = -radius
                    x_small = y_small * x_path / y_path
                else:
                    x_small = radius
                    y_small = x_small * y_path / x_path
            elif x_path >= 0 and y_path >= 0:
                # 90-180
                if x_path_abs <= y_path_abs:
                    # Составляем пропроцию
                    y_small = radius
                    x_small = y_small * x_path / y_path
                else:
                    x_small = radius
                    y_small = x_small * y_path / x_path
                pass
            elif x_path <= 0 and y_path >= 0:
                # 180 - 270
                if x_path_abs <= y_path_abs:
                    # Составляем пропроцию
                    y_small = radius
                    x_small = y_small * x_path / y_path
                else:
                    x_small = -radius
                    y_small = x_small * y_path / x_path

                pass
            else:
                # 270 - 360
                if x_path_abs <= y_path_abs:
                    # Составляем пропроцию
                    y_small = -radius
                    x_small = y_small * x_path / y_path
                else:
                    x_small = -radius
                    y_small = x_small * y_path / x_path

            dist_small = [x_small, y_small]
            # pr(self.module_name(),'dist_small', dist_small)

            # Координаты мини-цели на мини-карте
            self.local_target_crd = [round(dist_small[0] + hero[0], 0), round(dist_small[1] + hero[1], 0)]
            pr(self.module_name(), 'Назначена текущая мини цель: ', self.local_target_crd)

            # Позиция героя на экране
            hero_screen = self.hero_screen(hero)
            # pr(self.module_name(),'hero_screen', hero_screen)

            # Координаты на экране
            mini_map_crd = [hero_screen[0] + (dist_small[0]), hero_screen[1] + (dist_small[1])]

        mini_map_crd = [round(mini_map_crd[0], 0), round(mini_map_crd[1], 0)]

        # Определяем относительные координаты мыши
        abs_crd = self.abs_crd(mini_map_crd[0], mini_map_crd[1])

        # Валидация координат относительно реального экрана
        border = 30
        min_left = cfg.screen_shift['x'] + border
        if abs_crd[0] < min_left:
            abs_crd[0] = min_left

        max_right = cfg.screen_shift['x'] + cfg.main_window['x'] - border
        if abs_crd[0] > max_right:
            abs_crd[0] = max_right

        min_top = cfg.screen_shift['y'] + border
        if abs_crd[1] < min_top:
            abs_crd[1] = min_top

        max_bottom = cfg.screen_shift['y'] + cfg.main_window['ym'] - border
        if abs_crd[1] > max_bottom:
            abs_crd[1] = max_bottom

        # pr(self.module_name(),'abs_crd', abs_crd)
        walk_crd = self.walk_shift_from_mini_map_abs_crd(abs_crd)
        self.next_click_abs_crd = [int(walk_crd[0]), int(walk_crd[1])]
        pass


class WalkS(Walk):
    task_name = 'Движение в режиме боеготовности'

    def add_walk_action(self):
        # Создание действия перемещения героя к контрольной точке.
        self.next_step()
        self.action = action.WalkS(self.unit_group, self.next_click_abs_crd)
        self.action.priority = 1


class ClearSector(Walk):
    # Разведка сектора и приманивание целей к группе
    # Герой запоминает свои координаты
    # Идёт к точке зачистки, если видит врага - сокращает с ним дистанцию до установленной.
    # Затем идёт к своей сохранённой точке. Если дистанция до врага разрывается, ожидает.
    # Когда возвращается на базу, миссия выполнена.

    # Начальные координаты героя
    unit_crd_first = []

    # Прогресс достижения цели
    walk_progress = 0

    # Отступление героя
    retreat_hero = 0

    # Минимальная дистанция до героя
    min_dst = 5
    # Максимальная дистанция до героя
    max_dst = 5
    # Дистанция, после которой мы возвращаемя к объекту
    return_dst = 7

    # Основная цель, назначенная в самом начале
    main_target_crd_first = []
    # Куда идёт герой. Домой или к врагам
    #   0 - к врагам
    #   1 - домой
    way_to_home = 0

    # Разброс дистации для контрольной точки
    walk_min_ct_radius = 2
    # Разброс дистацнии для локальной цели
    walk_min_move_radius = 5
    walk_main_ct_radius = 2
    # Цель обнаружена
    target_detected = False
    # Ожидание врагов, при достижении цели.
    target_done_wait_validate = 3
    # Время достижения цели
    target_done_time = 0
    # Убивать юнитов определённого типа, если они близко
    type_to_kill = []
    # Если эти юниты присутствуют близко, уходим
    exclude_close = dict()
    max_close = 2
    min_other_enemy_dst = 40

    def __init__(self, target_abs=[], unit_group=1, min_dst=5, max_dst=5, return_dst=7, first_abs=[], type_to_kill=[],
                 exclude_close=[]):
        self.unit_crd_first = []

        self.set_main_target(target_abs)
        self.main_target_crd_first = self.main_target_crd
        self.unit_group = unit_group
        self.min_dst = min_dst
        self.max_dst = max_dst
        self.return_dst = return_dst
        self.type_to_kill = type_to_kill
        if exclude_close:
            self.exclude_close = dict()
            for exclude in exclude_close:
                self.exclude_close[exclude] = 1

        self.task_name = 'Разведка сектора: ' + str(target_abs)
        if first_abs:
            # Назначаем стартовую точку героя
            self.set_unit_crd(first_abs)

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        # Получаем координаты героя на мини-карте
        self.unit_crd_last = self.unit_crd
        self.unit_crd = self.get_hero_map_crd()
        if not self.unit_crd:
            pr(self.module_name(), self.ms.cmd)
            pr(self.module_name(), '# Центрирование, нет координат героя на мини-карте')
            self.action = action.Center(self.unit_group)
            self.action.priority = 1
            return

        if not self.unit_crd_first:
            # Рассчитываем текущие координаты
            self.unit_crd_first = self.unit_crd

        # Получаем координаты героя на экране
        self.unit_abs_last = self.unit_abs
        self.unit_abs = self.ms.get_hero_abs(self.unit_group)

        # Требуется ли центрирование?
        if self.need_center():
            pr(self.module_name(), '# Необходимо центрирование')
            self.action = action.Center(self.unit_group)
            self.action.priority = 1
            return

        # Враг обнаружен?
        self.ms.p.update_units(self.ms.other, self.ms.mm.screen)

        # Проверяем врагов
        enemy = self.ms.p.get_valid_units()
        enemy_dst = []
        if enemy:
            # Рассчитываем дистанцию до героя
            hero = self.ms.cmd.get(self.unit_group)
            if hero and hero[0] > 0:
                for e in enemy:
                    # pr(self.module_name(),hero): [427, 705]
                    # pr(self.module_name(),e): [21, [122, 79, 3, [22.0, 6.0, 689, 190], 0, 0], 1, '0-0']
                    dst = int(self.get_distance(hero, [e[1][3][3], e[1][3][2]]))
                    # Дистанция, враг, герой
                    enemy_dst.append([dst, e])
                    pass
            pass
        # Враг обнаружен?
        if enemy_dst:
            # Враги обнаружены
            enemy_dst.sort(key=lambda x: x[0], reverse=False)
            # [163, 7]
            min_dst = enemy_dst[0]
            pr(self.module_name(), 'Враг обнаружен, дистанция:', min_dst[0], 'количество:', len(enemy_dst), 'состав:',
               enemy_dst)
            if min_dst[0] < self.min_dst * 32:
                if not self.way_to_home:
                    # Проверяем только если сейчас идёт цикл поиска врагов
                    # Враг близко
                    # Для создания действия к отступлению, герой должен подтвердить, что его враг ближайший.
                    # if self.retreat_hero != 1:
                    #    self.retreat_hero = 1
                    #    return
                    kill_target = False
                    enemy = enemy_dst[0][1]
                    if self.type_to_kill:
                        for target_type in self.type_to_kill:
                            if enemy[1][5] == target_type:
                                kill_target = True
                                break
                    if kill_target:
                        # Ближайшая цель, входит в список убийств. Смотрим расстояние до других целей.
                        other_close = 0
                        for en in enemy_dst:
                            enemy_o = en[1]
                            # Если враг ближе 40 пикселей
                            if en[0] < self.min_other_enemy_dst:
                                add_other = True
                                for target_type in self.type_to_kill:
                                    if enemy_o[1][5] == target_type:
                                        if not self.exclude_close.get(target_type):
                                            add_other = False
                                            break
                                if add_other:
                                    other_close += 1
                        if other_close < self.max_close:
                            # Если есть ещё враги других типов, но их количество допустимо (<2), атакуем цель
                            enemy_abs = self.ms.get_unit_abs_crd(enemy)
                            pr(self.module_name(), 'Атака на цель', enemy, 'врагов:', other_close)
                            self.attack_action(enemy_abs, enemy[0])
                            return
                        else:
                            pr(self.module_name(), 'Цель близко, но врагов слишком много:', other_close)

                    pr(self.module_name(), 'Отступаем')
                    # Создаём действие к остановке. Меняем координаты на возврат домой.
                    self.main_target_crd = self.unit_crd_first
                    self.local_target_crd = self.unit_crd
                    self.action = action.Stop(self.unit_group)
                    self.action.priority = 1
                    self.way_to_home = 1
                    # Цель обнаружена, приманиваем
                    self.target_detected = True
                    return

            if self.way_to_home:
                if self.max_dst * 32 < min_dst[0] < self.return_dst * 32:
                    # Дистанция слишком велика, останавливаемся
                    pr(self.module_name(), 'Дистанция слишком велика, останавливаемся')
                    self.action = action.Stop(self.unit_group)
                    return

                if min_dst[0] > self.return_dst * 32:
                    # Враг отступает, возвращаемся
                    pr(self.module_name(), 'Враг отступает, возвращаемся')
                    self.way_to_home = 0
                    self.main_target_crd = self.main_target_crd_first
                    self.local_target_crd = self.unit_crd
                    self.action = action.Stop(self.unit_group)
                    return

        else:
            # Враг не был обнаружен
            if self.way_to_home and self.target_detected:
                # Если мы шли домой и цель была ранее обнаружена. Возвращаемся к цели
                # Враг отступает, возвращаемся
                pr(self.module_name(), 'Враг потерян из виду, возвращаемся')
                self.way_to_home = 0
                self.main_target_crd = self.main_target_crd_first
                self.local_target_crd = self.unit_crd
                self.action = action.Stop(self.unit_group)
                return
            pass

        # Была ли достигнута локальная цель?
        task_target_done = False
        action_target_done = False
        if self.local_target_crd:
            # Да. Проверяем её достижение
            # Расстояние до локальной цели
            local_target_dst = self.get_distance(self.local_target_crd, self.unit_crd)

            # Минимальный радиус до цели
            min_radius = self.walk_min_ct_radius
            if local_target_dst > 10:
                # При большом расстоянии до цели, перемещаемся свободно
                min_radius = self.walk_min_move_radius

            if local_target_dst < min_radius:
                main_target_dst = self.get_distance(self.main_target_crd, self.unit_crd)
                # Была ли достигнута глобальная цель:
                if main_target_dst < self.walk_main_ct_radius:
                    # Глобальная цель достигнута
                    task_target_done = True
                # Локальная цель достигнута, создаём новую.
                action_target_done = True

        if task_target_done:
            # Если цель достигнута. Возвращаемя назад
            if self.walk_progress == 0:
                # Ожидаем врагов. Только когда мы в режиме поиска.
                if self.target_done_wait_validate and not self.way_to_home:
                    curr_time = time.time()
                    if not self.target_done_time:
                        self.target_done_time = curr_time
                    wait_time = round(curr_time - self.target_done_time, 2)
                    if wait_time < self.target_done_wait_validate:
                        pr(self.module_name(), 'Ожидаем врагов', wait_time, '<', self.target_done_wait_validate)
                        return
                # Поверяем наличие врагов
                pr(self.module_name(), 'Сектор чист. Возвращаемся к команде')
                self.walk_progress = 1
                self.target_detected = False
                self.main_target_crd = self.unit_crd_first
                return
            else:
                pr(self.module_name(), 'Задание выполнено')
                self.status = 0
                return

        if action_target_done:
            # Мы достигли мини-цели. Создаём новую цель
            self.last_move_time = 0
            self.add_walk_action()
            return

        # Ожидаем прибытия к цели. Создаём новую цель, если герой стоит на месте.

        # Это первый вход в цикл?
        if not self.local_target_crd:
            # Создаём новую цель
            self.add_walk_action()
            return

        # Проверяем, когда была последняя команда движения
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Наша предыдущая команад была запущена
            if self.last_action.act_type == 1:
                # Если это была команда передвижения
                # Ждём 1 сек.
                self.last_move_time = time.time()
                # Сбрасываем счётчик остановки
                self.unit_stop = 0

        # Проверка времени последней команды движения
        if self.last_move_time:
            curr_time = time.time()
            if curr_time - self.last_move_time < 1:
                # Последняя команда была менее секунды назад, выходим.
                return

        # Проверяем движение к цели. Прошло более секунды, после последней команды
        if self.max_unit_stop_count():
            # Цель стоит более 10 циклов. Создаём новую задачу перемещения
            self.add_walk_action()

        pass

    def set_unit_crd(self, target_abs):
        # Назначить стартовую КТ
        # Перевод в относительные координаты
        self.unit_crd_first = [target_abs[0] - cfg.mon_map['x'], target_abs[1] - cfg.mon_map['y']]
        pr(self.module_name(), 'Назначена стартовая точка ', self.unit_crd_first, ' для группы ', self.unit_group)
        pass

    def add_walk_action(self):
        # Создание действия перемещения героя к контрольной точке.
        self.next_step()
        self.action = action.Walk(self.unit_group, self.next_click_abs_crd)
        self.action.priority = 1

    def attack_action(self, hero_abs, target_id):
        target_code = ''
        self.action = action.Attack(self.unit_group, hero_abs, target_code, target_id)


class Group(WalkTask):
    # Сгруппироваться вокруг героя
    # Можно сделать динамическую группировку, но она пока не нужна. Сделаем предустановленную.
    # Есть класс группа, в котором распределены названия команд. Мы его один раз создаём, а затем добавляем в задачи.
    task_name = 'Группировка'
    # Допустимый разброс, при формировании группы
    max_dst = 1
    # Группа героев
    group = Group()

    # Список управляемых героев.
    heroes = dict()

    # Расположение группы, относительно танка.
    #   0 - сверху
    #   1 - спрва
    #   2-  снизу
    #   3 - слева
    rotate = 2

    # Максимальное количество остановок, до пропуска валидации
    max_stop = 5
    stop_count = dict()

    # Максимальное количество не найденых, для пропуска валидации
    max_not_found_count = 5
    not_found_count = dict()

    def __init__(self, group, max_dst=0, rotate=2):
        self.heroes = dict()
        self.max_stop = 5
        self.stop_count = dict()
        self.max_not_found_count = 5
        self.not_found_count = dict()
        self.group = group
        self.max_dst = max_dst
        self.rotate = rotate

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        # Создаём список тех, кто ещё не ходил.
        if not self.heroes:
            if self.group.archers:
                for archer in self.group.archers:
                    # Ключ - номер группы
                    self.heroes[archer] = self.default_unit_info()
            if self.group.mages:
                for mage in self.group.mages:
                    self.heroes[mage] = self.default_unit_info(0)

        need_center = True

        # Проверяем последнее действие - действие активировано?
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            if self.last_action.act_type == 1:
                # Получаем юнита, который ходил последним
                unit = self.last_action.unit_group
                curr_time = time.time()
                # Юнит использовал свой ход
                self.heroes[unit][0] = 1
                # Время последнего хода
                self.heroes[unit][1] = curr_time
            else:
                # Последняя команда была - центрирование
                need_center = False

        # Создаём список координат прибытия и фактических координат героев
        if not self.get_groups_crd(need_center):
            # Координаты танка не найдены, назначаем центрирование танка
            # Центрируем
            self.action = action.Center(self.group.tank)

        # Сравниваем списки циклом перехода по героям, составляем список.
        valid_dst = 16 + 32 * self.max_dst

        # Кто-то не достиг координат?
        not_valid_crd = []

        for key, unit in self.heroes.items():
            if unit[2][0] and unit[3][0] and self.validate_crd(unit[2], unit[3], valid_dst) \
                    and not self.ms.hero_not_found.get(key):
                # Координаты есть, и они проходят валидацию
                pass
            else:
                if not self.not_found_count_validate(key):
                    # Если мы не отчаялись искать героя, добавляем его в список
                    # Проверяем валидацию остановки
                    if self.stop_count.get(key) and self.stop_count.get(key) > self.max_stop:
                        pr(self.module_name(), 'Герой', key, 'стоит на месте, пропускаем его валидацию')
                    else:
                        not_valid_crd.append(key)
                else:
                    pr(self.module_name(), 'Герой', key, 'не найден, пропускаем его валидацию')

        # 1. Нет - меняем статус задачи, выходим.
        if not not_valid_crd:
            self.status = 0
            return

        # Все герои ходили?
        if self.all_units_turn(not_valid_crd):
            # Да - обнуляем список тех, кто ходил
            self.next_units_turn()

        # 2. Да - проходим по списку.
        pr(self.module_name(), 'Группы, к перемещению:', not_valid_crd)
        for key in not_valid_crd:
            # Герой ходил?
            hero = self.heroes[key]
            # pr(self.module_name(),hero)
            if hero[0] == 0:
                # Координаты есть? Они реальные или из прошлого цикла?
                if hero[3][0] and hero[4][0] and not self.ms.hero_not_found.get(key):
                    self.reset_not_found_count(key)
                    # 2. Нет, Герой движется?
                    if self.validate_crd(hero[3], hero[4], 0):
                        # Кто-то занял место героя?
                        other_hero_in_crd = self.crd_not_empty(hero)
                        # pr(self.module_name(),other_hero_in_crd)
                        if other_hero_in_crd:
                            # self.update_stop_count(key)
                            # Отводим героя
                            # other_hero = self.heroes.get(other_hero_in_crd)
                            next_hero_crd = hero[2]
                            if self.rotate == 0:
                                if hero[5]:
                                    # Лучники двигаются ближе к герою
                                    next_hero_crd[1] += 32
                                else:
                                    # Маги отходят назад
                                    next_hero_crd[1] -= 32
                            elif self.rotate == 1:
                                if hero[5]:
                                    next_hero_crd[0] -= 32
                                else:
                                    next_hero_crd[0] += 32
                            elif self.rotate == 2:
                                if hero[5]:
                                    next_hero_crd[1] -= 32
                                else:
                                    next_hero_crd[1] += 32
                            elif self.rotate == 3:
                                if hero[5]:
                                    next_hero_crd[0] += 32
                                else:
                                    next_hero_crd[0] -= 32

                            # pr(self.module_name(),'walk back', key, next_hero_crd)
                            self.update_stop_count(key)
                            self.add_walk_action(key, next_hero_crd)
                            break
                        else:
                            # 2. Создаём действие.
                            # pr(self.module_name(),'walk', key, hero[2])
                            self.update_stop_count(key)
                            self.add_walk_action(key, hero[2])
                            break
                    else:
                        # Сбрасываем счётчик остановки
                        self.reset_stop_count(key)
                else:
                    # 3. Нет координат, не известно. Когда было последнее действие?
                    curr_time = time.time()
                    if curr_time - hero[1] > 3:
                        # 2. Более секунды назад. Создаём задание.
                        pr(self.module_name(), 'Нет координат, не известно когда было последнее действие', key)
                        self.update_not_found_count(key)
                        self.add_walk_action(key, hero[2])
                        break

            # Переходим к следующему.
            pass

    def reset_stop_count(self, key):
        self.stop_count[key] = 0

    def update_stop_count(self, key):
        if self.stop_count.get(key):
            self.stop_count[key] += 1
            # if self.stop_count.get(key) > self.max_stop:
            #    # Увеличиваем разброс
            #    self.max_dst += 1
            #    pr(self.module_name(), 'Увеличиваем разброс цели:', self.max_dst)
            #    self.stop_count[key] = 1
        else:
            self.stop_count[key] = 1
        pr(self.module_name(), 'Обновлён счётчик ошибок:', key, self.stop_count)

    def reset_not_found_count(self, key):
        self.not_found_count[key] = 0

    def update_not_found_count(self, key):
        if self.not_found_count.get(key):
            self.not_found_count[key] += 1
        else:
            self.not_found_count[key] = 1

    def not_found_count_validate(self, key):
        if self.not_found_count.get(key) and self.not_found_count.get(key) > self.max_not_found_count:
            return True
        return False

    def crd_not_empty(self, our_hero, valid_dst=16):
        # Проверка, занято ли место героя, другим героем
        ret = 0
        for key, hero in self.heroes.items():
            # pr(self.module_name(),our_hero[2], hero[3])
            if self.validate_crd(our_hero[2], hero[3], valid_dst):
                ret = key
        return ret

    def validate_crd(self, crd1, crd2, valid_dst=32):
        # Валидация координат
        # pr(self.module_name(),crd1, crd2)
        if abs(crd1[0] - crd2[0]) <= valid_dst and abs(crd1[1] - crd2[1]) <= valid_dst:
            return True
        return False

    def get_groups_crd(self, need_center):
        # Создаём список координат для доступных групп
        tank = self.group.tank
        tank_abs = self.ms.get_hero_abs(tank)
        ret = True
        if not tank_abs:
            ret = False
        if self.ms.hero_not_found.get(tank):
            # Координаты героя не найдены
            ret = False

        if not ret and need_center:
            # Если координаты не обнаружены, а мы уже центрировали, берём координаты по центру карты
            tank_screen = [425, 342]
            tank_x_y = self.ms.abs_to_screen(tank_screen[0], tank_screen[1])
            tank_abs = [tank_x_y[1], tank_x_y[0]]
            pr(self.module_name(), 'Координаты героя не найдены, берём по центру карты', tank_abs)
            pass

        tank_x = tank_abs[0]
        tank_y = tank_abs[1]
        if self.group.archers:
            self.get_group_crd(tank_x, tank_y, self.group.archers, self.group.archers_layer)
        if self.group.mages:
            self.get_group_crd(tank_x, tank_y, self.group.mages, self.group.mages_layer)
        return ret

    def get_group_crd(self, tank_x, tank_y, group, dst):
        # Список координат для конкретной группы
        # pr(self.module_name(), 'Расчёт координат группы:', tank_x, tank_y, group, dst)
        if self.rotate == 0 or self.rotate == 2:
            # Группа сверху от танка
            unit_y = tank_y - dst * 32
            if self.rotate == 2:
                # Группа - снизу от танка
                unit_y = tank_y + dst * 32
            unit_left = int(tank_x - (int(len(group) / 2)) * 32)
            unit_x = unit_left

        else:
            # self.rotate == 1 or self.rotate == 3:
            # Группа справа от танка
            unit_x = tank_x + dst * 32
            if self.rotate == 3:
                # Группа - слева от танка
                unit_x = tank_x - dst * 32
            unit_left = int(tank_y + (int(len(group) / 2)) * 32)
            unit_y = unit_left

        for unit in group:
            # Создаём координаты прибытия для юнитов
            unit_dst = [unit_x, unit_y]
            self.heroes[unit][2] = unit_dst
            if self.rotate == 2 or self.rotate == 0:
                unit_x += 32
            elif self.rotate == 1 or self.rotate == 3:
                unit_y -= 32

            # Сохраняем текущие координаты как прошедшие
            self.heroes[unit][4] = self.heroes[unit][3]
            # Обновляем фактические координаты
            # Узнаём фактические координаты юнитов
            unit_abs = self.ms.get_hero_abs(unit)
            if unit_abs:
                # Обновляем координаты юнита, только в случае фактического получения данных.
                self.heroes[unit][3] = unit_abs

        # pr(self.module_name(), 'Результат расчёта:', self.heroes)

    def next_units_turn(self):
        # Переход хода. Сбрасываем данные о том, что юниты ходили.
        for key in self.heroes:
            self.heroes[key][0] = 0
            pass
        pr(self.module_name(), 'Переход хода')

    def all_units_turn(self, not_valid_crd):
        # Все юиниты сходили по очереди
        i = 0
        for key, unit in self.heroes.items():
            if unit[0] == 1:
                i += 1
        if i >= len(not_valid_crd):
            return True
        return False

    def default_unit_info(self, archer=1):
        # 0 - 0 - информация о том, ходила ли группа. 0-нет. 1-да
        # 1 - 0 - время последнего хода группы.
        # 2 - [0,0] - куда нужно двигаться
        # 3 - [0,0] - фактические координаты
        # 4 - [0,0] - прошлые координаты
        # 5 - 1 - лучник, 0 - маг
        return [0, 0, [0, 0], [0, 0], [0, 0], archer]

    def add_walk_action(self, unit, abs_crd):
        # Создание действия перемещения героя к контрольной точке.
        shift_crd = self.walk_shift_abs_crd(abs_crd)
        if self.validate_screen_crd(shift_crd):
            # Проверяем соответствие координат экрану
            self.action = action.Walk(unit, shift_crd)
        else:
            # Иначе центрируем
            self.action = action.Center(self.group.tank)


class EnemyDown(Task):
    task_name = 'Ожидание смерти врага'
    # Враг, соответствующий типу
    # Условия победы:
    #   враг должен появиться
    #   хп должно быть потрачено
    #   должен исчезнуть в пределах радиуса ближайшего из героев
    #   или дожно пройти определённое время, чтобы отменить цель

    # Основная задача
    weight = 1

    # Прогресс выполнения задачи.
    #   При появлении врага, меняется на 1.
    #   После смерти врага, задача снимается
    progress = 0

    # Определение врага по типу, а не по имени.
    # Этот алгоритм быстрее стандартного, так как не требует дополнительного навода мыши на цель.
    target_type = 0

    # Текущий цикл валидации
    validate_count = 0
    # Максимальное количество циклов, после которых валидация пройдена
    validate_max_count = 10

    def __init__(self, target_type=1, time_out=0):
        self.target_type = target_type
        self.close_time = time_out

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        self.ms.p.update_units(self.ms.other, self.ms.mm.screen)
        # pr(self.module_name(),'other', self.ms.other)
        # pr(self.module_name(),'valid', self.ms.p.get_valid_units())
        # Проверяем врагов
        # Смотрим прогресс
        if self.progress == 0:
            # 0 - ждём врагов
            # target = self.ms.get_by_type(self.target_type)
            target = self.ms.p.get_valid_by_type(self.target_type)
            if target:
                # Враг появлися?
                # [[225, 643, 1, 1]]
                self.progress = 1
                self.validate_count = 0
                pr(self.module_name(), 'Цель', self.target_type, 'обнаружена', target)
                return

        if self.progress == 1:
            # target = self.ms.get_by_type(self.target_type)
            target = self.ms.p.get_valid_by_type(self.target_type)
            if not target:
                # 1 - врага нет? Проводим валидацию
                self.validate_count += 1
            else:
                # 2 - враг снова появился - сбрасывам счётчик валидации.
                self.validate_count = 0

        # Валидация пройдена, снимаем задачу
        if self.validate_count > self.validate_max_count:
            self.status = 0
            pr(self.module_name(), 'Цель', self.target_type, 'устранена.')

    pass


class EnemyClear(WalkTask):
    # Ожидаем ликвидации врагов ближнего боя.
    # 1. Враг должен быть ближе 3х клеток
    # 2. Если нет врага, проходим валидацию
    task_name = 'Ожидание смерти врага'

    # Герои, расстояние до которых мы считаем
    units = []
    # Максимальная дистанция до врагов.
    min_dst = 3
    # Минимальное время ожидания между проверками
    min_wait = 1
    # Текущий цикл валидации
    validate_count = 0
    # Максимальное количество циклов, после которых валидация пройдена
    validate_max_count = 3
    # Последнее время валидации
    last_validate_time = 0
    # Интервал между валидациями
    last_validation = 0
    validation_interval = 0
    # Максимальное время ожидания, после которого снимается задача. 3 минуты
    max_wait = 180
    run_time = 0

    def __init__(self, units=[], min_dst=3, min_wait=1, validation_interval=0, max_wait=180):
        self.units = units
        self.min_dst = min_dst
        self.min_wait = min_wait
        self.validation_interval = validation_interval
        self.max_wait = max_wait
        if max_wait:
            self.run_time = time.time()

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        curr_time = time.time()

        # Проверка max_wait
        if self.max_wait:
            if curr_time - self.run_time > self.max_wait:
                pr(self.module_name(), 'Истекло время ожидания убийства цели', self.max_wait)
                self.status = 0
                return

        enemy_close = False

        self.ms.p.update_units(self.ms.other, self.ms.mm.screen)
        # Проверяем врагов
        enemy = self.ms.p.get_valid_units()
        enemy_dst = []
        if enemy:
            # Рассчитываем дистанцию до героев
            for unit in self.units:
                hero = self.ms.cmd.get(unit)
                if hero and hero[0] > 0:
                    for e in enemy:
                        # pr(self.module_name(),hero): [427, 705]
                        # pr(self.module_name(),e): [21, [122, 79, 3, [22.0, 6.0, 689, 190], 0, 0], 1, '0-0']
                        dst = int(self.get_distance(hero, [e[1][3][3], e[1][3][2]]))
                        # Дистанция, враг, герой
                        enemy_dst.append([dst, unit, e])
                        pass
                pass
            pass

        if enemy_dst:
            if len(enemy_dst) < 3:
                # Проверка на неопредилившуюся команду союзников. Если осталось 1 или 2 врага
                # Если юнит находится вблизи нашей группы и у него зелёное хп, это дружеский юнит
                new_enemy_dst = []
                for enemy in enemy_dst:
                    e = enemy[2]
                    i = 0
                    for k, hero in self.ms.cmd.items():
                        c = False
                        for unit in self.units:
                            # Пропускаем героя на передовой
                            if k == unit:
                                c = True
                                break
                        if c:
                            continue

                        if hero and hero[0] > 0:
                            # Проверяем расстояние от неизвестного героя до других героев.
                            hp = e[1][4]
                            if hp == 0:
                                # У героя зелёное ХР
                                dst = int(self.get_distance(hero, [e[1][3][3], e[1][3][2]]))
                                if dst < 64:
                                    # Если расстояние в пределах 64 пикселей, добавляем счётчик.
                                    i += 1
                    if i > 0:
                        # Если есть герой, который стоит рядом с целью и у цели полное ХП, пропускаем её
                        pr(self.module_name(), 'Юнит', e[0], 'опознан как свой:', enemy)
                        pass
                    else:
                        # Добавляем юнит в список врагов
                        new_enemy_dst.append(enemy)

                enemy_dst = new_enemy_dst
            pass

        if enemy_dst:
            # Враги обнаружены
            enemy_dst.sort(key=lambda x: x[0], reverse=False)
            # [163, 7, 2]
            # pr(self.module_name(),enemy_dst)
            min_dst = enemy_dst[0]
            if min_dst[0] <= self.min_dst * 32:
                # Враг близко
                enemy_close = True
                self.block = True
                pr(self.module_name(), 'Враг обнаружен:', enemy_dst, 'cmd', self.ms.cmd)
                pass

        if not enemy_close:
            self.block = False
            # Если враг далеко или отсутствует

            # Проверка времени последней команды движения
            if self.last_validate_time:
                if curr_time - self.last_validate_time < self.min_wait:
                    # Последняя команда была менее секунды назад, выходим.
                    return

            self.last_validate_time = curr_time

            if self.validation_interval:
                # Вадилация между интервалами
                if self.last_validation == 0:
                    self.last_validation = curr_time
                if curr_time - self.last_validation < self.validation_interval:
                    # Если назначен интервал валидации, используем проверку по его времени
                    wait_time = round(curr_time - self.last_validation, 1)
                    pr(self.module_name(), 'Ожидаем', wait_time, '<', self.validation_interval)
                    return
                else:
                    self.last_validation = curr_time

            self.validate_count += 1
            pr(self.module_name(), 'Врагов на дистанции', self.min_dst, 'нет, цикл валидации', self.validate_count)

        else:
            self.validate_count = 0

        # Валидация пройдена, снимаем задачу
        if self.validate_count >= self.validate_max_count:
            self.status = 0
            pr(self.module_name(), 'Ближние цели отсутствуют')

    pass


class GetLoot(Walk):
    # Сбор лута
    task_name = 'Сбор лута'

    first_run = True
    progress = 0
    max_loot_dst = 5

    # Уменьшение приоритета задачи. Чем больше число, тем ниже приоритет.
    priority_down = 0
    priority = 4

    wait_interval = 2
    last_action_time = 0

    # Группа героев
    units = []
    # Первый элемент массива
    current_unit = 0

    def __init__(self, unit_group=1, priority_down=0, wait_interval=2, units=[]):
        self.units = []

        self.unit_group = unit_group
        self.priority_down = priority_down
        self.wait_interval = wait_interval
        self.get_current_unit(units)

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        curr_time = time.time()
        if self.units:
            if not self.last_action_time:
                self.last_action_time = curr_time

            if curr_time - self.last_action_time > self.wait_interval:
                self.next_unit()
                self.last_action_time = curr_time

        # Получаем координаты героя на мини-карте
        self.unit_crd_last = self.unit_crd
        self.unit_crd = self.get_hero_map_crd()
        if not self.unit_crd:
            pr(self.module_name(), 'Координаты героя не найдены', self.ms.cmd)
            if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id \
                    and self.last_action.act_type == 2:
                pr(self.module_name(), 'Берём координаты по центру карты')
                # Последняя выполненная задача - центрирование
                # Берём координаты по центру карты
                # y,x
                unit_abs = [332, 425]
                hero_shift = self.ms.get_shift(unit_abs)
                self.unit_crd = self.abs_to_map_crd(hero_shift)
            else:
                # Центрируем
                self.action = action.Center(self.unit_group)
                self.action.priority = self.get_priority()
                return

        # Получаем координаты героя на экране
        self.unit_abs_last = self.unit_abs
        self.unit_abs = self.ms.get_hero_abs(self.unit_group)

        # Требуется ли центрирование?
        if self.need_center():
            self.action = action.Center(self.unit_group)
            self.action.priority = self.get_priority()
            return

        # Проверяем, когда была последняя команда сбора мешков
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Наша предыдущая команад была запущена
            if self.last_action.act_type == 3:
                # Если это была команда сбора мешков
                self.first_run = False
                # Ждём 1 сек.
                self.last_move_time = time.time()
                # Сбрасываем счётчик остановки
                self.unit_stop = 0
                if self.progress == 0:
                    self.progress = 1

        # Это первый вход в цикл?
        if self.first_run:
            # Создаём новую цель
            self.add_get_loot_action()
            return

        # Проверка времени последней команды движения
        if self.last_move_time:
            if curr_time - self.last_move_time < 1:
                # Последняя команда была менее секунды назад, выходим.
                return

        # Проверяем движение к цели. Прошло более секунды, после последней команды
        if self.max_unit_stop_count():
            # Цель стоит более 10 циклов. Создаём новую задачу перемещения
            # Ищем мешки
            find = self.ms.find_loot()
            found = len(find)
            pr(self.module_name(), 'Группа', self.unit_group, 'обнаружила мешков:', found)
            if found:
                # Мешки обнаружены?
                # Рассчитываем дистанцию до мешков
                hero = self.ms.cmd.get(self.unit_group)
                loot_dst = []
                for e in find:
                    # pr(self.module_name(),hero): [427, 705]
                    # pr(self.module_name(),e): [21, [122, 79, 3, [22.0, 6.0, 689, 190], 0, 0], 1, '0-0']
                    dst = int(self.get_distance(hero, e))
                    # Дистанция, враг, герой
                    loot_dst.append([dst, e])
                    pass
                loot_dst.sort(key=lambda x: x[0], reverse=False)
                loot_closer = loot_dst[0]
                if loot_closer[0] > self.max_loot_dst * 32:
                    pr(self.module_name(), loot_closer)
                    self.next_click_abs_crd = self.ms.screen_to_abs(loot_closer[1][1], loot_closer[1][0])
                    self.action = action.Walk(self.unit_group, self.next_click_abs_crd)
                    self.action.priority = self.get_priority()
                    pr(self.module_name(), 'Идём к дальнему мешку', self.next_click_abs_crd, 'дистанция',
                       loot_closer[0])
                    return

                self.add_get_loot_action()
                return

            if self.progress == 2:
                # Сбор завершён
                self.status = 0
                return

            self.progress = 2
            # Мешки не обнаружены? Финальный сбор.
            self.add_get_loot_action()

    def add_get_loot_action(self):
        # Сбор лута
        self.action = action.GetLoot(self.unit_group)
        self.action.priority = 4 + self.priority_down

    def get_priority(self):
        return self.priority + self.priority_down

    def next_unit(self):
        # Выбор следующего персонажа
        self.current_unit += 1
        if self.current_unit >= len(self.units):
            self.current_unit = 0
        self.unit_group = self.units[self.current_unit]
        pr(self.module_name(), 'Переход хода группе', self.unit_group)
        pass

    def get_current_unit(self, units):
        if units:
            self.units = units
            self.current_unit = 0
            self.unit_group = self.units[self.current_unit]
        pass

    pass


class WalkGroup(Walk):
    task_name = 'Передвижение группы к цели'

    # Разброс дистации для контрольной точки
    walk_min_ct_radius = 5
    # Разброс дистацнии для локальнйо цели
    walk_min_move_radius = 5
    walk_main_ct_radius = 5
    walk_s = True

    def __init__(self, target_abs=[], unit_group=1, walk_s=True):
        self.set_main_target(target_abs)
        self.unit_group = unit_group
        self.walk_s = walk_s

    def add_walk_action(self):
        # Создание действия перемещения героя к контрольной точке.
        self.next_step()
        if self.walk_s:
            self.action = action.GroupWalkS(self.unit_group, self.next_click_abs_crd)
        else:
            self.action = action.GroupWalk(self.unit_group, self.next_click_abs_crd)


class SelfHealthMon(Task):
    # Монитор здовровья персонажа

    # Уровень здоровья.
    # 0 - Желтое ХП.
    # 1 - Красное ХП
    health_level = 0

    # Последний каст
    last_cast_time = 0
    # Время межу кастами
    wait_time = 0.1

    # Дополнительная задача
    weight = 0

    # Количество зелий с красным ХП
    num_if_red = 1
    # Количество зелий
    num = 1

    def __init__(self, unit_group=1, health_level=0, wait_time=100, num=1, num_if_red=1):
        self.unit_group = unit_group
        self.health_level = health_level
        self.wait_time = wait_time / 1000
        if num > num_if_red:
            num_if_red = num
        self.num_if_red = num_if_red
        self.num = num
        self.task_name = 'Мониторинг своего здоровья:', unit_group

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        curr_time = time.time()
        # Проверяем, когда была последняя команда Атаки
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Устанавливаем таймер на текущую группу.
            self.last_cast_time = curr_time

        if curr_time - self.last_cast_time < self.wait_time:
            return

        hero = self.ms.cmd.get(self.unit_group)
        if hero[0] == 0:
            # Если координаты не найдены, пропускаем ход
            return

        hp = self.ms.health_status(hero)

        if hp == 1:
            # Половина ХП
            if self.health_level == 0:
                self.health_task(self.num)

        elif hp == 2:
            # Красное ХП
            self.health_task(self.num_if_red)

    def health_task(self, custom_num=1):
        self.action = action.SelfHeal(self.unit_group, custom_num)
        pass


class Retreat(WalkTask):
    # Отступление мага, если рядом враг.
    # Передаём скрипту группы магов.
    # Скрипт смотрит координаты монстров, если находит, рассчитвает дистанции до магов.
    # Маг, чья дистанция ближе допусимого, отступает.
    task_name = 'Отступление'
    # Список магов
    units = dict()

    # Минимальная дистанция до врага
    min_dst = 3

    # Дополнительная задача
    weight = 0

    all_units = []

    retreat_hero = 0

    def __init__(self, units=[], all_units=[], min_dst=1):
        self.units = dict()
        self.min_dst = min_dst
        self.default_units_info(units)
        self.all_units = all_units

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        # Валидация отображения всех юнитов
        if not self.find_all_heroes_validate():
            return

        # Проверяем, когда была последняя команда отступления
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Устанавливаем таймер на текущую группу.
            curr_time = time.time()
            self.units[self.last_action.unit_group] = curr_time

        self.ms.p.update_units(self.ms.other, self.ms.mm.screen)

        # Проверяем врагов
        enemy = self.ms.p.get_valid_units()
        enemy_dst = []
        if enemy:
            # Рассчитываем дистанцию до героев
            for unit in self.units:
                hero = self.ms.cmd.get(unit)
                if hero and hero[0] > 0:
                    for e in enemy:
                        # pr(self.module_name(),hero): [427, 705]
                        # pr(self.module_name(),e): [21, [122, 79, 3, [22.0, 6.0, 689, 190], 0, 0], 1, '0-0']
                        dst = int(self.get_distance(hero, [e[1][3][3], e[1][3][2]]))
                        # Дистанция, враг, герой
                        enemy_dst.append([dst, e[0], unit])
                        pass
                pass
            pass

        if enemy_dst:
            # Враги обнаружены
            enemy_dst.sort(key=lambda x: x[0], reverse=False)
            # [163, 7, 2]
            min_dst = enemy_dst[0]
            if min_dst[0] < self.min_dst * 32:
                # Враг близко
                hero = min_dst[2]
                last_move_time = self.units.get(hero)
                curr_time = time.time()
                if curr_time - last_move_time < 1:
                    # Герой уже отступал менее секунды назад
                    return
                else:
                    # Для создания действия к отступлению, герой должен подтвердить, что его враг ближайший.
                    if self.retreat_hero != hero:
                        self.retreat_hero = hero
                        return

                    # Создаём действие к оступлению
                    self.action = action.Retreat(hero)
                    self.retreat_hero = 0
                pass

    def find_all_heroes_validate(self):
        # Проверка правильного отображения юнитов
        ret = False
        if self.all_units and self.ms.cmd:
            ret = True
            for unit in self.all_units:
                hero = self.ms.cmd.get(unit)
                # pr(self.module_name(),unit, hero)
                if hero and hero[0]:
                    continue
                else:
                    ret = False
                    break
        return ret

    def default_units_info(self, units):
        # 0 - время последнего хода группы.
        self.units = dict()
        for unit in units:
            self.units[unit] = 0

    pass


class WaitingForAnAttack(Task):
    # Ожидание атаки.
    # Ждём, когда у контрольной группы начнёт тратиться ХП,
    # затем снимаем задачу
    task_name = 'Ожидание атаки'

    units = []

    validate_count = 0
    validate_max_count = 3
    last_time = 0
    min_wait = 1

    def __init__(self, units=[], validate_max_count=3, min_wait=1):
        self.units = units
        self.validate_max_count = validate_max_count
        self.min_wait = min_wait

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        curr_time = time.time()
        if self.last_time == 0:
            self.last_time = curr_time

        if curr_time - self.last_time < self.min_wait:
            return

        self.last_time = curr_time

        # Смотрим ХП юнитов
        for n in self.units:
            if self.ms.hero_health_not_full(n):
                pr(self.module_name(), 'У команды', n, 'не полное здоровье')
                self.validate_count += 1
                break

        # Валидация пройдена, снимаем задачу
        if self.validate_count > self.validate_max_count:
            self.status = 0


class Wait(Task):
    # Ожидание, сек
    wait = 0
    start_time = 0
    task_name = 'Ожидание'

    def __init__(self, wait=1):
        self.start_time = time.time()
        self.wait = wait
        self.task_name = 'Ожидание', wait, 'сек.'

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        curr_time = time.time()
        if curr_time - self.start_time > self.wait:
            pr(self.module_name(), 'Ожидание', self.wait, 'закончено')
            self.status = 0


class Bless(WalkTask):
    # Каст удачи группы на избранную группу
    task_name = 'Каст удачи'

    # Список юнитов, на которых будет наложена магия
    units = dict()
    # Время ожидания повторного каста
    wait_time = 10

    # Дополнительная задача
    weight = 0

    # Если цель дальше этой дистанции, не кастуем магию
    max_dst = 6

    # Герой должен стоять для каста магии
    hero_is_stay = True

    def __init__(self, units=[], unit_group=1, wait_time=10, max_dst=6):
        # Список юнитов, на которых будет наложена магия
        self.units = dict()

        self.unit_group = unit_group
        self.default_units_info(units)
        self.wait_time = wait_time
        self.max_dst = max_dst

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        # Проверяем, когда была последняя команда благославления
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Устанавливаем таймер на текущую группу.
            curr_time = time.time()
            if self.last_run_act_result == 1:
                self.units[self.last_action.unit_target] = curr_time

        # Проходим по списку юнитов
        # Проверяем время, если время истекло - кастуем магию
        curr_time = time.time()
        group_abs = self.ms.get_hero_abs(self.unit_group)
        if not group_abs:
            # Если координаты героя не найдены, пропускаем ход.
            return
        if self.ms.hero_not_found.get(self.unit_group):
            # Если герой не найден, значит он может быть за экраном, пропускаем ход.
            return

        for key, last_time in self.units.items():
            if curr_time - last_time > self.wait_time:
                hero_abs = self.ms.get_hero_abs(key)
                if hero_abs:
                    # Проверяем дистанцию до цели
                    dst = int(self.get_distance(hero_abs, group_abs))
                    if dst < self.max_dst * 32:
                        # Герой стоит?
                        if self.hero_is_stay and not self.ms.hero_is_stay(key):
                            continue
                        self.cast_action(hero_abs, key)
                        break
        pass

    def cast_action(self, hero_abs, key):
        # Создание каста магии
        self.action = action.CastMagic(self.unit_group, self.magic_cast_shift(hero_abs), 'bless', key, priority=7)

    def magic_cast_shift(self, crd_abs):
        # Сдвиг по X, для каста магии
        return [crd_abs[0] - 10, crd_abs[1]]

    def default_units_info(self, units):
        # 0 - время последнего хода группы.
        self.units = dict()
        for unit in units:
            self.units[unit] = 0

    pass


class HealthMon(Bless):
    # Мониторинг здоровья.
    # Если у какой-то группы мало ХП, создаём задачу медику лечить эту группу
    # Уровень здоровья.
    # 0 - Красное ХП
    # 1 - Жёлтое ХП
    # 2 - Не полное ХП

    health_level = 0

    # Последний каст
    last_cast_time = 0
    # Время межу кастами
    wait_time = 0.1

    # Если цель дальше этой дистанции, не кастуем магию
    # 0 - Не важно где цель, маг идёт и лечит
    # 1 - Лечит в рациусе цели, если маг прошёл валидацию координат
    max_dst = 0

    # Дополнительная задача
    weight = 0

    # Группа, которую нужно лечить. Если не выбрано - лечим всех
    target_group = []

    def __init__(self, unit_group=1, health_level=1, wait_time=200, max_dst=0, target_group=[]):
        # Группа, которую нужно лечить. Если не выбрано - лечим всех
        self.unit_group = unit_group
        self.health_level = health_level
        self.wait_time = wait_time / 1000
        self.max_dst = max_dst
        self.target_group = target_group
        self.task_name = 'Мониторинг здоровья группы ' + str(unit_group)

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        curr_time = time.time()
        # Проверяем, когда была последняя команда Атаки
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Устанавливаем таймер на текущую группу.
            self.last_cast_time = curr_time

        if curr_time - self.last_cast_time < self.wait_time:
            return

        # Получаем список героев и их характеристики
        heroes = self.ms.cmd
        if self.target_group:
            heroes = dict()
            for item in self.target_group:
                heroes[item] = self.ms.cmd.get(item)

        small_hp = []
        half_hp = []
        not_full_hp = []

        for key, hero in heroes.items():
            if hero[0]:
                hp = self.ms.health_status(hero)
                if hp == 1:
                    half_hp.append(key)
                elif hp == 2:
                    small_hp.append(key)
                if self.ms.hero_health_not_full(key):
                    not_full_hp.append(key)

        group_abs = [0, 0]
        if self.max_dst:
            # Если указана максимальная дистанция, проводим дополнительные проверки
            group_abs = self.ms.get_hero_abs(self.unit_group)
            if not group_abs or self.ms.hero_not_found.get(self.unit_group):
                # Если координаты героя не найдены, пропускаем ход.
                return

        # Лечим первого потраченого
        ret = False
        if small_hp:
            ret = self.find_hero_to_cast(small_hp, group_abs)

        if not ret and half_hp and self.health_level > 0:
            ret = self.find_hero_to_cast(half_hp, group_abs)

        if not ret and not_full_hp and self.health_level > 1:
            ret = self.find_hero_to_cast(not_full_hp, group_abs)

    pass

    def find_hero_to_cast(self, items, group_abs):
        ret = False
        for item in items:
            hero_abs = self.ms.get_hero_abs(item)
            # Проверяем дистанцию до цели
            if self.max_dst:
                dst = int(self.get_distance(hero_abs, group_abs))
                if dst > self.max_dst * 32:
                    continue
            if self.hero_is_stay and not self.ms.hero_is_stay(item):
                continue
            self.action = action.Heal(self.unit_group, self.magic_cast_shift(hero_abs))
            ret = True
            break

        return ret


class KillEnemyMagic(Bless):
    # Убийство дальних врагов магией.
    # Убиваем с приоритетом.
    # 1. Сначала магов, потом остальных.
    # 2. Сначала дальних, потом ближних

    task_name = 'Убийство врага магией'

    # Убийство именного врага, магией
    cast_magic = ''

    # Минимальная дистанция до врага. Если враг ближе, не атакуем. Это или свой или его убьют свои.
    min_dst = 5
    # Максимальная дистанция, дальше которой маг не атакует.
    max_dst = 10

    # Последний каст
    last_cast_time = 0
    # Время межу кастами
    wait_time = 0.1

    # Не обязательная задача
    weight = 0

    # Сперва атакуем дальние цели
    far_target = True

    # Атаковать только если наша группа видна на карте и определяется
    self_validate = False

    # Определение врага по типу, а не по имени.
    # Этот алгоритм быстрее стандартного, так как не требует дополнительного навода мыши на цель.
    target_type = 0

    # Имя врага
    target_code = ''
    # Проверять имя врага перед атакой.
    validate_target = False
    target_names = dict()
    # Останавливаться, если цель найдена.
    stop_if_target_found = True
    # Валидация магического курсора. Необходима, чтобы герой атакавал магией, я не шёл к цели к месту атаки.
    validate_magic_cursor = True
    # Атака магией через кнопку A
    magic_attack = False

    # Последнее полявление врага
    last_enemy_detect = 0
    # Блокировка других задач, в течении 3 сек., после появления врага
    enemy_detect_block = 0
    # Количество элексиров маны, выпиваемых после каста
    up_mana = 0
    # Только атака на цель. После успешной атаки, снимаем задачу
    just_attack = False

    def __init__(self, min_dst=5, unit_group=3, cast_magic="fire_ball", time_out=0, wait_time=100, target_type=-1,
                 far_target=True, max_dst=10, self_validate=False, target_name='', validate_target=False,
                 magic_attack=False, priority_custom=0, enemy_detect_block=0, magic_cursor=True,
                 stop_if_target_found=True, up_mana=0, just_attack=False):

        self.target_names = dict()
        self.min_dst = min_dst
        self.unit_group = unit_group
        self.cast_magic = cast_magic
        self.close_time = time_out
        self.wait_time = wait_time / 1000
        self.target_type = target_type
        self.far_target = far_target
        self.max_dst = max_dst
        self.self_validate = self_validate
        self.validate_target = validate_target
        self.magic_attack = magic_attack
        self.priority_custom = priority_custom
        self.enemy_detect_block = enemy_detect_block
        self.validate_magic_cursor = magic_cursor
        self.stop_if_target_found = stop_if_target_found
        self.up_mana = up_mana
        self.just_attack = just_attack
        if target_name:
            names = Names()
            target_code = names.get_code_by_name(target_name)
            # Если задано имя цели, получаем её код
            self.target_code = target_code
            # Сброс информации о врагах
            FoundEnemy.found = dict()

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)
        self.block = False

        curr_time = time.time()
        # Проверяем, когда была последняя команда Атаки
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            if self.last_action.act_type == 10:
                # Поиск врага
                target_id = self.last_action.target_id
                if self.last_run_act_result:
                    # Добавляем полученную информацию о цели в список
                    FoundEnemy.found[target_id] = self.last_run_act_result
            else:
                # Атака
                if self.validate_target:
                    # Если была валидация цели, назначаем полученные валидацией значения
                    if self.last_run_act_result == self.target_code:
                        self.last_cast_time = curr_time
                    else:
                        # Атака провалена, цель отличается от заявленной
                        FoundEnemy.found[self.last_action.target_id] = self.last_run_act_result
                else:
                    if self.just_attack:
                        # Если это была разовая атака, снимаем задачу.
                        self.status = 0
                        return
                        # Устанавливаем таймер на текущую группу.
                    if self.last_run_act_result == 1:
                        self.last_cast_time = curr_time

        if curr_time - self.last_cast_time < self.wait_time:
            # pr(self.module_name(),'Блокировка побочных задач')
            self.block = True
            return

        if curr_time - self.last_enemy_detect < self.enemy_detect_block:
            # pr(self.module_name(),'Блокировка побочных задач')
            self.block = True

        if self.self_validate:
            unit_group = self.ms.cmd.get(self.unit_group)
            if not unit_group[0]:
                pr(self.module_name(), 'Герой', self.unit_group, 'не найден, пропуск хода')
                return
            if self.ms.hero_not_found.get(self.unit_group):
                pr(self.module_name(), 'Герой', self.unit_group, 'не найден, пропуск хода')

        self.ms.p.update_units(self.ms.other, self.ms.mm.screen)
        # Проверяем врагов
        target = self.ms.p.get_valid_units()
        hero = self.ms.cmd.get(self.unit_group)
        if not hero:
            return

        valid_target = []
        hp_target = []
        need_find = []
        # Цели выбранного типа врагов
        if target:
            for e in target:
                dst = int(self.get_distance(hero, [e[1][3][3], e[1][3][2]]))
                if self.min_dst * 32 < dst < self.max_dst * 32:
                    # Добавляем только элементы, на соответствующей дистанции
                    unit_type = e[1][5]
                    if 0 <= self.target_type != unit_type:
                        # Если назначена специальная цель, добавляем только её, остальных пропускаем
                        continue

                    if self.target_code:
                        # Если у цели должно быть имя, ищем его
                        if FoundEnemy.found.get(e[0]):
                            if self.target_code != FoundEnemy.found.get(e[0]):
                                # Имя отличается, пропускаем
                                continue
                            else:
                                # Проверка пройдена, добавляем цель в лист
                                pass
                        else:
                            # Имя не найдено, добавляем в список поиска имён.
                            need_find.append(e)
                            # И пропускаем
                            continue

                    valid_target.append([dst, e])
                    hp = e[1][4]
                    hp_target.append([hp, e])

        if valid_target:
            pr(self.module_name(), 'Враг обнаружен', valid_target)
            self.last_enemy_detect = curr_time
            # Выбираем самую опасную цель
            # [87, [69, 50, 3, [9.0, 10.0, 274, 320], 2, 3], 1, '0-0']
            enemy = []
            if self.target_type == -1:
                for e in valid_target:
                    unit_type = e[1][1][5]
                    if unit_type == 3:
                        # Находим некромансера
                        enemy = e[1]
                        break
            if not enemy:
                # Атакуем цель, у которой меньше всех хп
                hp_target.sort(key=lambda x: x[0], reverse=True)

                if hp_target[0][0] != 0:
                    enemy = hp_target[0][1]
                else:
                    # Атакуем самую дальнюю цель
                    reverse = True
                    if not self.far_target:
                        # Атакуем сперва ближние цели
                        reverse = False
                    valid_target.sort(key=lambda x: x[0], reverse=reverse)
                    enemy = valid_target[0][1]

            # Атакуем врага
            enemy_abs = self.ms.get_unit_abs_crd(enemy)
            pr(self.module_name(), 'Атака на цель', valid_target)
            self.cast_action(enemy_abs, enemy[0])

        if need_find:
            enemy = need_find[0]
            self.find_name_action(enemy)

    def get_enemy_abs(self, enemy):
        # Получить координаты врага
        # pr(self.module_name(),enemy)
        crd_abs = self.ms.screen_to_abs(enemy[1], enemy[0])
        shift_abs = self.ms.get_shift(crd_abs)
        return shift_abs

    def find_name_action(self, enemy):
        self.block = True
        enemy_abs = self.ms.get_unit_abs_crd(enemy)
        pr(self.module_name(), 'Запрос информации о цели', enemy)
        self.action = action.TargetInfo(enemy_abs, enemy[0], self.unit_group, self.stop_if_target_found)

    def cast_action(self, hero_abs, target_id):
        self.block = True
        # Создание каста магии
        priority = 2
        if self.priority_custom:
            priority = self.priority_custom
        self.action = action.CastMagic(self.unit_group, self.magic_cast_shift(hero_abs), self.cast_magic, target_id,
                                       attack=self.magic_attack, cursor=self.validate_magic_cursor, priority=priority,
                                       up_mana=self.up_mana)


class KillEnemy(KillEnemyMagic):
    # Убийство врага
    task_name = 'Убийство врага'

    def cast_action(self, hero_abs, target_id):
        target_code = ''
        if self.validate_target:
            target_code = self.target_code

        self.action = action.Attack(self.unit_group, hero_abs, target_code, target_id, self.stop_if_target_found)


class LightResist(Bless):
    # Защита от магии воздуха
    task_name = 'Защита от магии воздуха'

    def cast_action(self, hero_abs, key):
        # Создание каста магии
        self.action = action.CastMagic(self.unit_group, self.magic_cast_shift(hero_abs), 'light_res', key, priority=8)


class FireResist(Bless):
    # Защита от магии огня
    task_name = 'Защита от магии огня'

    def cast_action(self, hero_abs, key):
        # Создание каста магии
        self.action = action.CastMagic(self.unit_group, self.magic_cast_shift(hero_abs), 'fire_res', key, priority=8)


class WaterResist(Bless):
    # Защита от магии воды
    task_name = 'Защита от магии воды'

    def cast_action(self, hero_abs, key):
        # Создание каста магии
        self.action = action.CastMagic(self.unit_group, self.magic_cast_shift(hero_abs), 'water_res', key, priority=8)


class EarthResist(Bless):
    # Защита от магии земли
    task_name = 'Защита от магии земли'

    def cast_action(self, hero_abs, key):
        # Создание каста магии
        self.action = action.CastMagic(self.unit_group, self.magic_cast_shift(hero_abs), 'earth_res', key, priority=8)


class StoneWall(Bless):
    # Каменная сена
    task_name = 'Каменная сена'
    # Расстояние, относительно мага в единицах измерения мини-карты: x,y
    target_shift = [1, 1]
    # Время последней активации
    last_action_time = 0
    # Дополнительная задача
    weight = 0

    def __init__(self, unit_group=1, target_shift=[], wait_time=10):
        self.unit_group = unit_group
        self.target_shift = target_shift
        self.wait_time = wait_time

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)
        curr_time = time.time()

        # Проверяем, когда была последняя команда
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Устанавливаем таймер на текущую группу.
            if self.last_run_act_result == 1:
                self.last_action_time = curr_time

        # Проходим по списку юнитов
        curr_time = time.time()

        if curr_time - self.last_action_time > self.wait_time:
            hero_abs = self.ms.get_hero_abs(self.unit_group)
            if hero_abs:
                cast_abs = [hero_abs[0] + self.target_shift[0] * 32, hero_abs[1] + self.target_shift[1] * 32]
                self.cast_action(cast_abs)

    def cast_action(self, target_abs):
        # Создание каста магии
        self.action = action.StoneWall(self.unit_group, self.magic_cast_shift(target_abs))

    pass


class ShiftWalk(WalkTask):
    # Занимаем позицию, относительно отряда
    # Расстояние, относительно отряда в единицах измерения мини-карты: x,y

    target_shift = [1, 1]
    # Время последней активации
    last_action_time = 0
    # Группа, к которой мы идём
    target_group = 1
    # Валидация исполнения команды
    validate = True
    # Задача выполнена
    task_stage = 0
    target_center = False
    # 0 - не центрировать, 1 - центрирование на юните, 2 - на цели
    last_center_group = 0

    def __init__(self, unit_group=1, target_group=1, target_shift=[], last_center_group=0, validate=True):
        self.task_stage = 0
        self.target_center = False
        self.last_action_time = 0
        self.unit_group = unit_group
        self.target_group = target_group
        self.target_shift = target_shift
        self.validate = validate
        self.last_center_group = last_center_group
        self.task_name = 'Смещение группы ' + str(unit_group) + ' к группе ' + str(target_group) + str(target_shift)
        # Одинарная задача. Снимается после выполнения.

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            if self.last_action.act_type == 2:
                # Центрирование
                self.target_center = True
            else:
                # Последняя команда была движение. Меняем статус задачи
                if self.last_center_group == 0:
                    # Если центрировать не надо, пропускаем этот стаж
                    self.task_stage = +2
                else:
                    self.task_stage = +1

        if self.task_stage > 0:
            # Мы уже сходили - центрируем
            if self.task_stage > 1:
                self.status = 0
                # Мы сходили и отцентрировались
                return
            group_to_center = self.unit_group
            if self.last_center_group == 2:
                group_to_center = self.target_group
            self.action = action.Center(group_to_center)
            self.task_stage += 1
            return

        # Проверяем валидность целевой группы и группы смещения. Если целевая группа не валидна, центрируем её.
        unit_crd = self.ms.get_hero(self.unit_group)
        if not self.validate and not unit_crd:
            self.status = 0
            # Координаты героя не найдены, снимаем задачу
            return

        target_abs = self.ms.get_hero_abs(self.target_group)
        if target_abs and not self.ms.hero_not_found.get(self.target_group):
            # Цель найдена, двигаемся к ней
            self.walk_action(target_abs)
        else:
            if self.target_center:
                # Последняя выполненная задача - центрирование
                pr(self.module_name(), 'Берём координаты по центру карты')
                # Берём координаты по центру карты
                # y,x
                tank_screen = [425, 342]
                tank_x_y = self.ms.abs_to_screen(tank_screen[0], tank_screen[1])
                target_abs = [tank_x_y[1], tank_x_y[0]]
                self.walk_action(target_abs)
            else:
                pr(self.module_name(), 'Координаты целевой группы не найдены, центрируемся на ней:', self.target_group)
                self.action = action.Center(self.target_group)

    def walk_action(self, target_abs):
        abs_crd = [target_abs[0] + self.target_shift[0] * 32, target_abs[1] + self.target_shift[1] * 32]
        shift_crd = self.walk_shift_abs_crd(abs_crd)
        self.action = action.Shift(self.unit_group, self.target_group, shift_crd)


class ShiftWalkColor(TaskOne):
    # Занимаем позицию, уникального цвета на карте
    # Расстояние, относительно отряда в пикселях: x,y

    target_shift = [32, 32]
    # Валидация исполнения команды
    validate = True
    # Задача выполнена
    task_stage = 0
    target_color = []
    target_center = False

    def __init__(self, unit_group=1, target_shift=[], target_color=[]):
        self.unit_group = unit_group
        self.target_shift = target_shift
        self.target_color = target_color
        self.task_name = 'Смещение группы ' + str(unit_group) + ' по координатам ' + str(target_shift) + \
                         'относительно цвета' + str(target_color)

    def default_action(self):
        found = self.ms.find_color_crd(self.target_color)
        crd = []
        if len(found):
            crd = found[0]
            abs_crd = [crd[1] + self.target_shift[0], crd[0] + self.target_shift[1]]
            # shift_crd = self.walk_shift_abs_crd(abs_crd)
            self.action = action.Walk(self.unit_group, abs_crd)


class Center(TaskOne):
    # Центрирование группы

    def __init__(self, unit_group=1):
        self.unit_group = unit_group
        self.task_name = 'Центрирование группы', unit_group

    def default_action(self):
        self.action = action.Center(self.unit_group)


class AllStop(TaskOne):
    # Остановка группы
    task_name = 'Остановка всех групп'

    def __init__(self):
        pass

    def default_action(self):
        self.action = action.AllStop()


class HealRegen(TaskOne):
    # Регенирация здоровья
    task_name = 'Регенерация здоровья'

    def __init__(self, unit_group=1):
        self.unit_group = unit_group

    def default_action(self):
        self.action = action.HealRegen(self.unit_group)


class ManaRegen(TaskOne):
    # Регенирация маны
    task_name = 'Регенирация маны'

    def __init__(self, unit_group=1):
        self.unit_group = unit_group

    def default_action(self):
        self.action = action.ManaRegen(self.unit_group)


class SpeedPlus(TaskOne):
    # Увеличение скорости
    task_name = 'Увеличение скорости'

    speed = 4
    plus = True

    def __init__(self, speed=4):
        self.plus = True
        self.speed = speed

    def default_action(self):
        self.action = action.Speed(self.speed, self.plus)


class SpeedMinus(SpeedPlus):
    # Уменьшение скорости
    task_name = 'Уменьшение скорости'

    def __init__(self, speed=4):
        self.plus = False
        self.speed = speed


class SetHeroesGroup(TaskOne):
    # Назначить группы героев
    task_name = 'Назначить группы героев'
    units = []

    # Центрирование группы
    def __init__(self, units=[]):
        self.units = units

    def default_action(self):
        self.action = action.SetGroups(self.units, self.ms)


class MagicMenu(TaskOne):
    task_name = 'Меню магии (показать/скрыть)'
    show = True

    def __init__(self, show=True):
        self.show = show

    def default_action(self):
        self.action = action.MagicMenu(self.show)


class SetOtherHeroesGroup(TaskOne):
    task_name = 'Назначение других юнитов в группу'

    # Назначить остальных героев в группу
    def __init__(self, unit_group=1):
        self.unit_group = unit_group

    def default_action(self):
        self.action = action.SetOtherGroups(self.unit_group, self.ms)


class BindAutoMagic(TaskOne):
    # Назначение авто-магии
    task_name = 'Назначение авто-магии'
    # Название магии
    magic_name = ''

    def __init__(self, unit_group=1, magic_name=''):
        self.unit_group = unit_group
        self.magic_name = magic_name

    def default_action(self):
        self.action = action.BindAutoMagic(self.unit_group, self.magic_name)


class BindHotKeyInvent(TaskOne):
    # Назначение горячей клавиши на инвентарь
    task_name = 'Назначение горячей клавиши на инвентарь'
    key = ''
    color = []

    def __init__(self, unit_group=1, key='', color=[]):
        self.unit_group = unit_group
        self.key = key
        self.color = color

    def default_action(self):
        self.action = action.BindHotKeyInvent(self.unit_group, self.key, self.color)


class Teleport(TaskOne):
    # Каменная сена
    task_name = 'Телепорт'
    # Расстояние, относительно мага в единицах измерения мини-карты: x,y
    target_shift = [1, 1]

    def __init__(self, unit_group=1, target_shift=[]):
        self.unit_group = unit_group
        self.target_shift = target_shift

    def default_action(self):
        hero_abs = self.ms.get_hero_abs(self.unit_group)
        if hero_abs:
            cast_abs = [hero_abs[0] + self.target_shift[0] * 32, hero_abs[1] + self.target_shift[1] * 32]
        self.action = action.CastMagic(self.unit_group, self.magic_cast_shift(cast_abs), 'teleport')

    def magic_cast_shift(self, crd_abs):
        # Сдвиг по X, для каста магии
        return [crd_abs[0] - 10, crd_abs[1]]


class DarkDetector(Bless):
    # Детектор затемнения героя.
    # Идея. Когда героя затемняет некромант или дракон, герой это обнаруживает и искользует свиток оствеления на себя.
    # Так как дракон может превращать в камень, то лучше если свиток использует другой герой.
    # Класс должен принимать два аргумента: целевой герой и герой со свитком.
    # В случае затемнения целевого героя, герой со свитком освещает его и ставит паузу на 20 сек.
    task_name = 'Детектор затемнения героя'

    weight = 0
    last_action_time = 0
    wait_time = 12

    def __init__(self, unit_group=1, target_group=1, wait_time=12):
        self.unit_group = unit_group
        self.target_group = target_group
        self.wait_time = wait_time

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        curr_time = time.time()
        # Проверяем, когда была последняя команда
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Устанавливаем таймер на текущую группу.
            self.last_action_time = curr_time

        if curr_time - self.last_action_time > self.wait_time:
            if self.ms.dark_detect(self.target_group):
                # Обнаружено затемнение
                hero_abs = self.ms.get_hero_abs(self.target_group)
                if hero_abs:
                    self.cast_action_light(hero_abs)

    def cast_action_light(self, target_abs):
        # Создание каста магии
        self.action = action.Light(self.unit_group, self.magic_cast_shift(target_abs))

    pass


class LifeHiredUnits(Task):
    # Проверка жизни наёмников. Цикл мониторит статус ошибок нахождения бойцов.
    # Если ошибка больше определённого числа, проверяет состояние бойца.
    # Если состояние стабильное, ожидает следующей проверки, иначе выдаёт исключение, прерывающее цикл.

    # Список наёмников
    units = dict()

    # Валидация смерти наёмников
    unit_validate = dict()
    max_validate_count = 2

    # Дополнительна задача
    weight = 0

    # Проверяем жизнь юнита каждые 3 секунды
    default_wait_time = 3
    not_found_wait_time = 1
    wait_time = default_wait_time

    def __init__(self, units=[]):
        self.unit_validate = dict()
        self.default_units_info(units)
        self.task_name = 'Валидация жизни наёмников:', units

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        curr_time = time.time()
        # Проверяем, когда была последняя команда проверки
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Устанавливаем таймер на текущую группу.
            if self.last_run_act_result:
                # Меняем вес задачи. Она не будет закрыта, пока валидация не будет пройдана
                self.weight = 1
                self.wait_time = self.not_found_wait_time
                # Герой не найден, обновляем счётчик
                if self.update_validate_units():
                    self.defeat_action()
                    return
            else:
                if self.reset_validate_units():
                    # Нет ошибок нахождения наёмников, меняем задачу на дополнительную
                    self.weight = 0
                    self.wait_time = self.default_wait_time

            # Обновляем счётик для группы
            self.units[self.last_action.unit_group][0] = curr_time

        # Проходим по списку юнитов
        # Проверяем время, если время истекло - перепроверяем
        for key, unit_info in self.units.items():
            last_time = unit_info[0]
            hero = unit_info[1]
            if curr_time - last_time > self.wait_time:
                # Проверка жизни героя
                hero_error = self.ms.hero_not_found.get(key)
                if hero_error and hero_error > 10:
                    # Если ошибок больше 10, запрашиваем проверку жизни героя
                    self.set_action(hero)
                    break

    def reset_validate_units(self):
        key = self.last_action.unit_group
        self.unit_validate[key] = 0
        # pr(self.module_name(),'Сброс счётчика смерти группы', key)
        i = 0
        for key, val in self.unit_validate.items():
            i += val
        if i > 0:
            return False
        return True

    def update_validate_units(self):
        key = self.last_action.unit_group
        ret = False
        pr(self.module_name(), 'Обновление счётчика смерти группы', key)
        if self.unit_validate.get(key):
            self.unit_validate[key] += 1
            if self.unit_validate[key] > self.max_validate_count:
                ret = True
        else:
            self.unit_validate[key] = 1

        return ret

    def defeat_action(self):
        self.action = action.Defeat()

    def set_action(self, hero):
        # Создание каста магии
        self.action = action.ValidateLife(hero)

    def default_units_info(self, units):
        # 0 - время последнего хода группы.
        self.units = dict()
        for unit in units:
            self.units[unit.group] = [0, unit]

    pass
