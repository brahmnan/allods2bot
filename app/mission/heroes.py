# Отслеживание информации о состоянии героев
# Получаем данные из монитора, находим по ним героев, сохраняем данные о здоровье и координатах
from app.mission.actions import Magic, Actions
from app.commands import Mouse, Color
import app.config as cfg
import time


class Hero:
    # Герой
    name = ''
    group = 0
    health = 0
    code_name = 0
    crd_abs = [0, 0]
    # Пиксель для определения героя по картинке
    face_pixel = [942, 313, 0, 0, 0]

    mouse = Mouse()
    act = Actions()
    color = Color()
    pass

    def __str__(self):
        return self.group


class Mage(Hero):
    # Маг
    magic = Magic()

    # Авто магия
    main_magic = magic.fire_arrow
    current_magic = magic.fire_arrow

    # Магия
    last_shield = 0

    # Действия. Задаются в условях миссии. Герой возвращается к ним, после лечения или отступления
    act_walk_to_target = 0
    act_walk_war_to_target = 1
    act_defence_target = 2
    act_heal_target = 3
    act_shield_self = 4

    # Параметры целей
    target_crd = [0, 0]
    target_group = 1

    main_action = act_walk_to_target
    last_action = act_walk_to_target

    def reset(self):
        self.main_magic = self.magic.fire_arrow
        self.main_action = self.act_walk_to_target
        self.last_action = self.act_walk_to_target
        pass

    def set_defence(self, n):
        # Защита группы
        self.main_action = self.act_defence_target
        self.target_group = n
        pass

    def set_auto_magic_arrow(self, force=False):
        # Назначение огненной стрелы по умолчанию
        self.main_magic = self.magic.fire_arrow
        self.select_auto_magic_arrow(force)

    def set_auto_healing(self, force=False):
        # Назначение лечения по умолчанию
        self.main_magic = self.magic.heal
        self.select_auto_healing(force)

    def select_auto_magic_arrow(self, force=False):
        # Выбор огненной стрелы
        if self.current_magic != self.magic.fire_arrow or force:
            self.current_magic = self.magic.fire_arrow
            self.act.select_group(self.group)
            self.act.fire_arrow()
            self.act.auto_magic()
            print(self.name, 'включает авто-огненную стрелу')

    def select_auto_healing(self, force=False):
        # Выбор лечения
        if self.current_magic != self.magic.heal or force:
            self.current_magic = self.magic.heal
            self.act.select_group(self.group)
            self.act.magic_heal()
            self.act.auto_magic()
            print(self.name, 'включает авто-хилинг')

    def mana_regen(self):
        # Регенерация маны
        self.act.select_group(self.group)
        self.act.mana_regen()

    def set_default_magic(self):
        # Устанавливаем магию по умолчанию
        if self.current_magic != self.main_magic:
            # Если текущая магия не равна основной
            self.act.select_group(self.group)
            # Выбираем магию
            if self.main_magic == self.magic.fire_arrow:
                self.act.fire_arrow()
            elif self.main_magic == self.magic.heal:
                self.act.magic_heal()
            else:
                self.magic.select_magic(self.main_magic)

            # Назначаем автомагию
            self.act.auto_magic()
            print(self.name, 'возвращает автомагию:', self.main_magic)
            self.current_magic = self.main_magic

    def cast_to_target(self, name, x, y):
        print('Каст магии', name, 'на цель', x, y)
        self.magic.select_magic(name)
        self.mouse.left(x, y)
        pass

    def auto_heal_self(self):
        if self.current_magic != self.magic.heal:
            self.act.select_group(self.group)
            # Лечение с помощью горяей клавиши
            self.act.magic_heal()
            self.act.auto_magic()
            self.mouse.left(self.crd_abs[0], self.crd_abs[1])
            print(self.name, 'лечит себя и включает автомагию:', self.magic.heal)
            # Текущая магия
            self.current_magic = self.magic.heal

    def auto_heal(self, x, y):
        if self.current_magic != self.magic.heal:
            self.act.select_group(self.group)
            # Лечение с помощью горяей клавиши
            self.act.magic_heal()
            self.act.auto_magic()
            self.mouse.left(x, y)
            print(self.name, 'включает автомагию:', self.magic.heal)
            # Текущая магия
            self.current_magic = self.magic.heal

    def shield_status(self, matrix, cmd, x=0, y=0):
        if x == 0:
            x = self.crd_abs[0]
            y = self.crd_abs[1]

        hs = self.shield_info(matrix, cmd)
        if hs == 2:
            t1 = time.time()
            if self.last_shield == 0 or (t1 - self.last_shield) > 10:
                # Активируем щит, только если прошло 10 сек
                self.act.select_group(self.group)
                if self.magic_shield_off():
                    self.act.use_shield(0)
                    self.mouse.left(x, y)
                    if not self.magic_shield_off():
                        self.last_shield = t1
                        print(self.name, '- активация щита')
        return hs

    def magic_shield_off(self):
        # Проверка магического щита
        # Проверяем наличие нуля в меню брони
        if self.color.validate(1008, 649, 64, 92, 72):
            return True
        return False

    def shield_info(self, matrix, cmd):
        a = matrix
        f = cmd.get(self.group)
        # Герой не найден
        ret = 0
        min_color = 127
        if f[0] != 0:
            # Поиск пикселя в матрице
            # Координаты команд x,y: 8,8,8
            # 15, -9
            # Проверка ОДЗ
            if f[0] - 9 < cfg.view_window['ym'] and f[1] + 15 < cfg.view_window['x']:
                # print(a[f[0] - 9, f[1] + 15])
                if a[f[0] - 9, f[1] + 15][0] > min_color and a[f[0] - 9, f[1] + 15][1] > min_color \
                        and a[f[0] - 9, f[1] + 15][2] > min_color:
                    ret = 1

                else:
                    # Щит отключён
                    ret = 2

        return ret


class Igles(Hero):
    # Иглез
    name = 'Иглез'
    group = 1
    code_name = 59
    face_pixel = [942, 313, 56, 40, 24]

    def heal_regen(self):
        # Регенерация маны
        self.act.select_group(self.group)
        self.act.heal_regen()

    def heal_up(self):
        # Выпить снадобье задоровья
        self.act.select_group(self.group)
        self.act.heal_up()

    pass


class Hildar(Mage):
    # Гильдариус
    name = 'Гильдариус'
    group = 2
    code_name = 115


class Diana(Mage):
    # Дайна
    name = 'Дайна'
    group = 3
    code_name = 73
    face_pixel = [942, 313, 216, 172, 136]


class Healer(Mage):
    # Наёмник. Маг - хилер
    name = 'Хилер'
    group = 4
    face_pixel = [942, 313, 128, 88, 56]


class Elf(Mage):
    # Наёмник. Маг - эльф
    name = 'Эльф'
    group = 5
    face_pixel = [942, 313, 32, 48, 16]


class Ork(Hero):
    # Наёмник. Орк - лучник
    name = 'Орк'
    group = 6
    face_pixel = [942, 313, 96, 36, 8]


class Archer(Hero):
    # Наёмник. Лучница
    name = 'Лучница'
    group = 7
    face_pixel = [942, 313, 224, 160, 112]


class Warrior(Hero):
    # Наёмник. Воин
    name = 'Воин'
    group = 8
    face_pixel = [942, 313, 216, 160, 112]


class Troll(Hero):
    # Наёмник. Воин
    name = 'Тролль'
    group = 9
    face_pixel = [942, 320, 96, 76, 112]


class Healer(Mage):
    # Наёмник. Маг - хилер
    name = 'Хилер'
    group = 4
    face_pixel = [942, 313, 128, 88, 56]


class Elf(Mage):
    # Наёмник. Маг - эльф
    name = 'Эльф'
    group = 5
    face_pixel = [942, 313, 32, 48, 16]


class Enchantress1(Mage):
    # Волшебница
    name = 'Волшебница 1'
    group = 4
    face_pixel = [942, 313, 232, 192, 152]


class Enchantress2(Mage):
    # Волшебница
    name = 'Волшебница 2'
    group = 9
    face_pixel = [942, 313, 232, 192, 152]


class Enchantress3(Mage):
    # Волшебница
    name = 'Волшебница 3'
    group = 0
    face_pixel = [942, 313, 232, 192, 152]


class Catapult(Mage):
    # Катапульта
    name = 'Катапульта'
    group = 7
    face_pixel = [942, 313, 48, 24, 8]


class Other(Hero):
    # Другие герои
    name = 'Другие герои'
    group = 6


class Group:
    # Группа, раздёленная на типы
    # Танк - герой, собирающий урон. Должен быть один
    tank = 0
    # Группа лучников
    archers = []
    # Маги
    mages = []

    # Расстояние до первого слоя лучников
    archers_layer = 3
    # Расстояние до магов
    mages_layer = 4

    def set_group(self, tank=1, archers=[], mages=[]):
        self.tank = tank
        self.archers = archers
        self.mages = mages

    pass
