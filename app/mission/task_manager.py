# Система управления задачами
import time
from operator import attrgetter
from app.mission.monitor import MainScreen
from app.ineterface import MainMenu, pr


class TaskManager:
    # Мэнеджер задач
    # Цикл запуска и отслеживания выполнения задач.
    # Идея: создаётся экземляр класса, в него добавляются задачи. Затем он запускает задачи в соответствии с их
    # приоритетом, давая каждой задачи выполнить одно действие.
    # Задачи крутятся в цикле до тех пор, пока:
    #   1. Не останется задач.
    #   2. Не возникнет фатальная ошибка, вынуждающая загрузить игру.
    #   3. Задачи не будут прерваны, в связи с победой.
    # В начале каждого цикла, менеджер опрашивает профильные модули и они формируют задачи.
    # Модули рассматиривают статус выполнения текущих задач и обновляют их, и добавляют новые.
    # Менеджер сортирует задачи по степени важности, удаляет выполненные и последовательно выполняет остальные.
    # Задачи могут добаляться, как заранее запланированные - управляющие задачи - задаются в стратегии.
    # Так и по ходу выполонения - текущие. Задаются модулями.
    # Задачи скармливаются профильным модулям, которые, на время выполнения (шага) задачи, перехватывают управление.
    # Выполнение задачи, должно быть не более 100мс.
    #
    # Реализация: задачи добавляются из вне.
    # Каждая задача содержит в себе функционал, обрабатывающий данные с экрана.
    # В результате обработки данных, задача создаёт действие с определённым приоритетом.
    # Менеджер задач выполняет самое приоритетное действие и цикл повторяется.
    # По факту выполнения действий, задача меняет свой статус.
    # Если задача выполнена, она меняет свой статус и удаляется.

    # Массив текущих задач
    module_name = 'МЗ'
    tasks = dict()
    # Для каждой задачи назначается уникальный номер
    task_id = 1
    # Массив действий
    actions = []
    act_id = 1
    # Последнее запущенное задание
    last_run_action_id = 0
    # Результат выполнения последнего задания
    last_run_action_result = 0
    # Ожидание после каждого цикла
    sleep = 10

    # Информация об игре
    ms = MainScreen()
    # Управление игрой
    menu = MainMenu()

    # Победа при появлении окна
    win_final = 1
    # Победа в миссии
    win = 0
    # Выход из цикла
    win_exit = 0
    # Поражение в миссии
    defeat = 0

    def __init__(self, validate=[], bs=False):
        self.tasks = dict()
        self.actions = []
        self.find_in_black_sector(bs)
        if validate:
            self.set_validate_group(validate)

    def add_task(self, task, parent_id=0, block_task_id=0):
        if self.defeat or self.win_exit:
            return
        # Добавление задачи в цикл
        # Назначение id задачи
        task_id = self.task_id
        task.task_id = task_id
        self.task_id += 1
        if parent_id > 0:
            task.task_parent = parent_id
            task.status = 2
            pr(self.module_name, 'Добавлена дочерняя задача:', str(parent_id) + '.', task_id, task.task_name)
        else:
            pr(self.module_name, 'Добавлена задача:', str(task_id) + '.', task.task_name)

        if block_task_id:
            # Задача, которая будет блокировать все, ссылающиеся на неё.
            task.block_task_id = block_task_id

        # Добавление задачи в цикл
        self.tasks[task_id] = task
        return task_id

    def run_task(self, task='', parent_id=0, block_task_id=0):
        if task:
            task_id = self.add_task(task, parent_id, block_task_id)
        self.run()
        return task_id

    def remove_task(self, task_id):
        if self.defeat or self.win_exit:
            return
        # Удаление задачи
        if self.tasks.get(task_id):
            pr(self.module_name, 'Удаление задачи:', task_id)
            self.tasks[task_id].status = 0

    def clear_tasks(self):
        self.tasks = dict()

    def add_action(self, action):
        if self.defeat or self.win_exit:
            return
        # Добавление действия
        act_id = self.act_id
        action.act_id = act_id
        self.act_id += 1
        self.actions.append(action)
        pr(self.module_name, 'Добавлено действие:', str(act_id) + '.', action.act_name)
        return act_id

    def run(self):
        # Запуск цикла выполнения задач.
        while True:
            if self.defeat or self.win_exit:
                break

            # time1 = time.time()
            if not self.tasks:
                # Если нет задач, значит мы победили.
                break

            main_tasks = 0
            for key, task in self.tasks.items():
                if task.weight == 1:
                    main_tasks += 1

            if not main_tasks:
                # Нет основных задач
                break

            # 1. Обновляем информацию об игре
            self.ms.find()

            # 2. Проверяем появление сообщений.
            dialog = self.mod_dialog()
            if dialog:
                if dialog == 1:
                    # Победа
                    self.win = 1
                    pr(self.module_name, "Мы победили")
                    if self.win_final == 1:
                        self.win_exit = 1
                        self.clear_tasks()
                        self.menu.win_menu_click()
                    else:
                        self.menu.win_cancel()
                    pass
                elif dialog == 2:
                    # Стандартный диалог
                    self.menu.propusk('enter', 335, 335)
                    continue
                elif dialog == 3 or dialog == 4:
                    # Поражение
                    self.menu.to_main_menu()
                    self.defeat = 1
                    break

            # Обнуляем список действий, которые нужно выполнить
            self.actions = []

            # 3. Опрашиваем профильные модули, создаём список задач
            new_tasks = dict()
            task_to_remove = []
            block_tasks = dict()
            for task_id, task in self.tasks.items():
                # Обновляем задачу
                if task.status == 1:
                    # Рассматриваем только активные задачи
                    task.update(self.ms, self.last_run_action_id, self.last_run_action_result)
                    # Получаем действие
                    action = task.get_action()
                    if action:
                        action.block_task_id = task.block_task_id
                        action.task_id = task_id
                        act_id = self.add_action(action)
                        # Сохраняем в задачу id последнего действия
                        task.act_id = act_id

                    new_tasks[task_id] = task
                    # Проверяем блокировку
                    if task.block:
                        block_tasks[task_id] = task.block
                elif task.status == 2:
                    # Ожидающие задачи не обновляем
                    new_tasks[task_id] = task
                elif task.status == 0:
                    # Отработанные задачи, удаляем
                    pr(self.module_name, 'Задача:', str(task_id) + '.', task.task_name, 'выполнена')
                    task_to_remove.append(task.task_id)

            # Обновляем список задач
            self.tasks = new_tasks
            # Меняем статус дочерних задач, если их родители были удалены
            if task_to_remove:
                for parent in task_to_remove:
                    for task_id, task in self.tasks.items():
                        if task.status == 2 and task.task_parent == parent:
                            # Активируем задачу
                            task.status = 1
                            pr(self.module_name, 'Дочерняя задача активирована:', str(task.task_id) + '.',
                               task.task_name)
                            self.tasks[task_id] = task

            # time2 = time.time()
            # print(time2 - time1)
            if self.actions and block_tasks:
                # Проверяем заблокированные задачи
                new_actions = []
                for act in self.actions:
                    if act.blocked and block_tasks.get(act.block_task_id):
                        # print('Действие', act.act_id, ' заблокированно другой задачей:', act.block_task_id)
                        continue
                    new_actions.append(act)
                self.actions = new_actions

            if self.actions:
                # Если есть действия к выполнению
                # 4. Сортируем действия по приоритету
                # Если есть задачи
                act_sort = sorted(self.actions, key=attrgetter('priority'))
                # print(act_sort)
                # 5. Выполняем самое важное действие
                action = act_sort[0]
                self.last_run_action_id = action.act_id
                pr(self.module_name,
                   'Запуск действия: Задача-' + str(action.task_id) + ' - ' + str(action.act_id) + '.',
                   action.act_name)
                action.do()
                self.last_run_action_result = action.result
                if action.defeat:
                    # Если возникло поражение, прерываем цикл.
                    self.defeat = 1
                    break
            else:
                self.last_run_action_id = 0

            # Пауза между проверками
            time.sleep(self.sleep / 1000)
        pass

    def mod_dialog(self):
        # Мониторинг начавшегося диалога
        ret = 0
        # Типы диалогов
        #   1. Победа: 429,361,40,24,40; 389,347,248,220,152
        #   2. Сообщение: 323,316,40,24,40; 293,498,248,220,152
        #   3  Поражение: 323,316,40,24,40; 293,561,248,220,152
        #   4. Поражение: Иглез погиб
        dialogs = {1: [[[429, 361], [40, 24, 40]], [[389, 347], [248, 220, 152]]],
                   2: [[[323, 316], [40, 24, 40]], [[293, 498], [248, 220, 152]]],
                   3: [[[323, 316], [40, 24, 40]], [[293, 561], [248, 220, 152]]],
                   4: [[[300, 380], [40, 24, 40]], [[293, 346], [248, 220, 152]]]}
        for key, val in dialogs.items():
            # Переводим относительные координаты в абсолютные
            r = 0
            for item in val:
                screen_crd = self.ms.abs_to_screen(item[0][0], item[0][1])
                # print(key, self.ms.matrix[screen_crd[1], screen_crd[0]])
                if self.ms.matrix[screen_crd[1], screen_crd[0]][0] == item[1][2] \
                        and self.ms.matrix[screen_crd[1], screen_crd[0]][1] == item[1][1] \
                        and self.ms.matrix[screen_crd[1], screen_crd[0]][2] == item[1][0]:
                    r += 1
                    pass
            if r == 2:
                # Если оба цвета прошли валидацию, возвращаем ключ сообщения
                ret = key
                break
            pass
        return ret

    def set_validate_group(self, groups):
        # Установить валидацию нахождения групп юнитов
        pr(self.module_name, 'Добавлена группа валидации:', groups)
        self.ms.set_validate_groups(groups)

    def find_in_black_sector(self, find=True):
        # Поиск врагов в темноте
        pr(self.module_name, 'Поиск врагов в темноте включён')
        self.ms.find_black_sector = find

    pass
