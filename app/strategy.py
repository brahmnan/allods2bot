# Стратегия управления героем.
# Создание персонажа, закупка в магазине, диалоги, запуск карт.

# Вся игра делится на этапы, для каждого этапа есть своя функция с набором комманд. Для упрощения тестирования, каждый
# этап можно запустить отдельно, он должен иметь сохранённую запись с номером этапа. По умолчанию игра запускается
# с 0 этапа, затем это значение меняется, запуская новые функции.

import app.ineterface as inter
import app.tavern as tavern
import time
import app.mission.mission as mission
from app.mission.heroes import *
from app.mission.mission import Mission
from app.ineterface import pr
from app.shop import Shop


class Strategy:
    # Управление стратегией при прохождении игры

    # Этап прохождения
    stage = 0
    ms = Mission()
    mm = inter.MainMenu()
    tv = tavern.Tavern()
    shop = Shop()
    name = 'СТР'

    hildar_cl = Hildar()
    diana_cl = Diana()
    igles_cl = Igles()

    defeats = 0
    max_defeats = 10
    win_game = 0

    # Этапы прохождения
    stages = {
        0: 'new_char',
        1: 'buy_book',
        2: 'tavern',
        3: 'first_mission',
        4: 'mission_2_to_kraag',
        5: 'kaarg',
        6: 'mission_3_lairisha',
        7: 'mission_3_mage',
        8: 'mission_3_final_war',
        9: 'kaarg_2_sell_all',
        10: 'kaarg_2_buy_head',
        11: 'kaarg_2_buy_heal',
        12: 'kaarg_2_mission',
        13: 'mission_4_dayna',
        14: 'mission_4_1_necromansers',
        15: 'kaarg_3_sell_loot',
        16: 'mission_5_drevniy_les',
        17: 'mission_5_1_raptor',
        18: 'mission_5_2_raptor_2',
        19: 'mission_5_3_necro',
        20: 'kaarg_4_sell_loot',
        21: 'mission_6_ork_star',
        22: 'mission_6_1',
        23: 'kaarg_5_sell_loot',
        24: 'kaarg_5_buy_fireball',
        241: 'kaarg_5_buy_mana_regen',
        25: 'kaarg_5_taven',
        26: 'mission_7_elf',
        27: 'mission_7_1_go_to_necromansers',
        28: 'mission_7_2_necromansers_batle',
        29: 'mission_7_3',
        30: 'mission_7_4',
        31: 'mission_7_5',
        32: 'mission_7_6',
        33: 'kaarg_6_sell_loot',
        34: 'kaarg_6_buy_arb',
        35: 'kaarg_6_to_mission',
        36: 'mission_8_ork_star',
        37: 'mission_8_1_dragon',
        38: 'mission_8_2_trolls',
        39: 'mission_8_3_orks',
        40: 'kaarg_7_equip_the_gold_crown',
        41: 'kaarg_7_buy_igles_1',
        42: 'kaarg_7_buy_igles_2',
        43: 'kaarg_7_buy_igles_3',
        44: 'kaarg_7_buy_igles_4',
        45: 'kaarg_7_buy_igles_5',
        46: 'kaarg_7_sell_loot_buy_hp',
        47: 'kaarg_7_tavern',
        48: 'mission_9_prikaz',
        49: 'mission_9_1_battle',
        50: 'kaarg_8_sell_loot',
        51: 'kaarg_8_equip_amulet',
        52: 'kaarg_8_buy_teleport',
        53: 'kaarg_8_tavern',
        54: 'mission_10_king_hunting',
        55: 'mission_10_1',
        57: 'kaarg_9_sell_loot',
        58: 'kaarg_9_buy_sword',
        59: 'kaarg_9_buy_heal',
        60: 'kaarg_9_buy_grad',
        61: 'kaarg_9_buy_fireball',
        62: 'kaarg_9_buy_mana_regen',
        621: 'kaarg_9_buy_mana_hildar',
        622: 'kaarg_9_buy_mana_diana',
        63: 'kaarg_9_buy_teleport',
        64: 'kaarg_9_tavern',
        65: 'before_mission',
        66: 'mission_11',
        67: 'mission_11_1',
        68: 'mission_11_2',
        69: 'mission_11_3',
        70: 'mission_11_5',
        71: 'mission_11_6',
        72: 'mission_11_7',

    }

    def __init__(self):
        self.shop.findtext.init_model()
        pass

    # запуск стратегии
    def run(self):
        time1 = time.time()
        # Запускаем текущий этап
        if self.stages.get(self.stage):
            f = getattr(self, self.stages[self.stage])
            f()
        time2 = time.time()
        total = time2 - time1
        i = 0
        m = 0
        while total - i >= 60:
            i += 60
            m += 1
        sec = total - i
        print('total time: ', m, ':', sec)

    def set_stage(self, stage):
        self.stage = stage
        pass

    def ns(self):
        # Следующий стаж
        ck = self.stage
        nk = ck
        br = False
        for k in self.stages.keys():
            nk = k
            if br:
                break
            if k == ck:
                br = True
        return nk

    def ps(self):
        # Предыдущий стаж
        ck = self.stage
        pk = ck
        for k in self.stages.keys():
            if k == ck:
                break
            pk = k
        return pk

    def next_stage(self, stage=0):
        # Сохранение игры и выбор следующего стажа
        if not stage:
            stage = self.ns()

        if self.win_game:
            return
        self.defeats = 0
        sg = inter.SaveGame()
        sg.save_game(stage)
        self.set_stage(stage)
        # Запускаем следующий стаж
        self.run()

    def retry_stage(self, stage=0):
        self.defeats += 1
        pr(self.name, 'Миссия провалена', self.defeats)
        sg = inter.SaveGame()

        if self.defeats > self.max_defeats:
            # Загруаем предыдущий уровень.
            self.defeats = 0
            stage = self.ps()

        findtext = self.shop.findtext
        sg.load_from_menu(stage, findtext)

        self.set_stage(stage)
        self.run()

    def tol(self, result=True):
        # try or load
        if result:
            self.next_stage(self.ns())
        else:
            self.retry_stage(self.stage)
        pass

    def try_or_load(self, result=True, current_stage=0, next_stage=0):
        if result:
            self.next_stage(next_stage)
        else:
            self.retry_stage(current_stage)

    def new_char(self):
        pr(self.name, 'Начало')

        # Проверка стартового меню
        mm = inter.MainMenu()
        if not mm.is_start():
            return
        # Новая игра
        mm.new_game()
        # Проверка подсказок
        mm.help_off()
        # Выбираем сложность
        mm.select_level()
        # Выбираем класс
        mm.select_char()
        # Подтверждаем изменения
        inter.validate_left_cicle(787, 612)
        # Принимаем настройки героя
        inter.validate_left_cicle(750, 240)

        # Меняем стаж
        self.next_stage()

    def buy_book(self):
        pr(self.name, 'Покупаем книгу')
        s = self.shop
        # Заходим в лавку
        s.go_shop()
        # Переходим к меню книг
        s.books()
        # Ищем книгу
        coords = s.find_fire_book()
        # Книга найдена?
        book_find = False
        if coords[0] != 0:
            # Покупаем книгу
            pr(self.name, ' покупаем книгу')
            s.buy_item(coords[0], coords[1])
            # Применяем книгу
            s.equip(270, 610)
            # Книга найдена
            book_find = True
        else:
            # Книга не найдена - загружаем игру
            pr(self.name, ' книга не найдена, загружаемся')
            s.quit()
            sg = inter.SaveGame()
            sg.load_game()
            self.buy_book()

        if not book_find:
            # Продолжаем только в случае находки книги
            return False

        # Снимаем посох
        pr(self.name, 'Продаём посох')
        s.de_equip_posoh()
        # Продаём посох
        s.to_sell()
        s.sell()
        pr(self.name, 'Посох продан')
        # Выходим из лавки
        s.quit()

        # Меняем стаж
        self.next_stage()

    def tavern(self):
        pr(self.name, 'Беседа в Таверне')
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern()
        tv.talk()
        # Пропуск диалога
        mm.propusk('enter', 330, 330)
        # Выход
        tv.quit()
        # Переход на миссию
        mm.go_to_first_mission()
        time.sleep(500 / 1000)
        # Пропускаем диалог
        mm.propusk('enter', 330, 330)
        # Первичные настройки
        # mm.game_options()

        # Меняем стаж
        self.next_stage()

        pass

    def first_mission(self):
        pr(self.name, 'Идём на миссию')
        result = self.ms.first_mission()
        if result:
            # Ожидаем начала диалога второй миссии
            # time.sleep(10000 / 1000)
            # Ускоряем переход
            inter.validate_left_cicle(533, 557)
            self.mm.wait_for_color(335, 335, 40, 24, 40, 1000)
            # Пропускаем диалог
            self.mm.propusk('enter', 330, 330)
        self.tol(result)

    def mission_2_to_kraag(self):
        pr(self.name, 'Вторая миссия')
        result = self.ms.second_mission()
        if result:
            # Переход в город Каарг
            self.mm.caarg_click()
        self.tol(result)

    def kaarg(self):
        pr(self.name, 'Диалоги в Каарге')
        pr(self.name, 'Беседа в Таверне')
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern_kaarg()
        tv.talk_kaarg_2()
        # Пропуск диалога
        mm.propusk('enter', 330, 330)
        # Выход
        tv.quit()
        # Заходим в магазин и покупаем ускорители здоровья и маны
        s = self.shop
        pr(self.name, 'Заходим в лавку')
        s.go_kaarg_shop()
        # Переходим к меню книг
        s.books_kaarg()
        pr(self.name, 'Ищем элексир магии')
        # TODO Возможно цвет не будет найден. загрузить игру
        magic = s.find_color(240, 246, 64, 112, 208)
        if magic:
            s.buy_items(magic[0], magic[1], 5)
        # TODO Выбор героев через меню магазина
        # Выбираем Иглеза
        s.select_hero(self.igles_cl)
        pr(self.name, 'Ищем элексир здоровья')
        health = s.find_color(240, 246, 200, 164, 184)
        if health:
            s.buy_items(health[0], health[1], 5)
        s.quit()
        pr(self.name, 'Идём на миссию')
        inter.validate_left_cicle(380, 475)
        time.sleep(500 / 1000)
        # Ускоряем переход
        inter.validate_left_cicle(533, 557)
        # Ожидаем начала миссии
        mm.wait_for_color(335, 335, 40, 24, 40, 1000)

        # Пропускаем диалог
        mm.propusk('enter', 330, 330)

        self.next_stage()

    def mission_3_lairisha(self):
        pr(self.name, 'Миссия - Деревня Лайриша')
        self.tol(self.ms.mission_3())

    def mission_3_mage(self):
        pr(self.name, 'Сражение с магом')
        self.tol(self.ms.mission_3_2())

    def mission_3_final_war(self):
        pr(self.name, 'Финальная битва')
        result = self.ms.mission_3_3()
        if result:
            mm = inter.MainMenu()
            # Ускоряем переход
            inter.validate_left_cicle(533, 557)
            # Переход в город Каарг
            mm.caarg_click()
        # Меняем стаж
        self.tol(result)

    def kaarg_2_sell_all(self):
        pr(self.name, 'Каарг. Покупка и продажа')
        pr(self.name, 'Беседа в Таверне')
        tv = self.tv
        # Переход в таверну
        tv.go_tavern_kaarg()
        tv.talk_kaarg_3()
        # Пропуск диалога
        self.mm.propusk('enter', 330, 330)
        # Выход
        tv.quit()
        # Заходим в магазин и покупаем ускорители здоровья и маны
        s = self.shop
        pr(self.name, 'Продажа лута')
        s.go_kaarg_shop()
        s.sell_all_hero(self.hildar_cl)
        s.sell_all_hero(self.igles_cl)
        s.quit()
        # Сохраняем игру
        self.next_stage()
        pass

    def kaarg_2_buy_head(self):
        # Покупка шлема
        pr(self.name, 'Каарг. Покупка шлема')
        # Задача - найти и купить железный закрытый шлем
        s = self.shop
        price = 2700
        name = 'СТАЛЬНОЙ ПОЛНЫЙ ШЛЕМ'
        s.find_or_load(price, hero=self.igles_cl, name_validate=name)
        self.next_stage()

    def kaarg_2_buy_heal(self):
        print('Покупаем целебные снадобья Иглезу')
        s = self.shop
        price = 1000
        color = [0, 10, 224, 104, 120]
        stop_price = 10000
        s.find_or_load(price, hero=self.igles_cl, place='books_kaarg', item_count=3, color_validate=color, equip=False,
                       stop_price=stop_price)
        self.next_stage()

    def kaarg_2_mission(self):
        pr(self.name, 'Идём на миссию')
        self.mm.caarg_quit()
        time.sleep(500 / 1000)
        # Ускоряем переход
        inter.validate_left_cicle(539, 390)
        # Ожидаем начала миссии
        self.mm.wait_for_color(335, 335, 40, 24, 40, 1000)
        # Пропускаем диалог
        self.mm.propusk('enter', 330, 330)
        self.next_stage()
        pass

    def mission_4_dayna(self):
        pr(self.name, 'Миссия Звездочёт или спасение Дайны')
        self.tol(self.ms.mission_4())

    def mission_4_1_necromansers(self):
        pr(self.name, 'Устранить некромансеров')
        result = self.ms.mission_4_1()
        if result:
            self.mm.caarg_click()
        self.tol(result)

    def kaarg_3_sell_loot(self):
        pr(self.name, 'Задания в Каарге')
        pr(self.name, 'Беседа в Таверне')
        tv = self.tv
        mm = self.mm
        # Переход в таверну
        tv.go_tavern_kaarg()
        tv.talk_kaarg_3()
        # Пропуск диалога
        mm.propusk('enter', 330, 330)
        # Выход
        tv.quit()
        # Заходим в магазин и покупаем ускорители здоровья и маны
        s = self.shop
        pr(self.name, 'Продажа лута')
        s.go_kaarg_shop()
        # Выбираем Иглеза
        s.select_hero(self.igles_cl)
        s.sell_all()

        # Покупаем элексиры здоровья
        s.books_kaarg()
        heal_color = [72, 72, 64]
        heal = s.find_color_in_pages(heal_color)
        if heal:
            s.buy_items(heal[0], heal[1], 10)
        # s.quit()
        s.armor_kaarg()
        s.books_kaarg()
        heal_regen_color = [216, 220, 216]
        heal_regen = s.find_color_in_pages(heal_regen_color)
        if heal_regen:
            s.buy_items(heal_regen[0], heal_regen[1], 5)
        # Покупаем ману Гильдариусу
        s.select_hero(self.hildar_cl)
        mana_regen_color = [104, 144, 232]
        regen = s.find_color_in_pages(mana_regen_color)
        if regen:
            # Покупаем ману Дайне
            s.buy_items(regen[0], regen[1], 10)
        s.quit()
        # Идём на миссию
        self.mm.caarg_quit()
        time.sleep(500 / 1000)
        # Ускоряем переход
        inter.validate_left_cicle(413, 432)
        # Ожидаем начала миссии
        self.mm.wait_for_color(335, 335, 40, 24, 40, 1000)

        # Пропускаем диалог
        self.mm.propusk('enter', 330, 330)

        self.next_stage()

    def mission_5_drevniy_les(self):
        pr(self.name, 'Миссия 5. Древний лес')
        self.tol(self.ms.mission_5())

    def mission_5_1_raptor(self):
        pr(self.name, 'Миссия 5.1. Ящеры')
        self.tol(self.ms.mission_5_1())

    def mission_5_2_raptor_2(self):
        pr(self.name, 'Миссия 5.1. Ящеры у моста')
        self.tol(self.ms.mission_5_2())

    def mission_5_3_necro(self):
        pr(self.name, 'Миссия 5. Устранение некромантов')
        result = self.ms.mission_5_3()
        if result:
            self.mm.caarg_click()
        self.tol(result)

    def kaarg_4_sell_loot(self):
        pr(self.name, 'Задания в Каарге')
        pr(self.name, 'Беседа в Таверне')
        # Заходим в магазин и покупаем ускорители здоровья и маны
        # s.quit()
        # Идём на миссию
        self.mm.caarg_quit()
        time.sleep(500 / 1000)
        # Ускоряем переход
        inter.validate_left_cicle(712, 471)
        # Ожидаем начала миссии
        self.mm.wait_for_color(335, 335, 40, 24, 40, 1000)

        # Пропускаем диалог
        self.mm.propusk('enter', 330, 330)

        self.next_stage()

    def mission_6_ork_star(self):
        pr(self.name, 'Миссия 6. Орковская звезда')
        self.tol(self.ms.mission_6())

    def mission_6_1(self):
        result = self.ms.mission_6_1()
        if result:
            self.mm.caarg_click()
        self.tol(result)

    def kaarg_5_sell_loot(self):
        # Заходим в магазин и покупаем ускорители здоровья и маны
        s = self.shop
        pr(self.name, 'Продажа лута')
        s.go_kaarg_shop()
        s.sell_all_hero(self.hildar_cl)
        s.sell_all_hero(self.igles_cl)
        s.sell_all_hero(self.diana_cl)
        s.quit()
        # Сохраняем игру
        # Меняем стаж
        self.next_stage()

    def kaarg_5_buy_fireball(self):
        pr(self.name, 'Покупка файрбола')
        s = self.shop
        price = 50000
        name = 'КНИГА С ЗАКЛИНАНИЕМ ОГНЕННЫЙ ШАР'
        s.find_or_load(price, hero=self.diana_cl, place='books_kaarg', name_validate=name)
        self.next_stage()

    def kaarg_5_buy_mana_regen(self):
        print('Покупаем регенерацию магии Дайне')
        s = self.shop
        price = 100
        color = [0, 0, 104, 144, 232]
        s.find_or_load(price, hero=self.diana_cl, place='books_kaarg', color_validate=color, buy_all=True)
        self.next_stage()

    def kaarg_5_taven(self):
        # Заходим в Таверну
        tv = self.tv
        mm = self.mm
        # Переход в таверну
        tv.go_tavern_kaarg()
        tv.talk_1()
        mm.propusk('enter', 330, 330)
        tv.talk_2()
        mm.propusk('enter', 330, 330)
        tv.talk_3()
        mm.propusk('enter', 330, 330)
        # Выход
        tv.quit()
        mm.caarg_quit()

        # Идём к эльфам
        pr(self.name, 'Нанимаем эльфов')
        mm.elf_click()
        tv.go_tavern_elf()
        tv.talk_1()
        mm.propusk('enter', 330, 330)
        tv.talk_2()
        mm.propusk('enter', 330, 330)
        tv.talk_4()
        mm.propusk('enter', 330, 330)
        tv.quit()
        tv.go_tavern_elf()
        tv.talk_2()
        mm.propusk('enter', 330, 330)
        tv.quit()
        mm.elf_quit()

        time.sleep(500 / 1000)
        # Ускоряем переход
        inter.validate_left_cicle(286, 367, double=True)
        # Ожидаем начала миссии
        self.mm.wait_for_color(335, 335, 40, 24, 40, 1000)

        # Пропускаем диалог
        self.mm.propusk('enter', 330, 330)

        self.next_stage()

    def mission_7_elf(self):
        pr(self.name, 'Миссия 7 - Эльф')
        self.tol(self.ms.mission_7())

    def mission_7_1_go_to_necromansers(self):
        pr(self.name, 'Миссия 7.1 - Идём в гости к некромантам')
        self.tol(self.ms.mission_7_1())

    def mission_7_2_necromansers_batle(self):
        pr(self.name, 'Миссия 7.2 - Битва с некоромантами')
        self.tol(self.ms.mission_7_2())

    def mission_7_3(self):
        pr(self.name, 'Миссия 7.3 - Идём к мосту')
        self.tol(self.ms.mission_7_3())

    def mission_7_4(self):
        pr(self.name, 'Миссия 7.4 - Битва у моста')
        self.tol(self.ms.mission_7_4())

    def mission_7_5(self):
        pr(self.name, 'Миссия 7.5 - Битва на севере')
        self.tol(self.ms.mission_7_5())

    def mission_7_6(self):
        pr(self.name, 'Миссия 7.6 - Спускаемся вниз')
        result = self.ms.mission_7_6()
        if result:
            self.mm.caarg_click()
        self.tol(result)

    def kaarg_6_sell_loot(self):
        # Заходим в магазин и покупаем ускорители здоровья и маны
        s = self.shop
        pr(self.name, 'Продажа лута')
        s.go_kaarg_shop()
        # Выбираем Иглеза
        s.sell_all_hero(self.hildar_cl)
        s.sell_all_hero(self.igles_cl)
        s.sell_all_hero(self.diana_cl)
        s.quit()
        # Сохраняем игру
        # Меняем стаж
        self.next_stage()

    def kaarg_6_buy_arb(self):
        pr(self.name, 'Покупка арбалета')
        # Мифрильный арбалет
        sword_price = 64000
        sword_name = 'МИФРИЛЬНЫЙ АРБАЛЕТ'
        s = self.shop
        s.find_or_load(sword_price, hero=self.igles_cl, place='weapon_kaarg', name_validate=sword_name)
        self.next_stage()

    def kaarg_6_to_mission(self):
        mm = self.mm
        tv = self.tv
        mm.caarg_quit()

        # Идём к эльфам
        pr(self.name, 'Нанимаем Тролля')
        mm.elf_click()
        tv.go_tavern_elf()
        tv.talk_2()
        mm.propusk('enter', 330, 330)
        tv.quit()
        mm.elf_quit()

        time.sleep(500 / 1000)
        # Ускоряем переход
        inter.validate_left_cicle(756, 468)
        # Ожидаем начала миссии
        self.mm.wait_for_color(335, 335, 40, 24, 40, 1000)

        # Пропускаем диалог
        self.mm.propusk('enter', 330, 330)

        self.next_stage()
        # Сохраняем игру

    def mission_8_ork_star(self):
        pr(self.name, 'Миссия 8. Орковская звезда')
        self.tol(self.ms.mission_8())

    def mission_8_1_dragon(self):
        pr(self.name, 'Миссия 8.1. Встреча дракона')
        self.tol(self.ms.mission_8_1())

    def mission_8_2_trolls(self):
        pr(self.name, 'Миссия 8.2. Охота на троллей')
        self.tol(self.ms.mission_8_2())

    def mission_8_3_orks(self):
        pr(self.name, 'Миссия 8.3. Орки')
        result = self.ms.mission_8_3()
        if result:
            # Переход в город Каарг
            inter.validate_left_cicle(533, 557)
            time.sleep(500 / 1000)
            self.mm.caarg_click()

        self.tol(result)

    def kaarg_7_equip_the_gold_crown(self):
        pr(self.name, 'Одеваем корону')
        # Заходим в магазин и продаём всё кроме амулета
        s = self.shop
        # Исключаем зелёную броню
        s.exclude_price = [500000]
        print('Продажа лута')
        s.go_kaarg_shop()
        s.sell_all_hero(self.hildar_cl)
        s.sell_all_hero(self.igles_cl, equip_exclude=True)
        s.sell_all_hero(self.diana_cl)
        s.quit()
        # Сохраняем игру
        # Меняем стаж
        self.next_stage()

    def kaarg_7_buy_igles_1(self):
        # Покупка снаряжения для Иглеза
        # Кольчуга 42000
        # Кираса 210000
        # Наручи 90000
        # Перчатки 60000
        # Поножи 120000
        print('Покупка Кольчуги')
        price = 42000
        self.shop.find_or_load(price, hero=self.igles_cl)
        self.next_stage()

    def kaarg_7_buy_igles_2(self):
        # Покупка снаряжения для Иглеза
        # Кираса 210000
        print('Покупка Кирасы')
        price = 210000
        self.shop.find_or_load(price, hero=self.igles_cl)
        self.next_stage()

    def kaarg_7_buy_igles_3(self):
        # Покупка снаряжения для Иглеза
        # Наручи 90000
        print('Покупка Наручей')
        price = 90000
        self.shop.find_or_load(price, hero=self.igles_cl)
        self.next_stage()

    def kaarg_7_buy_igles_4(self):
        # Покупка снаряжения для Иглеза
        # Перчатки 60000
        print('Покупка Перчаток')
        price = 60000
        self.shop.find_or_load(price, hero=self.igles_cl)
        self.next_stage()

    def kaarg_7_buy_igles_5(self):
        # Покупка снаряжения для Иглеза
        # Поножи 120000
        print('Покупка Поножей')
        price = 120000
        self.shop.find_or_load(price, hero=self.igles_cl)
        self.next_stage()

    def kaarg_7_sell_loot_buy_hp(self):
        # Заходим в магазин и покупаем ускорители здоровья и маны
        s = self.shop
        print('Продажа лута')
        s.go_kaarg_shop()
        # Выбираем Иглеза
        s.select_hero(self.igles_cl)

        # Покупаем элексиры здоровья
        s.books_kaarg()
        heal_regen_color = [216, 220, 216]
        heal = s.find_color_in_pages(heal_regen_color)
        if heal:
            s.buy_all(heal[0], heal[1])

        heal_color = [72, 72, 64]
        heal = s.find_color_in_pages(heal_color)
        if heal:
            s.buy_all(heal[0], heal[1])

        s.sell_all()
        s.quit()

        self.next_stage()

    def kaarg_7_tavern(self):
        # Заходим в Таверну
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern_kaarg()
        tv.talk_1()
        mm.propusk('enter', 330, 330)
        tv.talk_3()
        mm.propusk('enter', 330, 330)
        tv.talk_4()
        mm.propusk('enter', 330, 330)
        # Выход
        tv.quit()
        mm.caarg_quit()

        # Идём к эльфам
        print('Нанимаем эльфов')
        mm.elf_click()
        tv.go_tavern_elf()
        tv.talk_1()
        mm.propusk('enter', 330, 330)
        tv.talk_3()
        mm.propusk('enter', 330, 330)

        tv.quit()
        mm.elf_quit()

        time.sleep(500 / 1000)
        # Ускоряем переход
        inter.validate_left_cicle(482, 420)
        # Ожидаем начала миссии
        self.mm.wait_for_color(335, 335, 40, 24, 40, 1000)

        # Пропускаем диалог
        self.mm.propusk('enter', 330, 330)

        self.next_stage()

    def mission_9_prikaz(self):
        print('Миссия - 9. Королевский приказ')
        self.tol(self.ms.mission_9())

    def mission_9_1_battle(self):
        print('Миссия - 9. Бой с некромантами')
        result = self.ms.mission_9_1()
        if result:
            # Переход в город Каарг
            self.mm.caarg_click()
        self.tol(result)

    def kaarg_8_sell_loot(self):
        # Заходим в магазин и продаём всё кроме амулета
        s = self.shop
        print('Продажа лута')
        s.go_kaarg_shop()
        # Исключаем амулет
        s.exclude_price = [500000]
        s.sell_all_hero(self.hildar_cl)
        s.sell_all_hero(self.igles_cl)
        s.sell_all_hero(self.diana_cl)
        s.quit()
        self.next_stage()

    def kaarg_8_equip_amulet(self):
        # Одеваем амулет. Переключаемся между героями и ищем амулет.
        # Если он у Гильдариуса или Иглеза, перемещаем его в магазин.
        # Если он у Дайны одеваем сразу.
        # 1. Поиск амулета.
        # 2. Перемещение.
        # 3. Снаряжение
        s = self.shop
        s.go_kaarg_shop()
        price = 500000
        # Ищем у Гильдариуса
        amulet = s.find_by_price(price, 'invent')
        s.select_hero(self.hildar_cl)
        if amulet:
            s.drug_to_shop(amulet[0], amulet[1])
            s.select_hero(self.igles_cl)
            s.drug_to_invent()
        else:
            # Ищем амулет у Дайны
            s.select_hero(self.diana_cl)
            amulet = s.find_by_price(price, 'invent')
            if amulet:
                s.drug_to_shop(amulet[0], amulet[1])
                s.select_hero(self.igles_cl)
                s.drug_to_invent()
            pass
        # Снаряжаем амулей Иглезу
        s.select_hero(self.igles_cl)
        amulet = s.find_by_price(price, 'invent')
        if amulet:
            s.equip(amulet[0], amulet[1])

        s.quit()
        self.next_stage()

    def kaarg_8_buy_teleport(self):
        print('Покупаем телепорт Иглезу')
        # Задача. Быстро найти свиток свечения.
        # Ищем по цене, пока она не станет больше 10к
        # Смотрим только сопадающие по цвету
        # После покупки смотрим количество, цикл продолжается до тех пор, пока нужное количество не будет достигнуто.
        s = self.shop
        price = 50000
        # Ставим букву З заместо Э
        name = 'СВИТОК С ЗАКЛИНАНИЕМ ТЕЛЕПОРТ'
        color = [0, 0, 120, 40, 128]
        stop_price = 100000

        s.find_or_load(price, hero=self.igles_cl, place='books_kaarg', name_validate=name, color_validate=color,
                       equip=False, stop_price=stop_price)
        print(s.purchased_items)
        self.next_stage()

    def kaarg_8_tavern(self):
        # Заходим в Таверну
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern_kaarg()
        tv.talk_4()
        mm.propusk('enter', 330, 330)

        # Выход
        tv.quit()
        mm.caarg_quit()

        time.sleep(200 / 1000)

        # Идём на миссию
        inter.validate_left_cicle(589, 306)
        # Ожидаем начала миссии
        self.mm.wait_for_color(335, 335, 40, 24, 40, 1000)

        # Пропускаем диалог
        self.mm.propusk('enter', 330, 330)
        self.next_stage()
        # Сохраняем игру

    def mission_10_king_hunting(self):
        print('Королевская охота')
        self.tol(self.ms.mission_10())

    def mission_10_1(self):
        print('Королевская охота')
        result = self.ms.mission_10_1()
        if result:
            # Переход в город Каарг
            self.mm.caarg_click()
        self.tol(result)

    def kaarg_9_sell_loot(self):
        # Заходим в магазин и продаём всё кроме амулета
        s = self.shop
        # Исключаем зелёную броню
        s.exclude_price = [1800000, 900000, 1350000, 3150000, 175000, 225000, 4500000]
        print('Продажа лута')
        s.go_kaarg_shop()
        s.sell_all_hero(self.igles_cl, equip_exclude=True)
        s.quit()
        # Сохраняем игру
        # Меняем стаж
        self.next_stage()

    def kaarg_9_buy_sword(self):
        # Покупка меча
        price = 600000
        print('Покупка Меча')
        s = self.shop
        name = 'МЕТЕОРИТНЫЙ ДЛИННЫЙ МЕЧ'
        s.find_or_load(price, hero=self.igles_cl, place='weapon_kaarg', name_validate=name)
        self.next_stage()

    def kaarg_9_buy_heal(self):
        print('Покупаем целебные снадобья Иглезу')
        s = self.shop
        price = 10000
        color = [0, 10, 208, 104, 120]
        stop_price = 100000
        item_count = 3
        s.find_or_load(price, hero=self.igles_cl, place='books_kaarg', color_validate=color, equip=False,
                       stop_price=stop_price, item_count=item_count, buy_all=True, current_stage=self.stage)
        print(s.purchased_items)
        self.next_stage()

    def kaarg_9_buy_teleport(self):
        print('Покупаем телепорт Иглезу')
        # Задача. Быстро найти свиток свечения.
        # Ищем по цене, пока она не станет больше 10к
        # Смотрим только сопадающие по цвету
        # После покупки смотрим количество, цикл продолжается до тех пор, пока нужное количество не будет достигнуто.
        s = self.shop
        price = 50000
        # Ставим букву З заместо Э
        name = 'СВИТОК С ЗАКЛИНАНИЕМ ТЕЛЕПОРТ'
        color = [0, 0, 120, 40, 128]
        stop_price = 100000
        s.find_or_load(price, hero=self.igles_cl, place='books_kaarg', name_validate=name, color_validate=color,
                       equip=False, stop_price=stop_price)
        print(s.purchased_items)
        self.next_stage()

    def kaarg_9_buy_grad(self):
        print('Покупаем град Дайне')
        s = self.shop
        price = 1000000
        name = 'КНИГА С ЗАКЛИНАНИЕМ ГРАД'
        s.find_or_load(price, hero=self.diana_cl, place='books_kaarg', name_validate=name)
        self.next_stage()

    def kaarg_9_buy_fireball(self):
        pr(self.name, 'Покупка файрбола Гильдариусу')
        s = self.shop
        price = 50000
        name = 'КНИГА С ЗАКЛИНАНИЕМ ОГНЕННЫЙ ШАР'
        s.find_or_load(price, hero=self.hildar_cl, place='books_kaarg', name_validate=name)
        self.next_stage()

    def kaarg_9_buy_mana_regen(self):
        print('Покупаем регенерацию магии Гильдариусу')
        s = self.shop
        price = 100
        color = [0, 0, 104, 144, 232]
        s.find_or_load(price, hero=self.hildar_cl, place='books_kaarg', color_validate=color, buy_all=True, equip=False)
        self.next_stage()

    def kaarg_9_buy_mana_hildar(self):
        print('Покупаем ману Гильдариусу')
        s = self.shop
        price = 1000
        color = [0, 0, 64, 68, 88]
        s.find_or_load(price, hero=self.hildar_cl, place='books_kaarg', color_validate=color, buy_all=True, equip=False,
                       stop_price=5000)
        self.next_stage()

    def kaarg_9_buy_mana_diana(self):
        print('Покупаем ману Дайне')
        s = self.shop
        price = 1000
        color = [0, 0, 64, 68, 88]
        s.find_or_load(price, hero=self.diana_cl, place='books_kaarg', color_validate=color, buy_all=True, equip=False,
                       stop_price=5000)
        self.next_stage()

    def kaarg_9_tavern(self):
        # Заходим в Таверну
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern_kaarg()

        tv.talk_4()
        mm.propusk('enter', 330, 330)

        tv.talk_5()
        mm.propusk('enter', 330, 330)

        # Выход
        tv.quit()
        self.next_stage()

    def before_mission(self):
        mm = inter.MainMenu()
        mm.caarg_quit()
        self.max_defeats = 10

        time.sleep(200 / 1000)

        # Идём на миссию
        inter.validate_left_cicle(650, 310)
        # Ожидаем начала миссии
        self.mm.wait_for_color(335, 335, 40, 24, 40, 1000)

        # Пропускаем диалог
        self.mm.propusk('enter', 330, 330)

        self.next_stage()
        # Сохраняем игру

    def mission_11(self):
        print('Миссия 11. Поход к Урду')
        self.max_defeats = 3
        self.tol(self.ms.mission_11())

    def mission_11_1(self):
        print('Миссия 11. Поднимаемся на север')
        self.max_defeats = 10
        self.tol(self.ms.mission_11_1())

    def mission_11_2(self):
        print('Миссия 11. Группировка')
        self.tol(self.ms.mission_11_2())

    def mission_11_3(self):
        print('Миссия 13. Враги у моста')
        self.tol(self.ms.mission_11_3())

    def mission_11_5(self):
        print('Миссия 13. Враги у моста')
        self.tol(self.ms.mission_11_5())

    def mission_11_6(self):
        print('Миссия 13. Враги у моста')
        self.tol(self.ms.mission_11_6())

    def mission_11_7(self):
        print('Миссия 13. Враги у моста')
        result = self.ms.mission_11_7()
        if result:
            self.win_game = 1
        self.tol(result)
