# Интерфейс взаимодействия логики с игрой
# Загрузка, сохранение игры, настройки опций.

from app.commands import *
import app.mission.actions as actions
import time


def pr(module='', *args):
    a = time.strftime("%H:%M:%S", time.localtime())
    print(a, module, *args)


# Набор функций создания героя

def validate_left_cicle(x, y, wait=100, color_x=0, color_y=0, double=False, more_pos=False, max_wait=5000):
    # Цикл перехода меню с валидацией. Если не сработает один раз, ждём и жмём снова
    mouse = Mouse()
    # Цикл входа
    ret = False
    while not ret:
        ret = mouse.left_validate(x, y, wait, color_x, color_y, double, more_pos)
        if ret:
            print(' переход по меню')
            break

        wait += wait
        if wait > max_wait:
            print(' ! аварийная остановка скрипта клика')
            break

    return ret


def validate_drug_cicle(x, y, x1, y1, wait=200, color_x=0, color_y=0):
    # Цикл перетаскивания с валидацией. Если не сработает один раз, ждём и жмём снова
    mouse = Mouse()
    # Цикл входа
    ret = False
    while not ret:
        ret = mouse.drug_validate(x, y, x1, y1, wait, color_x, color_y)
        if ret:
            print(' перетаскивание предмета')
            break

        wait += wait
        if wait > 5000:
            print(' ! аварийная остановка скрипта перетаскивания')
            break

    return ret


def validate_press_cicle(keys, color_x=0, color_y=0, wait=100):
    # Цикл перехода меню с валидацией. Если не сработает один раз, ждём и жмём снова
    ret = False
    k = Keys()
    while not ret:
        ret = k.press_validate(keys, color_x, color_y)
        if ret:
            print(' переход по меню')
            break

        wait += wait
        if wait > 5000:
            print(' ! аварийная остановка скрипта нажатия клавиши')
            break

    return ret


class SaveGame:
    # Сохранение и загрузка игры
    def save_game(self, stage):
        # Выходим в меню
        # validate_press_cicle('esc', 430, 330)

        # Выбор пункта меню сохранение
        # validate_left_cicle(510, 354, 100, 320, 280)

        # Вводим название записи
        keys = Keys()
        mouse = Mouse()
        keys.press('f2', 100)
        # mouse.left(335, 315)
        keys.type(str(stage))
        # validate_press_cicle(str(stage), 436, 534)
        ret = validate_left_cicle(436, 534, 100)
        print(' игра сохранена на ', stage)

        return ret

    def load_from_menu(self, stage=0, findtext=''):
        # Проверка нахождения в основном меню и загруза от туда. Либо стандартная загрузка
        color = Color()
        if color.validate(160, 160, 0, 0, 0) and color.validate(370, 300, 128, 112, 88):
            # Кликаем на загрузку
            print('Кликаем на загрузку')
            validate_left_cicle(590, 260)
        else:
            # Выходим в меню
            print('Выходим в меню')
            validate_press_cicle('esc', 430, 330)

            # Выбор пункта меню сохранение
            print('Выбор пункта меню сохранение')
            validate_left_cicle(510, 350, 100, 320, 280)

        # Находим нужную запись
        crd = self.find_save_by_name(stage, findtext)

        if not crd:
            # Ставим стандартные
            print('Координаты записи', stage, 'не найдены. Загружаем верхнюю запись')
            crd = [330, 335]

        validate_left_cicle(crd[0], crd[1], 100, double=True)
        print(' игра загружена')
        pass

    def find_prev_save(self):
        # Пропускаем названия: Начать миссию с начала и отказаться от миссии
        color = Color()
        x = 357
        y = 355
        while True:
            if color.validate(x, y, 208, 208, 208):
                y += 19
            else:
                break
        return [x, y]

    def load_game(self):
        # Выходим в меню
        validate_press_cicle('esc', 430, 330)

        # Выбор пункта меню сохранение
        validate_left_cicle(510, 384, 100, 320, 280)

        # Нажимаем загрузить верхнюю запись (по умолчанию)
        ret = validate_left_cicle(390, 555, 100)
        print(' игра загружена')

        return ret

    def save_game_mission(self, stage):
        # Выходим в меню
        # validate_press_cicle('esc', 430, 330)

        # Выбор пункта меню сохранение
        # validate_left_cicle(510, 320, 100, 320, 280)

        # Вводим название записи
        # validate_press_cicle(str(stage), 436, 534)
        keys = Keys()
        mouse = Mouse()
        keys.press('f2', 200)
        mouse.left(335, 315)
        keys.type(str(stage))
        ret = validate_left_cicle(436, 534, 100)
        print(' игра сохранена на ', stage)

        return ret

    def find_save_by_name(self, save_name, findtext):
        findtext.min_color = 120
        top = 330
        left = 318
        width = 100
        height = 10
        crd = []
        for i in range(10):
            mon_map = {'top': top, 'left': left, 'width': width, 'height': height}
            found = findtext.get_price_by_mon_map(mon_map)
            if found == str(save_name):
                crd = [left + 5, top + 5]
                print('Запись', save_name, 'найдена, координаты', crd)
                break
            top += 19
        return crd

    pass


class MainMenu:
    color = Color()
    keys = Keys()
    mouse = Mouse()

    # Магия и инвентарь
    magic_show = True
    invent_show = False

    def elf_quit(self):
        # Выход из Лесного города
        validate_left_cicle(209, 484)

    def caarg_quit(self):
        # Выход из города
        validate_left_cicle(380, 475)

    def caarg_click(self, double=True):
        # Переход в город Крааг из меню
        validate_left_cicle(576, 467, 100)
        self.wait_for_color(400, 250, 72, 140, 224)

    def elf_click(self):
        # Переход в Лесной город из меню
        validate_left_cicle(390, 390, 100, double=True)
        validate_left_cicle(390, 390, 100, double=True)
        self.wait_for_color(484, 288, 16, 24, 8)

    def is_start(self):
        # Проверка стартового меню
        ret = self.color.validate(440, 254, 208, 108, 40)
        return ret

    def new_game(self):
        ret = validate_left_cicle(440, 254, 100)
        print(' новая игра')
        return ret

    def help_off(self):
        # Проверка текста подсказок
        ret = False
        help_on = self.color.validate(480, 326, 56, 52, 64)
        if help_on:
            help_off = validate_left_cicle(480, 326, 100)
            if help_off:
                ret = validate_left_cicle(732, 326, 100)
                print(' отключение подсказок')

        return ret

    def select_level(self):
        ret = validate_left_cicle(230, 215, 100)
        print(' выбор лёгкой сложности')
        return ret

    def select_char(self):
        ret = validate_left_cicle(686, 344, 100, 332, 292)
        print(' выбор мага')
        return ret

    def win_menu_click(self):
        validate_left_cicle(475, 427)

    def win_cancel(self):
        self.keys.press('esc')

    def to_main_menu(self):
        # Выход в основное меню
        self.keys.press('esc')
        time.sleep(100 / 1000)
        self.keys.press('esc')

    def propusk(self, keys, color_x, color_y):
        # Пропуск разговора
        wait = 0
        k = Keys()
        ret = False
        while not ret:
            ret = k.press_validate(keys, color_x, color_y, 10)
            if ret:
                break

            wait += 1
            if wait > 100:
                print(' ! аварийная остановка скрипта нажатия клавиши')
                break
        pass

    def go_to_first_mission(self):
        # Идём на первую миссию
        validate_left_cicle(400, 355)
        time.sleep(500 / 1000)
        # Ускоряем переход
        validate_left_cicle(400, 355)
        pass

    def win_option(self):
        # Выбор опции - победа
        # Выходим в меню опций
        validate_press_cicle('esc', 430, 330)
        # Выбор пункта меню опции
        validate_left_cicle(505, 470)
        validate_left_cicle(519, 363)

    def go_options(self):
        # Выходим в меню опций
        validate_press_cicle('esc', 430, 330)
        # Выбор пункта меню опции
        validate_left_cicle(510, 380, 100, 244, 218)

    def set_min_auto_cast(self):
        # Лечим всех союзников
        if self.color.validate(617, 502, 32, 4, 32):
            validate_left_cicle(617, 502)

    def set_med_auto_cast(self):
        if self.color.validate(617, 526, 32, 4, 32):
            validate_left_cicle(617, 526)

    def opt_no_retreat(self):
        self.go_options()
        self.set_no_retreat()
        self.quit_options()

    def opt_panic_retreat(self):
        self.go_options()
        self.set_panic_retreat()
        self.quit_options()

    def set_no_retreat(self):
        # Паническое авто-отступление
        if self.color.validate(450, 502, 32, 4, 32):
            validate_left_cicle(450, 502)

    def set_panic_retreat(self):
        # Паническое авто-отступление
        if self.color.validate(450, 550, 32, 4, 32):
            validate_left_cicle(450, 550)

    def set_healing_union(self, on=True):
        # Лечим всех союзников
        if on:
            if self.color.validate(520, 418, 32, 4, 32):
                validate_left_cicle(520, 418)
        else:
            if not self.color.validate(520, 418, 32, 4, 32):
                validate_left_cicle(520, 418)

    def set_life_all(self, on=True):
        # Жизнь у всех
        if on:
            if self.color.validate(520, 255, 32, 4, 32):
                validate_left_cicle(520, 255)
        else:
            if not self.color.validate(520, 255, 32, 4, 32):
                validate_left_cicle(520, 255)

    def set_day_and_night(self, on=True):
        # Жизнь у всех
        if on:
            if self.color.validate(280, 334, 32, 4, 32):
                validate_left_cicle(280, 334)
        else:
            if not self.color.validate(280, 334, 32, 4, 32):
                validate_left_cicle(280, 334)

    def set_healing_all(self, on=True):
        # Лечим всех союзников
        if on:
            if self.color.validate(520, 446, 32, 4, 32):
                validate_left_cicle(520, 446)
        else:
            if not self.color.validate(520, 446, 32, 4, 32):
                validate_left_cicle(520, 446)

    def set_no_damage_info(self, on=False):
        # Отключить отлетающий урон
        if on:
            if self.color.validate(520, 283, 32, 4, 32):
                validate_left_cicle(520, 283)
        else:
            if not self.color.validate(520, 283, 32, 4, 32):
                validate_left_cicle(520, 283)

    def quit_options(self):
        # Выход из опций игры
        validate_left_cicle(390, 580)

    def game_options(self):
        # Начальная установка настроек игры
        time.sleep(500 / 1000)

        # Выходим в меню
        validate_press_cicle('esc', 430, 330)

        # Выбор пункта меню опции
        validate_left_cicle(510, 380, 100, 244, 218)

        # Высокая скорость игры
        color = Color()
        if not color.validate(432, 295, 176, 148, 48):
            mouse = Mouse()
            # Листаем вправо
            mouse.left_down(450, 298, 500, 100)

        # Смена дня и ночи
        if color.validate(280, 337, 104, 52, 104):
            validate_left_cicle(280, 337)

        # Жизнь у всех
        if color.validate(519, 258, 40, 4, 40):
            validate_left_cicle(519, 258)

        # Выход
        validate_left_cicle(390, 580)

        pass

    def wait_for_color(self, x, y, r, g, b, sleep=1000):
        # Ожидание появления цвета
        while True:
            if self.color.validate(x, y, r, g, b):
                break
            time.sleep(sleep / 1000)
        return True

    pass

    def enter_game(self):
        self.mouse.left(889, 524, sleep=100)
        # Проверка открытой книги магии и закрытого инвентаря
        if self.magic_show:
            self.show_magic_menu()
        else:
            self.hide_magic_menu()

        if self.invent_show:
            self.open_invent()
        else:
            self.close_invent()

    def open_invent(self):
        # Открыть инвентарь
        if self.color.validate(882, 485, 208, 196, 128):
            print('Инвентарь закрыт, открываем')
            validate_press_cicle('i', 882, 485)
        pass

    def close_invent(self):
        # Закрыть инвентарь
        if self.color.validate(882, 485, 32, 12, 0):
            print('Инвентарь открыт, закрываем')
            validate_press_cicle('i', 882, 485)
        pass

    def show_magic_menu(self):
        # Показать меню магии
        if self.color.validate(882, 287, 40, 28, 8):
            print('Книга магии закрыта, открываем')
            validate_left_cicle(882, 287)
            # inter.validate_press_cicle('q', 882, 287)
        pass

    def hide_magic_menu(self):
        # Показать меню магии
        if not self.color.validate(882, 287, 40, 28, 8):
            print('Книга магии открыта, закрываем')
            validate_left_cicle(882, 287)
            # inter.validate_press_cicle('q', 882, 287)
        pass

    def find_in_invent_mission(self, c=[]):
        # Поиск предмета в инвентаре во время миссии
        x = 110
        y = 754

        ret = []
        for i in range(9):
            if self.color.validate(x + c[0], y + c[1], c[2], c[3], c[4]):
                ret = [x, y]
                break
            x += 80

        return ret


class Info:
    # Вывод информации
    last_msg = ''

    def p(self, msg):
        # Вывод уникальных сообщений
        if msg != self.last_msg:
            msg = self.last_msg
            print(msg)
        pass

    pass
