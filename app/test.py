# Проверка функций
from app.mission.monitor import MiniMap, Mage, MainScreen
import app.commands as cmd
import time
from app.commands import Mouse
from app.mission.tactic import *
from app.commands import Mouse
from app.mission.task_manager import TaskManager
import app.mission.task as task
from app.mission.actions import *
from app.mission.heroes import *
from app.shop import Shop

mouse = Mouse()
# mm = MiniMap()
# mouse.left(904, 116, sleep=100)

# ms.find()
cp = CheckPoint()
tm = TaskManager()
act = Actions()
mouse = Mouse()
mm = MainMenu()


def tm_test_walk():
    # Тест движения к кт
    # Идём к мосту
    unit = 1
    # Воин идёт к месту сбора
    mouse.left(896, 167, sleep=100)
    act.center_group(unit)
    task_walk_id = tm.add_task(task.Walk([929, 113], unit))
    tm.run()

    tm.add_task(task.Walk([896, 167], unit))
    tm.run()
    pass


def tm_test_clear_sector():
    # Тест движения к кт
    # Идём к мосту
    unit = 1
    # Воин идёт к месту сбора
    mouse.left(955, 97, sleep=100)
    act.center_group(unit)
    task_walk_id = tm.add_task(task.ClearSector([943, 93], unit))
    tm.run()

    pass


def tm_test_hp():
    # Тест лечения
    unit = 2
    # Воин идёт к месту сбора
    mouse.left(896, 167, sleep=100)
    act.center_group(unit)
    tm.add_task(task.HealthMon(2))
    tm.run()
    pass


def tm_test_loot():
    # Тест лечения
    unit = 1
    # Воин идёт к месту сбора
    mouse.left(750, 750, sleep=100)
    act.center_group(unit)
    tm.add_task(task.GetLoot(unit))
    tm.run()
    pass


def tm_test_get_loot():
    # Тест движения к кт
    # Идём к мосту
    unit = 1
    # Воин идёт к месту сбора
    mouse.left(896, 167, sleep=100)
    act.center_group(unit)
    tm.add_task(task.GetLoot(1))
    tm.run()
    pass


def tm_test_enemy_down():
    # Тест движения к кт
    # Идём к мосту
    unit = 1
    # Воин идёт к месту сбора
    mouse.left(896, 167, sleep=100)
    act.center_group(unit)
    tm.add_task(task.EnemyDown(1))
    tm.run()
    pass


def tm_test_multitask():
    # Тест движения к кт
    # Идём к мосту
    unit = 8
    # Воин идёт к месту сбора
    mouse.left(1000, 161, sleep=100)
    act.center_group(unit)
    tm.add_task(task.Walk([1000, 161], unit))
    enemy_down = tm.add_task(task.EnemyDown(1))
    tm.add_task(task.Walk([995, 151], unit), enemy_down)
    tm.run()

    pass


def tm_test_bind_magic():
    # Тест автомагии
    unit = 3
    # Воин идёт к месту сбора
    mm.enter_game()

    tm.add_task(task.Center(unit))
    tm.run()

    tm.add_task(task.BindAutoMagic(unit, 'heal'))
    tm.run()

    pass


def tm_group_test():
    # Группа героев
    group = Group()
    # Классы героев
    igles = Igles()
    hildar = Hildar()
    diana = Diana()
    # Наёмники
    healer = Healer()
    elf = Elf()
    ork = Ork()
    archer = Archer()
    warrior = Warrior()

    # Назначения для формирования строя
    tank = warrior.group
    archers = [ork.group,
               archer.group,
               igles.group]
    mages = [elf.group,
             healer.group,
             hildar.group,
             diana.group]
    group.set_group(tank, archers, mages)

    # Воин идёт к месту сбора
    mouse.left(999, 159, sleep=100)
    act.center_group(tank)
    walk_id = tm.add_task(task.Walk([1000, 159], tank))
    tm.add_task(task.Group(group), walk_id)
    # Убить людоеда
    enemy_down = tm.add_task(task.EnemyDown(1))
    tm.run()

    # Поднятся на север и убить волков
    wolf = tm.add_task(task.EnemyDown(0))
    walk_top = tm.add_task(task.Walk([999, 155], tank))
    tm.add_task(task.Group(group), walk_top)
    tm.add_task(task.GetLoot(1), wolf)
    tm.run()
    pass


def tm_test_life():
    # Тест жизни наёмника

    # Воин идёт к месту сбора
    mm.enter_game()

    elf = Elf()
    en1 = Enchantress1

    validate_group = [elf.group, en1.group]
    tm.set_validate_group(validate_group)
    tm.add_task(task.Center(elf.group))
    tm.add_task(task.LifeHiredUnits([elf, en1]))
    tm.add_task(task.Wait(60))
    tm.run()

    pass


def tm_test_dark():
    # Тест затемнения
    unit = 2
    target = 1
    # Воин идёт к месту сбора
    mm.enter_game()

    tm.add_task(task.Center(unit))

    tm.add_task(task.DarkDetector(unit, target))
    tm.add_task(task.EnemyClear([target], min_dst=10))
    tm.run()

    pass


def tm_test_resist():
    # Тест затемнения
    unit = 3
    target = 1
    # Воин идёт к месту сбора
    mm.enter_game()

    tm.add_task(task.Center(unit))

    resist = tm.add_task(task.WaterResist([1, 2], unit, wait_time=3, max_dst=6))

    tm.add_task(task.EnemyClear([1, 2], min_dst=10))
    tm.run()

    pass


def tm_group_test_2():
    # Выстраиваем группу
    mm.enter_game()

    # Маги
    hildar = Hildar()
    diana = Diana()
    elf = Elf()
    # Воины
    igles = Igles()

    # Волшебницы из последней миссии
    en1 = Enchantress1()
    en2 = Enchantress2()
    en3 = Enchantress3()

    group = Group()
    # Назначения для формирования строя
    tank = igles.group
    archers = [en3.group]
    mages = [en2.group, en1.group, hildar.group, diana.group, elf.group]

    group.archers_layer = 2
    group.mages_layer = 3

    group.set_group(tank, archers, mages)

    tm.add_task(task.Center(igles.group))
    # Выстраиваем группу
    tm.add_task(task.Group(group, 0, 0))
    tm.run()

    # Выстраиваем группу
    tm.add_task(task.Group(group, 0, 1))
    tm.run()

    # Выстраиваем группу
    tm.add_task(task.Group(group, 0, 2))
    tm.run()

    # Выстраиваем группу
    tm.add_task(task.Group(group, 0, 3))
    tm.run()


def tm_test_group_3():
    # Выстраиваем группу
    mm.enter_game()

    # Маги
    hildar = Hildar()
    diana = Diana()
    elf = Elf()
    # Воины
    igles = Igles()

    # Волшебницы из последней миссии
    en1 = Enchantress1()
    en2 = Enchantress2()
    en3 = Enchantress3()

    # Выстраиваем группу
    group = Group()
    # Назначения для формирования строя
    tank = igles.group
    archers = [en1.group, en2.group, en3.group]
    mages = [hildar.group, diana.group, elf.group]

    group.archers_layer = 6
    group.mages_layer = 7

    group.set_group(tank, archers, mages)

    tm.add_task(task.Center(igles.group))
    tm.run()
    # Выстраиваем группу
    tm.add_task(task.Group(group, 0, 1))
    tm.run()


def tm_test_find_by_name():
    # Выстраиваем группу
    mm.enter_game()

    igles = Igles()
    diana = Diana()
    en2 = Enchantress2()

    tm.find_in_black_sector(True)

    tm.set_validate_group([igles.group, diana.group, en2.group])

    tm.add_task(task.Center(igles.group))
    tm.add_task(task.SelfHealthMon(igles.group, 0, num=2, num_if_red=4))

    tm.add_task(task.SpeedMinus())
    tm.run()

    # Иглез убивает некромантов и возвращается к команде
    enemy_name = 'СКЕЛЕТ-МАГ'
    kill = tm.add_task(task.KillEnemy(0, igles.group, max_dst=6, far_target=False,
                                      target_name=enemy_name, wait_time=1000, validate_target=True))
    walk1 = tm.add_task(task.Walk([962, 76], igles.group), block_task_id=kill)
    walk2 = tm.add_task(task.Walk([957, 76], igles.group), walk1, block_task_id=kill)

    # Дайна, в это время, кастует град с низким приоритетом.
    # grad = tm.add_task(task.KillEnemyMagic(5, diana.group, cast_magic='grad', max_dst=20, wait_time=1000,
    #                                       priority_custom=6))

    tm.run()
    tm.add_task(task.SpeedPlus())
    tm.add_task(task.ShiftWalk(igles.group, en2.group, [7, 0]))
    tm.run()


def tm_test_clear_sector_2():
    # Выстраиваем группу
    mm.enter_game()

    igles = Igles()
    en2 = Enchantress2()

    tm.add_task(task.Center(igles.group))
    tm.add_task(task.SelfHealthMon(igles.group, 0, num=2, num_if_red=4))

    tm.run()

    # Иглез приманивает врагов
    tm.add_task(task.ClearSector([940, 76], igles.group, min_dst=6, max_dst=8, return_dst=11))
    tm.run()

    tm.add_task(task.ShiftWalk(igles.group, en2.group, [7, 0]))
    tm.run()


def tm_test_cursor():
    # Выстраиваем группу
    mm.enter_game()

    igles = Igles()

    tm.add_task(task.Center(igles.group))
    tm.run()
    act.mouse.move(550, 350)
    act.select_group(2)
    # 80 180 248 b g r
    print(act.color.color(547, 350))


def tm_test_group_attack():
    # Выстраиваем группу
    mm.enter_game()

    # Маги
    hildar = Hildar()
    healer = Healer()
    diana = Diana()
    elf = Elf()
    # Воины
    igles = Igles()
    warrior = Warrior()
    troll = Troll()
    # Лучники
    ork = Ork()
    archer = Archer()
    group = Group()
    # Волшебницы из последней миссии
    en1 = Enchantress1()
    en2 = Enchantress2()
    en3 = Enchantress3()

    tm.add_task(task.Center(igles.group))
    tm.run()

    grad = tm.add_task(task.KillEnemyMagic(5, diana.group, cast_magic='fire', max_dst=20, wait_time=10000,
                                           magic_attack=True))
    grad = tm.add_task(task.KillEnemyMagic(5, hildar.group, cast_magic='fire', max_dst=20, wait_time=10000,
                                           magic_attack=True))
    grad = tm.add_task(task.KillEnemyMagic(5, elf.group, cast_magic='grad', max_dst=20, wait_time=5000))
    grad = tm.add_task(task.KillEnemyMagic(5, en1.group, cast_magic='chain', max_dst=20, wait_time=1000))
    grad = tm.add_task(task.KillEnemyMagic(5, en2.group, cast_magic='chain', max_dst=20, wait_time=1000))
    grad = tm.add_task(task.KillEnemyMagic(5, en3.group, cast_magic='chain', max_dst=20, wait_time=1000))

    clear = tm.add_task(task.EnemyClear([igles.group], min_dst=20, validation_interval=2))
    tm.run()


def tm_test_group_2():
    # Выстраиваем группу
    mm.enter_game()
    # Маги
    hildar = Hildar()
    healer = Healer()
    diana = Diana()
    elf = Elf()
    # Воины
    igles = Igles()
    warrior = Warrior()
    troll = Troll()
    # Лучники
    ork = Ork()
    archer = Archer()
    group = Group()
    # Волшебницы из последней миссии
    en1 = Enchantress1()
    en2 = Enchantress2()
    en3 = Enchantress3()

    tm.add_task(task.Center(igles.group))
    tm.run()

    # Выстраиваем группу
    group = Group()
    # Назначения для формирования строя
    tank = igles.group
    archers = [en1.group, en2.group, en3.group]
    mages = [hildar.group, diana.group, elf.group]
    group.archers_layer = 6
    group.mages_layer = 7
    group.set_group(tank, archers, mages)

    tm.run_task(task.Group(group, 0, 0))
    tm.run_task(task.Group(group, 0, 1))
    tm.run_task(task.Group(group, 0, 2))
    tm.run_task(task.Group(group, 0, 3))

def tm_test_group_3():
    # Выстраиваем группу
    mm.enter_game()
    # Маги
    hildar = Hildar()
    healer = Healer()
    diana = Diana()
    elf = Elf()
    # Воины
    igles = Igles()
    warrior = Warrior()
    troll = Troll()
    # Лучники
    ork = Ork()
    archer = Archer()
    group = Group()
    # Волшебницы из последней миссии
    en1 = Enchantress1()
    en2 = Enchantress2()
    en3 = Enchantress3()

    tm.run_task(task.Center(igles.group))

    # Выстраиваем группу
    group = Group()
    # Назначения для формирования строя
    tank = igles.group
    archers = [en1.group, en2.group, en3.group]
    mages = [hildar.group, diana.group, elf.group]
    group.archers_layer = 6
    group.mages_layer = 7
    group.set_group(tank, archers, mages)

    tm.run_task(task.Group(group, 0, 1))

def tm_test_shift():
    # Выстраиваем группу
    mm.enter_game()

    tm.run_task(task.Center(1))
    tm.run_task(task.ShiftWalk(6, 1, [-3, -3]))


def tm_test_enemy_clear():
    # Выстраиваем группу
    mm.enter_game()
    igles = Igles()

    tm.run_task(task.Center(igles.group))
    # Иглез приманивает врагов
    tm.run_task(task.EnemyClear([igles.group], 10))

def sell_test():
    s = Shop()
    s.findtext.save = False
    s.findtext.print_data = True
    s.findtext.init_model()
    s.sell_all()


time1 = time.time()

# cp.update_speed_test()

# cp.find_in_screen_test()
# cp.numpy_test()
# cp.update_speed_test()
# cp.find_hero_test()
# mm.find()
# tm_test_walk()
# tm_group_test()
# tm_test_get_loot()
# tm_test_hp()
# tm_test_clear_sector()
# tm_test_loot()

# tm_test_bind_magic()
# tm_test_life()
# tm_test_dark()
# tm_test_resist()
# tm_test_group_3()
# tm_test_find_by_name()
# tm_test_clear_sector_2()

# tm_test_group_2()
# tm_test_shift()
#tm_test_enemy_clear()
# tm_test_cursor()
# tm_test_group_attack()

#tm_test_group_3()
sell_test()

time2 = time.time()
print(time2 - time1)
