# Управление диалогами в таверне

import app.ineterface as inter
from app.commands import Keys
from app.commands import Mouse


class Tavern:
    # Таверна
    def go_tavern_kaarg(self):
        # Переход в таверну
        inter.validate_left_cicle(760, 480)
        pass

    def go_tavern_elf(self):
        # Переход в таверну
        inter.validate_left_cicle(346, 430)
        pass

    def talk_1(self):
        inter.validate_left_cicle(397, 621, 100, 329, 329)
        print('Разгвор')
        pass

    def talk_2(self):
        inter.validate_left_cicle(447, 621, 100, 329, 329)
        print('Разгвор')
        pass

    def talk_3(self):
        inter.validate_left_cicle(497, 621, 100, 329, 329)
        print('Разгвор')
        pass

    def talk_4(self):
        inter.validate_left_cicle(541, 621, 100, 329, 329)
        print('Разгвор')
        pass

    def talk_5(self):
        inter.validate_left_cicle(590, 621, 100, 329, 329)
        print('Разгвор')
        pass

    def talk_kaarg_2(self):
        inter.validate_left_cicle(447, 621, 100, 329, 329)
        print('Разгвор')
        pass

    def talk_kaarg_3(self):
        inter.validate_left_cicle(497, 621, 100, 329, 329)
        print('Разгвор')
        pass

    def go_tavern(self):
        # Переход в таверну
        inter.validate_left_cicle(350, 540)
        pass

    def quit(self):
        # Выход из таверны
        inter.validate_left_cicle(752, 335)
        pass

    def talk(self):
        inter.validate_left_cicle(495, 620, 100, 329, 329)
        print('Разгвор с волшебницей')
        pass

    def hire(self):
        inter.validate_left_cicle(754, 237, 100, 329, 329)
        print('Нанять персонажа')
        pass

    pass
